#include "procedures.h"


int fill_object_in_message (void **msg, object_type object, int number_of_dimensions)
{
	int *msg_int;
	float *values_msg;

	if (object.dimension_values==NULL) return 0;

	int size=2*sizeof(int)+number_of_dimensions*sizeof(float);
	*msg=malloc(size);
	msg_int=*msg;
	msg_int[MSG_TYPE_FIELD]=OBJECT_VALUES_MSG;
	msg_int[OBJECT_NUMBER_FIELD]=object.number;
	values_msg=(float *)&(msg_int[OBJECT_VALUES_FIELD]);

	int i;
	for (i=0; i<number_of_dimensions; i++)
		values_msg[i]=object.dimension_values[i];
	
	return size;
}

int fill_mean_in_message(void **msg, statistic_type statistic, int number_of_dimensions)
{
	int *msg_int;
	float *values_msg;
	
	if (statistic.dimension_values==NULL) return 0;

	int size=2*sizeof(int)+number_of_dimensions*sizeof(float);
	*msg=malloc(size);
	msg_int=*msg;
	msg_int[MSG_TYPE_FIELD] = MEAN_MSG;
	msg_int[NUMBER_OBJECTS_FIELD]=statistic.number_of_objects_considered;
	values_msg=(float *)&(msg_int[STATISTIC_VALUES_FIELD]);
	int i;
	for (i=0; i<number_of_dimensions; i++){
		values_msg[i]=statistic.dimension_values[i];
	}	
	return size;
}

int fill_variance_in_message(void **msg, statistic_type statistic, int number_of_dimensions)
{
	int *msg_int;
	float *values_msg;

	if (statistic.dimension_values==NULL) return 0;

	int size=2*sizeof(int)+number_of_dimensions*sizeof(float);
	*msg=malloc(size);
	msg_int=*msg;
	msg_int[MSG_TYPE_FIELD] = VARIANCE_MSG;
	msg_int[NUMBER_OBJECTS_FIELD]=statistic.number_of_objects_considered;
	values_msg=(float *)&(msg_int[STATISTIC_VALUES_FIELD]);
	int i;
	for (i=0; i<number_of_dimensions; i++)
		values_msg[i]=statistic.dimension_values[i];

	return size;
}



int fill_attribute_in_message(void **msg, attribute_type attribute)
{
	int *msg_int;
	char *values_msg;

	if (attribute.name==NULL) return 0;
	
	int size=3*sizeof(int)+ MAX_ATTRIBUTENAME*sizeof(char);
	*msg=malloc(size);
	msg_int=*msg;
	msg_int[MSG_TYPE_FIELD] = ATTRIBUTE_MSG;
	msg_int[ATTRIBUTE_NUMBER_FIELD] = attribute.number;
	msg_int[ATTRIBUTE_TYPE_NUMBER_FIELD]=attribute.type_number;
	values_msg=(char *)&(msg_int[ATTRIBUTE_TYPE_NAME_FIELD]);
	sprintf(values_msg,"%s", attribute.name);

	return size;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Get parameters of the algorithm from the work passed bu the AntHill.
 * This function is responsible to read all the information contained in the 
 * work and to return this information in each of the variables passed by 
 * reference. 
 */

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, int *number_of_dimensions, char *tool_transformation, char *conf_database)
{
	/** Stores the work in a array of char */
	char *work_char;				
	char *temp_string;

	
	/* Transforms the work in a array of char */
	work_char = (char *) work;
	
	/** Reads all the information of the work */
	/** Gets the input file name */
	temp_string = strtok (work_char, ",");		
	strcpy(input_file_name,temp_string);	
	
	/** Gets the output file name */
	temp_string = strtok (NULL, ",");		
	strcpy(output_file_name,temp_string);	

	/** Gets the number of dimensions */
	temp_string = strtok (NULL, ",");		
	*number_of_dimensions = atoi (temp_string);

	/** Gets the number of dimensions */
	temp_string = strtok (NULL, ",");		
	strcpy(tool_transformation,temp_string);

	/** Gets the conf_database name */
	temp_string = strtok (NULL, ",");		
	strcpy(conf_database,temp_string);

	//free(temp_string);
}
	

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Print all the elements of an array of ints on a file.
 * @param array the array of integers that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 * @param file Prints the array on this file
 */
int print_int_array(FILE* file, int *array, int number_of_elements)
{
	int i;

	if (array==NULL) 
	{
		fprintf(file, "INT ARRAY NULL!!!");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%d ", array[i]);
	}

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** 
 * Print all the elements of an array of floats on a file.
 * @param array the array of floats that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 * @param file Prints the array on this file
 */
int print_float_array(float *array, int number_of_elements, FILE *file)
{
	int i;

	if (array==NULL) 
	{
		fprintf(file, "FLOAT ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, "%f ", array[i]);
	}
	fprintf(file, "\n");
	
	return 1;
}

/* Get the values of the statistic from the message. The structures of the object will be allocated. */ 
int get_statistic_value_from_msg(statistic_type *statistic, int number_of_dimensions, void *msg)
{
	int *msg_int;
	float *msg_float;
	int i, type_msg;
	
	if (statistic->dimension_values==NULL)
		malloc_statistic(statistic, number_of_dimensions);
	
	type_msg = ((int *)msg)[MSG_TYPE_FIELD];
	/* Check the type of msg*/
	
	if ((type_msg != MEAN_MSG) & (type_msg != VARIANCE_MSG) ) return 0;
	 
	/* read the object number from the message */
	msg_int=(int *)msg;
	(*statistic).number_of_objects_considered=((int *)msg)[NUMBER_OBJECTS_FIELD];
	
	msg_float=(float *)&(msg_int[STATISTIC_VALUES_FIELD]);


	/* read the dimension values from message */
	for (i=0; i<number_of_dimensions; i++)	
	{
		statistic->dimension_values[i]=msg_float[i];

	}
	
	return 1;
}

/* Get the values of the attribute from the message. The structures of the attribute will be allocated. */ 
int get_attribute_from_msg(attribute_type *attribute, void *msg)
{
	int *msg_int;
	char *msg_char;
	int type_msg;
	
	malloc_attribute(attribute);

	msg_int=(int *)msg;
	type_msg = msg_int[MSG_TYPE_FIELD];
	/* Check the type of msg*/
	if (type_msg != ATTRIBUTE_MSG) return 0;
	 
	/* read the object number from the message */
	attribute->number = msg_int[ATTRIBUTE_NUMBER_FIELD];
	attribute->type_number=msg_int[ATTRIBUTE_TYPE_NUMBER_FIELD];
	msg_char=(char *)&(msg_int[ATTRIBUTE_TYPE_NAME_FIELD]);
	strcpy(attribute->name,msg_char);

	return 1;

}

/* Get the values of the object from the message. The structures of the object will be allocated. */ 
int get_object_from_msg(object_type *object, int number_of_dimensions, void *msg)
{
	int *msg_int;
	float *msg_float;
	int i,type_msg;
	
	malloc_object(object, number_of_dimensions);
	
	type_msg = ((int *)msg)[MSG_TYPE_FIELD];
	/* Check the type of msg*/
	if ( type_msg != OBJECT_VALUES_MSG) return 0;
	 
	/* read the object number from the message */
	msg_int=(int *)msg;
	(*object).number=((int *)msg)[OBJECT_NUMBER_FIELD];
	
	msg_float=(float *)&(msg_int[OBJECT_VALUES_FIELD]);

	/* read the dimension values from message */
	for (i=0; i<number_of_dimensions; i++)	
	{
		((*object).dimension_values)[i]=msg_float[i];
	}
	return 1;
}


int get_object_from_line(object_type* object, char *line, int size)
{
	char *temp_string;// auxiliary to store temporarily a string of the line
	char temp_line[size];	// auxiliary to store temporarily the line
	int num_attributes = 0;	// auxiliary to store the number of attributes

	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);

	//read the number object
	object->number = atoi(temp_string);

	temp_string = (char *) strtok(NULL, SEPARATORS);
	while (temp_string != NULL)
	{
		object->dimension_values[num_attributes]=atof(temp_string);
		num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}

	return num_attributes;
}

int get_attribute_from_line(attribute_type* attribute, char *line, int size)
{
	char *temp_string;// auxiliary to store temporarily a string of the line
	char temp_line[size];	// auxiliary to store temporarily the line
	int num_attributes = 0;	// auxiliary to store the number of attributes
	char error[100] = "The type of attribute isn't correct!";

	// copies the line to a auxiliary variable
	strcpy(temp_line, line);

	//starts reading the position attributes in the dimesions
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	attribute->number = atoi(temp_string);
	num_attributes++;

	//read the name of attribute
	temp_string = (char *) strtok(NULL, SEPARATORS);
	strcpy(attribute->name,temp_string);
	num_attributes++;

	//read the number of the attribute type
	temp_string = (char *) strtok(NULL, SEPARATORS);
	if (!strcmp(temp_string, "real"))
		attribute->type_number = REAL;
	else if (!strcmp(temp_string, "categorical"))
		attribute->type_number = CATEGORICAL;
	else if (!strcmp(temp_string, "discrete"))
		attribute->type_number = DISCRETE;
	else ahExit(error);

	num_attributes++;
	
	

	
	return num_attributes;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** This function is responsible to get float values from one line and write them on an array */

int get_float_array_from_line(float *values, char *line, int size)
{
	char *temp_string; 			// auxiliary to store temporarily a string of the line
	char temp_line[size];			// auxiliary to store temporarily the line
	int num_attributes = 0;			// auxiliary to store the number of attributes
	
	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	while (temp_string != NULL)
	{
		values[num_attributes]=atof(temp_string);
		num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}

	return num_attributes;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Calculate the Euclidian distance betweem two points */
/*
 * The Euclidian Distance between two bidimensional points \f$(x_1,y_1)\f$ and \f$(x_2,y_2)\f$ is 
 * \f$\sqrt{(x_2-x_1)^2+(y_2-y_1)^2}\f$.
 */
float distance_between_points(float *point1, float *point2, int dimensions)
{
	float sum = 0;
	register int i;
	
	for(i=0; i < dimensions; i++) 
	{
		sum += (point1[i] - point2[i]) * (point1[i] - point2[i]);
	}
	return sqrtf(sum);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#ifdef GET_TIME

/*
 * Print the time beetween the start and the end of the execution
 */
int print_time(struct timeval *start, struct timeval *end, char *name_of_file)
{
	FILE *file;		// file for time output
	double execution_time;	// gets the execution time

	// opens the file
	file = fopen(name_of_file,"a");

	// calculates the execution time in seconds
	execution_time  = ( end->tv_sec - start->tv_sec )*1000000;
	execution_time += end->tv_usec - start->tv_usec;
	execution_time /= 1000000;

	// print the time
	fprintf(file, "%.3lf\n", execution_time);

	// closes the file
	fclose(file);

	return 1;
}

#endif


