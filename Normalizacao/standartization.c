
/*THESE ARE THE FUNCTIONS TO STANDARTIZATION THE OBJECTS IN THE DATABASE*/

#include "standartization.h"

/*Calculate the local mean of the objects list*/

int calculate_local_mean_value(list_type *object_list, statistic_type *local_mean, int number_of_dimensions)
{	
	int i;
	list_pointer aux;
	
	if (!get_first_in_list(object_list, &aux))
		return 0;
	
	//add the first object in the statistic mean
	for (i=0; i<number_of_dimensions;i++)
	{
		(*local_mean).dimension_values[i] = (*aux).item.dimension_values[i];
	}

	do
	{	
		go_next(object_list, &aux);
		for (i=0; i<number_of_dimensions;i++)
		{
			(*local_mean).dimension_values[i] += (*aux).item.dimension_values[i];
		}
	}
	while (!(is_last(aux, object_list)));

	for (i=0;i<number_of_dimensions;i++)
	{	
		(*local_mean).dimension_values[i] /= object_list->length;
	}
	(*local_mean).number_of_objects_considered = object_list->length;
	
	return 1;
}


int calculate_local_variance_value(list_type *object_list, statistic_type *local_variance, statistic_type *global_mean, int number_of_dimensions)
{	
	int i;
	list_pointer aux;
	
	if (!get_first_in_list(object_list, &aux))
		return 0;
	
	//add the first object in the statistic variance
	for (i=0; i<number_of_dimensions;i++)
	{
		(*local_variance).dimension_values[i] = (((*aux).item.dimension_values[i]-(*global_mean).dimension_values[i])*((*aux).item.dimension_values[i]-(*global_mean).dimension_values[i]));

	}

	do 
	{	
		go_next(object_list, &aux);
		for (i=0; i<number_of_dimensions;i++)
		{
			(*local_variance).dimension_values[i] += (((*aux).item.dimension_values[i]-(*global_mean).dimension_values[i])*((*aux).item.dimension_values[i]-(*global_mean).dimension_values[i]));
		}
	}
	while (!(is_last(aux, object_list)));


	for (i=0;i<number_of_dimensions;i++)
	{
		(*local_variance).dimension_values[i] /= ((global_mean->number_of_objects_considered)-1);
	}
	(*local_variance).number_of_objects_considered = object_list->length;

	return 1;
}


int normalize_database(list_type *object_list, statistic_type global_mean, statistic_type global_variance, int number_of_dimensions)
{
	
	int i;
	list_pointer aux;
	
	if (is_empty(object_list)) return 0;
	if (!get_first_in_list(object_list, &aux)) return 0;
	
	for (i=0;i<number_of_dimensions;i++)
	{
		(*aux).item.dimension_values[i] -= (global_mean).dimension_values[i]; 
		(*aux).item.dimension_values[i] /= sqrt((global_variance).dimension_values[i]);
	}


	do
	{
		go_next(object_list, &aux);
		for (i=0;i<number_of_dimensions;i++)
		{
			(*aux).item.dimension_values[i] -= (global_mean).dimension_values[i]; 
			(*aux).item.dimension_values[i] /= sqrt((global_variance).dimension_values[i]);
		}
	}
	while(!(is_last(aux, object_list)));

	return 1;

}
