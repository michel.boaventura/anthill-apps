#ifndef _CONSTANTS_H_
#define _CONSTANTS_H

/* Constante utilizada em fase de implementação que faz os filtros apenas receberem a msg do filtro anterior e terminarem assim que o filtro anterior terminar */ 

/*****************************************************************************/
/* Define the separators of the values on the database */
#define SEPARATORS ", ;\n"

/* The maximum length of the name of one file */
#define MAX_FILENAME 100

/*The maximum length of the name of the tool transformation*/
#define MAX_TOOLNAME 20

/*The maximum length of the string of the attribute*/
#define MAX_ATTRIBUTENAME 20
/* The maximum length of a value of an dimension in number of characters */
/* The maximum length of a line on the database is equal to ((MAX_VALUE_LEN+2)*number_of_dimensions+1)*/
/* Example: 
 * If in the database there is the line:
 * 1.034, 2.134, 3.489, 4.249 
 * Then the MAX_VALUE_LEN can be just 5
 */ 
#define MAX_VALUE_LEN 30  	

// The constant representing and EMPTY LINE 
#define EMPTY_LINE -3

#define GET_TIME 0

/* Constants that defines the fields of the messages sent by the Distributor filter to the Assigner Filter */

// Constant that defines the first field of all msgs. 
#define MSG_TYPE_FIELD 0

//Constants that defines the types of msg.
#define OBJECT_VALUES_MSG 0 // MSG_TYPE_FIELD
#define MEAN_MSG 1 // MSG_TYPE_FIELD
#define VARIANCE_MSG 2
#define ATTRIBUTE_MSG 3

//Constants that defines the msg of the type object
#define OBJECT_NUMBER_FIELD 1
#define OBJECT_VALUES_FIELD 2

//Constants that defines the msg of the type statistic
#define NUMBER_OBJECTS_FIELD 1
#define STATISTIC_VALUES_FIELD 2

// Constants that defines the msg of the type attributes
#define ATTRIBUTE_NUMBER_FIELD 1
#define ATTRIBUTE_TYPE_NUMBER_FIELD 2
#define ATTRIBUTE_TYPE_NAME_FIELD 3

//Types of the attributes
#define REAL 1
#define CATEGORICAL 2
#define DISCRETE 3

//confdatabase
#define NUMBER_ATTRIBUTE_INFO 3 

// If this constant is set, the execution will be processed in DEBUG mode
#define DEBUG 1

#endif
