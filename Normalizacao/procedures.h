#ifndef PROCEDURES_H
#define PROCEDURES_H
#include "dbtransformation.h"

int fill_object_in_message (void **msg, object_type object, int number_of_dimensions);
int fill_mean_in_message(void **msg, statistic_type statistic, int number_of_dimensions);
int fill_variance_in_message(void **msg, statistic_type statistic, int number_of_dimensions);
int fill_attribute_in_message(void **msg, attribute_type attribute);

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, int *number_of_dimensions, char *tool_transformation, char *conf_database);
int print_int_array(FILE* file, int *array, int number_of_elements);
int print_float_array(float *array, int number_of_elements, FILE *file);
int get_float_array_from_line(float *values, char *line, int size);
int get_object_from_line(object_type* object, char *line, int size);
int get_object_from_msg(object_type *object, int number_of_dimensions, void *msg);
int get_statistic_value_from_msg(statistic_type *statistic, int number_of_dimensions, void *msg);
int get_attribute_from_line(attribute_type* attribute, char *line, int size);
int get_attribute_from_msg(attribute_type *attribute, void *msg);

float distance_between_points(float *point1, float *point2, int dimensions);


#ifdef GET_TIME
int print_time(struct timeval *start, struct timeval *end, char *name_of_file);
#endif

#endif


