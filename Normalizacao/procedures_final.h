#ifndef PROCEDURES_FINAL_H
#define PROCEDURES_FINAL_H
#include "dbtransformation.h"

int calculate_global_mean_value (statistic_type *global_statistic, statistic_type *local_statistic, int number_nomalizationlocal_filter, int number_of_dimensions, int iterate);

int calculate_global_variance_value (statistic_type *global_statistic, statistic_type *local_statistic, int number_of_dimensions);



#endif
