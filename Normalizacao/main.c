#include "dbtransformation.h"

int main (int argc, char *argv[])
{	
	char *work;
	int work_size;
	
	char *input_file_name=NULL;
	char *output_file_name=NULL;
	char *number_of_dimensions=NULL;
	char tool_transformation[MAX_TOOLNAME];
	char *conf_database = NULL;

	Layout *console;
	char confFile[] = "./conf.xml";

	char *transformation_initial = "standartization";
    	static int tool_flag = 0;
    	int option_index = 0;
    	
	static struct option long_options[] =
             {
               /* These options set a flag. */
               {"tool", optional_argument, &tool_flag, 1},
               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"dimensions",  required_argument, 0, 'd'},
               {"input",  required_argument, 0, 'i'},
               {"output",    required_argument, 0, 'o'},
               {0, 0, 0, 0}
             };

	int option;

	// loop to receive information from the command line
        while ( (option = getopt_long (argc, argv, "i:o:d:",long_options, &option_index)) != EOF )
	{
                switch (option) 
		{
               		case 0:
               		/* If this option set a flag, do nothing else now. */
               			if (long_options[option_index].flag != NULL){
				 	printf ("option %s ", long_options[option_index].name);
					if (optarg){
                		 		printf (" with arg %s", optarg);
               					printf ("\n");
						strcpy(tool_transformation,optarg);
					}
					else {
						printf("without  argument : \n");
						printf("normalization, standartization\n");
						printf("To use transformation defaut don't need to use the option tool\n");
						exit(1);
					}	
				}
				
			    	break;
	
			// input file
                        case 'i':
				input_file_name=(char *)malloc(MAX_FILENAME);
				strcpy(input_file_name,optarg);
                       		printf ("option -o with value `%s'\n", input_file_name);
				// database conf
				conf_database = (char *)malloc(MAX_FILENAME + 5);
				sprintf (conf_database, "%s.conf", input_file_name);
				printf ("conf database name is `%s'\n", conf_database);


				break;
			
			// output file 
                        case 'o':
				output_file_name=(char *)malloc(MAX_FILENAME);
				strcpy(output_file_name,optarg);
                                printf ("option -o with value `%s'\n", output_file_name);
				break;
                        
			// number of dimensions
			case 'd':
				number_of_dimensions=(char *)malloc(MAX_VALUE_LEN);
				strcpy(number_of_dimensions,optarg);
				printf ("option -d with value `%s'\n", number_of_dimensions);
				break;
			
			// error message
			default:
				printf ("\nError in the command line: \nOption found: %d -> %c", option, (char)option );
			        printf ("Usage: ./main -i <database_file> -o <output_file> -d <#dimensions>\n");
				printf ("To define the type of transformation use the option --tool\n");
				exit(1);
                }
        }

/*********************************************************************/
	//arguments the arguments.
	
	if (input_file_name==NULL || output_file_name==NULL || number_of_dimensions==NULL) {
	        printf ("Usage: ./main -i <database_file> -o <output_file> -d <#dimensions>\n");
		printf ("To define the type of transformation use the option --tool\n");
		exit(1);
	}
	
	if(tool_flag) {
		if ((strcmp(tool_transformation,"normalization")) & (strcmp(tool_transformation,"standartization")) & (strcmp(tool_transformation,"hybrid"))) {
			printf("invalid argument! \n");
			printf("use : normalization, standartization\n");
			printf("To use transformation defaut don't need to use the option tool\n");
			exit(1);
		}
	}
	else strcpy(tool_transformation,transformation_initial);


	if (atoi(number_of_dimensions) <= 0)
        {
                printf ("\tERROR. The parameters passed are invalid.\n");
		printf("\tNumber of dimensions: %d\n", atoi(number_of_dimensions));
                exit(1);
        }
/***********************************************************************/
	
	// fills in the work with the all the parameters received

	/*************************************
	 * 	WORK			     *
	 *				     * 	
	 * 	1. input file name	     *
	 * 	2. output file name          *
	 * 	3. number of dimensions      *
	 *************************************/	

	
	work_size = strlen(input_file_name) + strlen(output_file_name) + strlen(number_of_dimensions) + strlen(tool_transformation) + strlen(conf_database)+ 7 /*extra space*/;
	
	work = (char *) malloc (work_size);
	sprintf(work, "%s,%s,%s,%s,%s", input_file_name, output_file_name, number_of_dimensions, tool_transformation, conf_database);
	
	
	console = initAh(confFile, argc, argv); 
	appendWork(console, (void *)work, work_size);
	finalizeAh(console);
	
	return 0;
}

