#ifndef DOUBLE_LINKED_ATTRIBUTE_LIST_H
#define DOUBLE_LINKED_ATTRIBUTE_LIST_H
#include "dbtransformation.h"
list_attribute_pointer alloc_attribute_cell(void);
void create_empty_attribute_list(list_attribute_type *list);
int is_empty_attribute_list(list_attribute_type *list);
void insert_in_list_attribute(attribute_type item, list_attribute_type *list);
void insert_in_list_attribute_on_position(attribute_type item, list_attribute_pointer *p, list_attribute_type *list);
void insert_in_begin_list_attribute(attribute_type item, list_attribute_type *list);
attribute_type remove_of_list_attribute(list_attribute_pointer *p, list_attribute_type *list);
attribute_type remove_first_of_list_attribute(list_attribute_type *list);
int get_first_in_list_attribute(list_attribute_type *list, list_attribute_pointer *pointer);
int get_last_in_list_attribute(list_attribute_type *list, list_attribute_pointer *pointer);
int go_next_attribute(list_attribute_type *list, list_attribute_pointer *pointer);
int is_last_attribute(list_attribute_pointer p, list_attribute_type *list);
list_attribute_pointer search_item_in_list_attribute(attribute_type item, list_attribute_type *list);
void swap_attribute_items(list_attribute_pointer previous, list_attribute_pointer next, list_attribute_type *list);
void make_list_attribute_empty(list_attribute_type *list);
void destroy_list_attribute(list_attribute_type *list);
int concatenate_lists_attribute(list_attribute_type *list1, list_attribute_type *list2);
int get_list_attribute_length(list_attribute_type *list);
void print_list_attribute(list_attribute_type *list, FILE *file);

#endif
