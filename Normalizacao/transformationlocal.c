#include "transformationlocal.h"
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*************************** INPUT AND OUTPUT PORTS *************************/

InputPortHandler Distributor_input, Distributor_attributes_input, Final_input;	// input ports
OutputPortHandler Final_output;			// output port that sends the messages to the filter bin

/**************************** GLOBAL VARIABLES ******************************/


/* Parameters of the program read from the work*/
int number_of_dimensions;			// number of dimensions (attributes) of the database 

char conf_database[MAX_FILENAME + 5];

char output_file_name[MAX_FILENAME];		// stores the name of the output file
char tool_transformation[MAX_TOOLNAME];		// stores the name of the transformation type


/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	/* Parameters of the program read from the work*/
	char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read 

	char error_msg[100];
	
	// opens input and output ports
	Distributor_input = ahGetInputPortByName ("Distributor_to_TransformationLocal_input");
	Distributor_attributes_input = ahGetInputPortByName ("Distributor_to_TransformationLocal_at_input");
	Final_input = ahGetInputPortByName ("Final_to_TransformationLocal_input");
	Final_output = ahGetOutputPortByName ("TransformationLocal_to_Final_output");
	
	if ( (Distributor_input == -1) || (Distributor_attributes_input==-1) || (Final_input == -1) || (Final_output == -1) )
	{
		sprintf(error_msg,"ERROR. Could not open the Assigner filter's ports.\n");
		ahExit (error_msg);
	}
	
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &number_of_dimensions, tool_transformation, conf_database);
	
	return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int processFilter(void *work, int worksize) 
{
        list_type object_list;
	list_attribute_type attribute_list;
        statistic_type local_mean, local_variance,  global_mean, global_variance;
	char error_msg[100];
	

#ifdef GET_TIME
	struct timeval initial_time, receive_database_time, first_phase_time;
	FILE *fp_time;
	char name_of_fp_time[MAX_FILENAME];
	
	sprintf(name_of_fp_time, "%s.transformationlocal_%d.time", output_file_name, ahGetMyRank()); 
	
	// Clean the files of times
	fp_time = fopen(name_of_fp_time,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);

#endif

#ifdef GET_TIME	
	gettimeofday(&initial_time, NULL);
#endif
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_TransformationLocal_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/
        
        /* Inicialize the structs */
	create_empty_attribute_list(&attribute_list);
	create_empty_list(&object_list);
	create_empty_statistic(&local_mean, number_of_dimensions);
	create_empty_statistic(&local_variance, number_of_dimensions);
	create_empty_statistic(&global_mean, number_of_dimensions);
	create_empty_statistic(&global_variance, number_of_dimensions);	

	/*Receive the attributes types*/
	receive_attributes_value(&attribute_list,number_of_dimensions, Distributor_attributes_input);

	
	/* Send the attributes to Filter Final*/
	int rank = ahGetMyRank();
	if (rank==0)
		send_attribute_list(&attribute_list, Final_output);

	/* Receive the database from the Distributor Filter */
	receive_database(&object_list, number_of_dimensions, Distributor_input);

#ifdef GET_TIME	
	gettimeofday(&receive_database_time, NULL);
#endif

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "FILTER %s RECEIVED DATABASE: \n", ahGetFilterName());
	print_list(&object_list, number_of_dimensions, fp_log);
	fprintf(fp_log, "FILTER %s END OF DATABASE\n", ahGetFilterName());
	fclose(fp_log);
#endif		
/****************************************************************************/


/* Read the List of objects. After, calculate the local mean value and variance value of each atribute.*/

    calculate_local_mean_value(&object_list, &local_mean, number_of_dimensions);
 
/*Send to Final Filter the value of the means calculates*/

    send_statistic_msg(&local_mean, MEAN_MSG, number_of_dimensions, Final_output);

/*Receive to Final Filter the value of the globals means*/

    receive_statistic_value(&global_mean, number_of_dimensions, Final_input);
    
/* Read the List of objects. After, calculate the local variance value and variance value of each atribute.*/

    calculate_local_variance_value(&object_list, &local_variance,  &global_mean, number_of_dimensions);

/*Send to Final Filter the objects of the local List.*/

    send_statistic_msg(&local_variance, VARIANCE_MSG, number_of_dimensions, Final_output);

/* Receive Global Variance */

    receive_statistic_value(&global_variance, number_of_dimensions, Final_input);

/* Normalize the Local objects */

   normalize_database(&object_list, global_mean, global_variance, number_of_dimensions);
 
/* Send to Final Filter all local objects  */

    send_object_list(&object_list, number_of_dimensions, Final_output);

  
#ifdef GET_TIME	
	gettimeofday(&first_phase_time, NULL);
	print_time(&initial_time, &receive_database_time, name_of_fp_time);
	print_time(&receive_database_time, &first_phase_time, name_of_fp_time);
#endif


	
	free_statistic(local_mean);
	free_statistic(local_variance);
	free_statistic(global_mean);
	free_statistic(global_variance);
	destroy_list(&object_list);
	
	return 0;
}

int finalizeFilter()
{	


	return 0;
}
