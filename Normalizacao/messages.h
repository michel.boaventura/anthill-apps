#ifndef _MESSAGES_H_
#define _MESSAGES_H_
#include "dbtransformation.h"

int send_attribute_values(attribute_type *attribute, OutputPortHandler port);
int send_object_values(object_type *object, int number_of_dimensions, OutputPortHandler port);
int send_object_list(list_type *list, int number_of_dimensions, OutputPortHandler port);
int send_statistic_msg(statistic_type *statistic, int type_msg, int number_of_dimensions, OutputPortHandler port);
int receive_database(list_type *object_list, int number_of_dimensions, InputPortHandler input_port);
int receive_statistic_value(statistic_type *statistic, int number_of_dimensions, InputPortHandler input_port);
int receive_attributes_value(list_attribute_type *attribute_list, int number_of_dimensions,InputPortHandler input_port);
int send_attribute_values(attribute_type *attribute, OutputPortHandler port);
int send_attribute_list(list_attribute_type *list, OutputPortHandler port);


void print_object_message(void *msg, int number_of_dimensions, FILE *file);
void print_statistic_message(void *msg, int number_of_dimensions, FILE *file);

void print_attribute_message(void *msg, FILE *file);

#endif

