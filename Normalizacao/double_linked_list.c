/* Arquivo: lists.c */

/* Inclusoes. */
#include "double_linked_list.h"

/* Function alloc_cell allocs memory necessary to creating a new cell */

list_pointer alloc_cell(void)
{
   list_pointer aux;
   aux=malloc(sizeof(cell));
   return aux;
}

/* Procedure that creates an empty list */

void create_empty_list(list_type *list)
{
   list->first=alloc_cell();
   list->last=list->first;
   list->first->next=list->first;
   list->first->previous=list->first;
   list->length=0;
}

/* Answer if a list is empty */
int is_empty(list_type *list)
{
   return !(list->length);
}

/* insert_in_list inserts a new item on the last position of a list */
void insert_in_list(object_type item, list_type *list)
{
   list->last->next=alloc_cell();
   list->last->next->previous=list->last;
   list->last=list->last->next;
   list->last->item=item;
   list->last->next=list->first;
   list->first->previous=list->last;
   list->length++;
}

/* insert_in_list_on_position inserts a new item on the indicated position of a list */
void insert_in_list_on_position(object_type item, list_pointer *p, list_type *list)
{
	list_pointer aux=*p;

	if (aux==NULL)
	{
		*p=NULL;
		return;
	}
	/* Tests if the pointer is pointing to the dummy cell.
	 * In this case we will just insert the item on the last of the list */
	if (aux==list->first)
	{
		insert_in_list(item, list);
	}
	else
	{
		/* Tests if the pointer is pointing to the real first item of the list.
		 * In this case we will insert the item on the first position of the list */
		
		if(aux==list->first->next)	
			insert_in_begin_list(item, list);
		else
		{
			aux->previous->next=alloc_cell();
			aux->previous->next->previous=aux->previous;
			aux->previous=aux->previous->next;
			aux->previous->item=item;
			aux->previous->next=aux;
			list->length++;
		}
	}
	*p=aux->previous;
}

/* Inserts a new item on the first position of a list */
void insert_in_begin_list(object_type item, list_type *list)
{
   list_pointer aux;
   aux=alloc_cell();
   aux->item=item;
   aux->next=list->first->next;
   aux->next->previous=aux;
   list->first->next=aux;
   aux->previous=list->first;
   if (list->length==0)
	   list->last=aux;
   list->length++;
}   

/* Remove the item that is pointed.
 * After the remove, the pointer will be pointed to the previous item. 
 * Then, the next list of the item pointed will be the same */

object_type remove_of_list(list_pointer *p, list_type *list)
{
   object_type item;
   list_pointer aux;

   /* Tests is the list is empty or the pointer is null */
   if (is_empty(list) || !(*p) || (*p==list->first))
      printf("Error: List is_empty or position doesn't exists.\n");
   else
   {
      item=(*p)->item;
      (*p)->next->previous=(*p)->previous;
      (*p)->previous->next=(*p)->next;
      if ((*p)==list->last)
         list->last=(*p)->previous;
      aux=(*p);
      (*p)=(*p)->previous;
      free_cell(aux);
      list->length--;
   }
   return item;
}

/* Remove the first item of the list, and retornn this*/

object_type remove_first_of_list(list_type *list)
{
   object_type item;
   list_pointer aux=list->first->next;
   item=remove_of_list(&aux, list);
   return item;
}

/* Get the first item of the list if it exists */
int get_first_in_list(list_type *list, list_pointer *pointer)
{
   if (is_empty(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->first->next;
   return 1;
}

/* Get the last item of the list if it exists */
int get_last_in_list(list_type *list, list_pointer *pointer)
{
   if (is_empty(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->last;
   return 1;
}

/* Go to the next position of the list */
int go_next(list_type *list, list_pointer *pointer)
{
	/* Tests if the pointer is valid */
	if (*pointer)
	{
		/* Tests if the object is the last one of the list. In this case it do not have a next item. */
		if ((*pointer)==list->last)
		{
			get_first_in_list(list, pointer);
			return 0;
		}
		else
		{
			*pointer=(*pointer)->next;
			return 1;
		}
	}
	else return 0;     
}

/* Tests is the item is the last of the list */
int is_last(list_pointer p, list_type *list)
{

   /* If the pointer is NULL, there is an error */
   if (!p)
   {
      printf("Erro: Este apontador n�o aponta para c�lula alguma");
      return 0;
   }
   return (p->next==list->first);
}

/* Searchs an item on the list, returning this position or NULL if the item was not found */

list_pointer search_item_in_list(object_type item, list_type *list)
{
   list_pointer aux;

   /* If there is no item on the list, it returns NULL*/
   if(!get_first_in_list(list, &aux))
	   return NULL; 

   /* While there is an next item, keep searching */
   do
   {
      if (compare_objects(aux->item, item))
      {
	 return aux;
      }
      go_next(list, &aux);
   }   
   while (!is_last(aux, list));
   
   /* Tests if the object is the last */
   if (compare_objects(aux->item, item))
   {
         return aux;
   }

   return NULL;
}

/* Swap two items that are adjacent on the list */
void swap_items(list_pointer previous, list_pointer next, list_type *list)
{
   if (previous!=list->last && list->length>1)
   {
      if (previous->next==next)
      {
         previous->previous->next=next;
         next->next->previous=previous;
         previous->next=next->next;
         next->previous=previous->previous;
         previous->previous=next;
         next->next=previous;
      }
      if (previous==list->first) 
         list->first=next;
      if (next==list->last) 
         list->last=previous;
   }
   else printf("Error: the swap can�t be completed. \n");
}

/* Make an list empty, is frees the memory allocated*/
void make_list_empty(list_type *list)
{
   object_type aux;
   while (!is_empty(list))
   {
      aux=remove_first_of_list(list);
      free_object(aux);
   }
}

/* Destroys a list. First make this empty, then frees the memory */
void destroy_list(list_type *list)
{
	make_list_empty(list);
	free(list->first);
}

/* Concatenates two lists. The second list structure will get empty on the operation */
int concatenate_lists(list_type *list1, list_type *list2)
{
   list_pointer last_of_the_first_list;
   list_pointer first_of_the_second_list;
   
   /* If the lists are invalid or the two lists are the same couldn't concatenate them */
   if (list1==NULL || list2==NULL || list1==list2)
	   return 0;
   
   /* If the second list is empty, nothing on the first list will be changed */
   if(!is_empty(list2))
   {
      if (is_empty(list1))
      {
         /* Update the fields of the dummy cell of the first list */
         list1->first->next=list2->first->next;
	 list1->first->previous=list2->first->previous;

	 /* Update the fields of the items that points to the dummy cell of the list */
	 list2->first->next->previous=list1->first;
	 list2->last->next=list1->first;
         
	 /* Update the field last of the first list */
	 list1->last=list2->last;
      }
      else
      {
         /* Get a Pointer to the previous last of the first list */
         get_last_in_list(list1, &last_of_the_first_list);
      
         /* Get a Pointer to the previous last of the first list */
         get_first_in_list(list2, &first_of_the_second_list);
	   
         /* Points the field last of the list to the last item of the second list */
         get_last_in_list(list2, &(list1->last));

         /* Points the previous field of the dummy cell to the new last item */
         get_last_in_list(list2, &(list1->first->previous));
	
         /* Points the field next of the new last item to the dummy cell of the list 1 */
         list1->last->next=list1->first;
    
         /* Updates the fields of the previous last item of the first list */
         last_of_the_first_list->next=first_of_the_second_list;
     
         /* Updates the fields of the previous first item of the second list */
         first_of_the_second_list->previous=last_of_the_first_list;
      }
 
      /* updates the length of the first list */ 
      list1->length+=list2->length;
            
      /* Make the list2 seems now empty, without remove the items that is now on the list1 */
      list2->last=list2->first;
      list2->first->previous=list2->first;
      list2->first->next=list2->first;
      list2->length=0;
   }
   return 1;
}   

/* Gets the size of the list */
int get_list_length(list_type *list)
{
	return (list->length);
}

/* Print all the items of the list */
void print_list(list_type *list, int number_of_dimensions, FILE *file)
{
   
	list_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_list(list, &aux))
	{	
		fprintf(file, "Object List is Empty!!!");
		return;
	}

	/* Prints the First Object of the List*/
	print_object(aux->item, number_of_dimensions, file);

	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		print_object(aux->item, number_of_dimensions, file);
	}
}

