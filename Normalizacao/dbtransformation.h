#ifndef DBNORMALIZATION_H
#define DBNORMALIZATION_H

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include "anthill.h"
#include "constants.h"
#include "structs.h"
#include "procedures.h"
#include "double_linked_list.h"
#include "double_linked_attribute_list.h"
#include "object.h"
#include "attribute.h"
#include "statistic.h"
#include "messages.h"
#endif
