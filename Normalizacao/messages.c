#include "messages.h"

/* Send an object to the filter FINAL */
int send_attribute_values(attribute_type *attribute, OutputPortHandler port)
{
	int msg_size;

	void *msg;
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/
	
	/* Fill the values on the message */ 
	if (attribute->name==NULL) return 0;
	
	msg_size = fill_attribute_in_message(&msg, (*attribute));

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "SENDING: \n");
	print_attribute_message(msg, fp_log);
	fprintf(fp_log, "END OF MESSAGE\n");
	fclose(fp_log);
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}



/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send an object to the filter FINAL */
int send_object_values(object_type *object, int number_of_dimensions, OutputPortHandler port)
{
	int msg_size;

	void *msg;
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/
	
	/* Fill the values on the message */ 
	if ((*object).dimension_values==NULL) return 0;
	
	msg_size = fill_object_in_message(&msg, (*object), number_of_dimensions);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "SENDING: \n");
	print_object_message(msg, number_of_dimensions, fp_log);
	fprintf(fp_log, "END OF MESSAGE\n");
	fclose(fp_log);
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Sends all the objects of the list to the filter BIN*/
int send_object_list(list_type *list, int number_of_dimensions, OutputPortHandler port)
{
   
	list_pointer aux;

	/* If the first item is not found, it justs returns */
	if (!get_first_in_list(list, &aux))
	{	
		return 0;
	}

	/* Send the first object of the List*/
	send_object_values(&(aux->item), number_of_dimensions, port);

	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		send_object_values(&(aux->item), number_of_dimensions, port);
	}
	return 1;
}

/* Sends all the attributes of the list to the filter BIN*/
int send_attribute_list(list_attribute_type *list, OutputPortHandler port)
{
   
	list_attribute_pointer aux;

	/* If the first item is not found, it justs returns */
	if (!get_first_in_list_attribute(list, &aux))
	{	
		return 0;
	}

	/* Send the first object of the List*/
	send_attribute_values(&(aux->item), port);

	while (!is_last_attribute(aux, list))
	{
		go_next_attribute(list, &aux);
		send_attribute_values(&(aux->item), port);
	}
	return 1;
}

//******************************************************************************************//


int send_statistic_msg(statistic_type *statistic, int type_msg, int number_of_dimensions, OutputPortHandler port)
{
	int msg_size;
	void *msg;
	char error_msg[100];
	
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/
	/* Fill the values on the message */ 
	if ((*statistic).dimension_values==NULL) return 0;
		
	if (type_msg == MEAN_MSG){
		msg_size = fill_mean_in_message(&msg, (*statistic), number_of_dimensions);
	}

	else {
		msg_size = fill_variance_in_message(&msg, (*statistic), number_of_dimensions);
	
	}

	if (msg_size == 0)
		{
			sprintf(error_msg,"ERROR. There was an error on reading an statistic type, this seems do not have the right number of dimensions.\n Statistic Vector");
			ahExit (error_msg);
		}
	else
		{
			
		
		/********************************** DEBUG ***********************************/
		#ifdef DEBUG
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \n");
		print_statistic_message(msg, number_of_dimensions, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
		#endif		
		/****************************************************************************/
	

	
		/* Sends the message */
		ahWriteBuffer (port, msg, msg_size);
	}
	free(msg);
	return 1;
}



int receive_database(list_type *object_list, int number_of_dimensions, InputPortHandler input_port)
{
	int msg_size=2*sizeof(int)+number_of_dimensions*sizeof(float);	// size of the mesage that will be received 
	void *message;
	message=malloc(msg_size);	// message that will received

	object_type object;
	
	while (ahReadBuffer (input_port, message, msg_size) != EOW)
	{
		/* Get an object from the message */
		
		get_object_from_msg(&object, number_of_dimensions, message);
		insert_in_list(object, object_list);
	}

	free(message);
	return 1;
}



int receive_attributes_value(list_attribute_type *attribute_list, int number_of_dimensions, InputPortHandler input_port)
{
	int i;
	int msg_size=3*sizeof(int)+MAX_ATTRIBUTENAME*sizeof(char);	// size of the mesage that will be received 
	void *message;
	
	message=malloc(msg_size);	// message that will received

	attribute_type attribute;

	for (i=0; i<number_of_dimensions; i++) {
		
		
		ahReadBuffer (input_port, message, msg_size); 
		
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/

		get_attribute_from_msg(&attribute, message);
		insert_in_list_attribute(attribute, attribute_list);

/********************************** DEBUG ***********************************/
	#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "RECEIVING: \n");
	print_attribute_message(message, fp_log);
	fprintf(fp_log, "END OF MESSAGE\n");
	fclose(fp_log);
	#endif		
/****************************************************************************/
	}
	free(message);
	return 1;

	
}

int receive_statistic_value(statistic_type *statistic, int number_of_dimensions, InputPortHandler input_port)
{
	int msg_size=2*sizeof(int)+number_of_dimensions*sizeof(float);	// size of the mesage that will be received 
	void *message;
	
	message=malloc(msg_size);	// message that will received
	ahReadBuffer (input_port, message, msg_size);

/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/

	get_statistic_value_from_msg(statistic, number_of_dimensions, message);

/********************************** DEBUG ***********************************/
	#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "RECEIVING: \n");
	print_statistic_message(message, number_of_dimensions, fp_log);
	fprintf(fp_log, "END OF MESSAGE\n");
	fclose(fp_log);
	#endif		
/****************************************************************************/

	free(message);
	return 1;

	
}



/* This function is responsible to print Object Values message sent from the NormalizationLocal filter to the Final filter on a file. */
void print_object_message(void *msg, int number_of_dimensions, FILE *file)
{
	int *int_msg;
	float *values_msg;
	int_msg=(int *)msg;
	int i;
	
	fprintf(file, "MESSAGE: OBJECT_MSG\t");
	fprintf(file, "Type msg: %d\t", int_msg[MSG_TYPE_FIELD]);
	fprintf(file, "Object Number: %d\t", int_msg[OBJECT_NUMBER_FIELD]);

	/* Prints the object values from the message */
	fprintf(file, "Dimension Values: ");
		
	values_msg=(float *)(&(int_msg[OBJECT_VALUES_FIELD]));
	for (i=0; i<number_of_dimensions; i++)
		fprintf(file, "%f ",values_msg[i]);
	fprintf(file, "\n");
}



/* This function is responsible to print Atribute Values message sent from the transformationLocal filter to the Final filter on a file. */
void print_attribute_message(void *msg, FILE *file)
{
	int *int_msg;
	char *values_msg;
	int_msg=(int *)msg;
	
	fprintf(file, "MESSAGE: ATTRIBUTE_MSG\t");
	fprintf(file, "Type msg: %d\t", int_msg[MSG_TYPE_FIELD]);
	fprintf(file, "Attribute Number: %d\t", int_msg[ATTRIBUTE_NUMBER_FIELD]);

	fprintf(file, "Attribute Type Number: %d\t", int_msg[ATTRIBUTE_TYPE_NUMBER_FIELD]);

	/* Prints the attribute name from the message */
	values_msg=(char *)(&(int_msg[ATTRIBUTE_TYPE_NAME_FIELD]));
	fprintf(file, "Type Name : %s",values_msg);
	fprintf(file, "\n");
}

/* This function is responsible to print Statistic Values message sent from the NormalizationLocal filter to the Final filter on a file. */
void print_statistic_message(void *msg, int number_of_dimensions, FILE *file)
{
	int *int_msg;
	int msg_type;
	float *values_msg;
	int_msg=(int *)msg;
	int i;
	
	fprintf(file, "MESSAGE: STATISTIC_MSG\t");
	fprintf(file, "Type msg: %d\t", int_msg[MSG_TYPE_FIELD]);
	fprintf(file, "Object Number: %d\t", int_msg[OBJECT_NUMBER_FIELD]);

	/* Prints the object values from the message */
	fprintf(file, "Dimension Values: ");
	//check the type of message
	msg_type = int_msg[MSG_TYPE_FIELD];
	values_msg=(float *)(&(int_msg[STATISTIC_VALUES_FIELD]));
		
	for (i=0; i<number_of_dimensions; i++)
		fprintf(file, "%f ",values_msg[i]);
	fprintf(file, "\n");
}





	
