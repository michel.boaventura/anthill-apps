#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H
#include "dbtransformation.h"
void free_cell_attribute(list_attribute_pointer q);
void print_attribute(attribute_type attribute, FILE *fp_log);
void free_attribute(attribute_type attribute);
void malloc_attribute(attribute_type *attribute);
int compare_attributes(attribute_type attribute1, attribute_type attribute2);



#endif
