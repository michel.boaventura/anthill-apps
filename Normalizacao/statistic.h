#ifndef _STATISTIC_H
#define _STATISTIC_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
#include "math.h"
/* Prototypes */
void print_statistic(statistic_type *statistic, int number_of_dimensions, FILE *fp_log);
void free_statistic(statistic_type statistic);
void malloc_statistic(statistic_type *statistic, int number_of_dimensions_values);
void empty_statistic(statistic_type *statistic, int number_of_dimensions);
void create_empty_statistic(statistic_type *statistic, int number_of_dimensions);

#endif

