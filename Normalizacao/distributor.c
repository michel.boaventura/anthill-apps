#include"distributor.h"
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*************************** INPUT AND OUTPUT PORTS *************************/

OutputPortHandler TransformationLocal_output, TransformationLocal_attributes_output; // output port that sends the objects to the filter

/**************************** GLOBAL VARIABLES ******************************/

/* Parameters of the program read from the work*/

char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read. 

char conf_database[MAX_FILENAME+5];

int number_of_dimensions;			// number of dimensions (attributes) of the database 
int number_types[2];
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	char output_file_name[MAX_FILENAME];		// stores the name of the output file
	char tool_transformation[MAX_TOOLNAME];
	char error_msg[100];
	
	// opens input and output ports
	TransformationLocal_output = ahGetOutputPortByName("Distributor_to_TransformationLocal_output");
	if ( (TransformationLocal_output == -1) )
	{
		sprintf(error_msg,"ERROR. Could not open the distributor filter's ports.\n");
		ahExit (error_msg);
	}
	
	TransformationLocal_attributes_output = ahGetOutputPortByName("Distributor_to_TransformationLocal_at_output");
	if ( (TransformationLocal_attributes_output == -1) )
	{
		sprintf(error_msg,"ERROR. Could not open the distributor filter's ports.\n");
		ahExit (error_msg);
	}


 
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &number_of_dimensions, tool_transformation, conf_database);
	
	return 1;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/* */
int processFilter(void *work, int workSize)
{
	char error_msg[100];
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_Distributor_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/

	// in case there is more than one instance of the distributor filter,
	// the others are killed
	if (ahGetMyRank() > 0)
		return 1;

	FILE *input_file;
	open_file(&input_file, input_file_name);
	FILE *input_file_conf;
	open_file(&input_file_conf, conf_database);
	void *msg;
	int msg_size;
	object_type aux_object;
	attribute_type aux_attribute;

	//read the firt line of confdatabase
	int line_len = MAX_ATTRIBUTENAME*2;
	char *line= malloc(line_len * sizeof(char));
	if (fgets(line, line_len, input_file_conf))
		sscanf(line, "%d %d %d", &(number_types[0]), &(number_types[1]), &(number_types[2]));
	

	while(get_attribute_from_file(&aux_attribute,number_types, input_file_conf))
	{
		// Allocates the msg and return the message already filled 
		msg_size = fill_attribute_in_message (&msg, aux_attribute);

		// checks for an empty line or a wrong number of dimensions
		if (msg_size == 0)
		{
			sprintf(error_msg,"ERROR. There was an error on reading an object, this seems do not have the right number of dimensions.\nObject number: %s\n", aux_attribute.name);
			ahExit (error_msg);
		}
		else
		{
			ahWriteBuffer (TransformationLocal_attributes_output, msg, msg_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG		
			fp_log = fopen(name_of_fp_log,"a");
			print_attribute_message(msg, fp_log);
			fclose(fp_log);
#endif		
/****************************************************************************/

		}
			
		free(msg);
		free_attribute(aux_attribute);
	}
	

	// each line of the database file is distributed to one of the
	// assigners (round robin policy)
	// the function get_object_from_file reads a line and fill it in an object	
	while (get_object_from_file(&aux_object, number_of_dimensions, input_file))
	{
		// Allocates the msg and return the message already filled 
		msg_size = fill_object_in_message (&msg, aux_object, number_of_dimensions);

		// checks for an empty line or a wrong number of dimensions
		if (msg_size == 0)
		{
			sprintf(error_msg,"ERROR. There was an error on reading an object, this seems do not have the right number of dimensions.\nObject number: %d\n", aux_object.number);
			ahExit (error_msg);
		}
		else
		{
			ahWriteBuffer (TransformationLocal_output, msg, msg_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG		
			fp_log = fopen(name_of_fp_log,"a");
			print_object_message(msg, number_of_dimensions, fp_log);
			fclose(fp_log);
#endif		
/****************************************************************************/

		}
			
		free(msg);
		free_object(aux_object);
	}
	
	fclose(input_file);
	fclose(input_file_conf);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/* */
int finalizeFilter()
{
	return 1;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


