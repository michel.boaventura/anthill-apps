#include "final.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*************************** INPUT AND OUTPUT PORTS *************************/

InputPortHandler TransformationLocal_input;			// input ports
OutputPortHandler TransformationLocal_output;			//output ports
/**************************** GLOBAL VARIABLES ******************************/

/* Parameters of the program read from the work*/
int number_of_dimensions;			// number of dimensions (attributes) of the database 
char output_file_name[MAX_FILENAME];		// stores the name of the output file
char tool_transformation[MAX_TOOLNAME];	// stores the name of the transformation type

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	/* Parameters of the program read from the work*/
	char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read
	char conf_database[MAX_FILENAME + 5];
	char error_msg[100];
	
	// opens input and output ports
	TransformationLocal_input = ahGetInputPortByName ("TransformationLocal_to_Final_input");
	TransformationLocal_output = ahGetOutputPortByName ("Final_to_TransformationLocal_output");
	if ( TransformationLocal_input == -1 || TransformationLocal_output == -1)
	{
		sprintf(error_msg,"ERROR. Could not open the Final filter's ports.\n");
		ahExit (error_msg);
	}
	
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &number_of_dimensions, tool_transformation, conf_database);
	
	return 1;
}

int processFilter(void *work, int worksize) 
{
	
	FILE *fp_out;
	char name_of_fp_out[MAX_FILENAME];
	sprintf(name_of_fp_out, "%s.out", output_file_name);
	
	list_type global_object_list;
	list_attribute_type attribute_list;
	statistic_type global_mean;
	statistic_type global_variance;
	statistic_type local_mean;
	statistic_type local_variance;
	int i, Number_TransformationLocal_Instances;
	
	char error_msg[100];

#ifdef GET_TIME
	struct timeval initial_time, final_time;
	FILE *fp_time;
	char name_of_fp_time[MAX_FILENAME];
	sprintf(name_of_fp_time, "%s.time", output_file_name); 
	
	// Clean the files of times
	fp_time = fopen(name_of_fp_time,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);

#endif

#ifdef GET_TIME	
	gettimeofday(&initial_time, NULL);
#endif

	

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_Final_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/	

	//Inicialize the structs
	create_empty_attribute_list(&attribute_list);
	create_empty_list(&global_object_list);
	create_empty_statistic(&global_mean, number_of_dimensions);
	create_empty_statistic(&global_variance, number_of_dimensions);
	create_empty_statistic(&local_mean, number_of_dimensions);
	create_empty_statistic(&local_variance, number_of_dimensions);

	//Receive the attributes list
	receive_attributes_value(&attribute_list, number_of_dimensions,TransformationLocal_input);

	//Receive the Locals Means of each TransformationLocal Filter and
	//Calculate the Global Mean Value

	Number_TransformationLocal_Instances = ahGetNumWriters(TransformationLocal_input);

	for (i=0; i<Number_TransformationLocal_Instances;i++)
	{
		receive_statistic_value(&local_mean, number_of_dimensions, TransformationLocal_input);
		calculate_global_mean_value(&global_mean, &local_mean, Number_TransformationLocal_Instances ,number_of_dimensions, i);
	}
		
	//Send the Global Mean Value to all TransformationLocal Filters
	send_statistic_msg(&global_mean, MEAN_MSG , number_of_dimensions, TransformationLocal_output);

	//Receive the Locals Variances of each TransformationLocal Filter
	//Calculate the global variance value
	for (i=0; i<Number_TransformationLocal_Instances ;i++)
	{
		receive_statistic_value(&local_variance, number_of_dimensions, TransformationLocal_input);
		calculate_global_variance_value(&global_variance, &local_variance, number_of_dimensions);
	}

	//Send the Global Variance to all Transformation Filter
	send_statistic_msg(&global_variance, VARIANCE_MSG , number_of_dimensions, TransformationLocal_output);

	//Receive the Transformed Objects of the TransformationLocal Filters
	
	receive_database(&global_object_list, number_of_dimensions, TransformationLocal_output);
	


/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Objects Transformed and Reiceived\n ");
	print_list(&global_object_list, number_of_dimensions, fp_log);
	fclose(fp_log);
#endif		
/****************************************************************************/
	
	// Open the Output File to write on this
	fp_out = fopen(name_of_fp_out,"w");
	if (!fp_out)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}

	//Print the Transformed Object List in the output file
	
	print_list(&global_object_list, number_of_dimensions, fp_out);

	// Close the Output File
	fclose(fp_out);

#ifdef GET_TIME
	gettimeofday(&final_time, NULL);	
	print_time(&initial_time, &final_time, name_of_fp_time);
#endif
	
	free_statistic(local_mean);
	free_statistic(local_variance);
	free_statistic(global_mean);
	free_statistic(global_variance);
	destroy_list(&global_object_list);

	
	return 0;
}


int finalizeFilter(void)
{	
	return 0;
}
