/* Inclusoes. */
#include "attribute.h"

/* Procedure that free memory of a cell .
 * It not free the memory for the objects that the cell points */
void free_cell_attribute(list_attribute_pointer q)
{
/*   free_object(q->item);	
   free(q->item);*/
   free(q);
}

/* Print an object */
void print_attribute(attribute_type attribute, FILE *fp_log)
{
	if (attribute.name!=NULL){
		fprintf(fp_log, "Attribute name: %s  ", attribute.name);
		fprintf(fp_log, "Attribute number colun: %d  ", attribute.number);
		fprintf(fp_log, "Attribute type number: %d  ", attribute.type_number);
	}	
	else 
		fprintf(fp_log, "There isn't attribute name\n");

	
}

void free_attribute(attribute_type attribute)
{
	free(attribute.name);
	attribute.type_number = 0;
	attribute.number = 0;
}

void malloc_attribute(attribute_type *attribute)
{
	attribute->name=malloc(MAX_ATTRIBUTENAME*sizeof(char));
	attribute->type_number = 0;
	attribute->number = 0;
}
int compare_attributes(attribute_type attribute1, attribute_type attribute2)
{	int aux;
	if ((attribute1.number != attribute2.number) || (!strcmp(attribute1.name,attribute2.name)) || (attribute1.type_number!= attribute2.type_number))
		aux = 0;
	
	else aux = 1;

	return aux;
}


