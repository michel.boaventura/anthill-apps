#ifndef _OBJECT_H
#define _OBJECT_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"

/* Prototypes */
void free_cell(list_pointer q);
void print_object(object_type object, int number_of_dimensions, FILE *fp_log);
int compare_objects(object_type object1, object_type object2);
void free_object(object_type object);
void malloc_object(object_type *object, int number_of_dimensions_values);
float distance_between_objects(object_type object1, object_type object2, int number_of_dimensions);

#endif

