#ifndef _STRUCTS_H_
#define _STRUCTS_H_


/* Definitions */

typedef struct attribute{
	char *name;
	int number;
	int type_number;	
		
}attribute_type;

typedef struct cell_attribute
{
   attribute_type item;
   struct cell_attribute *previous;
   struct cell_attribute *next;

} cell_attribute, *list_attribute_pointer;

typedef struct{
   list_attribute_pointer first;
   list_attribute_pointer last;
   int length;
}list_attribute_type;


typedef struct statistic{
	float *dimension_values;		// Dimension values  
	int number_of_objects_considered;	// Number of objects considered to calculate the new_dimension_values
} statistic_type;


/* Definitions about the structure of an object and the lists of object */
/* structure responsible for storing one object */

typedef struct others_attributes{
	int position;
	char *value_char;
}others_attributes_type;

typedef struct real_attributes{
	int position;
	float value_float;
}real_attributes_type;

typedef struct  object_prototype{	
	real_attributes_type *real_attributes;
	others_attributes_type *others_attributes;
}object_prototype_type;

/****************************************************************/

typedef struct
{
	int number;				// Object Number
	float *dimension_values;		// dimension values  

} object_type;

typedef struct cell
{
   object_type item;
   struct cell *previous;
   struct cell *next;
} cell, *list_pointer;

typedef struct
{
   list_pointer first;
   list_pointer last;
   int length;
} list_type;

/* Definitions about the structure of an message and the lists of messages */
/* structure responsible for storing one message */
typedef void* message_type;

typedef struct msg_cell
{
   message_type item;
   struct msg_cell *previous;
   struct msg_cell *next;
} msg_cell, *msg_list_pointer;

typedef struct
{
   msg_list_pointer first;
   msg_list_pointer last;
   int length;
} msg_list_type;


#endif
	
