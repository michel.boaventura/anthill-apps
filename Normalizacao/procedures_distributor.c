/****************************************************************************/
/*      This file stores the auxiliary functions of the filter Distributor  */
/****************************************************************************/

#include "procedures_distributor.h"

/* This function is responsible to opens the database and checks if any errors occured */
void open_file (FILE **input_file, char *input_file_name)
{

	char error_msg[100];				// auxiliary to store the message printed when a 
							// error occurs
	// opens the file and checks if any errors occured
	if ((*input_file = fopen (input_file_name, "r")) == NULL)
	{
		sprintf(error_msg,"ERROR. Could not open the file: %s !!!\n", input_file_name);
		ahxit (error_msg);
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to check if the number of attributes of the 
 * current object matches the number of attributes of the database */

int check_attributes (char *line, int number_of_attributes)
{
	char *temp_string; 				// auxiliary to store temporarily a string of the line
	char *temp_line;			// auxiliary to store temporarily the line
	int temp_num_attributes = 0;			// auxiliary to store the number of attributes

	int max_line_len=((MAX_VALUE_LEN+2)*number_of_attributes+1);
	temp_line = malloc(max_line_len*sizeof(char));
	char error_msg[100];
	
	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	while (temp_string != NULL)
	{
		temp_num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	// an empty line was found
	if (temp_num_attributes == 0)
	{
		return EMPTY_LINE;
	}
	else
	{
		// gives an error message if the number of attributes is different 
		// from the number of samples
		if ( (temp_num_attributes-1 != number_of_attributes))
		{	
			sprintf(error_msg,"ERROR. Value LINE=%d     Value Info=%d.\nReaded line:%s\n",temp_num_attributes,number_of_attributes,line);
			ahExit(error_msg);
			return 0;
		}
	}

	free(temp_line);
	return temp_num_attributes;
}


int check_attributes_conf (char *line, int number_info_attribute)
{
	char *temp_string; 				// auxiliary to store temporarily a string of the line
	char *temp_line;			// auxiliary to store temporarily the line
	int temp_num_attributes = 0;			// auxiliary to store the number of attributes

	int max_line_len=MAX_VALUE_LEN + (MAX_ATTRIBUTENAME*2)+2;
	temp_line = malloc(max_line_len*sizeof(char));
	char error_msg[100];
	
	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	while (temp_string != NULL)
	{
		temp_num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	// an empty line was found
	if (temp_num_attributes == 0)
	{
		return EMPTY_LINE;
	}
	else
	{
		// gives an error message if the number of attributes is different 
		// from the number of samples
		if ( (temp_num_attributes != number_info_attribute) )
		{	
			sprintf(error_msg,"ERROR. Value attributes real=%d     Value attribute Informaded=%d.\nReaded line:%s\n",temp_num_attributes,number_info_attribute,line);
			ahExit(error_msg);
			return 0;
		}
	}

	free(temp_line);
	return temp_num_attributes;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to read a line of a file and fill it on an object message
 * that will be sent to the NormalizationLocal filter.
 * It allocates space in memory for this msg */

int get_object_from_file(object_type *object, int number_of_dimensions, FILE *input_file)
{
	int check;
	char error_msg[100];				// auxiliary to store the message printed when a 
							// error occur
	char *file_line;
	int max_line_len=((MAX_VALUE_LEN+2)*number_of_dimensions+1);
	file_line=malloc(max_line_len*sizeof(char));
	
	/* read the file until gets a new line with an object or find the end of file */
	do
	{
		/* If was not possible to read the file, e.g end of the file, returns 0 */
		if (fgets(file_line, max_line_len-1, input_file)==NULL)
			return 0;
		check = check_attributes(file_line, number_of_dimensions);
	}
	while (check==EMPTY_LINE);
	if (!check || ((check-1) != number_of_dimensions))
	{	
		
		sprintf(error_msg,"ERROR. Inconsistent number of attributes. number find : %d \n", check);
		ahExit(error_msg);
	}

	malloc_object(object, number_of_dimensions);
	get_object_from_line(object, file_line, max_line_len);
	
	
	free(file_line);
	return 1;
}



/* This function is responsible to read a line of a file conf and fill it on an attribute message
 * that will be sent to the TransformationLocal filter.
 * It allocates space in memory for this msg */

int get_attribute_from_file(attribute_type *attribute,int *number_of_dimensions, FILE *input_file)
{
	int check;
	char error_msg[100];				// auxiliary to store the message printed when a 
							// error occur
	char *file_line;
	int max_line_len= MAX_VALUE_LEN + (MAX_ATTRIBUTENAME*2)+2;
	file_line=malloc( max_line_len*sizeof(char));
	
	/* read the file until gets a new line with an object or find the end of file */
	do
	{
		/* If was not possible to read the file, e.g end of the file, returns 0 */
		if (fgets(file_line, max_line_len-1, input_file)==NULL)
			return 0;
		check = check_attributes_conf(file_line, NUMBER_ATTRIBUTE_INFO);
	}
	while (check==EMPTY_LINE);
	if (!check)
	{	
		
		sprintf(error_msg,"ERROR. Inconsistent number of infos attributes confdatabase.\n");
		ahExit(error_msg);
	}

	malloc_attribute(attribute);
	get_attribute_from_line(attribute, file_line, max_line_len);
	
	
	free(file_line);
	return 1;
}

