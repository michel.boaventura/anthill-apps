#ifndef STANDARTIZATIONL_H
#define STANDARTIZATION_H
#include "dbtransformation.h"

int calculate_local_mean_value(list_type *object_list, statistic_type *local_mean, int number_of_dimensions);

int calculate_local_variance_value(list_type *object_list, statistic_type *local_variance, statistic_type *global_mean, int number_of_dimensions);

int normalize_database(list_type *object_list, statistic_type global_mean, statistic_type global_variance, int number_of_dimensions);


#endif
