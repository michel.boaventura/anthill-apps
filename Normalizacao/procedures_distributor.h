#ifndef PROCEDURES_DISTRIBUTOR_H
#define PROCEDURES_DISTRIBUTOR_H
#include "dbtransformation.h"

void open_file (FILE **input_file, char *input_file_name);

int check_attributes (char *line, int number_of_attributes);

int get_object_from_file (object_type *object, int number_of_dimensions, FILE *input_file);
int get_attribute_from_file(attribute_type *attribute_vector,int *number_of_dimensions, FILE *input_file);

#endif
