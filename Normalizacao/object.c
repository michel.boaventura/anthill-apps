/* Inclusoes. */
#include "object.h"

/* Procedure that free memory of a cell .
 * It not free the memory for the objects that the cell points */
void free_cell(list_pointer q)
{
/*   free_object(q->item);	
   free(q->item);*/
   free(q);
}

/* Print an object */
void print_object(object_type object, int number_of_dimensions, FILE *fp_log)
{
	fprintf(fp_log, "Object Number: %d Dimension Values: ", object.number);
	if (object.dimension_values!=NULL)
		print_float_array(object.dimension_values, number_of_dimensions, fp_log);
	else 
		fprintf(fp_log, "There aren't values\n");

}

int compare_objects(object_type object1, object_type object2)
{
	return (object1.number==object2.number);
}

void free_object(object_type object)
{
	free(object.dimension_values);
}

void malloc_object(object_type *object, int number_of_dimensions_values)
{
	(*object).dimension_values=malloc(number_of_dimensions_values*sizeof(float));
}

float distance_between_objects(object_type object1, object_type object2, int number_of_dimensions)
{
	return (distance_between_points(object1.dimension_values, object2.dimension_values, number_of_dimensions));
}
