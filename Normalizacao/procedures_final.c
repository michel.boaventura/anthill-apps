/****************************************************************************/
/*      This file stores the auxiliary functions of the filter Final        */
/****************************************************************************/

#include "procedures_final.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int calculate_global_mean_value (statistic_type *global_statistic, statistic_type *local_statistic, int number_transformationlocal_instances, int number_of_dimensions, int iterate)
{	
	int i;

	(*global_statistic).number_of_objects_considered += (*local_statistic).number_of_objects_considered;
	for (i=0 ; i<number_of_dimensions ; i++) 
		(*global_statistic).dimension_values[i] += ((*local_statistic).dimension_values[i]*(*local_statistic).number_of_objects_considered);
		
	
	if (iterate == number_transformationlocal_instances-1) {
		for (i=0 ; i<number_of_dimensions ; i++) 
			(*global_statistic).dimension_values[i] /=  (*global_statistic).number_of_objects_considered;
	}
	empty_statistic(local_statistic, number_of_dimensions);

	return 1;	
		
}

int calculate_global_variance_value (statistic_type *global_statistic, statistic_type *local_statistic, int number_of_dimensions)
{	
	int i;

	(*global_statistic).number_of_objects_considered += (*local_statistic).number_of_objects_considered;
	for (i=0 ; i<number_of_dimensions ; i++) 
		(*global_statistic).dimension_values[i] += (*local_statistic).dimension_values[i];
		
	empty_statistic(local_statistic, number_of_dimensions);

	return 1;	
		
}

