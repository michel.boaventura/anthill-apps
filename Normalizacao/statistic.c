/* Inclusoes. */
#include "statistic.h"

/* Print the value of a centroid */
void print_statistic(statistic_type *statistic, int number_of_dimensions, FILE *fp_log)
{
	fprintf(fp_log, "Dimension Values: ");
	if ((*statistic).dimension_values!=NULL)
	{
		print_float_array((*statistic).dimension_values, number_of_dimensions, fp_log);
	}
	else {
		fprintf(fp_log, "There aren't dimension values ");
	}
}


/* Frees the memory allocated for the structures of a centroid */ 
void free_statistic(statistic_type statistic)
{
	free(statistic.dimension_values);
}

/* Allocates the memory for the structures of a centroid */
void malloc_statistic(statistic_type *statistic, int number_of_dimensions_values)
{
	(*statistic).dimension_values=malloc(number_of_dimensions_values*sizeof(float));
	(*statistic).number_of_objects_considered=0;
}

void empty_statistic(statistic_type *statistic, int number_of_dimensions)

{	int i;
	for (i=0; i< number_of_dimensions; i++  )
		(*statistic).dimension_values[i]= 0;
	
	(*statistic).number_of_objects_considered=0;
}

void create_empty_statistic(statistic_type *statistic, int number_of_dimensions)
{
	malloc_statistic(statistic, number_of_dimensions);
	empty_statistic(statistic, number_of_dimensions);


}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


