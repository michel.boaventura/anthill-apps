#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>

#include "Limits.h"
#include "eventAPI.h"

class Filtro3: public AHFilter {

	public:

		streamInputHandler lineIn;
		constructFunctions(Filtro3);
		
		Filtro3() : AHFilter(1) {
			
/*			lineIn = getInputHandler("F3_in");
			
			 //Associate the function handler ''Classifica'' to the stream "lims_arq_in" 
			setHandler(lineIn, &Filtro3::ProcessData);*/
		}

		
		~Filtro3() {
			cout << "FIM FILTRO 3\n" << endl;
		
		}

		int ProcessData(AHData *data);

	private:

	map<set<int>, float> scores;
	map<set<int>, float> cache;
		
};

