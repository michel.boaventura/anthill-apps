#include "eventAPI.h"
#include "Similarity.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <set>
#include <sys/time.h>
#include <sys/resource.h>

class Filtro2: public AHFilter {

public:

	streamInputHandler candidatesInput;
	streamInputHandler groupsInput;

	constructFunctions(Filtro2);

	Filtro2() :	AHFilter(1) {

		candidatesInput = getInputHandler("f2_candidates");
		groupsInput = getInputHandler("f2_groups");
		GetParameter();
		
		setHandler(candidatesInput, &Filtro2::ReceiveCandidate);
		setHandler(groupsInput, &Filtro2::ReceiveGroup);
	}

	void GetParameter();
	double GetCPUTime();
	int ReceiveCandidate( AHData *data );
	int ReceiveGroup( AHData *data );
	void CreateDeletedCandidate(set< int > unionCandidates);
	void printDeleted(set< set< int > > deleted);
	void printSetInt (set< int > setInt);
	void printResult();

	~Filtro2() {
		printf("Time: %f\n", GetCPUTime());
		printResult();
		
  		FILE * GFile = fopen(groupsOut.c_str(), "w");
		
 		for(set < set< int> >::iterator it1=groups.begin();it1!=groups.end();it1++) {
 			for(set < int >::iterator it2=(*it1).begin();it2!=(*it1).end();it2++) {
				fprintf(GFile, "%d ", (*it2));
 			}
 			fprintf(GFile, "\n");
 		}
		
		
 		FILE * DFile = fopen(deletedOut.c_str(), "w");
 		for(set < set< int> >::iterator it3=deleted.begin();it3!=deleted.end();it3++) {
 			for(set < int >::iterator it4=(*it3).begin();it4!=(*it3).end();it4++) {
				fprintf(DFile, "%d ", (*it4));
			}
			fprintf(DFile, "\n");
		}
		
		fclose(GFile);
		fclose(DFile);
		fclose(MFile);
		//printDeleted(deleted);
		//fclose(timerFile);
	}

private:
	FILE * timerFile;
	FILE * inputPFile;
	
	FILE * MFile;
	FILE * GFile;
	FILE * DFile;
	
	string inputFile;
	string deletedOut;
	string groupsOut;
	string maximalOut;
	
	string nome_arquivo_timer;
	
	set< set< int > > deleted;
	set< set< int > > groups;
};
