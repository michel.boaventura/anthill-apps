#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <map>
#include <string>
#include <set>

using namespace std;

map<set<int>, float> scores;
map<set<int>, float> cache;

// Retorna a similaridade como sendo |S1 <interseção> S2| / |S1 <união> S2|
float similarity(const set<string>& s1, const set<string>& s2) {
	set<string> t1;
	for(set<string>::iterator i=s1.begin();i!=s1.end();i++) t1.insert(*i);
	for(set<string>::iterator i=s2.begin();i!=s2.end();i++) t1.insert(*i);

	set<string> t2;
	for(set<string>::iterator i=s1.begin();i!=s1.end();i++) {
		if(s2.find(*i)!=s2.end()) t2.insert(*i);
	}
	return(t2.size()/(float)t1.size());
}

float mean_similarity(const set<int>& s) {
	int k=0;
	float m=0;  
	for(set<int>::iterator i=s.begin();i!=s.end();i++) {
		for(set<int>::iterator j=i;j!=s.end();j++) {
			if(i!=j) {
				set<int> t;
				t.insert(*i);
				t.insert(*j);
				if(cache.find(t)==cache.end()) return(0);
				m+=cache[t];
				k++;
				t.clear();
			}
		}
	}
	return(m/(float)k);
}

// Retorna s1 <união> s2
set<int> check_set(const set<int>& s1, const set<int>& s2) {
	set<int> s;
	for(set<int>::iterator it=s1.begin();it!=s1.end();it++) s.insert(*it);
	for(set<int>::iterator it=s2.begin();it!=s2.end();it++) s.insert(*it);
	return(s);
}

int prefix(const set<int>& s1, const set<int>& s2) {
	unsigned int size=0;
	for(set<int>::iterator it=s1.begin(); it!=s1.end(); it++) {
		if(s2.find(*it)!=s2.end())
			size++;
	}
	return(size==s2.size()-1);
}

void print_set(const set<int>& s) {
	
	FILE * arq;
	arq = fopen("output/saidaserial", "w");
	
	for(set<int>::iterator it=s.begin();it!=s.end();it++) {
		printf("%d ", *it);
		fprintf(arq, "%d", *it);
	}
		printf("%f\n", scores[s]);
		fprintf(arq, "\n");
		
	fclose(arq);
}

int subset(const set<int>& a, const set<int>& b) {
	if(a.size()>=b.size()) return(0);
	for(set<int>::iterator i=a.begin();i!=a.end();i++) if(b.find(*i)==b.end()) return(0);
	return(1);
}

int main(int argc, char** argv) {
	set<set<int> > grupos, candidates;
	int max=atoi(argv[3]);		// tamanho maximo do cluster
	float mj=atof(argv[2]);		// limiar de similaridade
	map<int, set<string> > m;	// m = {[20->{20, palavra1, palavra2, ...}], [25->{25, palavra1, palavra2, ...}], ...}
	char l[20000];
	int i=0;
	FILE* f=fopen(argv[1], "r");
	fgets(l, 20000, f);

	// Lê todas as linhas do arquivo e monta a estrutura m
	while(!feof(f)) {
		i++;
		set<string> s;
		char* t=strtok(l, " ");
		i=atoi(t);
		s.insert(t);
		while((t=strtok(NULL, " "))!=NULL) s.insert(t);
		m[i]=s;
		fgets(l, 20000, f);
	}
	fclose(f);

	// Calcula a similaridade entre todos os itens de m, tomados 2 a 2. Aqueles com similaridade maior que o limiar imposto são inseridos na estrutura candidatos como pares de seus identificadores. Além disso armazena a similaridade nas estruturas cache e scores.
	for(map<int, set<string> >::iterator it1=m.begin();it1!=m.end();it1++) {
		for(map<int, set<string> >::iterator it2=it1;it2!=m.end();it2++) {
			if((*it1).first!=(*it2).first) {
				float k=similarity((*it1).second, (*it2).second);
				if(k>=mj) {
					set<int> t;
					t.insert((*it1).first);
					t.insert((*it2).first);
					candidates.insert(t);
					cache[t]=k;
					scores[t]=k;
					printf("%d\n", cache.size());
				}
			}
		}
	}




	set<set<int> > excluidos;
	int size=3;
	while(!(candidates.empty()) && size<=max) {
		printf("est: %d %d %d\n", size++, (int)candidates.size(), (int)grupos.size());
		fflush(stdout);
		for(set<set<int> >::iterator it=candidates.begin();it!=candidates.end();it++)
			grupos.insert(*it);
		set<set<int> > c;
		set<set<int> > processed;
		for(set<set<int> >::iterator it1=candidates.begin();it1!=candidates.end();it1++) 
		{
			for(set<set<int> >::iterator it2=it1;it2!=candidates.end();it2++) 
			{
				set<int> u=check_set(*it1, *it2);
				if(it1!=it2 && processed.find(u)==processed.end()) {
					if(!prefix(*it1, *it2))
						break;
					processed.insert(u);
					float mean=mean_similarity(u);
					if(mean>mj) {
						for(set<int>::iterator l=u.begin();l!=u.end();l++) {
							set<int> h;
							for(set<int>::iterator p=u.begin();p!=u.end();p++) {
								if(l!=p) h.insert(*p);
							}
							excluidos.insert(h);
						}
						//proximos candidatos da proxima iteraçao
						c.insert(u);
						scores[u]=mean;
					}
				}
			}
		}
		candidates.clear();
		for(set<set<int> >::iterator it1=c.begin();it1!=c.end();it1++) {
			candidates.insert(*it1);
		}
		c.clear();
		
	}
	

	printf("est: %d %d %d\n", size, (int)candidates.size(), (int)grupos.size());
	for(set<set<int> >::iterator i=grupos.begin();i!=grupos.end();i++) {
		//printf("grupo: ");
		//print_set(*i);
		if(excluidos.find(*i)==excluidos.end()) {
			printf("maximal: ");
			print_set(*i);
		}
	}
	return(0);
}
