#include "Filtro1.h"
#include "DataBuffer.h"

provide(Filtro1);

void Filtro1::GetParameter ( void ) {

	string parameter;

	parameter = getArgument ( "a" );
	if ( parameter.length () != 0 ) {
		char rank[5];
		sprintf ( rank, "%d", getMyRank () );
		inputFile = parameter;
	}

	parameter = getArgument ( "b" );
	if ( parameter.length () != 0 ) {
		mj = atof(parameter.c_str());
	}

	parameter = getArgument ( "c" );
	if ( parameter.length () != 0 ) {
		max = atoi(parameter.c_str());
	}

	parameter = getArgument ( "d" );
	if ( parameter.length () != 0 ) {
		numInstF1 = atoi(parameter.c_str());
	}
}

double Filtro1::GetCPUTime(void) {
	struct timeval tim;
	struct rusage ru;
	getrusage(RUSAGE_SELF, &ru);
	tim=ru.ru_utime;
	double t=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
	tim=ru.ru_stime;
	t+=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
	return t;
}

void Filtro1::GetTrainData( void ) {

	char l[20000];
	int i=0;

	fseek(inputPFile, 0, SEEK_SET); // seek back to beginning of file
	
	// Lê todas as linhas do arquivo e monta a estrutura m
	while(!feof(inputPFile)) {
		fgets(l, 20000, inputPFile);
		i++;
		set<string> s;
		char* t=strtok(l, " ");
		i=atoi(t);
		s.insert(t);
		while((t=strtok(NULL, " "))!=NULL) s.insert(t);
		m[i]=s;
		
	}
}

void Filtro1::GetInstanceData( void ) {
	char l[20000];
	int i=0; //indice
	int j=0; //percorre os limites do arquivo q devem ser lidos
	
	// Envia ponteiro para o inicio da linha que deve comecar a ler os testes
	fseek (inputPFile , vectorOfLimits[getMyRank()].begin, SEEK_SET );
	//cout << "GetInstanceByName: " << "b:" << vectorOfLimits[getMyRank()].begin << "e:" << vectorOfLimits[getMyRank()].end << endl;
	
	for ( j = vectorOfLimits[getMyRank()].begin; j < vectorOfLimits[getMyRank()].end; ) {
		fgets ( l, 20000, inputPFile );
		j += strlen ( l ) + 1;
		i++;
		set<string>s;
		char* t=strtok(l, " ");
		i=atoi(t);
		s.insert(t);
		while((t=strtok(NULL, " "))!=NULL) s.insert(t);
		instance_m[i]=s;
	}

}
// Calcula a similaridade entre todos os itens de m, com os de instance_m tomados 2 a 2. Aqueles com similaridade maior que o limiar imposto são inseridos na estrutura candidatos como pares de seus identificadores. Além disso armazena a similaridade nas estruturas cache e scores.
int Filtro1::GetSimilarity( /*AHData *data */) {
	
	for(map<int, set<string> >::iterator it1=m.begin(); it1!=m.end(); it1++) {
		for(map<int, set<string> >::iterator it2=it1; it2!=m.end(); it2++) {
			if((*it1).first!=(*it2).first) {

				float k=similarity((*it1).second, (*it2).second);
				set<int> t;
				t.insert((*it1).first);
				t.insert((*it2).first);
				cache[t]=k;
				scores[t]=k;
			
			}
		}
	}
	
 	for(map<int, set<string> >::iterator it1=instance_m.begin();it1!=instance_m.end();it1++) {
		for(map<int, set<string> >::iterator it2=m.find(it1->first); it2!=m.end(); it2++) {
			if((*it1).first!=(*it2).first) {
				set<int> t;
				t.insert((*it1).first);
				t.insert((*it2).first);
				
				if(cache.find(t)==cache.end()) {
					
					//assert(cache.find(t)!=cache.end());
					cerr << "GetSimilarity1(): ERROR: sentence similarity was not found in cache" << endl;
					exit(EXIT_FAILURE);
				}
				
				float k=cache[t];
				if(k >= mj) {
					cout <<  "Mt id: "<< getMyRank() <<" IT1: " << (*it1).first << " IT2: " << (*it2).first << " Similaridade: " << k << endl;	
					candidates.insert(t);
				}
			}
		}
	}
	
	map<int, set<string> >::iterator it = instance_m.end();
	--it;
	
	printf("ID: %d  InditialCand: %d Begin: %d End: %d\n", getMyRank(), (int)candidates.size(), instance_m.begin()->first, it->first);
	if (candidates.empty()) {
		return 0;
	}
	else {
		ExpandsCandidates(candidates); 
	}
	
	return 1;	
}

int Filtro1::ExpandsCandidates(set< set<int> > candidates) {

	int size=3;
	cout << "MAX:" << max << endl;
	
	while(!(candidates.empty()) && (size<=max)) {
		
		printf("ID: %d Size: %d Cand: %d Groups: %d\n", getMyRank(), size++, (int)candidates.size(), (int)grupos.size());
		
		
		fflush(stdout);
		for(set< set< int > >::iterator it=candidates.begin();it!=candidates.end();it++) {
			grupos.insert(*it);
			
		}

		set<set<int> > c;
		set<set<int> > processed;
		
		for(set<set<int> >::iterator it1=candidates.begin();it1!=candidates.end();it1++) 
		{
			for(set<set<int> >::iterator it2=it1;it2!=candidates.end();it2++) 
			{
				set<int> u=check_set(*it1, *it2);
		
				if(it1!=it2 && processed.find(u)==processed.end()) {
					if(!prefix(*it1, *it2)) { 
						break;
					}
					processed.insert(u);
		
					float mean=mean_similarity1(u);
					PrintSimilarity(u, mean, (mean>mj));
					if(mean>mj) {
						/*Envia uniao de candidatos*/
						CreateMsgUnion(u);
						//c será os candidatos da próxima iteração
						c.insert(u);
						scores[u]=mean;
 					}
				}
			}
		}
		candidates.clear();
		for(set<set<int> >::iterator it1=c.begin();it1!=c.end();it1++) {
			candidates.insert(*it1);
		}
		//printCandidates(candidates);
		/*Enviar candidatos paras as instancias maiores*/
		/*Receber das instancias menores novos candidatos e os adiciona em candidates*/
		c.clear();
	}
	
	return 1;
}

int Filtro1::PrintSimilarity(set< int > u,float mean, bool pass) {
	

	fprintf(arq, "MEAN: %f PASS: %d U: ", mean, pass);
	
	for(set < int >::iterator it=u.begin();it!=u.end();it++) {
		fprintf(arq, "%d ", (*it));
	}
	fprintf(arq, "\n");
	return 1;
}

int Filtro1::CreateMsgUnion(set< int> unionCandidates) {

	DataBuffer buf;
	buf.pack(unionCandidates);
	
	AHData * buffer = buf.toAHData(candidatesOutput);
	sendMsg (buffer);
	
	/*
	char aux[10];
	int msgSize=0;
	string str;
	AHData *data_to_send;
	
	//passa um valor determinando q a msg e um unia de candidatos
	sprintf(aux,"%d",UNION);
	str.append(aux);
	str.append("|");
	
	for(set< int >::iterator it=unionCandidates.begin();it!=unionCandidates.end();it++) {
		sprintf(aux,"%d",(*it));
		str.append(aux);
		str += ",";
	}
	str.erase(str.length()-1,1);
	msgSize = sizeof(int) + str.size()+1;
	
	char* buf = (char *) malloc ( str.size() + sizeof(int));
	memcpy (buf, &msgSize, sizeof(int));
	memcpy (buf + sizeof(int), str.c_str(), str.size()+1);
	
	data_to_send = new AHData ( buf, msgSize, candidatesOutput );
	sendMsg (data_to_send);*/

	return 1;
}

int Filtro1::CreateMsgGroups() {

	DataBuffer buf;
	buf.pack(grupos);
	
	AHData * buffer = buf.toAHData(groupsOutput);
	sendMsg (buffer);
	
// 	int msgSize=0;
// 	AHData *data_to_send;
// 	string str="";
// 	char aux[10];
// 	
// 	sprintf(aux,"%d",typeOfMessage);
// 	str+=aux;
// 	str+="|";
// 	
// 	for( set< set< int > >::iterator it1=grupos.begin();it1!=grupos.end();it1++) {
// 		for( set< int >::iterator it2=(*it1).begin();it2!=(*it1).end();it2++) {
// 			sprintf(aux,"%d",(*it2));
// 			str.append(aux);
// 			str += ",";
// 		}
// 		str.erase(str.length()-1,1);
// 		str += ";";
// 	}
// 	str.erase(str.length()-1,1);
// 	
// 	msgSize = sizeof(int) + str.size()+1;
// 	
// 	char* buf = (char *) malloc (msgSize);
// 	memcpy (buf, &msgSize, sizeof(int));
// 	memcpy (buf + sizeof(int), str.c_str(), str.size()+1);
// 	
// 	data_to_send = new AHData ( buf, msgSize, groupsOutput);
// 	sendMsg (data_to_send);
	
	return 1;
}

float Filtro1::mean_similarity1(const set<int>& s) {
	int k=0;
	float m=0;
	
	for(set<int>::iterator i=s.begin();i!=s.end();i++) {
		for(set<int>::iterator j=i;j!=s.end();j++) {
			if(i!=j) {
				set<int> t;
				t.insert(*i);
				t.insert(*j);
				if(cache.find(t)==cache.end()) {
		
					cout <<  "Mt id: "<< getMyRank() << " IT1: " << (*i) << " IT2: " << (*j) << endl;
					//assert(cache.find(t)!=cache.end());
					cerr << "GetSimilarity1(): ERROR: sentence similarity was not found in cache" << endl;
					exit(EXIT_FAILURE);
				}
				m+=cache[t];
				k++;
				t.clear();
			}
		}
	}
	return(m/(float)k);
}

void Filtro1::printSetInt ( set< int > setInt) {
	int count=0;
	
	for(set < int >::iterator it=setInt.begin();it!=setInt.end();it++) {
		cout << "Count : " << count << endl;
		cout << "Id: " << (*it) << endl;
		++count;
	}
	cout << "End PrintSet" << endl;
}

void Filtro1::printCache (map<set<int>, float> aux) {

	for(map<set< int >, float>::iterator it=cache.begin();it!=cache.end();it++) {
		cout << "MAP:" << endl;
		cout << " K "<< (*it).second << endl;
	}
}
void Filtro1::printCandidates ( set< set< int > > candidates) {
	int count=0;
	
	for(set < set< int> >::iterator it1=candidates.begin();it1!=candidates.end();it1++) {
		cout << "Candidates " << endl;
		cout << "GetMyRank: " << getMyRank() << endl;
		for(set < int >::iterator it2=(*it1).begin();it2!=(*it1).end();it2++) {
			cout << "Count" << count << " - ID: " << (*it2) << endl;
			++count;
		}
	}
}

void Filtro1::GetLimitsOfFile ( void ) {

	long tam_arq, com_parcial, inicio, fim;
	char c;
	int i;
	LimOfReading aux;
	
	fseek(inputPFile, 0, SEEK_END); // seek to end of file
	tam_arq = ftell(inputPFile); // get current file pointer
	fseek(inputPFile, 0, SEEK_SET); // seek back to beginning of file
	
	inicio = 0;
	for (i = 1; i < numInstF1; i++) {
		fseek(inputPFile, tam_arq / numInstF1, SEEK_CUR);
		//comeco parcial do proximo filtro = SEEK_CUR + "1/num_fil_lei bytes" (e' parcial porque pode dividir no meio de um termo)
		com_parcial = ftell(inputPFile);
		//loop que garante que o proximo comeco sera no inicio de um termo (e nao no meio do termo)
		fscanf(inputPFile, "%c", &c);
		while (c != '\n') {
			fscanf(inputPFile, "%c", &c);
		}
		//se o resto de arquivo que falta for menor do que "1/num_fil_lei bytes", entao o proximo filtro vai ate o fim do arquivo 
		if (ftell(inputPFile) >= tam_arq)
			break;
		fim = ftell(inputPFile);
		
		aux.begin = inicio;
		aux.end = fim;
		vectorOfLimits.push_back(aux);
		inicio = ftell(inputPFile); //inicio do proximo filtro = SEEK_CUR (ou seja, igual ao fim deste filtro)
	}

	fim = tam_arq;

	for (; i <= numInstF1; i++) {
		//se so' falta o ultimo 1/num_fil_lei para dividir (ou seja, so' falta inicio=ftell(arq) e fim = tam_arq)
		if (ftell(inputPFile) >= tam_arq) {
			aux.begin = inicio;
			aux.end = fim;
			vectorOfLimits.push_back(aux);
			inicio = -1;
		}
		else {
			aux.begin = inicio;
			aux.end = fim;
			vectorOfLimits.push_back(aux);
		}
	}
}