#ifndef DATABUFFER_H
#define DATABUFFER_H

#include "eventAPI.h"

class DataBuffer {

	private:
		int bufferSize;
		int dataSize;
		int consumedSize;
		
		bool bufferOurs;
		char * buffer;
	
	public:
		DataBuffer () {
			buffer = NULL;
			bufferOurs=true;
			
			dataSize=0;
			consumedSize=0;
			bufferSize=0;
			
		}
		
		DataBuffer (AHData * data) {
			
			buffer = (char *)data->getData();
			bufferOurs=false;
			
			bufferSize = data->getDataSize();
			dataSize = data->getDataSize();
			consumedSize=0;
			
		}
		
		
		~DataBuffer () {
			if(bufferOurs && (buffer!=NULL)) {
				free(buffer);
			}

		}
		
		int pack(int data);
		int pack(set< int > data );
		int pack(set< set< int > > data );
		int pack(int * data, int count);
		
		int unpack(int &data);
		int unpack(set< int > &data);
		int unpack(set< set< int > > &data);
		int unpack(int *&data, int &count);
		
		AHData * toAHData(streamOutputHandler out);
};

#endif