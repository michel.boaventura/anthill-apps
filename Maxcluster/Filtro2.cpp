#include "Filtro2.h"
#include "DataBuffer.h"

provide(Filtro2);

int Filtro2::ReceiveGroup( AHData *data ) {
	
	DataBuffer buf(data);
	buf.unpack(groups);
	
	return 1;
}

int Filtro2::ReceiveCandidate( AHData *data ) {

	set < int > unionCandidates;
	
	DataBuffer buf(data);
	buf.unpack(unionCandidates);
	
	CreateDeletedCandidate(unionCandidates);
	
// 	DataBuffer buf(data);
// 	
// 	int msg_size=0, aux=0, type=-1, stringSize=0;
// 	char * line;
// 	
// 	
// 	
// 	/*recebe o tamanho da mensagem*/
// 	memcpy ( &msg_size, inBuf, sizeof(int) );
// 	stringSize = msg_size - sizeof(int);
// 	
// 	/*recebe a msg*/
// 	line = (char *) malloc ( sizeof(char)*(stringSize));
// 	memcpy ( line, inBuf + sizeof(int), msg_size );
// 	
// 	//line[stringSize] ='\0';
// 	
// 	/*tokens recebe os ids */
// 	string str(line);
// 	tokens = Util::TokenizeString("|", str);
// 	
// 	/*verifica o tipo de msg*/
// 	type = atoi(tokens[0].c_str());
// 	if(type == UNION) {
// 		/*se for uma msg do tipo uniao de candidatos, o programa procura os candidatos para estrutura deleted*/
// 		tokens2 = Util::TokenizeString(",", tokens[1]);
// 		for( vector < string >::iterator it1=tokens2.begin();it1!=tokens2.end();it1++ ) {
//  			aux = atoi((*it1).c_str());
// 			unionCandidates.insert(aux);
//  		}
//  		
// 	}
// 	if(type == GROUP) {
// 		/*se for do tipo group, adiciona na variavel que recebe os grupos de candidatos a maxcluster*/
// 		
// 		vector < string > tokens3;
//  		tokens2 = Util::TokenizeString(";", tokens[1].c_str());
//  		
// 		/*separa grupos de inteiros, entre ';' estao os sets candidatos*/
// 		for(vector< string >::iterator it2=tokens2.begin(); it2!=tokens2.end(); it2++ ) {
// 			/*entre ',' estao os inteiros q sao ids dos documentos*/
// 			set< int > aux;
// 			tokens3 = Util::TokenizeString(",", (*it2).c_str());
// 			
// 			for(vector< string >::iterator it3=tokens3.begin(); it3!=tokens3.end(); it3++ ) {
// 				aux.insert(atoi((*it3).c_str()));
// 			}
// 			groups.insert(aux);
// 		}
// 	}
// 	
// 	line[0]='\0';
	return 1;
}

void Filtro2::CreateDeletedCandidate(set< int > unionCandidates) {

	for(set< int >::iterator l=unionCandidates.begin();l!=unionCandidates.end();l++) {
		set< int > h;
		for(set< int >::iterator p=unionCandidates.begin();p!=unionCandidates.end();p++) {
			if(l!=p) h.insert(*p);
		}
		deleted.insert(h);
	}
	/*imprimi o conjunto de candidatos a serem excluidos*/
	//printDeleted(deleted);
}

void Filtro2::printResult() {
	
	
 	for(set< set< int > >::iterator i=groups.begin();i!=groups.end();i++) {
		
		//printSetInt(*i);
 		if( deleted.find(*i)==deleted.end()) {
			cout << "Maximal: ";
			printSetInt(*i);
			cout << endl;
 		}
 	}
}

void Filtro2::printDeleted(set<set<int > > deleted) {
	int count=0;
	cout << "GetMyRank: " << getMyRank();
	cout << " Deleted: " << endl;
	for(set < set< int> >::iterator it1=deleted.begin();it1!=deleted.end();it1++) {
		for(set < int >::iterator it2=(*it1).begin();it2!=(*it1).end();it2++) {
			cout << (*it2) << " ";
			++count;
		}
		cout << " " << endl;
	}
}

void Filtro2::printSetInt ( set< int > setInt) {
	int count=0;

	FILE * MFile = fopen(maximalOut.c_str(), "a");
	for(set < int >::iterator it=setInt.begin();it!=setInt.end();it++) {
		cout << (*it) << " ";
		fprintf(MFile, "%d ", (*it));
		++count;
	}
	
	fprintf(MFile, "\n");
	fclose(MFile);
//	cout << "End PrintSet" << endl;
}

void Filtro2::GetParameter () {

	string parameter;

	parameter = getArgument ( "a" );
	if ( parameter.length () != 0 ) {
		char rank[5];
		sprintf ( rank, "%d", getMyRank () );
		inputFile = parameter;
	}

	parameter = getArgument ( "e" );
	if ( parameter.length () != 0 ) {
		groupsOut = parameter;
	}

	parameter = getArgument ( "f" );
	if ( parameter.length () != 0 ) {
		deletedOut = parameter;
	}
	
	parameter = getArgument ( "g" );
	if ( parameter.length () != 0 ) {
		maximalOut = parameter;
	}
	
	
	
}

double Filtro2::GetCPUTime(void) {
	struct timeval tim;
	struct rusage ru;
	getrusage(RUSAGE_SELF, &ru);
	tim=ru.ru_utime;
	double t=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
	tim=ru.ru_stime;
	t+=(double)tim.tv_sec + (double)tim.tv_usec / 1000000.0;
	return t;
}