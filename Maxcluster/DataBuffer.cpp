#include <set>
#include "DataBuffer.h"

int DataBuffer::pack(int data) {
	
	if(buffer==NULL) {
		buffer = (char *) malloc (sizeof(int));
		bufferSize = sizeof(int);
	}
	
	if(bufferSize < (dataSize + (int)sizeof(int))) {
		buffer = (char *)realloc(buffer, dataSize + sizeof(int));
		bufferSize = dataSize + sizeof(int);
	}
	
	int * p = (int *) &(buffer[dataSize]);
	*p = data;
	
	dataSize += sizeof(int);
	
	return 0;
}

int DataBuffer::pack(set< int > data ) {
	int serializedSetSize= sizeof(int)*(data.size()+1);
	
	if(buffer==NULL) {
		buffer = (char *) malloc (serializedSetSize);
		bufferSize = serializedSetSize;
	}
	
	if(bufferSize < (dataSize + serializedSetSize)) {
		buffer = (char *)realloc(buffer, dataSize + serializedSetSize);
		bufferSize = dataSize + serializedSetSize;
	}
	
	int * p = (int *) &(buffer[dataSize]);
	*p = data.size();
	dataSize += sizeof(int);
	
	for(set< int >::iterator it=data.begin(); it!=data.end(); it++) {
		pack(*it);
	}
	
	return 0;
}

int DataBuffer::pack(set< set< int > > data ) {
	int serializedSetSize = sizeof(int);
	
	for(set < set < int > >::iterator it=data.begin(); it!=data.end(); it++) {
		serializedSetSize += sizeof(int)*(it->size()+1);
	}
	
	if(buffer==NULL) {
		buffer = (char *) malloc (serializedSetSize);
		bufferSize = serializedSetSize;
	}
	
	if(bufferSize < (dataSize + serializedSetSize)) {
		buffer = (char *)realloc(buffer, dataSize + serializedSetSize);
		bufferSize = dataSize + serializedSetSize;
	}
	
	int * p = (int *) &(buffer[dataSize]);
	*p = data.size();
	dataSize += sizeof(int);
	
	
	for(set< set< int > >::iterator it=data.begin(); it!=data.end(); it++) {
		pack(*it);
	}
	return 0;
	
}

int DataBuffer::unpack(int &data) {
	
	if(buffer == NULL) {
		return -1;
	}
	
	if(dataSize < (consumedSize + (int)sizeof(int))) {
		return -2;
	}
	
	int * p = (int *) &(buffer[consumedSize]);
	data = *p;
	
	consumedSize += sizeof(int);
		
	return 0;
}


int DataBuffer::unpack(set< int > &data) {

	if(buffer == NULL) {
		return -1;
	}
	
	if(dataSize < (consumedSize + (int)sizeof(int))) {
		return -2;
	}

	int * p = (int *) &(buffer[consumedSize]);
	int setSize = *p;
	consumedSize += sizeof(int);
	
	if(dataSize < (consumedSize + (setSize * (int)sizeof(int)))) {
		return -3;
	}
	
	for(int i = 0; i < setSize; i++) {
		int setElement=0;
		unpack(setElement);
		data.insert(setElement);
	}
	
	return 0;
}

int DataBuffer::unpack(set< set< int > > &data) {
	
	if(buffer == NULL) {
		return -1;
	}
	
	if(dataSize < (consumedSize + (int)sizeof(int))) {
		return -2;
	}	

	int * p = (int *) &(buffer[consumedSize]);
	int numSets = *p;
	consumedSize += sizeof(int);

	for(int i = 0; i < numSets; i++) {
		set< int > newSet;
		unpack(newSet);
		data.insert(newSet);
	}	
	
	return 0;
}

AHData * DataBuffer::toAHData(streamOutputHandler out) {
	
	bufferOurs=false;
	return new AHData ( buffer, dataSize, out );
}