#ifndef _LIMITS_
#define _LIMITS_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define KB 1024
#define MB 1024*1024
#define GB 1024*1024*1024

#define NUM_FIL_CLA 1 	// numero de filtros classificadores

using namespace std;

#endif