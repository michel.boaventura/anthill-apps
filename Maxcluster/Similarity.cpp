#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <map>
#include <string>
#include <set>

using namespace std;

//map<set<int>, float> scores;
//map<set<int>, float> cache;

// Retorna a similaridade como sendo |S1 <interseção> S2| / |S1 <união> S2|
float similarity(const set<string>& s1, const set<string>& s2) {
	set<string> t1;
	for(set<string>::iterator i=s1.begin();i!=s1.end();i++) t1.insert(*i);
	for(set<string>::iterator i=s2.begin();i!=s2.end();i++) t1.insert(*i);

	set<string> t2;
	for(set<string>::iterator i=s1.begin();i!=s1.end();i++) {
		if(s2.find(*i)!=s2.end()) t2.insert(*i);
	}
	return(t2.size()/(float)t1.size());
}

float mean_similarity(const set<int>& s, map<set<int>, float> &cache) {
	int k=0;
	float m=0;
	
	for(set<int>::iterator i=s.begin();i!=s.end();i++) {
		for(set<int>::iterator j=i;j!=s.end();j++) {
			if(i!=j) {
				set<int> t;
				t.insert(*i);
				t.insert(*j);
				if(cache.find(t)==cache.end()) {
					return(0);
				}
				m+=cache[t];
				k++;
				t.clear();
			}
		}
	}
	return(m/(float)k);
}

// Retorna s1 <união> s2
set<int> check_set(const set<int>& s1, const set<int>& s2) {
	set<int> s;
	for(set<int>::iterator it=s1.begin();it!=s1.end();it++) s.insert(*it);
	for(set<int>::iterator it=s2.begin();it!=s2.end();it++) s.insert(*it);
	return(s);
}

bool prefix(const set<int>& s1, const set<int>& s2) {
	
	unsigned int position = 0;
	
	set< int >::iterator it1 = s1.begin();
	set< int >::iterator it2 = s2.begin();
	
	for(; position < (s1.size()-1); it1++, it2++, position++) {
		if((*it1)!=(*it2)) {
			return false;
		}
	}
	return true;
}

// int prefix(const set<int>& s1, const set<int>& s2) {
// 	unsigned int size=0;
// 	for(set<int>::iterator it=s1.begin(); it!=s1.end(); it++) {
// 		if(s2.find(*it)!=s2.end())
// 			size++;
// 	}
// 	return(size==s2.size()-1);
// }

void print_set(const set<int>& s, map<set<int>, float> &scores) {
	for(set<int>::iterator it=s.begin();it!=s.end();it++) printf("%d ", *it);
	printf("%f\n", scores[s]);
}

int subset(const set<int>& a, const set<int>& b) {
	if(a.size()>=b.size()) return(0);
	for(set<int>::iterator i=a.begin();i!=a.end();i++) if(b.find(*i)==b.end()) return(0);
	return(1);
}
