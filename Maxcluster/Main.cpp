#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include "anthill.h"

using namespace std;

typedef int Work;

int main (int argc, char *argv[]) {
   
	Work work;
	char confFile[11] = "./conf.xml";

	pvm_catchout(stdout);
	pvm_catchout(stderr);	

	Layout *layout = initAh(confFile, argc, argv);
	appendWork(layout, (void *)&work, sizeof(Work));

	finalizeAh(layout);

   return 0;
}

