#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <vector>
#include <map>
#include <string>
#include <set>
#include "Similarity.h"
#include "eventAPI.h"

// Estrutura que define os limites do arquivo (ou seja onde ele comecara' a ser lido e onde ele ira' parar de ser lido)
typedef struct LimOfReading {
	long begin; //dado em bytes para uso da funcao fseek em relacao ao inicio do arquivo
	long end; //dado em bytes para uso da funcao fseek em relacao ao inicio do arquivo
} LimOfReading;

class Filtro1: public AHFilter {

	public:

		streamOutputHandler candidatesOutput;
		streamOutputHandler groupsOutput;
		
		Filtro1() : AHFilter(1) {
			
			numInstF1 = 1;
			mj = 0;
			max = 0;
			candidatesOutput = getOutputHandler("f1_candidates"); // get the output stream handler
			groupsOutput = getOutputHandler("f1_groups"); // get the output stream handler
			
			GetParameter();
			
			arq = fopen("saidaTeste4", "a");
			inputPFile = fopen(inputFile.c_str(), "r");
			if (inputPFile == NULL) {
				printf("File open error\n");
				exit(1);
			}
				
			GetLimitsOfFile();
			GetTrainData();
			GetInstanceData();
			GetSimilarity();
			CreateMsgGroups();
		}
		
		double GetCPUTime( void );
		void GetParameter ( void );
		void GetLimitsOfFile ( void );
		void GetTrainData( void );
		void GetInstanceData( void );
		int GetSimilarity( void );
		string CreateMsg(set< set<int> > candidates, int typeOfMessage);
		int CreateMsgUnion(set< int> unionCandidates);
		int ExpandsCandidates(set< set<int> > candidates);
		void printCandidates ( set< set< int > > candidates);
		void printCache (map<set<int>, float> aux);
		float mean_similarity1(const set<int>& s);
		int CreateMsgGroups();
		void printSetInt ( set< int > setInt);
		int PrintSimilarity(set< int > u,float mean, bool pass);
		
		
		
		~Filtro1() {
			fclose(inputPFile);
			
/*			for(int i = 0; i < vectorOfLimits.size(); i++) {
				cout << "i: " << i << " | " << vectorOfLimits[i].begin << " -> " << vectorOfLimits[i].end << endl; 
			}*/
//			for (map<int, set<string> >::iterator it1=m.begin();it1!=m.end();++it1) {
// 				for (set<string>::iterator it2=it1->second.begin();it2!=it1->second.end();++it2) {
//					cout << "Word by M: " << *it2 << endl;
// 				}
// 			}
			
//  		for (map<int, set<string> >::iterator it1=instance_m.begin();it1!=instance_m.end();++it1) {
//  			for (set<string>::iterator it2=it1->second.begin();it2!=it1->second.end();++it2) {
//  				cout << getMyRank() << " Word by INSTANCE_M: " << *it2 << endl;
//  			}
//  		}
// 			printf("End of F1. Time spent: %f\n", GetCPUTime());

			fclose(arq);
		} //End Destructor

	private:
		
		int numInstF1;
		float mj; //minimo jaccard
		int max;
		
		FILE * inputPFile;
		FILE * timerFile;
		FILE * arq;

		map<int, set<string> > m;	// m = {[20->{20, palavra1, palavra2, ...}], [25->{25, palavra1, palavra2, ...}], ...}
		map<int, set<string> > instance_m;
		
		map<set<int>, float> scores;
		map<set<int>, float> cache;
		
		string nome_arquivo_timer;
		string teste_saidas;
		string inputFile;

		set<set<int> > grupos, candidates;
		vector <LimOfReading> vectorOfLimits;

};