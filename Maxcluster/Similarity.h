#ifndef _SIMILARITY_
#define _SIMILARITY_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <map>
#include <string>
#include <set>


#define CANDIDATOS 0
#define EXCLUIDOS 0


using namespace std;

float similarity(const set<string>& s1, const set<string>& s2);
float mean_similarity(const set<int>& s, map<set<int>, float> &cache);
set<int> check_set(const set<int>& s1, const set<int>& s2);
bool prefix(const set<int>& s1, const set<int>& s2);
void print_set(const set<int>& s);
int subset(const set<int>& a, const set<int>& b);

#endif
