/**
 * \file common/util.h
 * \author Rodrigo Silva Oliveira
 * \author Thatyene Louise Alves de Souza Ramos
 */

#ifndef CAHPETA_COMMON_UTIL_H_
#define CAHPETA_COMMON_UTIL_H_

/* C libraries */

/* C++ libraries */
#include <string>
#include <vector>

/* Other libraries */

/* Project's .h */

using namespace std;

/**
 * \class Util
 * \brief Implementation of a utility class.
 * \author Rodrigo Silva Oliveira
 * \author Thatyene Louise Alves de Souza Ramos
 * \version 1.0
 * \date 2011
 */
class Util {

	public:

		/**
		 * \brief Default constructor.
		 * \return Not applicable.
		 */
		Util ( void );

		/**
		 * \brief Destructor.
		 * \return Not applicable.
		 */
		virtual ~Util ();

		/**
		 * \brief Transforms a string in a vector of tokens according to delimiters.
		 * \param delimiters Delimiters used to split the string.
		 * \param word Word to be split.
		 * \return A vector containing the tokens.
		 */
		static vector < string > TokenizeString ( const string &delimiters, const string &word );

	protected:

	private:
};

#endif /* CAHPETA_COMMON_UTIL_H_ */
