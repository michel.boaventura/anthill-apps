/**
 * \file common/util.cc
 * \author Rodrigo Silva Oliveira
 * \author Thatyene Louise Alves de Souza Ramos
 */

#include "util.h"

vector<string> Util::TokenizeString(const string &delimiters, const string &word) {

	vector<string> tokens;

	// Skip delimiters at beginning.
	string::size_type lastPos = word.find_first_not_of(delimiters, 0);

	// Find first "non-delimiter".
	string::size_type pos = word.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(word.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = word.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = word.find_first_of(delimiters, lastPos);
	}
	return tokens;
}
