#include <stdlib.h>
#include "memWrapper.h"

void *mymalloc(int size) {
	char *ptr=(char*) malloc(size+sizeof(TYPE));
	TYPE *bytes=(TYPE*)ptr;
	*bytes=size;
	return (void*) ((char*)ptr+sizeof(TYPE));
}

void *myrealloc(void *ptr, int size) {
	ptr=((char*)ptr-sizeof(TYPE));
	ptr=(char*) realloc(ptr, size+sizeof(TYPE));
	TYPE *bytes=(TYPE*)ptr;
	*bytes=size;
	return (void*) ((char*)ptr+sizeof(TYPE));
}

void myfree(void* ptr) {
	free((char*)ptr-sizeof(TYPE));
}

TYPE getAllocatedMem(void* ptr) {
	TYPE *bytes=(TYPE*)((char*)ptr-sizeof(TYPE));
	return *bytes;
}

void verifyMemory(long int total_allocated,char *filter,int my_rank,int total_instances){
	if(total_allocated>=MAX_MEM) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,END_OF_MEMORY,time_s,filter,my_rank,total_instances);
		ahExit(fatalMessage);
	}
}
