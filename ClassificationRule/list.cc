/*list.cc*/

/*Inclusions*/
#include "list.h"

void createList(list *L){
	L->n_nodos = 0;
	L->current = NULL;
	L->header = NULL;
	L->MFreq  = NULL;
}

void deleteList(list *L){
	pointer auxilary;
	if(L != NULL){
		initializeList(L);
		while(L->current != NULL){
			walkList(L);
			free(auxilary);
			L->n_nodos--;
		}
	}
}

void fillNodo(nodo* n,int* item,int size,float conf,int freq){
	n->conf = conf;
	n->freq = freq;
	n->size = size - 1;
	n->Class = item[n->size];
	n->item = (int*) malloc(sizeof(int)*size);
	for(int i=0;i<n->size;i++)
		n->item[i] = item[i];
}

void getMFreqRule(list *L){
	L->current = L->MFreq;
}

void initializeList(list *L){
	L->current = L->header;
}

void insertList(list *L,int* item,int size,float conf,int freq){
	pointer current;
	pointer aux;
	nodo *n = (nodo*) malloc(sizeof(nodo));
	
	if(L == NULL){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,LIST_ERROR,time_s,"SELECTOR");
		ahExit(fatalMessage);
		exit(1);
	}
	
	//Primeiro elemento a ser inserido na lista
	if(L->n_nodos == 0){
		fillNodo(n,item,size,conf,freq);
		L->header =  n;
		L->current = n;
		n->next = NULL;
		L->MFreq = n;
	}
	else{
		current = L->header;
		aux = L->header;
		//caminha at� achar um item de tamanho menor ou igual
		while((current != NULL) && (current->size > (size-1))){
			aux = current;
			current = current->next;
		}
		fillNodo(n,item,size,conf,freq);

		if((current != NULL) && (current->size == (size-1))){
			//caminha at� achar algum item com confian�a menor ou igual
			while((current != NULL) && (current->conf > conf)){
				aux = current;
				current = current->next;
			}	
		}
		
		//atualiza a regra mais frequente
		if(freq > L->MFreq->freq)
			L->MFreq = n;
		
		//o item � inserido como o �ltimo da lista
		if(current == NULL){
			aux->next = n;
			n->next = NULL;
			L->n_nodos++;
			return;
		}
		
		//o item � inserido como primeiro da lista
		if(current == L->header){
			n->next = L->header;
			L->header = n;
			L->n_nodos++;
			return;
		}
		
		aux->next = n;
		n->next = current;
	}
	L->n_nodos++;
	return;
}

void walkList(list *L){
	if((L != NULL) && (L->current != NULL))
		L->current = L->current->next;
}
