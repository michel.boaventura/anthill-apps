/*config.h*/

/*Inclusoes*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include "FilterDev.h"
#include "getTime.h"

/*Definições*/
#ifndef _CONFIG_H
#define _CONFIG_H

using namespace std;

#define CONC_JOBS 4

#define ITEM_SIZE 450
#define LINE_SIZE 4500
#define MSG_SIZE 750
#define FILE_BLOCK 10000
#define T_MSG_SIZE MSG_SIZE*2
#define SIZE_FILE 3000

#define EMPTY_SET -1
#define MAX_ITEMSETS 10000000
#define BLOCK_ITEMSETS 100000
#define EOI MAX_ITEMSETS+1000
#define FREE EOI+1
#define MSG EOI+2

#define KB 1024
#define MB 1024*KB
#define GB 1024*MB

#define MAX_MEM 900*MB

#define MIN_ARGS 6

typedef struct {
	int anticipating;
	double min_supp;
	double min_conf;
	char out_file[SIZE_FILE];
	char in_file[SIZE_FILE];
	char test_file[SIZE_FILE];
} Work;

typedef struct {
   int n_trans_local;
} reader_msg;

typedef struct {
	int type;
	int filter_id;
	char layout[T_MSG_SIZE];
} adder_msg;

typedef struct {
	int type;
	int filter_id;
	char id[T_MSG_SIZE];
	int sum;
	int mod;
	int size;
	int local_count;
} merger_msg;

typedef struct {
	int size;
	int local_count;
	char layout[T_MSG_SIZE];
} output_msg;

typedef struct {
	int index;
	int size;
	char item[ITEM_SIZE];
} item_msg;

typedef struct {
	int max_item;
	int id;
	int n_transaction;
} max_item_msg;

typedef struct {
	int id;
	int n_trans;
	int size;
	int transaction[MSG_SIZE];
} data_block_t;

typedef struct {
	int item[MSG_SIZE];
	float conf;
	int size;
	int freq;
} rule_msg;

typedef struct{
	int a_class;
	int Class;
	int hit;
	int id;
	int filter_id;
	int n_tuples;
} class_msg;

#endif
