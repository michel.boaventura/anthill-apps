/*Aggregator.h*/

/*Inclusoes*/
#include <map>
#include <set>
#include <ext/hash_map>

#include "memWrapper.h"
#include "config.h"

/*Definições*/
#define INF 0
#define INIT 1

#define ALOC_ERROR "%s;FATAL;%s;instance %d; Error allocating memory in function copyInformations."
#define ID_ERRO "%s;FATAL;%s;instance %d;Id: %d ,passed to function getItemset, don't exist."
#define INST_ERROR "%s;FATAL;%s;instance %d; The number of Aggregator's instances must be one."
#define NUM_CLASS "%s;FATAL;%s;instance %d;Number of classes in the input file msut be at least one!"
#define WRG_MIN_CONF "%s;WARN;%s;instance %d; Confidence is so much low. The minimum default confidence, defined as 1%%, will be used !"
#define WRG_MAX_CONF "%s;WARN;%s;instance %d; Confidence is so much high. The maximum default confidence, defined as 100%%, will be used !"
using namespace __gnu_cxx;


typedef struct {
	int size;
	int* layout;
	int count;
} itemset_agg;

typedef struct{
	itemset_agg *its;
	int size;
} aggSet;

typedef struct{
	int Class;
	int attempt;
}tuple;

typedef struct{
	tuple* its;
	int size;
}table;

typedef struct{
	int occur;
	int attempt;
	int hit;
}ClassType;

typedef struct{
	ClassType* its;
	int size;
}ClassTable;

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
char out_file[SIZE_FILE];
double min_conf;
int my_rank, n_instances,n_selector_instances;
long int total_allocated;
hash_map<const char*, int, hash<const char*>, eqstr> item_map_id;

/*Prototipos*/
extern "C"{
int initFilter(void* work, int size);
int processFilter(void* work, int size);
int finalizeFilter(void);
}

int attribId();
void calculateMetrics(float* precision,float* recall,float* F1,ClassTable classes,table tuples, int id);
void copyInformations(aggSet itemSet,int n_freq_itemsets,output_msg* in_msg);
int does_A_contains_K(itemset_agg a, int k);
void generateRules(int n_freq_itemsets,aggSet itemSet,int n_transactions,map <set<int>, double> freq_map);
char *getItemset(int id);
void initializeArrays(float* precision,float* recall,float* F1,int size);
void initializeClasses(ClassTable* classes,int n_classes);
int is_A_superset_OF_B(itemset_agg a, itemset_agg b);
int isValidRule(itemset_agg a, itemset_agg b);
void printClasses(float* precision,float* recall,float* F1,ClassTable classes,int id,table tuples,FILE** file);
void sendRule(rule_msg* r_msg, itemset_agg a, itemset_agg b,int n_transactions,map <set<int>, double> freq_map);
void receiveClasses(ClassTable classes);
void reallocAggSet(aggSet *itemSet, int size);
void verifyIndex(table* tuples,int index);
void verifyMinConf(double min_conf);
void verifyNumInstances();
