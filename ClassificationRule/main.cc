/*main.cc*/

/*Inclusoes*/
#include <getopt.h>

#include "Manager.h"
#include "config.h"

using namespace std;

//funcao que imprime manual de utilizacao na tela
void usage() {
	printf("\nSample App for Main\n");
	printf("Options: \n");
	printf("         -h                  (prints this help)\n");
	printf("         -t                  (Anticipation trigger: 1(enabled),0(disabled))\n");
	printf("         -s                  (Minimum User support - a number between 0 and 100)\n");
	printf("         -c                  (Minimum user confidence - a number between 0 and 100)\n");
	printf("         -i                  (Training file)\n");
	printf("         -e                  (Test file)\n");
	printf("         -o                  (Output file)\n\n");
}

//funcao para obten��o dos parametros passados pelo usuario
void parseArgs(int argc, char *argv[], Work *work){
	int c;
	int num_args = 0;
	while((c = getopt(argc,argv,"s:i:o:t:c:e:"))!=-1) {
		switch(c) {
			case 'i':
				strncpy(work->in_file,strdup(optarg),SIZE_FILE);
				if (work->in_file == NULL) {
					fprintf(stderr, "CrazyMiner: main.cc: ERROR: you must give the input file\n");
					usage();
					exit(1);
				}
			break;
			case 'o': 
				strncpy(work->out_file,strdup(optarg),SIZE_FILE);
				if (work->out_file == NULL) {
					fprintf(stderr, "CrazyMiner: main.cc: ERROR: you must give the output file\n");
					usage();
					exit(1);
				}
			break;
			case 'e':
				strncpy(work->test_file,strdup(optarg),SIZE_FILE);
				if (work->test_file == NULL) {
					fprintf(stderr, "CrazyMiner: main.cc: ERROR: you must give the test file\n");
					usage();
					exit(1);
				}
			break;
			case 's': work->min_supp = atof(optarg);
			break;
			case 'c': work->min_conf = atof(optarg);
			break;
			case 't': work->anticipating = atoi(optarg);
			break;
			default:
				usage();
				exit(1);
		}
		num_args++;
	}
	
	//verifica se todos os parametros essenciais para a correta execu��o do algoritmo foram passadas
	if(num_args < MIN_ARGS){
		usage();
		exit(1);
	}
}

int main (int argc, char **argv) {
	//declara��o de variaveis
	char filename[5];
	float total_time = 0;
	char *confFile = "./conf.xml";
	timeval total_start, total_stop;
	Work *work = (Work *)calloc(1, sizeof(Work));

	//pega os argumentos passados pelo usuario
	parseArgs(argc,argv,work);
	
	Layout *layout = initAh(confFile, argc, argv);
	gettimeofday(&total_start, NULL);
	appendWork(layout, (void *)work, sizeof(Work));
	gettimeofday(&total_stop, NULL);
	finalizeAh(layout);
	
	//imprime tempo de execu��o em arquivo de saida default
	total_time = calculateInterval(total_start,total_stop);
	
	sprintf(filename, "TEMPO");
	FILE *ff = fopen(filename, "w");
	fprintf(ff, "Tempo total de execucao = %f\n", total_time); fflush(ff);
	fclose(ff);
	
	return 0;
}
