/*Aggregator.cc*/

/*Inclusoes*/
#include "Aggregator.h"

InputPortHandler in_port_reader_agg_NTRANS;
InputPortHandler in_port_reader_aggregator;
InputPortHandler in_port_adder_aggregator;
InputPortHandler in_port_selector_aggregator;
InputPortHandler in_port_selec_agg;
OutputPortHandler out_port_aggregator_reader;
OutputPortHandler out_port_aggregator_selector;
OutputPortHandler out_port_aggregator_selector_r;

int initFilter(void* work, int size) {
	my_rank = ahGetMyRank();
	n_instances = ahGetTotalInstances();

	//verifica se o n�mero de inst�ncias � um
	verifyNumInstances();
	
	//pega paramametros do usuario
	Work args = (*(Work *)work);
	strcpy(out_file,strdup(args.out_file));
	min_conf = args.min_conf/(double)100;

	//configura portas de comunicacao
	in_port_reader_agg_NTRANS = ahGetInputPortByName("in_reader_agg_NTRANS");
	in_port_reader_aggregator = ahGetInputPortByName("in_reader_aggregator");
	in_port_adder_aggregator  = ahGetInputPortByName("in_adder_aggregator");
	in_port_selector_aggregator  = ahGetInputPortByName("in_selector_aggregator");
	in_port_selec_agg = ahGetInputPortByName("in_selec_agg");
	out_port_aggregator_reader = ahGetOutputPortByName("out_aggregator_reader");
	out_port_aggregator_selector = ahGetOutputPortByName("out_aggregator_selector");
	out_port_aggregator_selector_r = ahGetOutputPortByName("out_aggregator_selector_r");

	n_selector_instances = ahGetNumWriters(in_port_selector_aggregator);
	
	//inicializa��o de variavel global
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declara��o de variaveis
	aggSet itemSet;
	ClassTable classes;
	int n_classes;
	int n_transactions = 0;
	int n_freq_itemsets = 0;
	float agg_time = 0;
	timeval start_time, stop_time;
	map <set<int>, double> freq_map;
		
	itemSet.its = (itemset_agg*) mymalloc(sizeof(itemset_agg)*BLOCK_ITEMSETS);
	total_allocated+=getAllocatedMem(itemSet.its);
	itemSet.size = BLOCK_ITEMSETS;
	reader_msg* r_msg = (reader_msg*) mymalloc(sizeof(reader_msg));
	total_allocated+=getAllocatedMem(r_msg);
	output_msg* in_msg = (output_msg*) mymalloc(sizeof(output_msg));
	total_allocated+=getAllocatedMem(in_msg);
		
	//criacao de arquivo para gera��o de logs
	char out_file[100];
	sprintf(out_file, "AGGREGATOR");
	FILE* ff = fopen(out_file, "w");
	
	//verifica se o min_conf possue valor coerente
	verifyMinConf(min_conf);
	
	//fun��o responsavel por receber os itemsets do filtro leitor, dar um id unico e global para cada
	//itemset recebido e informar ao filtro leitor os ids atribuidos a cada itemset
	n_classes = attribId();
	initializeClasses(&classes,n_classes);
	
	//recebe o numero total de transa��es lidas por todas as instancias de reader
	while (ahReadBuffer(in_port_reader_agg_NTRANS, r_msg, sizeof(reader_msg)) != EOW)
		n_transactions+=r_msg->n_trans_local;
	total_allocated-=getAllocatedMem(r_msg);
	myfree(r_msg);
	
	//copia as informa��es contidas na mensagem para um armazenamento temporario
	while (ahReadBuffer(in_port_adder_aggregator, in_msg, sizeof(output_msg)) != EOW){
			reallocAggSet(&itemSet,n_freq_itemsets);
			copyInformations(itemSet,n_freq_itemsets,in_msg);
			n_freq_itemsets++;
	}
	total_allocated-=getAllocatedMem(in_msg);
	myfree(in_msg);

	//calcula o suporte global de cada itemset frequente
	for(int i=0;i<n_freq_itemsets;i++) {
		set<int> key;
		for(int j=0;j<itemSet.its[i].size;j++) 
			key.insert(itemSet.its[i].layout[j]);
		freq_map[key] = (itemSet.its[i].count)/(double)(n_transactions);
		key.clear();
	}

	//gera e envia as regras de associa��o ,dos itemsets frequentes encontrados, para o Selector
	generateRules(n_freq_itemsets,itemSet,n_transactions,freq_map);

	//recebe e imprime no arquivo de sa�da as classes geradas pelo Selector
	receiveClasses(classes);

	//calcula tempo final de execu��o 
	gettimeofday(&stop_time, NULL);
	agg_time = calculateInterval(start_time,stop_time);
	fprintf(ff, "Tempo de execucao do filtro aggregator = %f\n", agg_time);
	fflush(ff);
	fclose(ff);
	
	total_allocated-=getAllocatedMem(itemSet.its);
	myfree(itemSet.its);
	
	return 1;
}

int finalizeFilter(void){
	//releaseInputPort(in_port_reader_aggregator);
	//releaseInputPort(in_port_merger_aggregator);
	return 1;
}

//metodoo responsavel por receber os itemsets do filtro leitor, dar um id unico e global para cada
int attribId(){
	char *item;
	int n_itemsets = 1;
	int n_classes = -1;
	item_msg* items = (item_msg*) mymalloc(sizeof(item_msg));
	total_allocated+=getAllocatedMem(items);
	
	//cria ids unicos e globais para cada itemset enviado pelo filtro leitor
	while (ahReadBuffer(in_port_reader_aggregator, items, sizeof(item_msg)) != EOW){
		item = (char*) mymalloc(sizeof(char)*(items->size+1));
		total_allocated+=getAllocatedMem(item);
		
		strcpy(item,items->item);
		if(!item_map_id[item]){
			if(!(strncmp("CLASS",item,5))){
				item_map_id[item] = n_classes;
				n_classes--;
			}
			else{
				item_map_id[item] = n_itemsets;
				n_itemsets++;
			}
		}
	}
	
	//envia para todas as instancias do filtro leitor e seletor os ids atribuidos a cada itemset
	hash_map<const char*, int, hash<const char*>, eqstr>::iterator i;
	for(i=item_map_id.begin();i!=item_map_id.end();i++) {
		items->index = i->second;
		items->size = strlen(i->first);
		strcpy(items->item,i->first);
		ahWriteBuffer(out_port_aggregator_reader,items, sizeof(item_msg));
		ahWriteBuffer(out_port_aggregator_selector,items, sizeof(item_msg));
	}
	
	//verifica se h� pelo menos uma classe
	if(n_classes > -2){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,NUM_CLASS,time_s,"AGGREGATOR",my_rank);
		ahExit(fatalMessage);
	}
	
	//encerra comunica��o com o Reader e com o Selector
	ahCloseOutputPort(out_port_aggregator_reader);
	ahCloseOutputPort(out_port_aggregator_selector);
	
	total_allocated-=getAllocatedMem(items);
	myfree(items);
	
	return abs(n_classes+1);
}

//metodo responsavel pelo calculo das metricas de qualidade de classifica��o
void calculateMetrics(float* precision,float* recall,float* F1,ClassTable classes,table tuples, int id){
	int index;

	//precisao = numero correto para classe/numero de tentativas para classse
	index = abs(tuples.its[id].attempt) - 1;
	precision[index] = (float) classes.its[index].hit/(float) classes.its[index].attempt;
	if((precision[index] + recall[index]) != 0 )
		F1[index] = (float) 2.0*precision[index]*recall[index]/(float) (precision[index] + recall[index]);
	else
		F1[index] = 0;
	
	//verifica se � uma classe conhecida
	if(tuples.its[id].Class != 0){
		//recall = correto para classe/numero que realmente s�o da classe
		index = abs(tuples.its[id].Class) - 1;
		recall[index] = (float) classes.its[index].hit/(float) classes.its[index].occur;
		if((precision[index] + recall[index]) != 0 )
			F1[index] = (float) 2.0*precision[index]*recall[index]/(float) (precision[index] + recall[index]);
		else
			F1[index] = 0;
	}

}

//copia as informa��es contidas na mensagem para um local de armazenamento temporario
void copyInformations(aggSet itemSet,int n_freq_itemsets,output_msg* in_msg){	
	itemSet.its[n_freq_itemsets].count = in_msg->local_count;
	itemSet.its[n_freq_itemsets].size = 0;
	itemSet.its[n_freq_itemsets].layout = (int*) mymalloc(sizeof(int)*in_msg->size);

	if(itemSet.its[n_freq_itemsets].layout == NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,ALOC_ERROR,time_s,"AGGREGATOR",my_rank);
		ahExit(fatalMessage);
	}
	
	char* item = strtok(in_msg->layout, " ");
	while(item!=NULL) {
		itemSet.its[n_freq_itemsets].layout[itemSet.its[n_freq_itemsets].size++] = atoi(item);
		item = strtok(NULL, " ");
	}
}

int does_A_contains_K(itemset_agg a, int k) {
	for(int i=0;i<a.size;i++) if(a.layout[i]==k) return 1;
	return(0);
}

//gera e imprime as regras de associa��o,para os itemsets frequentes encontrados,no arquivo de saida
void generateRules(int n_freq_itemsets,aggSet itemSet,int n_transactions,map <set<int>, double> freq_map){
	int i,j;
	rule_msg* r_msg = (rule_msg*) mymalloc(sizeof(rule_msg));
	
	for(i=0;i<n_freq_itemsets;i++) {
		for(j=0;j<n_freq_itemsets;j++) {
			if(is_A_superset_OF_B(itemSet.its[i], itemSet.its[j])) {
				sendRule(r_msg,itemSet.its[j], itemSet.its[i],n_transactions, freq_map);
			}
		}
	}
	//fecha porta de comunica��o com o Selector
	ahCloseOutputPort(out_port_aggregator_selector_r);
	
	myfree(r_msg);
}

//fun��o responsavel por pegar o itemset correspondente ao id passado como parametro
char *getItemset(int id){
	hash_map<const char*, int, hash<const char*>, eqstr>::iterator p;
	for(p=item_map_id.begin();p!=item_map_id.end();p++) {
		if(item_map_id[(*p).first] == id)
			return ((char*) (*p).first);
	}
	
	char *errorMessage = (char *)calloc(1000, sizeof(char));
	time_t now;
	char *time_s = getTime(now);
	sprintf(errorMessage,ID_ERRO,time_s,"AGGREGATOR",my_rank,id);
	ahExit(errorMessage);
	
	return NULL;
}

void initializeArrays(float* precision,float* recall,float* F1,int size){
	//inicializa valores 
	for(int i=0;i<size;i++){
		precision[i] = 0;
		recall[i] = 0;
		F1[i] = 0;
	}
	
}

void initializeClasses(ClassTable* classes,int n_classes){
	//aloca espa�o para a estrutura classes
	classes->its = (ClassType*) mymalloc(sizeof(ClassType)*n_classes); 
	total_allocated += getAllocatedMem(classes->its);
	
	for(int i=0;i<n_classes;i++){
		classes->its[i].attempt = 0;
		classes->its[i].occur = 0;
		classes->its[i].hit = 0;
	}
	
	classes->size = n_classes;
}

int is_A_superset_OF_B(itemset_agg a, itemset_agg b) {
	if(a.size <= b.size) return 0;
	for(int i=0;i<b.size;i++) {
		if(!does_A_contains_K(a, b.layout[i])) return 0;
	}
	return(1);
}

int isValidRule(itemset_agg a, itemset_agg b){	
	//verifica confian�a m�nima da regra
	if((b.count)/(double)(a.count) < min_conf)
		return 0;
	
	//verifica tamanho do consequente
	if(b.size > 2)
		return 0;
	
	//verifica se consequente � alguma classe
	if(b.layout[0] >= 0)
		return 0;
	
	//regra v�lida
	return 1;
}

//metodo responsavel por imprimir todas as classes bem como as m�tricas de cada uma	
void printClasses(float* precision,float* recall,float* F1,ClassTable classes,int id,table tuples,FILE** file){

	fprintf(*file,"Document: %d\tClasse found: %s\tTrue Classe: %s\n",id,getItemset(tuples.its[id].attempt),getItemset(tuples.its[id].Class));
	
	//imprime informa��es de todas as classes
	for(int i=0;i<classes.size;i++){
		fprintf(*file,"\tClasse: %s\t",getItemset(-(i+1)));
		fprintf(*file,"Precision: %f\t",precision[i]);
		fprintf(*file,"Recall: %f\t",recall[i]);
		fprintf(*file,"F1: %f\n",F1[i]);
	}
}

//m�todo respons�vel por alocar espa�o de mem�ria suficiente para a estrutura aggset
void reallocAggSet(aggSet *itemSet,int size){	
	//realoca espa�o para a estrutura itemSet
	if(size == itemSet->size){
		total_allocated-=getAllocatedMem(itemSet->its);
		itemSet->its = (itemset_agg*) myrealloc(itemSet->its,sizeof(aggSet) * (itemSet->size + BLOCK_ITEMSETS));
		total_allocated+=getAllocatedMem(itemSet->its);
		
		itemSet->size+=BLOCK_ITEMSETS;
	}
}

//metodo responsavel por receber todos so documentos j� classificados do SELECTOR
void receiveClasses(ClassTable classes){
	int index;
	float* precision = (float*) malloc(sizeof(float)*classes.size);
	float* recall = (float*) malloc(sizeof(float)*classes.size);
	float* F1 = (float*) malloc(sizeof(float)*classes.size);
	table tuples;
	int *size_file = (int*) malloc(sizeof(int)*n_selector_instances);
	class_msg* c_msg = (class_msg*) mymalloc(sizeof(class_msg));

	//metodo responsavel por inicializar arrays
	initializeArrays(precision,recall,F1,classes.size);
	
	FILE* file = fopen(out_file, "w");
	
	//recebe o numero de linhas lido por cada instancias de selector
	while(ahReadBuffer(in_port_selec_agg, c_msg, sizeof(class_msg)) != EOW)
		size_file[c_msg->filter_id] = c_msg->n_tuples;

	//recebe as classes de cada linha(i.e. tupla)
	tuples.size = 0;
	while((ahReadBuffer(in_port_selector_aggregator, c_msg, sizeof(class_msg))) != EOW){
		//gera o indice correto do documento no arquivo de teste
		index = c_msg->id;
		for(int i=0;i<c_msg->filter_id;i++)
			index += size_file[i];

		//verifica se o indice est� dentro do espa�o alocado
		verifyIndex(&tuples,index);

		//atualiza variaveis	
		tuples.its[index].attempt = c_msg->a_class;
		tuples.its[index].Class = c_msg->Class;
		classes.its[abs(c_msg->Class)-1].occur++;
		classes.its[abs(c_msg->a_class)-1].attempt++;
		classes.its[abs(c_msg->Class)-1].hit += c_msg->hit;
		
		//calcula medidas estatitiscas
		calculateMetrics(precision,recall,F1,classes,tuples,index);

		//imprime as classes no arquivo de saida
		printClasses(precision,recall,F1,classes,index,tuples,&file);
		
		if(index >= tuples.size)
			tuples.size = index + 1;
	}
	
	fclose(file);
}

//fun��o respons�vel por enviar todas as regras geradas ao filtro seletor
void sendRule(rule_msg* r_msg, itemset_agg a, itemset_agg b,int n_transactions,map <set<int>, double> freq_map) {
	if(isValidRule(a,b)){	
		//preenche a estrutura para envio da mensagem
		r_msg->conf = 100*((b.count)/(double)(a.count));
		r_msg->freq = b.count;
		
		r_msg->size = 0;
		for(int i=0;i<a.size;i++)
			r_msg->item[r_msg->size++] = a.layout[i];
		for(int i=0;i<b.size;i++) {
			if(!does_A_contains_K(a, b.layout[i])) 
				r_msg->item[r_msg->size++] = b.layout[i];
		}

		//envia mensagem para o Selector
		ahWriteBuffer(out_port_aggregator_selector_r, r_msg, sizeof(rule_msg));
	}
}

void verifyIndex(table* tuples,int index){
	if(index >= tuples->size){
		if(tuples->size == 0){
			tuples->its = (tuple*) mymalloc(sizeof(tuple)*(index+1));
			total_allocated+=getAllocatedMem(tuples->its);
		}
		else{
			total_allocated-=getAllocatedMem(tuples->its);
			tuples->its = (tuple*) myrealloc(tuples->its,sizeof(tuple)*(index+1));
			total_allocated+=getAllocatedMem(tuples->its);
		}
	}

	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"AGGREGATOR",my_rank,n_instances);
}

void verifyMinConf(double min_conf){
	//verifica se a confian�a m�nima � coerente
	if(min_conf <= 0.0) {
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MIN_CONF,time_s,"AGGREGATOR",my_rank);
		printf("%s\n",warningMessage);
		min_conf = 0.01;
	}
	
	//verifica se a confian�a m�xima � coerente
	if(min_conf > 1.0){
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MAX_CONF,time_s,"AGGREGATOR",my_rank);
		printf("%s\n",warningMessage);
		min_conf = 1.0;
	}

}

void verifyNumInstances(){
	if(n_instances != 1) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,INST_ERROR,time_s,"AGGREGATOR",my_rank);
		ahExit(fatalMessage);
	}
}
