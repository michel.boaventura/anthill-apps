/*list.h*/

#ifndef _list_h
#define _list_h

/*inclusoes*/
#include "config.h"

/*Definicoes*/
#define LIST_ERROR "%s;FATAL;%s;List passed as parameter to function insertList is empty."

typedef struct nodo *pointer;

typedef struct nodo{
	int size;
	int freq;
	int Class;
	float conf;
	int* item;
	pointer next;
}nodo;

typedef struct list{
	int n_nodos;
	pointer header;
	pointer current;
	pointer MFreq;
}list;

/*Prototipos*/
void createList(list *L);
void deleteList(list *L);
void fillNodo(nodo* n,int* item,int size,int conf,int freq);
void getMFreqRule(list *L);
void initializeList(list *L);
void insertList(list *L,int* item,int size,float conf,int freq);
void walkList(list *L);

#endif
