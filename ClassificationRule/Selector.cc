/*Reader.cc*/

/*Inclusoes*/
#include "Selector.h"

InputPortHandler in_port_aggregator_selector;
InputPortHandler in_port_aggregator_selector_r;
OutputPortHandler out_port_selector_aggregator;
OutputPortHandler out_port_selec_agg;

int initFilter(void* work, int size) {
	my_rank = ahGetMyRank();
	n_instances = ahGetTotalInstances();

	//pega paramametros do usuario
	Work args = (*(Work *)work);
	strcpy(test_file,strdup(args.test_file));
	
	//configura portas de comunicacao
	in_port_aggregator_selector = ahGetInputPortByName("in_aggregator_selector");
	in_port_aggregator_selector_r= ahGetInputPortByName("in_aggregator_selector_r");
	out_port_selector_aggregator = ahGetOutputPortByName("out_selector_aggregator");
	out_port_selec_agg = ahGetOutputPortByName("out_selec_agg");
	
	//inicializa variavel global
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declaracao de variaveis
	int n_tuples = 0;
	table tuples;
	float selector_time = 0;
	timeval start_time, stop_time;
	list* rule_list = (list*) malloc(sizeof(list));

	//criacao de arquivo para gera��o de logs
	char out_file[100];
	sprintf(out_file, "SELECTOR%d", my_rank);
	FILE* ff = fopen(out_file, "w");
	
	//abre arquivo de origem da  base de dados
	FILE* file;
	openFile(&file);
	
	//comeca contagem de tempo de execucao do filtro
	gettimeofday(&start_time, NULL);
	
	//inicializacao de variavel
	tuples.its = (tuple*) mymalloc(sizeof(tuple)*FILE_BLOCK);
	tuples.size = FILE_BLOCK;
	total_allocated+=getAllocatedMem(tuples.its);
	
	//metodo respons�vel por receber os ids globais atribuidos aos itemsets no filtro aggregator
	receiveId();

	//le conteudo de arquivo e copia conteudo para armazenamento temporario em memoria principal
	copyBase(file,&tuples,&n_tuples);
	fclose(file);

	//metodo que recebe todas as regras geradas pelo aggregator
	createList(rule_list);
	receiveRules(rule_list);

	//metodo responsavel pela classifica��o dos documentos de teste
	predictClass(rule_list,&tuples,n_tuples);
	
	//calcula tempo final de execu��o 
	gettimeofday(&stop_time, NULL);
	selector_time = calculateInterval(start_time,stop_time);
	fprintf(ff, "Tempo de execucao do filtro seletor = %f\n", selector_time);
	fflush(ff);
	fclose(ff);
	
	return 1;
}

int finalizeFilter(void){
	//releaseOutputPort(out_port_reader_aggregator);
	//releaseOutputPort(out_port_reader_adder);
	//releaseOutputPort(out_port_reader_merger);
	//releaseOutputPort(out_port_reader_merger_MAX_ITEMS);
	return 1;
}

//funcao responsavel por copiar a base de dados para a memoria principal e,
//enviar os items distintos para o aggregator afim de obter ids globais
void copyBase(FILE *file,table* tuple,int *n_tuples){
	char *line = (char *) mymalloc(sizeof(char)*LINE_SIZE);
	total_allocated+=getAllocatedMem(line);
	char* item = (char *) mymalloc(sizeof(char)*ITEM_SIZE);
	total_allocated+=getAllocatedMem(item);

	fgets(line, LINE_SIZE, file);
	//le toda a base linha(i.e. documento) por linha do arquivo de teste
	while(!feof(file)) {
		//verifica se tamanho do arquivo esta dentro do alocado
		verifyFileSize(tuple,*n_tuples);
		
		tuple->its[*n_tuples].item = (int*) mymalloc(sizeof(int)*(LINE_SIZE));
		total_allocated+=getAllocatedMem(tuple->its[*n_tuples].item);
		
		//le uma transa��o da base toda
		item = strtok(line, ";\n");
		tuple->its[*n_tuples].size = 0;
		while(item!=NULL) {
			//verifica se item � conhecido
			if(getId(item) != 0){
				//identifica a classe do documento
				if(!strncmp(item,"CLASS",5))
					tuple->its[*n_tuples].Class = getId(item);
				else
					tuple->its[*n_tuples].item[tuple->its[*n_tuples].size++] = getId(item);
			}
			item = strtok(NULL, ";\n");
		}
		//verifica se a memoria alocada est� dentro dos limites estabelecidos
		verifyMemory(total_allocated,"SELECTOR",my_rank,n_instances);
		
		*n_tuples = (*n_tuples) + 1;
		fgets(line, LINE_SIZE, file);
	}
	
	total_allocated-=getAllocatedMem(line);
	myfree(line);
}

//fun��o responsavel por substituir os itemsets por seus ids
int getId(char *item){
	int aux;
	
	aux = id_map_item[item];
	//verifica se item � conhecido
	if(!aux){
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,ID_ERROR,time_s,"SELECTOR",my_rank,n_instances,item);
		//printf("%s",warningMessage);
	}
	
	return aux;
}

//metodo responsavel por abrir o arquivo de origem da base de dados
void openFile(FILE **file){
	if(n_instances == 1)
		sprintf(test_file,"%s", test_file);
	else
		sprintf(test_file,"%s%d", test_file, my_rank);
	
	*file = fopen(test_file, "r");
	if (*file == NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,F_TEST_ERROR,time_s,"SELECTOR",my_rank,n_instances,test_file);
		ahExit(fatalMessage);
	}
}

//metodo responsavel por realizar a classifica��o de todas as tuplas em rela��o �s regras existentes
void predictClass(list* rule_list,table* tuple,int n_tuples){
	int i,j,k;
	int class_id;
	
	class_msg* c_msg = (class_msg*) mymalloc(sizeof(class_msg));
	total_allocated+=getAllocatedMem(c_msg);
	
	//envia ao aggregator o numero de tuplas da parti��o do arquivo de teste lido
	c_msg->filter_id = my_rank;
	c_msg->n_tuples = n_tuples;
	ahWriteBuffer(out_port_selec_agg, c_msg, sizeof(class_msg));
	ahCloseOutputPort(out_port_selec_agg);
	
	//para cada documento do conjunto de teste
	for(i=0;i<n_tuples;i++){
		initializeList(rule_list);
		//pesquisa pela maior regra que est� contida na tupla deste documento
		while(rule_list->current != NULL){
			//verifica se a regra est� contida na tupla
			for(j=0;j<=rule_list->current->size;j++){
				for(k=0;k<tuple->its[i].size;k++){
					if(rule_list->current->item[j] == tuple->its[i].item[k])
						break;
				}

				//elemento da regra n�o presente na tupla
				if(k == tuple->its[i].size){
					break;
				}
			}

			//regra pode ser aplicada � tupla
			if(j == rule_list->current->size){
				break;
			}
			
			walkList(rule_list);
		}
		
		//N�o h� regras para a tupla: Classificar elemento como a classe de maior frequencia
		if(rule_list->current == NULL)
			getMFreqRule(rule_list);

		//classe da tupla � igual � classe da regra corrente
		class_id = rule_list->current->Class;

		//envia classifica��o para o aggregator
		sendClassPredicted(c_msg,i,class_id,tuple->its[i].Class);
	}
	ahCloseOutputPort(out_port_selector_aggregator);
	
	total_allocated-=getAllocatedMem(c_msg);
	myfree(c_msg);
}

//metodo respons�vel por receber os ids globais atribuidos aos itemsets no filtro aggregator
void receiveId(){
	char *item;
	item_msg* items = (item_msg*) mymalloc(sizeof(item_msg));
	total_allocated+=getAllocatedMem(items);
	
	while (ahReadBuffer(in_port_aggregator_selector, items, sizeof(item_msg)) != EOW){
		item = (char*) mymalloc(sizeof(char)*(items->size+1));
		total_allocated+=getAllocatedMem(item);
		
		strcpy(item,items->item);
		id_map_item[item] = items->index;
	}
	
	total_allocated-=getAllocatedMem(items);
	myfree(items);
}

//metodo responsavel por armazenar todas as regras geradas por aggregator
void receiveRules(list* rule_list){
	rule_msg* rules = (rule_msg*) mymalloc(sizeof(rule_msg));
	total_allocated+=getAllocatedMem(rules);
	
	while(ahReadBuffer(in_port_aggregator_selector_r, rules, sizeof(rule_msg)) != EOW){
		insertList(rule_list,rules->item,rules->size,rules->conf,rules->freq);
	}
	
	//verifica se h� pelo menos uma regra 
	if(rule_list->n_nodos == 0){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,RULE_ERROR,time_s,"SELECTOR",my_rank,n_instances);
		ahExit(fatalMessage);
	}
		
	
	total_allocated-=getAllocatedMem(rules);
	myfree(rules);
	
}

//metodo responsavel por enviar a classifica��o realizada para o aggregator
void sendClassPredicted(class_msg* c_msg,int id,int class_id,int true_class){
	c_msg->id = id;
	c_msg->a_class = class_id;
	c_msg->Class = true_class;
	c_msg->filter_id = my_rank;

	if(class_id == true_class)
		c_msg->hit = 1;
	else c_msg->hit = 0;
	ahWriteBuffer(out_port_selector_aggregator, c_msg, sizeof(class_msg));
}

//verifica se tamanho do arquivo esta dentro do alocado
void verifyFileSize(table* tuples,int n_tuples){
		
	//realoca mais um bloco de memoria 
	if(n_tuples == tuples->size){
		total_allocated-=getAllocatedMem(tuples->its);
		tuples->its = (tuple*) myrealloc(tuples->its,sizeof(tuple)*(tuples->size + FILE_BLOCK));
		total_allocated+=getAllocatedMem(tuples->its);
		tuples->size+=FILE_BLOCK;
	}
}
