/*Adder.cc*/

/*Inclusoes*/
#include "Adder.h"

using namespace std;

InputPortHandler in_port_reader_adder;
InputPortHandler in_port_merger_adder;
OutputPortHandler out_port_adder_merger;
OutputPortHandler out_port_adder_aggregator;

int initFilter(void* work, int size){
	my_rank = ahGetMyRank();
	n_instances = ahGetTotalInstances();
	
	//pega paramametros do usuario
	Work args=(*(Work *)work);
	anticipating = args.anticipating;
	min_supp = args.min_supp/(double)100;

	//configura portas de comunicacao
	in_port_reader_adder  = ahGetInputPortByName("in_reader_adder");
	in_port_merger_adder  = ahGetInputPortByName("in_merger_adder");
	out_port_adder_merger = ahGetOutputPortByName("out_adder_merger");
	out_port_adder_aggregator = ahGetOutputPortByName("out_adder_aggregator");
	
	//inicializa��o de vari�veis globai
	n_merger_instances = ahGetNumWriters(in_port_merger_adder);
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declaracao de variaveis
	int id = 0;
	int min_count;
	int finish = 0;
	int n_trans = 0;
	int last_id = -1;
	int candidates = -1;
	float total_time = 0;
	int n_freq_itemsets = 0;
	int n_itemsets = my_rank + 1;
	timeval total_start, total_stop;
	
	merger_msg* in_msg = (merger_msg*) mymalloc(sizeof(merger_msg));
	total_allocated+=getAllocatedMem(in_msg);

	//inicializa a estrutura global map_itemset
	map_itemsets.its =(item*) mymalloc(sizeof(item)*BLOCK_ITEMSETS);
	total_allocated+=getAllocatedMem(map_itemsets.its);;
	map_itemsets.size = BLOCK_ITEMSETS;
	
	//criacao de arquivo para gera��o de logs
	char inst[100];
	sprintf(inst, "ADDER%d", my_rank);
	FILE* ff = fopen(inst, "w");

	//comeca contagem de tempo de execucao do filtro
	gettimeofday(&total_start, NULL);
	
	//fun��o que obtem o numero de transacoes total da base de dados
	n_trans = getTotalTransactions();

	//calcula o suporte minimo global definido pelo usuario
	min_count = getMinCount(n_trans);
	
	//Loop Principal - realiza a contagem de ocorrencias globais dos itemsets
	while (ahReadBuffer(in_port_merger_adder, in_msg, sizeof(merger_msg)) != EOW) {
		//mensagem de fim de execu��o de uma instancia do filtro merger
		if(in_msg->type==EOI) {
			finish++;
			if(finish == n_merger_instances) break;
		}
		//mensagem de soma de ocorrencias de itemsets
		else if(in_msg->type==MSG) { 
			//fun��o que busca o id global deste item 
			id = getId(in_msg->id,&n_itemsets);

			if(id > candidates){
				reallocTable(candidates,id);
				candidates = id;
			}
			if(map_itemsets.its[id].n_instances == 0){
				gettimeofday((&map_itemsets.its[id].start), NULL);
				map_itemsets.its[id].id = id;
				map_itemsets.its[id].layout =(char*) malloc(sizeof(char)*(strlen(in_msg->id)+1));
				strcpy(map_itemsets.its[id].layout,in_msg->id);
			}
			map_itemsets.its[id].global_count+=in_msg->local_count;
			map_itemsets.its[id].n_instances++;
			//identifica o tipo de itemset e o trata devidamente
			identifyItemset(id,min_count,&last_id,&n_freq_itemsets);
		}
	}
	
	total_allocated-=getAllocatedMem(in_msg);
	myfree(in_msg);
	
	//calcula as medias de "partitions" e "triggers"  dos itemsets recebidos pela instancia
	float avg_trigger_time=0, avg_partitions=0;
	calculateAverages(last_id,min_count,n_freq_itemsets,&avg_trigger_time,&avg_partitions);

	//calcula tempo final de execu��o 
	gettimeofday(&total_stop, NULL);
	total_time = calculateInterval(total_start,total_stop);
	
	fprintf(ff, "\n\nEXEC_TIME: %f   CANDIDATES: %d    FREQUENT_ITS: %d    AVG_PARTITIONS: %f   AVG_TRIGGER: %f \n\n\n", total_time, candidates, n_freq_itemsets, avg_partitions, avg_trigger_time);
	fclose(ff);
	return 1;
}

int finalizeFilter(void){
	//releaseInputPort(in_port_merger_adder);
	//releaseOutputPort(out_port_adder_merger);
	total_allocated-=getAllocatedMem(map_itemsets.its);
	myfree(map_itemsets.its);
	
	return 1;
}

//calcula as medias de "partitions" e "triggers"  dos itemsets recebidos pela instancia
void calculateAverages(int last_id,int min_count,int n_freq_itemsets,float *att,float *ap){	
	for(int i=0;i<last_id;i++) {
		if(map_itemsets.its[i].global_count<min_count && map_itemsets.its[i].which!=n_merger_instances) {
			map_itemsets.its[i].which = n_merger_instances;
			gettimeofday(&(map_itemsets.its[i].stop), NULL);
		}
		if(map_itemsets.its[i].global_count>=min_count) {
			*ap+=map_itemsets.its[i].which;
			*att+=(float)(map_itemsets.its[i].stop.tv_sec + map_itemsets.its[i].stop.tv_usec/(float)1000000)-(float)(map_itemsets.its[i].start.tv_sec + map_itemsets.its[i].start.tv_usec/(float)1000000);
		}
	}
	if(n_freq_itemsets > 0) {
		*att = *att/(float)n_freq_itemsets;
		*ap = *ap/(float)n_freq_itemsets;
	}
}

//fun��o que atribui e obtem id dos item utilizando uma tabela hash
int getId(char *layout,int *n_itemsets){
	int result;
	char *key =(char*) malloc(sizeof(char)*(strlen(layout)+1));
	strcpy(key,layout);
	if(!id_map_layout[key]){
		result = *n_itemsets;
		id_map_layout[key] = result;
		*n_itemsets = *n_itemsets + n_instances;
	}
	else{
		result = id_map_layout[key];
		free(key);
	}
	return result;
}

//fun��o responsavel por obter a contagem minima de ocorrencias para itemsets frequentes
int getMinCount(int n_trans){
	int min_count = 0;
	min_count = (int)(min_supp*n_trans + 0.5);
	
	//verifica se o suporte m�nimo � coerente
	if(min_count <= 0) {
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MIN_SUPP,time_s,"ADDER",my_rank);
		printf("%s\n",warningMessage);
		min_count = 1;
	}
	
	//verifica se o suporte m�ximo � coerente
	if(min_count > n_trans){
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MAX_SUPP,time_s,"ADDER",my_rank);
		printf("%s\n",warningMessage);
		min_count = n_trans;
	}
	
	return min_count;
}

//fun��o que obtem o numero de transacoes total da base de dados
int getTotalTransactions(){
	int n_trans = 0;
	reader_msg* r_msg = (reader_msg*)mymalloc(sizeof(reader_msg));
	total_allocated+=getAllocatedMem(r_msg);
	
	while(ahReadBuffer(in_port_reader_adder, r_msg, sizeof(reader_msg)) != EOW )
		n_trans+=r_msg->n_trans_local;

	total_allocated-=getAllocatedMem(r_msg);
	myfree(r_msg);
	return n_trans;
}

//fun��o que identifica se o itemset � frequente ou n�o e a trata devidamente
void identifyItemset(int id,int min_count,int *last_id,int *n_freq_itemsets){
	adder_msg* out_msg = (adder_msg*) mymalloc(sizeof(adder_msg));
	total_allocated+=getAllocatedMem(out_msg);
	
	if(!map_itemsets.its[id].triggered){
		if((anticipating) | (map_itemsets.its[id].n_instances == n_merger_instances)){
			//itemset frequente
			if(map_itemsets.its[id].global_count >= min_count){
				*n_freq_itemsets = *n_freq_itemsets + 1;
				*last_id = id;
				treatFreqItemset(out_msg,anticipating,id);
			}
			//itemset infrequente
			else if (map_itemsets.its[id].n_instances == n_merger_instances)
				//envia itemst Infrequente
				sendMessage(out_msg,FREE,my_rank,id);
		}
	}
	else if(map_itemsets.its[id].n_instances == n_merger_instances){
		//informa ao aggregator a frequencia local de cada itemset
		repassItemset(id);
	}
	
	total_allocated-=getAllocatedMem(out_msg);
	myfree(out_msg);
}

//m�todo respons�vel por alocar mais espa�o para a estrutura map_itemsets caso necess�rio
void reallocTable(int item1,int item2){
	while(item2 >= map_itemsets.size){
		total_allocated-=getAllocatedMem(map_itemsets.its);
		map_itemsets.its =(item*)  myrealloc(map_itemsets.its,sizeof(item) * (map_itemsets.size+BLOCK_ITEMSETS));
		total_allocated+=getAllocatedMem(map_itemsets.its);
		map_itemsets.size +=BLOCK_ITEMSETS;
	}

	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"ADDER",my_rank,n_instances);
	
	//inicializacao de variavel
	for(int i=(item1+1);i<=item2;i++) {
		map_itemsets.its[i].id = -1;
		map_itemsets.its[i].which = 0;
		map_itemsets.its[i].n_instances = 0;
		map_itemsets.its[i].global_count = 0;
		map_itemsets.its[i].triggered = 0;
	}
}

//informa ao aggregator a frequencia local de cada itemset
void repassItemset(int id){
	output_msg* agg_msg = (output_msg*) mymalloc(sizeof(output_msg));
	total_allocated+=getAllocatedMem(agg_msg);
	
	agg_msg->local_count = map_itemsets.its[id].global_count;
	strcpy(agg_msg->layout,map_itemsets.its[id].layout);
	agg_msg->size = strlen(agg_msg->layout);
	
	ahWriteBuffer(out_port_adder_aggregator, agg_msg, sizeof(output_msg));
	
	total_allocated-=getAllocatedMem(agg_msg);
	myfree(agg_msg);
}

//envia mensagem ao filtro merger informando se o itemset "id" � frequente ou infrequente se o item pertencer ao conjunto de itemsets desta instancia
void sendMessage(adder_msg* out_msg,int type,int filter_id,int id){
	if(((id - 1)%n_instances) == my_rank){
		out_msg->type = type;
		out_msg->filter_id = filter_id;
		strcpy(out_msg->layout,map_itemsets.its[id].layout);
		ahWriteBuffer(out_port_adder_merger, (void*)out_msg, sizeof(adder_msg));
	}
}

void treatFreqItemset(adder_msg* out_msg,int anticipating,int id){	
	//envia somente para o Merger o itemset frequente
	if((anticipating) & (map_itemsets.its[id].n_instances < n_merger_instances)){
		updateValues(id);
		sendMessage(out_msg,MSG,my_rank,id);
	}
	else{
		//envia itemset frequente ao Merger
		sendMessage(out_msg,MSG,my_rank,id);
	
		//informa ao aggregator a frequencia local de cada itemset
		repassItemset(id);
	}
}

void updateValues(int id){
	gettimeofday(&(map_itemsets.its[id].stop), NULL);
	map_itemsets.its[id].which = map_itemsets.its[id].n_instances;
	map_itemsets.its[id].triggered = 1;
}
