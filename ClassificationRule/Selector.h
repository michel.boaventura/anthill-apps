/*Reader.h*/

/*Inclusoes*/
#include <map>
#include <ext/hash_map>

#include "memWrapper.h"
#include "config.h"
#include "list.h"

/*Definições*/
#define F_TEST_ERROR "%s;FATAL;%s;instance %d of %d;Error opening test file: %s."
#define ID_ERROR "%s;WARN;%s;instance %d of %d;Item %s don't exist in training set."
#define MSG_ERROR "%s;FATAL;%s;instance %d;The size of Message is too much hight. The biggest message permited is %d items."
#define NO_TRANSACTIONS "%s;FATAL;%s;instance %d;There is no transactions"
#define STRANGE_MAX_ITEM "%s;FATAL;%s;instance %d;The biggest itemset found is zero"
#define RULE_ERROR "%s;FATAL;%s;instance %d of %d;No one rule passed to filter selection."
using namespace __gnu_cxx;


typedef struct{
	int *item;
	int size;
	int Class;
}tuple;

typedef struct {
   tuple* its;
   int size;
} table;

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
int my_rank, n_instances;
char test_file[SIZE_FILE];
long int total_allocated;
hash_map<const char*, int, hash<const char*>, eqstr> id_map_item;

/*Prototipos*/
extern "C"{
int initFilter(void* work, int size);
int processFilter(void* work, int size);
int finalizeFilter(void);
}


void copyBase(FILE *file,table* tuple,int *n_tuples);
int getId(char *item);
void openFile(FILE **file);
void predictClass(list* rule_list,table* tuple,int n_tuples);
void receiveId();
void receiveRules(list* rule_list);
void sendClassPredicted(class_msg* c_msg,int id,int class_id,int true_class);
void verifyFileSize(table* tuples,int n_tuples);
