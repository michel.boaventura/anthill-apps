/*labelfunc.c*/

#include "config.h"

// faz essa aqui se quiser, Anthill tem um hash proprio
int hash(char *label, int image) {
	return (atoi(label)%image);

}

/* func�o respons�vel pela identifica��o do filtro destino da mensagem */
void getLabel(void *msg, int size, char *label){
	merger_msg* my_msg = (merger_msg*) msg;
	
	//verifica se o numero de instancias do filtro destino est� correto
	if(my_msg->mod <= 0){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,time_s,"FATAL;labelfunc.c; Wrong number os adder's instances passed to function getLabel");
		ahExit(fatalMessage);
	}
	
	//realiza uma divis�o modularizada para identificacao atribuir uma mensagem ao seu destino correto
	if(my_msg->mod == 1)
		sprintf(label, "%d", 0);
	else
		sprintf(label, "%d", my_msg->sum % my_msg->mod);
}
