/*getTime.cc*/

#include "getTime.h"

//calcula o intervalo de tempo total entre o total_start e o total_stop
float calculateInterval(struct timeval total_start,struct timeval total_stop){
	float total_time = 0;
	float begin_time = (float)(total_start.tv_sec + total_start.tv_usec)/(float)1000000;
	float end_time = (float)(total_stop.tv_sec + total_stop.tv_usec)/(float)1000000;
	total_time = end_time - begin_time;

	return total_time;
}

//calcula o instante de tempo de chamada desta fun��o bem como a data 
//correspondente colocando-os no formato americado de datas
char *getTime(time_t now){
	char *time_s;
	char *Day;
	char *month;
	char *year;
	char *hour;
	
	time(&now);
	time_s = ctime(&now);
	strtok(time_s," ");
	month = strtok(NULL," ");
	if(!strcmp(month,"Jan")){ month = "1";}
	else if(!strcmp(month,"Feb")){ month = "2";}
	else if(!strcmp(month,"Mar")){ month = "3";}
	else if(!strcmp(month,"Apr")){ month = "4";}
	else if(!strcmp(month,"May")){ month = "5";}
	else if(!strcmp(month,"Jun")){ month = "6";}
	else if(!strcmp(month,"Jul")){ month = "7";}
	else if(!strcmp(month,"Aug")){ month = "8";}
	else if(!strcmp(month,"Sep")){ month = "9";}
	else if(!strcmp(month,"Oct")){ month = "10";}
	else if(!strcmp(month,"Nov")){ month = "11";}
	else if(!strcmp(month,"Dec")){ month = "12";}
	
	Day = strtok(NULL," ");
	hour = strtok(NULL," ");
	year = strtok(NULL,"\n");
	sprintf(time_s,"%s-%s-%s %s",Day,month,year,hour);
	return time_s;
}
