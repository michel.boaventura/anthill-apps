/*getTime.h*/

/*Inclusoes*/
#ifndef _GETTIME_H
#define _GETTIME_H

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

/*Prototipos*/
float calculateInterval(struct timeval total_start,struct timeval total_stop);
char *getTime(time_t now);

#endif
