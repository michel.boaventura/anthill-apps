/*Reader.cc*/

/*Inclusoes*/
#include "Reader.h"

InputPortHandler in_port_aggregator_reader;
OutputPortHandler out_port_reader_merger;
OutputPortHandler out_port_reader_merger_MAX_ITEMS;
OutputPortHandler out_port_reader_adder;
OutputPortHandler out_port_reader_aggregator;
OutputPortHandler out_port_reader_agg_NTRANS;

int initFilter(void* work, int size) {
	my_rank = ahGetMyRank();
	n_instances = ahGetTotalInstances();

	//pega paramametros do usuario
	Work args = (*(Work *)work);
	strcpy(in_file,strdup(args.in_file));

	//configura portas de comunicacao
	in_port_aggregator_reader = ahGetInputPortByName("in_aggregator_reader");
	out_port_reader_merger = ahGetOutputPortByName("out_reader_merger");
	out_port_reader_merger_MAX_ITEMS = ahGetOutputPortByName("out_reader_merger_MAX_ITEMS");
	out_port_reader_adder = ahGetOutputPortByName("out_reader_adder");
	out_port_reader_aggregator = ahGetOutputPortByName("out_reader_aggregator");
	out_port_reader_agg_NTRANS = ahGetOutputPortByName("out_reader_agg_NTRANS");
	
	//inicializa variavel global
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declaracao de variaveis
	int n_trans = 0;
	int max_item = 0;
	float reader_time = 0;
	timeval start_time, stop_time;
	
	//criacao de arquivo para gera��o de logs
	char out_file[100];
	sprintf(out_file, "READER%d", my_rank);
	FILE* ff = fopen(out_file, "w");

	//abre arquivo de origem da  base de dados
	FILE* file;
	openFile(&file);
	
	//comeca contagem de tempo de execucao do filtro
	gettimeofday(&start_time, NULL);
	
	//conta numero de transa��es da base de dados
	n_trans = countTransactions(file);

	//criacao e inicializacao de variavel
	char** transaction = (char**) mymalloc(sizeof(char*)*n_trans);
	total_allocated+=getAllocatedMem(transaction);
	for(int i=0;i<n_trans;i++)
		 transaction[i] = NULL;
	n_trans = 0;
	
	//le conteudo de arquivo e copia conteudo para armazenamento temporario em memoria principal
	copyBase(file,transaction,&n_trans);
	fclose(file);

	//metodo respons�vel por receber os ids globais atribuidos aos itemsets no filtro aggregator
	receiveId(&max_item);

	//informa ao merger o maior itemset encontrado na base de dados e o numero de transa��es da base
	sendMaxItemset(max_item,n_trans);
	
	//envia a base de dados lida para o filtro merger	
	sendBase(transaction,n_trans);
	total_allocated-=getAllocatedMem(transaction);
	myfree(transaction);

	//informa ao adder e ao aggregator o numero de transacoes encontradas na parti��o da base lida
	sendNTransactions(n_trans);
	
	//calcula tempo final de execu��o 
	gettimeofday(&stop_time, NULL);
	reader_time = calculateInterval(start_time,stop_time);
	fprintf(ff, "Tempo de execucao do filtro leitor = %f\n", reader_time); fflush(ff);
	fclose(ff);
	
	return 1;
}

int finalizeFilter(void){
	//releaseOutputPort(out_port_reader_aggregator);
	//releaseOutputPort(out_port_reader_adder);
	//releaseOutputPort(out_port_reader_merger);
	//releaseOutputPort(out_port_reader_merger_MAX_ITEMS);
	return 1;
}

//fun��o responsavel por substituir os itemsets por seus ids
int changeItemsetId(char *transaction,int *result){
	char *item = (char*) mymalloc(sizeof(char)*ITEM_SIZE);
	total_allocated+=getAllocatedMem(item);
	
	char *aux;
	int length = 0;
	aux = strtok(transaction, ";" );
	while(aux != NULL){
		strcpy(item,aux);
		result[length] = id_map_item[item];
		aux = strtok(NULL, ";" );
		length++;
	}
	
	//verifica se o tamanho da mensagem est� dentro do permitido
	if(length > MSG_SIZE){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,MSG_ERROR,time_s,"READER",my_rank,MSG_SIZE);
		ahExit(fatalMessage);
	}
	
	total_allocated-=getAllocatedMem(item);
	myfree(item);
	return length;
}

//funcao responsavel por copiar a base de dados para a memoria principal e,
//enviar os items distintos para o aggregator afim de obter ids globais
void copyBase(FILE *file,char** transaction,int *n_trans){
	map<string, int> item_map;
	char *line = (char *) mymalloc(sizeof(char)*LINE_SIZE);
	total_allocated+=getAllocatedMem(line);
	char* item = (char *) mymalloc(sizeof(char)*ITEM_SIZE);
	total_allocated+=getAllocatedMem(item);
	item_msg* items=(item_msg*) mymalloc(sizeof(item_msg));
	total_allocated+=getAllocatedMem(items);
	fgets(line, LINE_SIZE, file);
	items->index = 0;
	//le toda a base linha(i.e. transa��o) por linha(i.e. transa��o) do arquivo
	while(!feof(file)) {
		char *buf = (char*) mymalloc(sizeof(char)*(LINE_SIZE));
		total_allocated+=getAllocatedMem(buf);
		buf[0] = '\0';
		item = strtok(line, ";");
		//le uma transa��o da base toda
		while(item!=NULL) {
			if(item[strlen(item)-1]=='\n') item[strlen(item)-1] = 0;
			//primeira vez que um item aparece na base
			if(! (item_map.count(item))){
				item_map[item] = 1;

				//envia item para o aggregator para obter um id unico e global para o item
				strcpy(items->item, item);
				items->size = strlen(items->item);
				ahWriteBuffer(out_port_reader_aggregator, items, sizeof(item_msg));
			}
			strcat(buf, item);
			strcat(buf,";");
			item = strtok(NULL, ";");
		}
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
		transaction[*n_trans] = (char*) mymalloc((strlen(buf)+2)*sizeof(char));
		strcpy(transaction[*n_trans], buf);
		
		total_allocated+=getAllocatedMem(transaction[*n_trans]);
		*n_trans = (*n_trans) + 1;
		total_allocated-=getAllocatedMem(buf);
		myfree(buf);
		fgets(line, LINE_SIZE, file);
	}
	//encerra comunica��o com o Aggregator
	ahCloseOutputPort(out_port_reader_aggregator);
	
	total_allocated-=getAllocatedMem(line);
	myfree(line);
	total_allocated-=getAllocatedMem(items);
	myfree(items);
}

//funcao responsavel pela obtencao do numero de transacoes presentes na base de dados 
int countTransactions(FILE *file){
	int n_trans = 0;
	char *line = (char *) mymalloc(sizeof(char)*LINE_SIZE);
	total_allocated+=getAllocatedMem(line);
	char* status = fgets(line, LINE_SIZE, file);
	
	if(status==NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,NO_TRANSACTIONS,time_s,"READER",my_rank);
		ahExit(fatalMessage);
	}
	
	//le todo o arquivo contando o n�mero de linhas (i.e. transa��es)
	while(!feof(file)) {
		n_trans++;
		fgets(line, LINE_SIZE, file);
	}
	
	//coloca o seek no in�cio do arquivo
	rewind(file);
	total_allocated-=getAllocatedMem(line);
	myfree(line);
	return n_trans;
}

//metodo responsavel por abrir o arquivo de origem da base de dados
void openFile(FILE **file){
	if(n_instances == 1)
		sprintf(in_file,"%s", in_file);
	else
		sprintf(in_file,"%s%d", in_file, my_rank);
	*file = fopen(in_file, "r");
	
	if (*file == NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,F_TRAIN_ERROR,time_s,"READER",my_rank,n_instances,in_file);
		ahExit(fatalMessage);
	}
}

//metodo respons�vel por receber os ids globais atribuidos aos itemsets no filtro aggregator
void receiveId(int *max_item){
	char *item;
	item_msg* items = (item_msg*) mymalloc(sizeof(item_msg));
	total_allocated+=getAllocatedMem(items);
	
	while (ahReadBuffer(in_port_aggregator_reader, items, sizeof(item_msg)) != EOW){
		item = (char*) mymalloc(sizeof(char)*(items->size+1));
		total_allocated+=getAllocatedMem(item);
		
		strcpy(item,items->item);
		id_map_item[item] = items->index;
		if(items->index > (*max_item)) 
			*max_item = items->index;
	}
	
	//verifica se o max_item possui valor coerente
	if(*max_item <= 0){
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,STRANGE_MAX_ITEM,time_s,"READER",my_rank);
		ahExit(fatalMessage);
	}
	
	total_allocated-=getAllocatedMem(items);
	myfree(items);
}

//metodo responsavel por enviar ao filtro reader a base de dados lida
void sendBase(char** transaction,int n_trans){
	data_block_t* block = (data_block_t*) mymalloc(sizeof(data_block_t));
	total_allocated+=getAllocatedMem(block);
	
	block->id = my_rank;
	for(int i=0;i<n_trans;i++) {
		//converte os itemsets para os respectivos ids globais 
		block->size =  changeItemsetId(transaction[i],block->transaction);
		total_allocated-=getAllocatedMem(transaction[i]);
		myfree(transaction[i]);
		block->n_trans = i;
		ahWriteBuffer(out_port_reader_merger, (data_block_t*)block, sizeof(data_block_t));
	}
	ahCloseOutputPort(out_port_reader_merger);
	
	total_allocated-=getAllocatedMem(block);
	myfree(block);
}

//m�todo respons�vel por informar a todas as inst�ncias de merger o max_item e o n_transa��es encontrados
void sendMaxItemset(int max_item,int n_trans){
	max_item_msg* max_items = (max_item_msg*) mymalloc(sizeof(max_item_msg));
	total_allocated+=getAllocatedMem(max_items);
	
	max_items->max_item = max_item;
	max_items->id = my_rank;
	max_items->n_transaction = n_trans;
	ahWriteBuffer(out_port_reader_merger_MAX_ITEMS, (max_item_msg*)max_items, sizeof(max_item_msg));
	ahCloseOutputPort(out_port_reader_merger_MAX_ITEMS);
	
	total_allocated-=getAllocatedMem(max_items);
	myfree(max_items);
}

//m�todo respons�vel por informar ao adder e ao aggreagator o n�mero de transa��es da base local
void sendNTransactions(int n_trans){
	reader_msg* r_msg = (reader_msg*) mymalloc(sizeof(reader_msg));
	total_allocated+=getAllocatedMem(r_msg);
	
	r_msg->n_trans_local = n_trans;
	
	//informa �s instancias de adder o numero de transa��es encontradas
	ahWriteBuffer(out_port_reader_adder, (reader_msg*)r_msg, sizeof(reader_msg));
	ahCloseOutputPort(out_port_reader_adder);
	
	//informa ao aggregator o n�mero de transa��es encontradas
	ahWriteBuffer(out_port_reader_agg_NTRANS, (reader_msg*)r_msg, sizeof(reader_msg));
	ahCloseOutputPort(out_port_reader_agg_NTRANS);
	
	total_allocated+=getAllocatedMem(r_msg);
	myfree(r_msg);
}
