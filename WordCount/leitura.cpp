#include <iostream>
#include <string>
#include <string.h>
#include "FilterDev.h"
#include "Manager.h"
#include "palavra.h"

using namespace std;

OutputPortHandler saida_leitura;

int initFilter(void* work, int size) {
	saida_leitura = ahGetOutputPortByName("saida_leitura");
	return 1;
}

int processFilter(void* work, int size){
	char aux[100];
	int contador=0;
	float f_div=0;
	int i_div=0;
	int div=0;
	int num_instancias = -1, id_instancia = -1;
	int linha_inicial = 0;
	int linha_final = 0;
	int linha = 0;
	tipo_palavra palavra;

	
	FILE* out_file = fopen("LEITURA", "w"); /* Arquivo auxiliar para acompanhamento da execução. */
	
	num_instancias = ahGetTotalInstances();
	id_instancia = ahGetMyRank();
	
	fprintf(out_file, "TOTAL INSTANCIAS RETORNA: %d\n", num_instancias);
	fprintf(out_file, "INSTANCIA ATUAL RETORNA: %d\n", id_instancia);
	
	/* Abre arquivo para leitura dos dados de entrada. */
	FILE* fe;
	if((fe = fopen ("entrada", "r")) == NULL) {
		printf("O arquivo nao pode ser aberto.");
		exit(1);
	}
	
	/*Verifica quantas linhas possui a entrada*/
	while(!feof(fe)){
		fgets(aux, 100, fe);
		/*se nao chegar no fim do arquivo incrementa contador*/
		if(!feof(fe))++contador;
	}
	rewind(fe);//volta para o começo do arquivo

	fprintf(out_file, "NUMERO DE LINHAS: %d\n", contador);
	
	f_div = (float)contador/num_instancias;
	i_div = contador/num_instancias;
	
	fprintf(out_file, "F_DIV: (%0.3f) - I_DIV: (%d)\n", f_div, i_div);
	
	/*se a divisao resultar em um numero float as instancias devem ler uma linha a mais*/
	if(f_div > i_div){
		div = i_div + 1;
	}
	else div = i_div;
	
	fprintf(out_file, "DIVISAO: %d\n", div);

	/*variavel que limita o inicio da leitura que a instancia deve realizar*/
	linha_inicial = id_instancia*div;
	linha = 0;
	
	/*se não for a ultma instancia*/
	if(id_instancia != num_instancias-1){
		linha_final = linha_inicial+div;
	}
	else{
		linha_final = contador;
	}
	
	/*percorre ate encontrar as linhas que devem ser lidas pela instancia*/
	while(linha < linha_inicial){
		fgets(aux, 100, fe);
		++linha;
	}
	
	/*instancia le suas respectivas linhas*/
	while(linha < linha_final){
		fscanf(fe, "%s", palavra.texto);
		palavra.tamanho = strlen(palavra.texto);
		fprintf(out_file, "LINHA: %d Palavra: %s Tamanho: %d\n", linha, palavra.texto, palavra.tamanho);
		ahWriteBuffer(saida_leitura, &palavra, sizeof(tipo_palavra));
		++linha;
	}
		
	fclose(fe);
	fclose(out_file);
	return 1;
}

int finalizeFilter(void){
	return 1;
}
