#include <iostream>
#include <string>
#include <vector>
#include "FilterDev.h"
#include "Manager.h"
#include "palavra.h"

using namespace std;

InputPortHandler entrada_contador;
OutputPortHandler saida_contador;

int initFilter(void * vetor, int size) {
	entrada_contador = ahGetInputPortByName("entrada_contador");
	saida_contador = ahGetOutputPortByName("saida_contador");
	return 1;
}

int processFilter(void* work, int size) {
	
	vector<tipo_celula> vocabulario;
	tipo_celula celula_aux;

	int i, tam, flag;
	tipo_palavra palavra2;
	
	/* Arquivo auxiliar de acompanhamento de execução. */
	FILE* out_file = fopen("CONTADOR.txt", "w");
	
	/* enquanto há dados a serem lidos do canal de comunicação, leia-os e atualize a lista. */
	while((ahReadBuffer(entrada_contador, &palavra2, sizeof(tipo_palavra))) != EOW){
		flag = 0;
		tam = vocabulario.size();
		
		/*tipo_celula auxiliar recebe palavra e contador inicial igual a 1*/
		strcpy(celula_aux.palavra, palavra2.texto);
		celula_aux.contador = 1;
		
		/*se a lista nao estiver vazia pesquisa a palavra no vocabulario*/
		if(!vocabulario.empty()){
			for(i=0; i<vocabulario.size();i++){
				
				/*se encontra a palavra incrementa o contador*/
				if(strcmp(vocabulario[i].palavra,celula_aux.palavra)==0){
					++(vocabulario[i].contador);
					flag++;
				}
			}
			
			/*se nao encontrou a palavra adiciona à lista*/
			if(flag==0){
				vocabulario.push_back(celula_aux);
			}
		}
		/*se a lista ta vazia adiciona a palavra ao fim da lista*/
		else{
			vocabulario.push_back(celula_aux);
		}		
	}

	/*passa o vetor para o proximo filtro*/
	//ahWriteBuffer(saida_contador, &vocabulario, sizeof(tipo_celula)*vocabulario.size());
	

	for(i=0; i<vocabulario.size();i++){
		celula_aux.contador = vocabulario[i].contador;
		strcpy(celula_aux.palavra, vocabulario[i].palavra);
		fprintf(out_file, "Vocabulario:%d cont:%d -> %s\n", i, vocabulario[i].contador, vocabulario[i].palavra);
		ahWriteBuffer(saida_contador, &vocabulario[i], sizeof(tipo_celula));
	}
	
	fclose(out_file);
	return 1;
}

int finalizeFilter(void){
	return 1;
}
