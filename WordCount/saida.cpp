#include <iostream>
#include <string>
#include <vector>
#include "Manager.h"
#include "FilterDev.h"
#include "palavra.h"

using namespace std;

InputPortHandler entrada_saida;

int initFilter(void* work, int size) {
	entrada_saida = ahGetInputPortByName("entrada_saida");
	return 1;
}

int processFilter(void* work, int size){

	vector <tipo_celula> vocabulario;
	tipo_celula aux;
	int flag;
	int i;
	
	FILE* out_file = fopen("SAIDA", "w");

	while((ahReadBuffer(entrada_saida, &aux , sizeof(tipo_celula))) != EOW){
		flag = 0;
		/*se a lista nao estiver vazia pesquisa a palavra no vocabulario*/
		if(!vocabulario.empty()){
			for(i = 0; i<vocabulario.size();i++){
				
				/*se encontra a palavra incrementa o contador*/
				if(strcmp(vocabulario[i].palavra, aux.palavra)==0){
					vocabulario[i].contador += aux.contador;
					flag++;
				}
			}
			
			/*se nao encontrou a palavra adiciona à lista*/
			if(flag==0){
				vocabulario.push_back(aux);
			}
		}
		/*se a lista esta vazia adiciona a palavra ao fim da lista*/
		else{
			vocabulario.push_back(aux);
		}
		
	}
	
	for(i = 0; i < vocabulario.size(); i++){
		fprintf(out_file, "vector[%d]:%s contador:%d\n", i, vocabulario[i].palavra, vocabulario[i].contador);
	}

	return 1;
}

int finalizeFilter(void){
	return 1;
}

