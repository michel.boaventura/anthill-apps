#include <iostream>

#include "Layout.h"
#include "Manager.h"

typedef int Work;

using namespace std;

int main (int argc, char *argv[]) {

	char* confFile = "./conf.xml";
	Work work[1];

	/* Inicializa o ambiente anthill. Nesse caso, work é apenas um inteiro sem significado para a aplicação em questão. */
	Layout *layout = initAh(confFile, argc, argv);
	appendWork(layout, (void*)work, sizeof(Work));
	finalizeAh(layout);
	
	return 0;
}
