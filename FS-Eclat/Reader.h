/*Reader.h*/

#ifndef _READER_H
#define _READER_H

/*Inclusoes*/
#include <map>
#include <ext/hash_map>

#include "memWrapper.h"
#include "config.h"

using namespace std;
using namespace __gnu_cxx;

/*Definições*/
#define SEPARATOR ";"

#define FILE_ERROR "%s;FATAL;%s;Error opening input file: %s."
#define MSG_ERROR "%s;FATAL;%s;instance %d;The size of Message is too much hight. The biggest message permited is %d items."
#define NO_TRANSACTIONS "%s;FATAL;%s;instance %d;There is no transactions"
#define STRANGE_MAX_ITEM "%s;FATAL;%s;instance %d;The biggest itemset found is zero"

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
int my_rank, n_instances;
char in_file[SIZE_FILE];
long int total_allocated;
hash_map<const char*, int, hash<const char*>, eqstr> id_map_item;

/*Prototipos*/
extern "C"{
int initFilter(void* work, int size);
int processFilter(void* work, int size);
int finalizeFilter(void);
}


int changeItemsetId(char *transaction,int *result);
void copyBase(FILE *file,char** transaction,int *n_trans);
int countTransactions(FILE *file);
void openFile(FILE **file);
void receiveId(int *max_item);
void sendBase(char** transaction,int n_trans);
void sendMaxItemset(int max_item,int n_trans);
void sendNTransactions(int n_trans);

#endif

