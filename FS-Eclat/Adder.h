/*Adder.h*/

#ifndef _ADDER_H
#define _ADDER_H

/*Inclusoes*/
#include <set>
#include <map>
#include <unistd.h>
#include <ext/hash_map>
#include <sys/resource.h>

#include "memWrapper.h"
#include "config.h"

using namespace std;
using namespace __gnu_cxx;

/*Definições*/
#define WRG_MIN_SUPP "%s;WARN;%s;instance %d; Support is so much low. Number of need occurences is defined as 1."
#define WRG_MAX_SUPP "%s;WARN;%s;instance %d; Support is so much high. Number of need occurences is defined as the total number of transactions found on the base."

typedef struct {
  timeval start, stop;
  int which;
  int n_instances;
  int global_count;
  int triggered;
  int id;
  char *layout;
} item;

typedef struct{
	item *its;
	int size;
} table;

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
double min_supp;
int anticipating;
int my_rank, n_instances;
table map_itemsets;
long int total_allocated;
int n_merger_instances;
hash_map<const char*, int , hash<const char*>, eqstr> id_map_layout;

/*Prototipos*/
extern "C"{
int initFilter(void* work, int size);
int processFilter(void* work, int size);
int finalizeFilter(void);
}

void calculateAverages(int last_id,int min_count,int n_freq_itemsets,float *att,float *ap);
int changeState(int id,int filter_id,int position);
void identifyItemset(int id,int min_count,int *last_id,int *n_freq_itemsets);
int getId(char *layout,int *n_itemsets);
int getMinCount(int n_trans);
int getTotalTransactions();
void reallocTable(int item1,int item2);
void repassItemset(int id);
void sendMessage(adder_msg* out_msg,int type,int filter_id,int id);
void treatFreqItemset(adder_msg* out_msg,int anticipating,int id);
void updateValues(int id);

#endif
