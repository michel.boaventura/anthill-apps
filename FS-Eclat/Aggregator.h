/*Aggregator.h*/

#ifndef _AGGREGATOR_H
#define _AGGREGATOR_H

/*Inclusoes*/
#include <map>
#include <set>
#include <ext/hash_map>

#include "memWrapper.h"
#include "config.h"

using namespace std;
using namespace __gnu_cxx;

/*Definições*/
#define INF 0
#define INIT 1

#define ALOC_ERROR "%s;FATAL;%s;instance %d; Error allocating memory in function copyInformations."
#define FILE_ERROR "%s;FATAL;%s;Error opening output file: %s."
#define ID_ERROR "%s;FATAL;%s;instance %d;Id: %d ,passed to function getItemset, don't exist."
#define INST_ERROR "%s;FATAL;%s;instance %d; The number of Aggregator's instances must be one."
#define WRG_MIN_CONF "%s;WARN;%s;instance %d; Confidence is so much low. The minimum default confidence, defined as 1%%, will be used !"
#define WRG_MAX_CONF "%s;WARN;%s;instance %d; Confidence is so much high. The maximum default confidence, defined as 100%%, will be used !"

typedef struct {
	int size;
	int* layout;
	int count;
} itemset_agg;

typedef struct{
	itemset_agg *its;
	int size;
} aggSet;

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
char out_file[SIZE_FILE];
double min_conf;
int my_rank, n_instances;
long int total_allocated;
hash_map<const char*, int, hash<const char*>, eqstr> item_map_id;

/*Prototipos*/
extern "C"{
int initFilter(void* work, int size);
int processFilter(void* work, int size);
int finalizeFilter(void);
}

void attribId();
void copyInformations(aggSet itemSet,int n_freq_itemsets,output_msg* in_msg);
int does_A_contains_K(itemset_agg a, int k);
void generateRules(int n_freq_itemsets,aggSet itemSet,int n_transactions,map <set<int>, double> freq_map);
char *getItemset(int id);
int is_A_superset_OF_B(itemset_agg a, itemset_agg b);
void openFile(FILE **file);
int print_rule(FILE* file, itemset_agg a, itemset_agg b,int n_transactions,map <set<int>, double> freq_map);
void reallocAggSet(aggSet *itemSet, int size);
void verifyMinConf(double min_conf);
void verifyNumInstances();

#endif
