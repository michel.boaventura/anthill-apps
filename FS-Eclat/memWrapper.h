/*memWrapper.h*/

#ifndef _MEMWRAPPER_H
#define _MEMWRAPPER_H

/*Inclusoes*/
#include "config.h"

/*Definições*/
#define TYPE long int

#define END_OF_MEMORY "%s;FATAL;%s;instance %d of %d;Could not allocate memory"

void *mymalloc(int size);
void *myrealloc(void *ptr, int size);
void myfree(void* ptr);
TYPE getAllocatedMem(void* ptr);
void verifyMemory(long int total_allocated,char *filter,int my_rank,int total_instances);

#endif
