/*Merger.cc*/

/*Inclusoes*/
#include "Merger.h"

InputPortHandler in_port_reader_merger;
InputPortHandler in_port_adder_merger;
InputPortHandler in_port_reader_merger_MAX_ITEMS;
OutputPortHandler out_port_merger_adder;

int initFilter(void* work, int size) {
	my_rank = ahGetMyRank();
	n_instances = ahGetTotalInstances();

	//configura portas de comunicacao
	in_port_reader_merger = ahGetInputPortByName("in_reader_merger");
	in_port_reader_merger_MAX_ITEMS = ahGetInputPortByName("in_reader_merger_MAX_ITEMS");
	in_port_adder_merger = ahGetInputPortByName("in_adder_merger");
	out_port_merger_adder = ahGetOutputPortByName("out_merger_adder");

	//inicializa��o de vari�veis globais
	n_adder_instances = ahGetNumWriters(in_port_adder_merger);
	n_reader_instances = ahGetNumWriters(in_port_reader_merger_MAX_ITEMS);
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declara��es de variaveis
	int id, position;
	int n_itemsets = 0;
	int max_items = 0 , n_message = 0;
	int n_transactions_local[n_reader_instances];
	struct timeval t_start, t_stop;
	
	adder_msg* in_msg = (adder_msg*) mymalloc(sizeof(adder_msg));
	total_allocated+=getAllocatedMem(in_msg);
	merger_msg* out_msg = (merger_msg*) mymalloc(sizeof(merger_msg));
	total_allocated+=getAllocatedMem(out_msg);
	int *pendency = (int*) malloc(sizeof(int)*ITEM_SIZE);

	//comeca contagem de tempo de execucao do filtro
	gettimeofday(&t_start,NULL);
	
	//criacao de arquivo para gera��o de logs
	char inst[100];
	sprintf(inst, "MERGER%d", my_rank);
	FILE* ff = fopen(inst, "w");
	
	//metodo responsavel por encontrar o maior elemento de toda a base de dados e por coletar o numero 
	//de transa��es presentes em cada peda�o da base, lido por cada filtro Reader 
	receiveReaderMessage(n_transactions_local,&max_items);

	//metodo responsavel por inicializar a estrutura global tab
	initializeTable(max_items);
	
	//constroi as listas de ocorrencias dos itemsets presentes na base de dados como um todo
	constructLists(n_transactions_local);

	//envia itemsets para o filtro Adder para a contagem da frequencia global
	for(int i=0; i < max_items; i++){
		gettimeofday((&tab.its[i].start), NULL);
		gettimeofday((&tab.its[i].stop), NULL);
		sendMessage(out_msg,MSG,i,tab.its[i].count,&n_message,pendency,&position);
		//ordena lista de ocorrencia do itemset crescentemente
		qsort((int *) tab.its[i].list,tab.its[i].count, sizeof(int), sort_layout);
	}

	n_itemsets = max_items;
	map<set<int>, set<int> > prefix_tree;
	while((n_message != 0)&&(ahReadBuffer(in_port_adder_merger, in_msg, sizeof(adder_msg)) != EOW)) {
		//verifica se o numero de itemsets gerados est� dentro do limite permitido
		verifyMaxItemset(n_itemsets);
		
		//retorna o id local do item e decrementa o numero de mensagens pendentes
		id = getId(in_msg->layout,&n_message);
		position = -1;
		
		if((id != NOWNER) & (in_msg->type==FREE)){
			tab.its[id].is_freq = 0;
		}
		else while((id != NOWNER) & (in_msg->type==MSG)){
			tab.its[id].is_freq = 1;
			set<int> key;
			set<int> s;
			key.insert(EMPTY_SET);
			for(int i=0;i<tab.its[id].size-1;i++) 
				key.insert(tab.its[id].layout[i]);
			
			s = prefix_tree[key];
			
			set<int>::iterator its_iterator;
			//envia itemsets para o filtro Adder para a contagem da frequencia global dos mesmos
			for(its_iterator=s.begin();its_iterator!=s.end();its_iterator++) {
				//verifica se � necess�rio alocar mais espa�o de memoria
				reallocTable(n_itemsets);

				tab.its[n_itemsets] = meet(tab.its[id], tab.its[*its_iterator]);
				tab.its[n_itemsets].id = n_itemsets;
				sendMessage(out_msg,MSG,n_itemsets,tab.its[n_itemsets].count,&n_message,pendency,&position);
				n_itemsets++;
			}
			s.insert(id);
			prefix_tree[key]=s;
			
			if(position > -1)
				id = pendency[position--];
			else id = NOWNER;
		}
	}
	
	//sinaliza fim de trabalho para ad der
	sendMessage(out_msg,EOI,0,0,&n_message,pendency,&position);
	
	//calcula tempo final de execu��o
	gettimeofday(&t_stop, NULL);
	float merger_time = calculateInterval(t_start,t_stop);
	fprintf(ff,"Tempo de execu��o: %f\n",merger_time); fflush(ff);
	fclose(ff);
	
	total_allocated-=getAllocatedMem(in_msg);
	myfree(in_msg);
	total_allocated-=getAllocatedMem(out_msg);
	myfree(out_msg);
	return 1;
}


int finalizeFilter(void){
/*	char inst[100];
	sprintf(inst, "MERGER%d", my_rank);
	FILE* ff=fopen(inst, "w");
	fprintf(ff, "finalizando\n"); fflush(ff);
	fclose(ff);
*/	//releaseInputPort(in_port_reader_merger);
	//releaseOutputPort(out_port_merger_aggregator);
	//releaseInputPort(in_port_adder_merger);
	//releaseOutputPort(out_port_merger_adder);

	return 1;
}

//metodo respons�vel pela constru��o das listas de ocorrencias dos itemsets
void constructLists(int *n_transactions_local){
	int n_transactions, index;
	data_block_t* block = (data_block_t *) mymalloc(sizeof(data_block_t));
	total_allocated+=getAllocatedMem(block);
	
	while (ahReadBuffer(in_port_reader_merger, block, sizeof(data_block_t)) != EOW) {
		n_transactions = 0;
		//calcula o n�mero global da transa��o recebida
		for(int i=0;i<block->id;i++)
			n_transactions += n_transactions_local[i];
		for(int i=0;i<block->size;i++) {
			index = block->transaction[i];
			if(tab.its[index].count%PARSE==0){
				total_allocated-=getAllocatedMem(tab.its[index].list);
				tab.its[index].list = (int*) myrealloc(tab.its[index].list, sizeof(int)*(tab.its[index].count+PARSE));
				total_allocated+=getAllocatedMem(tab.its[index].list);
			}
			tab.its[index].list[tab.its[index].count++] = n_transactions + block->n_trans;
			tab.its[index].layout[0] = index;
		}
	}
	
	total_allocated-=getAllocatedMem(block);
	myfree(block);
}

//fun��o que verifica se um item esta presente em um itemset
int contains(itemset I, int k) {
	int i;
	for(i=0;i<I.size;i++) 
		if(I.layout[i]==k) return 1;
	return 0;
}

//m�todo responsavel pela cria��o do layout resultante da concatena��o dos itemsets a e b
void createLayout(itemset a,itemset b,itemset *result){
	int i;
	if(a.size==0){
		for(i=0;i<b.size;i++)
			result->layout[i] = b.layout[i];
		result->size = b.size;
		return;
	}
	result->count = 0;
	if(a.size > b.size) {
		result->layout = (int*) mymalloc(sizeof(int)*(a.size+1));
		total_allocated+=getAllocatedMem(result->layout);
		result->size = a.size;
		for(i=0;i<a.size;i++) result->layout[i] = a.layout[i];
		for(i=0;i<b.size;i++) {
			if(!contains(*result, b.layout[i])) {
				result->layout[result->size++] = b.layout[i];
			}
		}
	}
	else {
		result->layout = (int*) mymalloc(sizeof(int)*(b.size+1));
		total_allocated+=getAllocatedMem(result->layout);
		result->size = b.size;
		for(i=0;i<b.size;i++) result->layout[i] = b.layout[i];
		for(i=0;i<a.size;i++) {
			if(!contains(*result, a.layout[i])) {
				result->layout[result->size++] = a.layout[i];
			}
		}
	}
}

//metodo responsavel pela cria��o da lista de ocorrencias do itemsets result
void createList(itemset a,itemset b,itemset *result){
	int i,j;
	
	//verifica se n�o h� alguma ocorrencia dos itemsets na base local
	if((a.count == 0) | (b.count == 0)){
		result->count = 0;
		return;
	}
	
	//aloca espa�o para a lista de ocorrencia do itemset resultante
	if(a.count > b.count) result->list = (int*) mymalloc(sizeof(int)*b.count);
	else result->list=(int*) mymalloc(sizeof(int)*a.count);
	total_allocated+=getAllocatedMem(result->list);
	
	for(i=0,j=0; i < a.count && j < b.count;) {
		if(a.list[i] > b.list[j]) j++;
		else if(a.list[i]==b.list[j]) {
			result->list[result->count++]=a.list[i];
			j++;
			i++;
		}
		else i++;
	}
	total_allocated-=getAllocatedMem(result->list);
	if(result->count!=0 ) {
		result->list=(int*) myrealloc(result->list,sizeof(int)*result->count);
		total_allocated+=getAllocatedMem(result->list);
	}
	else myfree(result->list);
}

//fun��o respons�vel por mapear o item recebido para o seu id local na inst�ncia
int getId(char *layout,int *n_message){
	int result;
	result = layout_map_id[layout];
	if((result) | (!strcmp(layout,"0")))
		*n_message = *n_message - 1;
	else{
		char *key = (char*) malloc(sizeof(char)*(strlen(layout)+1));
		strcpy(key,layout);
		pendencies_table[key] = 1;
		result = NOWNER;
	}
	return result;
}

//aloca memoria e inicializa a estrutura tab
void initializeTable(int max_items){
	int n_block;
	
	//calcula o n�mero de blocos necess�rios
	n_block = max_items/BLOCK_ITEMSETS + 1;
	tab.its = (itemset*) mymalloc(sizeof(itemset)*BLOCK_ITEMSETS*n_block);
	total_allocated+=getAllocatedMem(tab.its);
	tab.size = BLOCK_ITEMSETS;
	
	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"MERGER",my_rank,n_instances);
	
	for(int i=0; i < max_items; i++) {
		tab.its[i].id = i;
		tab.its[i].size = 1;
		tab.its[i].count = 0;
		tab.its[i].is_freq = 0;
		tab.its[i].layout = (int*) mymalloc(sizeof(int));
		total_allocated+=getAllocatedMem(tab.its[i].layout);
		tab.its[i].layout[0] = i;
		tab.its[i].list = (int*) mymalloc(sizeof(int));
		total_allocated+=getAllocatedMem(tab.its[i].list);
	}
	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"MERGER",my_rank,n_instances);
}

//fun��o respons�vel pela cria��o dos (k+1)-itemsets apartir dos k-itemsets
itemset meet(itemset a, itemset b) {
	itemset result;

	gettimeofday((&result.start), NULL);
	result.is_freq = 0;
	
	//metodo responsavel pela cria��o do layout resultante da concatena��o dos itemsets a e b
	createLayout(a,b,&result);
	
	//ordena itens do itemset crescentemente
	qsort((int *) result.layout, result.size, sizeof(int), sort_layout);

	//metodo responsavel pela cria��o da lista de ocorrencias do itemsets result
	createList(a,b,&result);
	
	gettimeofday((&result.stop), NULL);
	
	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"MERGER",my_rank,n_instances);
	return result;
}

void print_itemset(FILE* ff, itemset it) {
	for(int i=0;i<it.size;i++) fprintf(ff, "%d ", it.layout[i]);
	fprintf(ff, "  %d\n", it.count);
	fflush(ff);
}

//metodo responsavel por encontrar o maior elemento de toda a base de dados e por coletar o numero 
//de transa��es presentes em cada peda�o da base, lido por cada filtro Reader 
void receiveReaderMessage(int *n_transactions_local,int *max_item){
	max_item_msg* m_item = (max_item_msg*) mymalloc(sizeof(max_item_msg)); 
	total_allocated+=getAllocatedMem(m_item);
	
	while(ahReadBuffer(in_port_reader_merger_MAX_ITEMS, m_item, sizeof(max_item_msg)) != EOW){
		if(m_item->max_item > *max_item) *max_item = m_item->max_item + 1;
		n_transactions_local[m_item->id] = m_item->n_transaction;
	}
	total_allocated-=getAllocatedMem(m_item);
	myfree(m_item);
}

//m�todo respons�vel por alocar mais espa�o para a estrutura tab caso necess�rio
void reallocTable(int size){
	//verifica se o numero de itemsets gerados est� dentro do limite permitido
	verifyMaxItemset(size);
	
	//realoca mais um bloco de memoria 
	if(size == tab.size){
		total_allocated-=getAllocatedMem(tab.its);
		tab.its = (itemset*) myrealloc(tab.its,sizeof(itemset)*(tab.size + BLOCK_ITEMSETS));
		total_allocated+=getAllocatedMem(tab.its);
		tab.size+=BLOCK_ITEMSETS;
	}
	
	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"MERGER",my_rank,n_instances);
}

//envia itemsets, com count > 0, para o filtro Adder para a contagem da frequencia global dos mesmos
void sendMessage(merger_msg* out_msg, int type,int index,int count,int* n_message,int *pendency,int *position){
	int i;
	char *character = (char*) malloc(sizeof(char)*(T_MSG_SIZE));
	
	//gera o string do layout
	sprintf(character, "%d",tab.its[index].layout[0]);
	out_msg->sum = tab.its[index].layout[0];
	strcpy(out_msg->id,character);
	for(i=1;i<tab.its[index].size;i++){
		strcat(out_msg->id, " ");
		sprintf(character,"%d",tab.its[index].layout[i]);
		strcat(out_msg->id,character);
		out_msg->sum += tab.its[index].layout[i];
	}
	out_msg->id[strlen(out_msg->id)] = '\0';
	
	//armazena string no hash
	char *key = (char*) malloc(sizeof(char)*(strlen(out_msg->id)+1));
	strcpy(key,out_msg->id);
	layout_map_id[key] = index;
	
	out_msg->type = type;
	out_msg->filter_id = my_rank;
	out_msg->size = tab.its[index].size;
	out_msg->mod = n_adder_instances;
	out_msg->local_count = count;
	ahWriteBuffer(out_port_merger_adder, (merger_msg*)out_msg, (sizeof(merger_msg)));

	//incrementa n_message pendentes se n�o tiver recebido uma mensagem do adder relacionada com este item
	if(pendencies_table[key] == 0){
		*n_message = *n_message + 1;
	}
	else if(pendencies_table[key] == 1){
		*position = *position + 1;
		pendency[*position] =  index;
		pendencies_table[key] = 2;
	}
	
	free(character);
}

//fun��o auxiliar no processo de ordena��o de itemsets (ordena��o crescente)
static int sort_layout(const void *a, const void *b) {
	int* i=(int*)a;
	int* j=(int*)b;

	if (*i > *j) return(1);
	if (*i < *j) return(-1);
	return 0;
}

//metodo se o numero de itemsets gerados est� dentro do limite permitido
void verifyMaxItemset(int n_itemsets){
	if(n_itemsets >= MAX_ITEMSETS) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,SIZE_OF_ITEM,time_s,"MERGER",my_rank, n_instances);
		ahExit(fatalMessage);
	}
}
