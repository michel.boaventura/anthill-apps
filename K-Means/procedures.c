#include "procedures.h"


/** Get parameters of the algorithm from the work passed bu the AntHill.
 * This function is responsible to read all the information contained in the 
 * work and to return this information in each of the variables passed by 
 * reference. 
 */

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, int *num_centroids, int *number_of_dimensions)
{
	/** Stores the work in a array of char */
	char *work_char;				
	char *temp_string;
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/

	
	/* Transforms the work in a array of char */
	work_char = (char *) work;
	
	/** Reads all the information of the work */
	/** Gets the input file name */
	temp_string = strtok (work_char, " ");		
	strcpy(input_file_name,temp_string);	
	
	/** Gets the output file name */
	temp_string = strtok (NULL, " ");		
	strcpy(output_file_name,temp_string);	

	/** Gets the number of centroids */
	temp_string = strtok (NULL, " ");		
	*num_centroids = atoi (temp_string);

	/** Gets the number of dimensions */
	temp_string = strtok (NULL, " ");		
	*number_of_dimensions = atoi (temp_string);
	
	

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "WORK: \n");
	print_work(input_file_name,output_file_name,(*num_centroids), (*number_of_dimensions),fp_log);
	fprintf(fp_log, "END OF WORK \n");
	fclose(fp_log);
#endif		
/****************************************************************************/
	
	//free(temp_string);
}


/*Print the Work .*/
void print_work(char *input_file_name, char *output_file_name,int num_centroids, int number_of_dimensions, FILE *file )
{

	fprintf(file,"Input File : %s\t", input_file_name);
	fprintf(file,"Output File : %s\t", output_file_name);
	fprintf(file,"Number Centroids: %d\t", num_centroids);
	fprintf(file,"Number Dimensions: %d\n", number_of_dimensions);



}


float** malloc_matrix_float(int lines, int coluns)
{
float **matrix;
int i;	
	matrix = (float **) malloc(lines * sizeof(float *));


	for (i=0;i<lines;i++) 
	{				
		matrix[i] = (float *) malloc(coluns * sizeof(float));
	}

return matrix;

}

void print_matrix_float(float **matrix,int lines, int coluns,  FILE *file )
{
	int i,j;


	for (i=0;i<lines;i++) 
	{	
		fprintf(file,"Line: %d\t", i);
		for (j=0;j<coluns;j++)	
			{
				fprintf(file, "%f\t ", matrix[i][j]);
			}
		fprintf(file,"End Matrix\n----------\n");
		
	}


}

void print_vector_float(float *vector, int len, FILE *file ) 
{
	
int j;
	
	for (j=0;j<len;j++)	
	{
		fprintf(file, "%f\t ", vector[j]);
	}

}
int print_time(FILE *file, struct timeval *start, struct timeval *end)
{
	double execution_time;	// gets the execution time

	// calculates the execution time in seconds
	execution_time  = ( end->tv_sec - start->tv_sec )*1000000;
	execution_time += end->tv_usec - start->tv_usec;
	execution_time /= 1000000;

	// print the time
	fprintf(file, "%.3lf\n", execution_time);

	return 1;
}
