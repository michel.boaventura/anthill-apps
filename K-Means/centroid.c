#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "FilterDev.h"
#include "constants.h"
#include "procedures.h"




// structure that represents a centroid
typedef struct {
	
	int weight;		// number of points associated to the local centroid received 
	float *coordinates;	// local centroid's coordinates
	int changes;		// number of points that changed the set they belonged
	int localCentroidsRecv; // number of messages received referring to this centroid, 
				// sent by each of the readers in the current iteration  	
} Centroid;


InputPortHandler inputP = -1;
OutputPortHandler new_centroidsP = -1;
int num_centroids = -1;
int num_dimensions = -1;


char output[MAXFILENAME]; 
char input[MAXFILENAME];

int initFilter(void *work, int worksize) {

	char error[100] = "Coud not open Port" ;
	get_information_from_work(work, input, output, &num_centroids , &num_dimensions);
	assert(num_centroids >= 0);

	// gets the input and output port handlers 
	inputP = ahGetInputPortByName("local_centroid_input");
	new_centroidsP = ahGetOutputPortByName("global_centroid_output");
	if (inputP < 0) {
		ahExit(error);
	}
	if (new_centroidsP < 0) {
		ahExit(error);
	}	
	return 0;
}


void sendNewCentroid(int centroidId, int iterationChanges, Centroid *pCentroid, int numDimensions) {
	unsigned int szMsg = 2*sizeof(int) + sizeof(float)*numDimensions;
	int *global_centroid = (int *)malloc(szMsg);
	float *coordinates;
	int i=0;
/*****************DEBUG******************************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
#endif
/********************************************************/

	if (numDimensions > 0) {
		global_centroid[0] = centroidId;
		global_centroid[1] = iterationChanges;

		coordinates = (float *) &(global_centroid[2]);
		for (i=0; i<numDimensions; i++) {
			coordinates[i] = pCentroid->coordinates[i];
		}
	}
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Send NewCentroid Msg: \n");
	fprintf(fp_log, "Centroid ID = %d ------ IterationChanges = %d\n", global_centroid[0], global_centroid[1]);
	print_vector_float(coordinates, num_dimensions, fp_log);
	fprintf(fp_log, "End NewCentroid Msg \n");
	fclose(fp_log);
#endif		
/****************************************************************************/

	ahWriteBuffer(new_centroidsP, global_centroid, szMsg);
	free(global_centroid);
}

int processFilter(void *work, int worksize) {
	
	char error[100] = "Number of dimensions error" ;
	int signal = 1;
	int iter = 0;
	int iterationChanges = 0;
	int rank = ahGetMyRank();
	if (rank >= num_centroids)
		return 0;
	
#ifdef TIME
	struct timeval initial_time, final_time;
	FILE *fp_time;
	char error_msg[100];
	char fp_time_name[100];
	sprintf(fp_time_name, "Filter_%s_Instance_%d.time", ahGetFilterName(), ahGetMyRank());
	// Clean the files of times
	fp_time = fopen(fp_time_name,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);
#endif

#ifdef TIME 
	gettimeofday(&initial_time, NULL);
#endif 	

	assert(num_centroids > 0);
	Centroid *centroids = (Centroid *)calloc( (unsigned int)num_centroids, sizeof(Centroid));
	// message's format:
		// 0: total readers (int)
		// 1: centroid's id (int)
		// 2: centroid's weight (int)
		// 3: number of dimensions (int)
		// 4: changes of the centroid's coordinates (int)
		// 5: coordinate1 (float)
		// 6: coordinate2 (float)
		// ...

	unsigned int szRecvMsg = (sizeof(int)*5) + (sizeof(float)*num_dimensions);
	int *local_centroid = (int *)malloc(szRecvMsg);
	ahReadBuffer(inputP, local_centroid, szRecvMsg);
	
	int totalReaders    = local_centroid[0];
	int centroid_id     = local_centroid[1];
	int centroid_weight = local_centroid[2];
	int num_dimensions_aux  = local_centroid[3];
	centroids[centroid_id].changes = local_centroid[4];
	float *local_centroid_coord = (float *) &(local_centroid[5]);
	int verification_centroid = centroid_id;
	
	if (num_dimensions_aux != num_dimensions)
		ahExit(error);

	iterationChanges += centroids[centroid_id].changes;
	centroids[centroid_id].localCentroidsRecv = 1;
	centroids[centroid_id].weight = centroid_weight;
	
	if (num_dimensions > 0) {
		int i;
		
		centroids[centroid_id].coordinates = (float *)malloc(sizeof(float)*num_dimensions);
		for (i=0; i<num_dimensions; i++) {
			centroids[centroid_id].coordinates[i] = local_centroid_coord[i];
		}
	}
	
	if (centroids[centroid_id].localCentroidsRecv == totalReaders) {
		/*if ((iterationChanges == 0) || (iter == MAXITER)) {
			signal = 0;
		} else*/ {
			// All the readers have sent their local centroids.
			// Sends the new global centroids to them.
			sendNewCentroid(centroid_id, iterationChanges, &(centroids[centroid_id]), num_dimensions);

			centroids[centroid_id].changes = 0;	
			centroids[centroid_id].localCentroidsRecv = 0;
			centroids[centroid_id].weight = 0;
		}

		// checks if the algorithm must be ended just when analysing the
		// verification centroid, so that this is done only once per 
		// iteration enven if the filter is in charge of more than one
		// centroid		
		if (centroid_id == verification_centroid) {
			iter++;	
			iterationChanges = 0;
		}		
	}

	while (signal) {
		// for each global centroid that belongs to this filter:
		//    -> reads local centroid from each reader
		//    -> determines global centroid e sends it to the readers
		int ret = ahReadBuffer(inputP, local_centroid, szRecvMsg);
		
		if (ret == EOW)
			break; // readers ended up their activity
		
		centroid_id     = local_centroid[1];
		centroid_weight = local_centroid[2];
		num_dimensions  = local_centroid[3];
		centroids[centroid_id].changes += local_centroid[4];
		iterationChanges += local_centroid[4];
	
		if (num_dimensions_aux != num_dimensions)
			ahExit(error);

		local_centroid_coord = NULL;
		if (num_dimensions > 0)
			local_centroid_coord = (float *) &(local_centroid[5]);

		Centroid *pCentroid = &(centroids[centroid_id]);

		if (pCentroid->weight == 0) {
			// it is a new centroid or a new iteration
			if (num_dimensions > 0) {
				int i;
				if (pCentroid->coordinates == NULL)
					pCentroid->coordinates = (float *)malloc(sizeof(float)*num_dimensions);
				for (i=0; i<num_dimensions; i++) {
					pCentroid->coordinates[i] = local_centroid_coord[i];
				}
			}
			pCentroid->weight = centroid_weight;
		} else {
			if (num_dimensions > 0)
			{
				int i;
				for (i=0; i<num_dimensions; i++) 
				{
					pCentroid->coordinates[i] = (pCentroid->weight*pCentroid->coordinates[i] + 
						centroid_weight*local_centroid_coord[i]) / (pCentroid->weight+centroid_weight);
				}
			}
			pCentroid->weight += centroid_weight;
		}		

		pCentroid->localCentroidsRecv++;
		if (pCentroid->localCentroidsRecv == totalReaders) 
		{
			/*if ((iterationChanges == 0) || (iter == MAXITER)) 
				signal = 0;
			else*/
			{			
				// All the readers have sent their local centroids.
				// Sends the new global centroids to them.
				sendNewCentroid(centroid_id, iterationChanges, pCentroid, num_dimensions);				

				pCentroid->changes = 0;	
				pCentroid->localCentroidsRecv = 0;
				pCentroid->weight = 0;
			}

			// checks if the algorithm must be ended just when analysing the
			// centroid 0, so that this is done only once per iteration, even 
			// if the filter is in charge of more than one centroid 
			if (centroid_id == verification_centroid) {
				iter++;
				iterationChanges = 0;
			}
		}
	}
	free(local_centroid);

	//label for output file's name
	char *temp_string = strtok(output, "/ ;");
	char *outputlabel = temp_string;
	while (temp_string!=NULL) {
		outputlabel=temp_string;
		temp_string = strtok(NULL, "/ ;");
	}	

#ifdef TIME
	gettimeofday(&final_time, NULL);
	fp_time = fopen(fp_time_name,"a");
	print_time(fp_time, &initial_time, &final_time);
	fclose(fp_time);
#endif

	return 0;
}

int finalizeFilter(void) {
	//releaseInputPort(inputP);
	//releaseOutputPort(new_centroidsP);

	return 0;
}
