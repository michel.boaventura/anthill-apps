#include "memory_data.h"

/* Function alloc_cell allocs memory necessary to creating a new cell */

list_data_pointer alloc_data_cell(void)
{
   list_data_pointer aux;
   aux = (data_cell *)malloc(sizeof(data_cell));
   return aux;
}

/* Procedure that creates an empty list */

void create_empty_data_list(list_data_type *list)
{
   list->first=alloc_data_cell();
   list->last=list->first;
   list->first->next=list->first;
   list->first->previous=list->first;
   list->length=0;
}

/* Answer if a list is empty */
int is_empty_data_list(list_data_type *list)
{
   return !(list->length);
}

/* insert_in_list inserts a new item on the last position of a list */
void insert_in_data_list(data_type item, list_data_type *list)
{
   list->last->next=alloc_data_cell();
   list->last->next->previous=list->last;
   list->last=list->last->next;
   list->last->item=item;
   list->last->next=list->first;
   list->first->previous=list->last;
   list->length++;
}

data_type remove_first_of_data_list(list_data_type *list)
{
   data_type item;

   list_data_pointer aux=list->first->next;
   item = remove_of_data_list(&aux, list);

   return item;
}

/* Remove the item that is pointed.
 * After the remove, the pointer will be pointed to the previous item. 
 * Then, the next list of the item pointed will be the same */

data_type remove_of_data_list(list_data_pointer *p, list_data_type *list)
{
   data_type item;
   list_data_pointer aux;

   /* Tests is the list is empty or the pointer is null */
   if (is_empty_data_list(list) || !(*p) || (*p==list->first))
      printf("Error: List is_empty or position doesn't exists.\n");
   else
   {
      item=(*p)->item;
      (*p)->next->previous=(*p)->previous;
      (*p)->previous->next=(*p)->next;
      if ((*p)==list->last)
         list->last=(*p)->previous;
      aux=(*p);
      (*p)=(*p)->previous;
      free_data_cell(aux);
      list->length--;
   }
   return item;
}

/* Get the first item of the list if it exists */
int get_first_in_data_list(list_data_type *list, list_data_pointer *pointer)
{
   if (is_empty_data_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->first->next;
   return 1;
}

/* Go to the next position of the list */
int go_next_data(list_data_type *list, list_data_pointer *pointer)
{
	/* Tests if the pointer is valid */
	if (*pointer)
	{
		/* Tests if the object is the last one of the list. In this case it do not have a next item. */
		if ((*pointer)==list->last)
		{
			get_first_in_data_list(list, pointer);
			return 0;
		}
		else
		{
			*pointer=(*pointer)->next;
			return 1;
		}
	}
	else return 0;     
}


/* Tests is the item is the last of the list */
int is_last_data(list_data_pointer p, list_data_type *list)
{

   /* If the pointer is NULL, there is an error */
   if (!p)
   {
      printf("Erro: Este apontador não aponta para célula alguma");
      return 0;
   }
   return (p->next==list->first);
}

/* Searchs an item on the list, returning this position or NULL if the item was not found */

list_data_pointer search_item_in_data_list(data_type item, list_data_type *list)
{
   list_data_pointer aux;

   /* If there is no item on the list, it returns NULL*/
   if(!get_first_in_data_list(list, &aux))
	   return NULL; 

   /* While there is an next item, keep searching */
   do
   {
      if (compare_datas(aux->item, item))
      {
	 return aux;
      }
      go_next_data(list, &aux);
   }   
   while (!is_last_data(aux, list));
   
   /* Tests if the object is the last */
   if (compare_datas(aux->item, item))
   {
         return aux;
   }

   return NULL;
}


/* Make an list empty, is frees the memory allocated*/
void make_data_list_empty(list_data_type *list)
{
   data_type aux;
   while (!is_empty_data_list(list))
   {
      aux = remove_first_of_data_list(list);
      free_data(aux);
   }
}

/* Destroys a list. First make this empty, then frees the memory */
void destroy_data_list(list_data_type *list)
{
	make_data_list_empty(list);
	free(list->first);
}


/* Gets the size of the list */
int get_data_list_length(list_data_type *list)
{
	return (list->length);
}


/* Print all the items of the list */
void print_data_list(list_data_type *list, FILE *file)
{
   
	list_data_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_data_list(list, &aux))
	{	
		fprintf(file, "data List is Empty!!!");
		return;
	}

	/* Prints the First Object of the List*/
	print_data(aux->item, file);

	while (!is_last_data(aux, list))
	{
		go_next_data(list, &aux);
		print_data(aux->item, file);
	}
}

/* Print all the items of the list */
void print_data_list_final(list_data_type *list, FILE *file)
{
   
	list_data_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_data_list(list, &aux))
	{	
		fprintf(file, "data List is Empty!!!");
		return;
	}

	/* Prints the First Object of the List*/
	print_data_final(aux->item, file);

	while (!is_last_data(aux, list))
	{
		go_next_data(list, &aux);
		print_data_final(aux->item, file);
	}
}
