#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "FilterDev.h"
#include "constants.h"
#include "dimensions.h"
#include "procedures.h"

OutputPortHandler outputP_rr;
OutputPortHandler outputP_centroid;
char data_base_name[MAXFILENAME];
char output[MAXFILENAME];
int num_centroids = -1;
int num_dimensions = -1;


int initFilter(void *work, int worksize) {		
	
    get_information_from_work(work, data_base_name , output, &num_centroids , &num_dimensions);
 
    // gets the input and output handlers 
    outputP_rr = ahGetOutputPortByName("database_line_output");
    outputP_centroid = ahGetOutputPortByName("initial_centroids_output");
    
    if (outputP_rr < 0) {
 	fprintf(stderr, "Coud not open OutPort database_line_output");
	exit(0);
    }
	if (outputP_centroid < 0) {
 	fprintf(stderr, "Coud not open OutPort initial centroid");
	exit(0);
    }

    return 0;
}


int processFilter(void *work, int worksize) {
	// in case there is more than one instance of the distributor filter,
	// the others are immediately killed
	char error[400] ;
	int rank = ahGetMyRank();
	if (rank > 0)
		return 0;


	// to generate the first centroids more properly, some initial points of the database are read
	FILE *arquivo = fopen (data_base_name, "r");
	if (!arquivo)
	{
		ahExit("ERROR: database not found!");
	}
	float maximo=0.0, temp;
	char *line = (char *)malloc(MAXLINELEN);
	float randomCentroid;
	void *msg_centroid;
	float *msg_float;
	int msg_size = num_dimensions*sizeof(float)+1;
	int i,j;
	
	msg_centroid =malloc(msg_size);
	msg_float = (float *)msg_centroid;

	if (arquivo != NULL)
	{
		for (i=0; i<FIRSTPOINTS; i++)
		{
			fgets(line, MAXLINELEN*sizeof(char), arquivo); 
			if(!feof(arquivo))
			{
				sscanf(line, "%f", &temp);
				if (temp > maximo)
					maximo = temp;
			}
		}
	} 
	if (maximo < 0.00001)
		maximo = 1;
	
/*****************DEBUG******************************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
#endif
/********************************************************/


	// generates the centroids and add them to work
	for (i=0; i<num_centroids; i++) 
	{
		randomCentroid = maximo*((float)1/(num_centroids+1))*(i+1);
		for (j = 0;j<num_dimensions;j++)
		{
			msg_float[j] = randomCentroid;
		}
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Send CentroidInitial Msg: \n");
	print_vector_float(msg_float, num_dimensions, fp_log);
	fprintf(fp_log, "End CentroidInitial Msg \n");
	fclose(fp_log);
#endif		
/****************************************************************************/
	
		ahWriteBuffer(outputP_centroid, msg_centroid , msg_size);

	}

	fclose(arquivo);
	free(line);

	
//******************************************************************************

	FILE *database = fopen(data_base_name, "r");    
	char *file_line = (char *)malloc(MAXLINELEN);
	void *msg;
	char *msg_char;
	int szMsg;
	int temp_dimension = 0;
	int iterate = 0;

	// each line of the database file is distributed to one of the
	// readers (round robin policy)
	fgets(file_line, MAXLINELEN*sizeof(char), database); 
	while (!feof(database))
	{	

		szMsg = sizeof(char) * MAXLINELEN;
		msg = malloc(szMsg);
		msg_char = (char *)msg;
		memcpy(msg, file_line, strlen(file_line)+1);
			
		// checks for an empty line or a wrong number of dimensions
		temp_dimension = getNumDimensions(msg_char);
		if (temp_dimension != num_dimensions)
		{	 
			sprintf(error, "Error : number of dimensions mistake in \n LINE : %d Dimension Found : %d Dimension Informed : %d \n %s", iterate, temp_dimension, num_dimensions, msg_char);
			ahExit(error);
		}
		ahWriteBuffer(outputP_rr, msg , szMsg);
		
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Send Object Msg: \n");
	fprintf(fp_log , "%s", (char *)msg);	
	fprintf(fp_log, "End Object Msg \n");
	fclose(fp_log);
#endif		
/****************************************************************************/
		fgets(file_line, MAXLINELEN*sizeof(char), database);
		iterate++;
	}
	
	fclose(database);
	free(file_line);

	return 0;
}		


int finalizeFilter(void) {	
	//releaseOutputPort(outputP_rr); 
	return 0;
}
