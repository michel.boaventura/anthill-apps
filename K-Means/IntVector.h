#ifndef _INT_VECTOR_H
#define _INT_VECTOR_H

#include <stdio.h>

/**
 * \file IntVector.h Definition of data structures of a expansible int vector.
 */

#ifdef INT_VECTOR_SEND
#define WRITE_IV(outputFile, vet) packIntVector(vet)
#define READ_IV(inputFile) unpackIntVector()
#else
#define WRITE_IV(outputFile, vet) writeIntVector(outputFile, vet)
#define READ_IV(inputFile) readIntVector(inputFile)
#endif

/// Task Id List data structure
typedef struct {
	int size; ///< List size
	int capacity; ///< List capacity
	int *vetor; ///< List positions
} IntVector;

//int intVectorGetMinCpacity(int size);
IntVector *intVectorCreate(int cap_inicial);
int intVectorAdd(IntVector *vet, int element);
int intVectorGetSize(IntVector *vet);
int intVectorGet(IntVector *vet, int pos);
int intVectorSet(IntVector *vet, int pos, int value);
int intVectorGetLast(IntVector *vet);
int *intVectorToArray(IntVector *vet, int *vetSize);
int intVectorCompare(IntVector *vet1, IntVector *vet2);
void intVectorDestroy(IntVector *vet);
IntVector *intVectorIntersection(IntVector *a, IntVector *b);
IntVector *intVectorCopy(IntVector* vet);
void intVectorSortAscendig(IntVector *vet);

////////////// Funcs to write the IntVector to File/////////////////

int writeIntVector(FILE *outputFile, void *vet);
void *readIntVector(FILE *inputFile);

int packIntVector(void *vet);
void *unpackIntVector();

#endif
