#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "anthill.h"
#include "constants.h"


int main (int argc, char *argv[]) 
{

	char confFile[] = "./conf.xml";
	char option;
	int num_centroids = -1;
	int number_of_dimension=-1;
	char *input = NULL;
	char *output = NULL;
	char *str_num_centroids = NULL;
	char *str_num_dimension = NULL;

		
	if (argc != 9) 
	{
		printf ("Usage: ./main -k <# centroids> -d <number_dimension> -i <database> -o <output_file>\n");
		exit(1);
	}
	
	// loop to receive information from the command line
	while ((option = getopt ( argc, argv, "k:d:i:o:" )) != EOF) {
		switch (option) {
			case 'k':
				num_centroids = atoi(optarg);
				str_num_centroids = strdup(optarg);
				break;
			case 'd':
				number_of_dimension = atoi(optarg);
				str_num_dimension = strdup(optarg);
				break;
			case 'i':
				input = strdup(optarg);
				break;
			case 'o':
				output = strdup(optarg);
				break;
			default:
				// error message 
				printf ("\nError in the command line: \n   ./main -k <number_centroids> -d <number_dimension> -i <database> -o <output_file>\n\n");
				exit(1);
				break;
		}
	}

	if (num_centroids <= 0) 
	{
		printf ("Number of centroids invalid.\n");
		exit(1);
	}
	if (number_of_dimension <= 0) 
	{
		printf ("Number of dimension invalid.\n");
		exit(1);

	}
	/* 
	 * WORK
	 * 1. input file
	 * 2. output file
	 * 3. number of centroids
	 * 4. first coordinates
	 * */	
	char *work = (char *) malloc (strlen(input) +1/*blank*/+ strlen(output) + 1/*blank*/ + 
	strlen(str_num_centroids) +1/*blank*/ + strlen(str_num_dimension) + 21*num_centroids/*blank + 20-digit number*/ + 1/*'\0' in the end*/);
	sprintf(work, "%s %s %s %s", input, output, str_num_centroids, str_num_dimension);

	Layout *layout = initAh(confFile, argc, argv);
	appendWork(layout, (void *)work, strlen(work)+1);
	finalizeAh(layout);
	
	return 0;
}
