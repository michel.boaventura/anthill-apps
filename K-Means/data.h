#ifndef DATA_H
#define DATA_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include "constants.h"
#include "structs.h"

void free_data_cell(list_data_pointer q);
void print_data(data_type data, FILE *fp_log);
void free_data(data_type data);
void malloc_data(data_type *data);
int compare_datas(data_type data1, data_type data2);	
void print_data_final(data_type data, FILE *fp_log);

#endif
