/* Inclusoes. */
#include "data.h"

/* Procedure that free memory of a cell .
 * It not free the memory for the objects that the cell points */
void free_data_cell(list_data_pointer q)
{
/*   free_object(q->item);	
   free(q->item);*/
   
   free(q);
   		
}

/* Print an object */
void print_data(data_type data, FILE *fp_log)
{
	
	if (data.string!=NULL){
		fprintf(fp_log, "data string: %s  ", data.string);
		fprintf(fp_log, "data centroid: %d \n", data.centroid);

	}	
	else 
		fprintf(fp_log, "There isn't data\n");

	
}

void print_data_final(data_type data, FILE *fp_log)
{
	
	if (data.string!=NULL){
		fprintf(fp_log, "%s;%d\n", data.string, data.centroid);

	}	
	
}

/*Free some data*/
void free_data(data_type data)
{
	free(data.string);
}

/*malloc some data*/
void malloc_data(data_type *data)
{	
//	if (data->string == NULL)
		data->string=(char *)malloc(MAXLINELEN * sizeof(char)) + 1;
		data->centroid = 0;
		
}

/*Compare two Datas */
int compare_datas(data_type data1, data_type data2)
{	int aux;
	if (!strcmp(data1.string, data2.string))  
		aux = 1;
	
	else aux = 0;

	return aux;
}

