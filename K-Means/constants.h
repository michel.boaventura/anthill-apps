#ifndef _K_MEANS_CONSTANTES_H_
#define _K_MEANS_CONSTANTES_H_

#define MAXLINELEN 1000
#define MAXITER 100
#define FIRSTPOINTS 50
#define MAXFILENAME 40
#define SEPARATORS ",;"
//#define DEBUG
#define TIME
#endif
