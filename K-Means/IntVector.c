#include <stdio.h>
#include <stdlib.h>
#include "IntVector.h"
#include "constants.h"


#ifdef INT_VECTOR_SEND
#define TO_NETWORK
#endif
#include "cser.h"


#ifndef INT_VECTOR_SEND

/**
 * \file vetor_doc.c Vetor de documentos com realoca??o exponencial. Sempre que 
 * n?o houver espa?o para um novo documento, o vetor ? realocado para ter o 
 * dobro do tamanho. Assim n?o corre o risco de copiar o vetor todo para outro
 * lugar a cada inser??o. Se o vetor crescer de 2^0 at? 2^n, ent?o ter?o sido 
 * feitas no m?ximo: uma copia de 1 posi??o, uma de 2, uma de 4, uma de 8,..., 
 * uma de 2^n-1 o que equivale a uma c?pia de (2^n)-1 posi??es (somat?rio de
 * 2^0 at? 2^n-1 = (2^n)-1 ). Sendo assim em m?dia cada posi??o do vetor ? 
 * copiada apenas 1 vez, ou seja a complexidade de inser??o ? O(1). 
 *
 * O desperd?cio de mem?ria dessa abordagem pode ser estimado da seguinte forma:
 * no pior caso, o vetor utiliza o dobro do espa?o necess?rio e no melhor n?o 
 * desperdi?a nada. Ent?o o desperd?cio na m?dia ? de 50%.
 */

//-----------------------------------------------------------------------------
/// Retorna a capacidade m?nima (de acordo com a pol?tica de 
/// realoca??o exponencial) necess?ria para armazenar um vetor de tamanho size.
//-----------------------------------------------------------------------------
int intVectorGetMinCpacity(int size){
	int cap;

	cap=1;
	for (cap=1; cap<size; cap=cap<<1);
	return cap;
}

//-----------------------------------------------------------------------------
/// Creates a vector
//-----------------------------------------------------------------------------
IntVector *intVectorCreate(int cap_inicial) {
	IntVector *vet = (IntVector *)malloc(sizeof(IntVector));

	vet->size     = 0;
	vet->capacity = intVectorGetMinCpacity(cap_inicial);
	vet->vetor    = (int *)calloc(vet->capacity, sizeof(int));

	return vet;
}

//-----------------------------------------------------------------------------
/** Adiciona um elemento ao vetor. Caso o vetor nao tenha a capacidade
 * necessaria, sua capacidade sera dobrada (de acordo com a politica de 
 * realocacao exponencial).
 * 	\param vet Vetor onde o elemento sera acrescentado.
 * 	\param element Elemento a ser acrescentado.
 * 	\return Posicao do elemento no vetor.
 */ 
//-----------------------------------------------------------------------------
int intVectorAdd(IntVector *vet, int element) {
	int ret = 0;
	
	// Se vai estourar capacidade, aumenta vetor (aumento exponencial)
	if (vet->size+1 > vet->capacity) {
		vet->capacity = vet->capacity << 1;
		vet->vetor = (int *)realloc(vet->vetor, sizeof(int)*vet->capacity);
	}

	vet->vetor[vet->size]  = element;

	ret = vet->size;
	vet->size++;

	return ret;
}

//-----------------------------------------------------------------------------
/// Returns the size of a vector
//-----------------------------------------------------------------------------
int intVectorGetSize(IntVector *vet) {
	if (vet == NULL) {
		return 0;
	}
	return vet->size;
}

//-----------------------------------------------------------------------------
/// Returns the position "pos" of the vector "vet"
//-----------------------------------------------------------------------------
int intVectorGet(IntVector *vet, int pos) {
	return ((vet->vetor[pos]) );
}

//-----------------------------------------------------------------------------
/// Sets the value the position "pos" of the vector "vet" to "value".
//-----------------------------------------------------------------------------
int intVectorSet(IntVector *vet, int pos, int value) {
	if (pos >= vet->size) {
		return -1;
	}
	
	vet->vetor[pos] = value;
	return 0;
}

//-----------------------------------------------------------------------------
/// Returns the last position of vector "vet".
//-----------------------------------------------------------------------------
int intVectorGetLast(IntVector *vet) {
	if (vet->size > 0) {
	  	return ((vet->vetor[vet->size-1]) );
	} else {
		return 0;
	}
}

int *intVectorToArray(IntVector *vet, int *vetSize) {
	int size = vet->size;
	int *ret = NULL;
	
	if (size > 0) {
		ret = (int *)calloc(size, sizeof(int));	
		memcpy(ret, vet->vetor, sizeof(int)*size);
	}
	
	*vetSize = size;
	return ret;
}


//-----------------------------------------------------------------------------
/// Compares two IntVectors. Returns -1, 0 or +1 if the vector "vet1" is found,
/// respectivelly, to be less than, to match, or be greater than "vet2". 
/// Comparison semantics is the same of strcmp().
//-----------------------------------------------------------------------------
int intVectorCompare(IntVector *vet1, IntVector *vet2) {
	int i=0;
	
	if (vet1->size < vet2->size) {
		for (i=0; i<vet1->size; i++) {
			if (vet1->vetor[i] > vet2->vetor[i]) {
				return 1;
			} else if (vet1->vetor[i] < vet2->vetor[i]) {
				return -1;
			}
		}
		// as vet2 has elements that vet1 hasn't, vet 2 is bigger
		return -1;
	} else if (vet1->size > vet2->size) {
		for (i=0; i<vet2->size; i++) {
			if (vet1->vetor[i] > vet2->vetor[i]) {
				return 1;
			} else if (vet1->vetor[i] < vet2->vetor[i]) {
				return -1;
			}
		}
		// as vet1 has elements that vet2 hasn't, vet 1 is bigger
		return 1;
	} else { // same size
		for (i=0; i<vet1->size; i++) {
			if (vet1->vetor[i] > vet2->vetor[i]) {
				return 1;
			} else if (vet1->vetor[i] < vet2->vetor[i]) {
				return -1;
			}
		}
		return 0;
	}
}


//-----------------------------------------------------------------------------
/// Destroy a vector
//-----------------------------------------------------------------------------
void intVectorDestroy(IntVector *vet) {
	if (vet != NULL) {
		if (vet->vetor != NULL) {
			free(vet->vetor);
		}
		free(vet);
	}
}


IntVector *intVectorCopy(IntVector* vet){
	if (vet == NULL) {
		return NULL;
	}
	
	int i, value, size = intVectorGetSize(vet);
	IntVector *auxList = intVectorCreate(size);
	
	for(i = 0; i < size; i++){
		value = intVectorGet(vet, i);
		intVectorAdd(auxList, value);
	}
	return auxList;
}


/// This function assume that 
IntVector *intVectorIntersection(IntVector *a, IntVector *b) {
	int i,j, resultSize=0;

	if(a->size < b->size) {
		resultSize = a->size;
	} else {
		resultSize = b->size;
	}
	IntVector *result = (IntVector *)intVectorCreate(resultSize);
	
	for(i=0,j=0; i < a->size && j < b->size;) {
		if(a->vetor[i] > b->vetor[j]) {
			j++;
		} else if(a->vetor[i]==b->vetor[j]) {
			intVectorAdd(result, a->vetor[i]);
			j++;
			i++;
		} else {
			i++;
		}
	}
	
	// Reduce the capacity of the vector only to the needed
	if(result->size > 0) {
		result->vetor=(int*)realloc(result->vetor,sizeof(int)*result->size);
		result->capacity = result->size;
	} else {
		free(result->vetor);
		result->vetor = NULL;
		result->capacity = 0;
	}

	return result;
}

/// Comparison function for sorting task ids in ascendig order
static int compareInts(const void *a, const void *b) {
	const int* i=(const int*)a;
	const int* j=(const int*)b;

	if (*i > *j) return(1);
	if (*i < *j) return(-1);
	return 0;
}

/// Sort the vector elements in ascending order
void intVectorSortAscendig(IntVector *vet) {
	if (vet->size > 1) {
		qsort(vet->vetor, vet->size, sizeof(int), &compareInts);
	}
}


#endif


////////////// Funcs to write the IntVector to File/////////////////
int WRITE_IV(FILE *outputFile, void *vet){
	int i, element, vetSize = intVectorGetSize((IntVector *)vet);

	WRITE_NUM(outputFile, "vetSize", vetSize);
	
	for(i = 0; i < vetSize; i++){
		element = intVectorGet((IntVector *)vet, i);
		
		WRITE_NUM(outputFile, "element", element);
	}
	
	return 1;
}

void *READ_IV(FILE *inputFile){
	int i, element, vetSize = -1;
	IntVector *vet = NULL;

	READ_BEGIN(inputFile);
		
	// Read data size from disk
	READ_NUM("vetSize", vetSize);
	if(vetSize == -1)fprintf(stderr,"Error: Reading vector Size\n");

	vet = intVectorCreate(vetSize); 
		
	for(i = 0; i< vetSize; i++){
		element = -2;
		READ_NUM("element", element);
		if(element == -1){
			fprintf(stderr,"Error: Reading vector\n");
			return NULL;
		}
		intVectorAdd(vet, element);


	}
	READ_END
	return ((void *) vet);
}

