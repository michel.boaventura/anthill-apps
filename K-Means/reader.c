#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include "FilterDev.h"
#include "constants.h"
#include "IntVector.h"
#include "dimensions.h"
#include "procedures.h"
#include "structs.h"
#include "data.h"
#include "memory_data.h"
#include "message_data.h"

InputPortHandler inputP = -1; ///< input port that receives the global centroids from filter(s) centroid
InputPortHandler initial_inputP = -1;	///< input port that receives the lines of the database from filter distributor
InputPortHandler inputP_centroid = -1;
OutputPortHandler outputP = -1;		///< output port that sends the local centroids to the filter(s) centroid
OutputPortHandler final_outputP = -1;		///< output port that sends the final centroids to the filter final
OutputPortHandler id_outputP = -1;    /// < used to send the final id's tuplas to the fiter final

/// Matrix filled with the coordinates of the global centroids
/// received, where each line correspond to a global centroid
float **centroids_coordinates = NULL;

/// Matrix filled with the coordinates of the new local centroids
/// dertemined from the local points associtation
float **new_centroids = NULL;			

float *temp_point = NULL;	///< temporary point

/// Array filled with the weight of each centroid, that is, the
/// number of points that were associated to each one
int *centroids_weights = NULL;			

int num_centroids = 0;	///< number of centroids
int num_dimensions = 0;

char input_file_name[MAXFILENAME];
char output[MAXFILENAME];
char *outputlabel = NULL;

list_data_type memory_database;


int initFilter(void *work, int worksize) {

	char error[100] = "Coud not open Port";
	// gets the input and output handlers
	initial_inputP = ahGetInputPortByName("database_line_input"); 
	inputP = ahGetInputPortByName("global_centroid_input"); 
	inputP_centroid = ahGetInputPortByName("initial_centroid_input"); 
	outputP = ahGetOutputPortByName("local_centroid_output");
	final_outputP = ahGetOutputPortByName("final_centroids_output");
	id_outputP = ahGetOutputPortByName("final_id_output");

	/*if (initial_inputP < 0) {
		ahExit("Coud not open Input Port database_line_input");
	}*/
	if (inputP < 0) {
		ahExit(error);
	}
	if (inputP_centroid < 0) {
		ahExit(error);
	}
	if (outputP < 0) {
		ahExit(error);
	}
	if (final_outputP < 0) {
		ahExit(error);
	}
	if (id_outputP < 0) {
		ahExit(error);
	}


	get_information_from_work(work, input_file_name, output, &num_centroids , &num_dimensions);

	
	return 0;
}


int processFilter(void *work, int worksize) {
	
	int i=0, j=0, k=0, id=0;
	int associated_centroid_id = -1;	
	int current_point=0;
	int changes=0;
	unsigned int bufSz = sizeof(char) * MAXLINELEN;
	int local_database_points = 0;	///< Corresponds to the current database point that is being analized
	//int rank = ahGetMyRank(); // rank of this instance
	IntVector *local_points_association = intVectorCreate(1024); ///< Each position of this array corresponds to a point of the local database and is filled with the number of the centroid that the point was associated	
	IntVector *local_id_points = intVectorCreate(1024); ///< Each position of this array corresponds to a point of the loca database and is filled with the id number point
	
	char *file_line = (char *)malloc(bufSz+1);	
	file_line[bufSz] = '\0';

	
 /*Receive the firts centroids*/

	centroids_coordinates = malloc_matrix_float(num_centroids,num_dimensions);
	new_centroids =  malloc_matrix_float(num_centroids,num_dimensions);

	void *msg_centroid;
	float *msg_float;
	int msg_centroid_size = num_dimensions*sizeof(float)+1;
	
	msg_centroid = malloc(msg_centroid_size);
	msg_float = (float *)msg_centroid;
	

/*****************DEBUG******************************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
#endif
/********************************************************/

#ifdef TIME
	struct timeval initial_time, final_time;
	FILE *fp_time;
	char error_msg[100];
	char fp_time_name[100];
	sprintf(fp_time_name, "Filter_%s_Instance_%d.time", ahGetFilterName(), ahGetMyRank());
	// Clean the files of times
	fp_time = fopen(fp_time_name,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);
#endif

#ifdef TIME 
	gettimeofday(&initial_time, NULL);
#endif 	
	
for (i=0; i<num_centroids; i++) 
	{
		ahReadBuffer (inputP_centroid, msg_centroid, msg_centroid_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Receiving CentroidInitial Msg: \n");
	print_vector_float(msg_float, num_dimensions, fp_log);
	fprintf(fp_log, "End CentroidInitial Msg \n");
	fclose(fp_log);
#endif		
/****************************************************************************/

		for (j = 0;j<num_dimensions;j++)
		{
			 centroids_coordinates[i][j] = msg_float[j];
			 new_centroids[i][j] = msg_float[j];
		}	

	}


	
	temp_point = (float *) malloc(num_dimensions * sizeof(float));
	centroids_weights = (int *)calloc(num_centroids, sizeof(int));
	void *msg_line;
	int msg_size = sizeof(char) * MAXLINELEN + 1 ;
	msg_line = malloc(msg_size);
	char *temp_string=NULL;	

	//variables data_type
	data_type data_object;
	list_data_pointer data_pointer;
	
	create_empty_data_list(&memory_database);

	// Receiving the local database
	while(ahReadBuffer(initial_inputP, msg_line, msg_size) != EOW)
	{
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "Receiving Object Msg: \n");
	fprintf(fp_log, "%s", (char *)msg_line);
	fprintf(fp_log, "Object Msg \n");
	fclose(fp_log);
#endif		
/****************************************************************************/

		float distance = 0;
		float minimum_distance = 0;
		file_line = (char *)msg_line;
		malloc_data(&data_object);
		strcpy(data_object.string , file_line);
		insert_in_data_list(data_object, &memory_database);

		// reads each point
		// Frist read is the ID of the point
		temp_string = strtok(file_line, SEPARATORS);

		if (num_dimensions > 0)
		{
			//Add new id in the variable ID's 
			id = atoi(temp_string); 
			//printf("Point %d ID = %d \n", current_point ,id );
			
			
			// Read coordinates of the point
			for (i = 0; i < num_dimensions; i++) 
			{
				temp_string = strtok(NULL, SEPARATORS);
				temp_point[i] = atof(temp_string); 
				//printf("Point %d Float = %f \n", current_point ,temp_point[i] );

			}
		}

		// association of the point with the minimum distance centroid
		minimum_distance = 0;
		associated_centroid_id = 0;
		if (num_dimensions > 0)
		{	
			for(j=0; j<num_dimensions; j++) 
			{
				minimum_distance += (temp_point[j] - centroids_coordinates[0][j]) * (temp_point[j] - centroids_coordinates[0][j]);
			}
		}

		for (i=1; i<num_centroids; i++) 
		{
			// calculates the distance from the current point
			// to each of the centroids
			distance = 0;
			if (num_dimensions > 0) 	{
				for(j=0; j<num_dimensions; j++) 
				{
					distance += (temp_point[j] - centroids_coordinates[i][j]) * (temp_point[j] - centroids_coordinates[i][j]);
				}
			}
				
			// gets the minimum distance centroid identification 
			if (distance < minimum_distance) 
			{
				minimum_distance = distance;
				associated_centroid_id = i;
			}
		}

		// compares the point's previous associated centroid with the point's
		// current associated centroid to verify changes
		int pos = intVectorAdd(local_points_association, associated_centroid_id);
		assert(pos == current_point);
		assert(pos == local_database_points);
		changes++;
		
		//Add the variable id in the struct IntVector
		int idpos = intVectorAdd(local_id_points, id);
		//printf("IDDDDD TUPLAVECTOR   %d  \n",id);
		assert(idpos == current_point);
		assert(idpos == local_database_points);

		
				
		// the calculus of the new centroid's coordinates is based on the average
		// of its associated points, therefore, when we add a new point to the 
		// centroid's set, the value of its coordinate is the average of the 
		// already associated points times the number of these points (weight),
		// plus the value of the new associated point's coordinate. Then, divide 
		// the result by the weight plus one.
		if (num_dimensions > 0)
		{
			for (j=0; j<num_dimensions; j++) 
			{
				float single_centroid_weight = (float) centroids_weights[associated_centroid_id];
				new_centroids[associated_centroid_id][j] = 
					(single_centroid_weight*new_centroids[associated_centroid_id][j] + temp_point[j]) 
					/ (single_centroid_weight + 1.0);
			}
		}
		centroids_weights[associated_centroid_id]++;
		current_point++;
		local_database_points++;

	
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "MEMORY_DATABASE: \n");
	print_data_list(&memory_database,fp_log);
	fprintf(fp_log, "%s", (char *)msg_line);
	fprintf(fp_log, "END MEMORY_DATABASE \n");
	fclose(fp_log);
#endif		
/****************************************************************************/

	// sends the message filled with each of the new centroids's coordinates and their 
	// respective weights
	for (i=0;i<num_centroids;i++)
	{
		int szMsg = sizeof(int)*5 + sizeof(float)*num_dimensions;	
		int *local_centroid = (int *)malloc(szMsg);
	 
		// message's format:
		// 0: total readers (int)
		// 1: centroid's id (int)
		// 2: centroid's weight (int)
		// 3: number of dimensions (int)
		// 4: changes of the centroid's coordinates (int)
		// 5: coordinate1 (float)
		// 6: coordinate2 (float)
		// ...
		local_centroid[0] = ahGetTotalInstances();
		local_centroid[1] = i;
		local_centroid[2] = centroids_weights[i];
		local_centroid[3] = num_dimensions;
		local_centroid[4] = changes;

		if (num_dimensions > 0)
		{
			float *coordenadas = (float *) &(local_centroid[5]);
			for (j=0; j<num_dimensions; j++)
			{
				coordenadas[j] = new_centroids[i][j];
			}
		}
		
		ahWriteBuffer(outputP, local_centroid, szMsg);
	}

	int iter = 0;
        

	iter++;
	unsigned int szRecvMsg = 2*sizeof(int) + sizeof(float)*num_dimensions;
	int *buf = (int *)malloc(szRecvMsg);
	int iterationChanges = 0;
	while (1) 
	{
		int empty_buffer = 0;
		for (i=0;i<num_centroids;i++) 
		{
			int ret = ahReadBuffer(inputP, buf, szRecvMsg);
			if (ret == EOW) {
				empty_buffer = 1;
				break;
			} else {
				int centroidId = buf[0];
				iterationChanges += buf[1];
				if (num_dimensions > 0)
				{
					float *current_centoid_coordinate = (float *) &(buf[2]);
					for (j=0;j<num_dimensions;j++) 
					{
						centroids_coordinates[centroidId][j] = current_centoid_coordinate[j];
					}
				}
			}
		}

		if (empty_buffer)
			break;

		// fill the local centroid's weights with zero 
		for(i=0;i<num_centroids;i++)
		 	centroids_weights[i] = 0;                                         
		

		get_first_in_data_list(&memory_database, &data_pointer);
		// reads the local database
		file_line = strdup(data_pointer->item.string);
	
		current_point = 0;
		changes = 0;
		for (k=0;k<local_database_points;k++)
		{
			float distance = 0;
			float minimum_distance = 0;
						
			// reads each point
			temp_string = strtok((file_line), SEPARATORS);
			if (num_dimensions > 0)
			{
				//Add new id in the struct ID's 
				id = atoi(temp_string); 
				//printf("Point %d ID = %d \n", current_point ,id );
				
				for (i = 0; i < num_dimensions; i++) 
				{
					temp_string = strtok(NULL, SEPARATORS);
					temp_point[i] = atof(temp_string); 
					//printf("Point %d Float = %f \n", current_point ,temp_point[i] );

				}
			}

			// association of the point with the minimum distance centroid
			minimum_distance = 0;
			associated_centroid_id = 0;
			if (num_dimensions > 0)
			{
				for(j=0; j<num_dimensions; j++) 
				{
					minimum_distance += (temp_point[j] - centroids_coordinates[0][j]) * 
						(temp_point[j] - centroids_coordinates[0][j]);
				}
			}
			
			for (i=1; i<num_centroids; i++) 
			{
				// calculates the distance from the current point
				// to each of the centroids
				distance = 0;
				if (num_dimensions > 0)
				{
					for(j=0; j<num_dimensions; j++) 
					{
						distance += (temp_point[j] - centroids_coordinates[i][j]) * 
							(temp_point[j] - centroids_coordinates[i][j]);
					}
				}

				// gets the minimum distance centroid identification 
				if (distance < minimum_distance) 
				{
					minimum_distance = distance;
					associated_centroid_id = i;
				}
			}

			// compares the point's previous associated centroid with the point's
			// current associated centroid to verify changes	                
			if (intVectorGet(local_points_association,current_point) != associated_centroid_id) {
				int ret = intVectorSet(local_points_association, current_point, associated_centroid_id);
				assert(ret == 0);
				changes++;                                                                            
			}
			                                                                                     
			// the calculus of the new centroid's coordinates is based on the average
			// of its associated points, therefore, when we add a new point to the 
			// centroid's set, the value of its coordinate is the average of the 
			// already associated points times the number of these points (weight),
			// plus the value of the new associated point's coordinate. Then, divide 
			// the result by the weight plus one.
			if (num_dimensions > 0)
			{
				for (j=0; j<num_dimensions; j++) {
					float single_centroid_weight = (float)centroids_weights[associated_centroid_id];
					new_centroids[associated_centroid_id][j] = 
						(single_centroid_weight*new_centroids[associated_centroid_id][j] + temp_point[j]) 
						/ (single_centroid_weight + 1.0);
				}
			}
			
			centroids_weights[associated_centroid_id]++;

			free (file_line);
			if (k != (local_database_points-1))
				go_next_data(&memory_database, &data_pointer);
				file_line = strdup(data_pointer->item.string);

	     		current_point++;
		}		

		// sends the message filled with each of the new centroids's coordinates and their 
		// respective weights
		for (i=0;i<num_centroids;i++)
		{
			int szMsg = sizeof(int)*5 + sizeof(float)*num_dimensions;
			int *local_centroid = (int *)malloc(szMsg);

			// message's format:
			// 0: total readers (int)
			// 1: centroid's id (int)
			// 2: centroid's weight (int)
			// 3: number of dimensions (int)
			// 4: changes of the centroid's coordinates (int)
			// 5: coordinate1 (float)
			// 6: coordinate2 (float)
			// ...
			local_centroid[0] = ahGetTotalInstances();
			local_centroid[1] = i;
			local_centroid[2] = centroids_weights[i];
			local_centroid[3] = num_dimensions;
			local_centroid[4] = changes;			

			if (num_dimensions > 0)
			{
				float *coordenadas = (float *) &(local_centroid[5]);
				for (j=0; j<num_dimensions; j++)
				{
					coordenadas[j] = new_centroids[i][j];
				}
			}
	
			ahWriteBuffer(outputP, local_centroid, szMsg);
		}
		
		// If there isn't changes in this iteration or we reached maximun iterations, finish
		if ((iterationChanges == 0) || (iter == MAXITER)) break;
		iter++;
		iterationChanges = 0;
	}
	free(buf);



	//send the message to final filter of the program

	list_data_pointer aux;
	i=0;
	if (!get_first_in_data_list(&memory_database, &aux)) return 0;
	aux->item.centroid = intVectorGet(local_points_association,i);
	send_data(&(aux->item), id_outputP);
	i++;
	while (!is_last_data(aux, &memory_database))
	{
		go_next_data(&memory_database, &aux);
		aux->item.centroid = intVectorGet(local_points_association,i);
		send_data(&(aux->item), id_outputP);
		i++;
	}
	
	//send the message to final filter of the program
	
	
/*	int szMsgid = sizeof(int)*2*local_database_points;
	int *msgid = (int *)malloc(szMsgid);
	
	* message's of centroids
	 * 0 - number of id centroid
	 * 1 - number of id tupla 1
	 * 2 - number of id centroid 
	 * 3 - number of id tupla 2
	 * ...
	 *
	if (num_dimensions > 0)
	{       

                int aux = 0;  
		for (i=0;i<local_database_points;i++)
		{
			msgid[aux] = intVectorGet(local_points_association,i);
			msgid[aux+1] = intVectorGet(local_id_points,i);
			printf("Vector IDCENTROID=%d   IDTUPLA=%d \n",msgid[aux],msgid[aux+1]  );
			aux = aux +2;

		}
	}
        
	ahWriteBuffer(id_outputP, msgid, szMsgid);

	if (rank > 0)
	        return 0;
	          
	// sends the message to the final filter of the program
	int szMsg = sizeof(int)*3 + sizeof(float)*num_dimensions*num_centroids;
	int *msg = (int *)malloc(szMsg);
	*
	 * message's format:
	 * 0 - number of centroids
	 * 1 - number of dimensions
	 * 2 - number of points of the local database
	 * 3 - coordinate 1 of centroid 0
	 * 4 - ...
	 *
	msg[0] = num_centroids;
	msg[1] = num_dimensions;
	msg[2] = local_database_points;
	if (num_dimensions > 0)
	{
		float *temp = (float *) &(msg[3]);
		for (i=0;i<num_centroids;i++)
		{
			for(j=0;j<num_dimensions;j++)
			{
				temp[i*num_dimensions+j]=centroids_coordinates[i][j];
			}
		}
	}

	ahWriteBuffer(final_outputP, msg, szMsg);


*/
#ifdef TIME
	gettimeofday(&final_time, NULL);
	fp_time = fopen(fp_time_name,"a");
	print_time(fp_time, &initial_time, &final_time);
	fclose(fp_time);
#endif
	return 0;
}


int finalizeFilter(void)
{
	int i;

    //releaseInputPort(inputP);
    //releaseOutputPort(outputP);
    //releaseOutputPort(final_outputP);

    for (i=0; i<num_centroids; i++)
        free(centroids_coordinates[i]);
    free(centroids_coordinates);

    for (i=0; i<num_centroids; i++)
        free(new_centroids[i]);
    free(new_centroids);


    free(temp_point);

    return 0;
}
