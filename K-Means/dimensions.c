#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include "dimensions.h"


// gets the number of dimensions of the database points
int getNumDimensions(char *line) 
{
	int dimensions = 0;
	//char * temp_string = malloc (sizeof(char)*(strlen(line)+1));
	//strncpy(temp_string, line, strlen(line)+1);
	char *temp_string = strdup(line);
		
	char *tok = strtok(temp_string, SEPARATORS);
	while (tok != NULL) {
		dimensions++; 
		tok = strtok(NULL, SEPARATORS);
	}

	free(temp_string);
	return dimensions-1;
}
