#include "message_data.h"

/*Fill the data in message. */
int fill_data_in_message(void **msg, data_type data)
{
	int *msg_int;
	char *msg_char;
	
	if (data.string==NULL) return 0;

	int size=MAXLINELEN * sizeof(char)+ sizeof(int) + 1;
	*msg=malloc(size);
	msg_int=*msg;
	msg_int[0] = data.centroid;
	msg_char=(char *)&(msg_int[1]);
	strcpy(msg_char, data.string);

	return size;
}


/* Send a data to the FINAL filter */
int send_data(data_type *data,  OutputPortHandler port)
{
	int msg_size;

	void *msg;
/*****************DEBUG******************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/
	
	/* Fill the values on the message */ 
	if (data->string==NULL) return 0;
	
	msg_size = fill_data_in_message(&msg, (*data));

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "SENDING DATA: \n");
	print_data(*data, fp_log);
	fprintf(fp_log, "END OF DATA\n");
	fclose(fp_log);
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}

/*Send a data list to final filter*/
int send_data_list(list_data_type *list, OutputPortHandler port)
{  
	list_data_pointer aux;
	
	/* If the first item is not found, it justs returns */
	if (!get_first_in_data_list(list, &aux))
		return 0;
	

	/* Send the first object of the List*/
	send_data(&(aux->item),  port);

	while (!is_last_data(aux, list))
	{
		go_next_data(list, &aux);
		send_data(&(aux->item), port);
	}
	return 1;
}


/* Get the data type from the message. */ 
int get_data_from_msg(data_type *data, void *msg)
{
	int *msg_int;
	char *msg_char;
	
	//if (data->string==NULL)
	malloc_data(data);
	
	/* read the data centroid number from the message */
	msg_int=(int *)msg;
	(*data).centroid=((int *)msg)[0];
	
	msg_char=(char *)&(msg_int[1]);

	strcpy(data->string, msg_char);
		
	return 1;
}
/*Receive all data messages. Usualy its used by Final Filter */
int receive_data(list_data_type *list, InputPortHandler input_port)
{
	int msg_size= MAXLINELEN*sizeof(char) + sizeof(int) + 1;	// size of the mesage that will be received 
	void *msg;
	data_type data;
	msg=malloc(msg_size);
	
	while (ahReadBuffer(input_port, msg, msg_size)!=EOW)
	{
/***********************DEBUG************************************************/
	#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_%s_%d.txt", ahGetFilterName(), ahGetMyRank()); 
	#endif
/********************************************************/
		get_data_from_msg(&data,msg);


/********************************** DEBUG ***********************************/
#ifdef DEBUG
	fp_log = fopen(name_of_fp_log,"a");
	fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
	fprintf(fp_log, "RECEIVING DATA: \n");
	print_data(data, fp_log);
	fprintf(fp_log, "END OF DATA\n");
	fclose(fp_log);
#endif		
/****************************************************************************/
		insert_in_data_list(data, list);

	}

	free(msg);
	return 1;
}
