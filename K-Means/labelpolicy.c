// Functions used in the lbstream policy

#include <stdlib.h>
#include <stdio.h>

//extern "C" {
void getLabel(void *msg, int size, char *label);
//}

/* This function assures that each different centroid
 * will be sent to their respective receptor*/
void getLabel(void *msg, int size, char *label)
{	
	// array[1] constains the id of the centroid
	// sent in the current message
	int *array = (int *) msg;
	sprintf(label, "%d", array[1]);
}


/* Makes sure that the first centroids will be sent to the
 * receptor with the first ranks. */
/*int hash (char *label, int image)
{
	int value = atoi(label);
	value = value%image;
	return value;
}
*/

