#ifndef PROCEDURES_H
#define PROCEDURES_H
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include "anthill.h"
#include "constants.h"

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, int *num_centroids, int *number_of_dimensions);
void print_work(char *input_file_name, char *output_file_name,int num_centroids, int number_of_dimensions, FILE *file );
float** malloc_matrix_float(int lines, int coluns);
void print_matrix_float(float **matrix,int lines, int coluns, FILE *file );
void print_vector_float(float *vector, int len, FILE *file ); 
int print_time(FILE *file, struct timeval *start, struct timeval *end);

#endif
