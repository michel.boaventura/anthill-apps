#ifndef MEMORY_DATA_H
#define MEMORY_DATA_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include "data.h"
#include "structs.h"

list_data_pointer alloc_data_cell();
void create_empty_data_list(list_data_type *list);
int is_empty_data_list(list_data_type *list);
void insert_in_data_list(data_type item, list_data_type *list);
data_type remove_first_of_data_list(list_data_type *list);
data_type remove_of_data_list(list_data_pointer *p, list_data_type *list);
int get_first_in_data_list(list_data_type *list, list_data_pointer *pointer);
int go_next_data(list_data_type *list, list_data_pointer *pointer);
int is_last_data(list_data_pointer p, list_data_type *list);
list_data_pointer search_item_in_data_list(data_type item, list_data_type *list);
void make_data_list_empty(list_data_type *list);
void destroy_data_list(list_data_type *list);
void print_data_list(list_data_type *list, FILE *file);
void print_data_list_final(list_data_type *list, FILE *file);

#endif
