#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include "FilterDev.h"
#include "constants.h"
#include "procedures.h"
#include "message_data.h"
#include "data.h"
#include "memory_data.h"
#include "structs.h"

InputPortHandler inputP = -1;
InputPortHandler id_inputP = -1;

char input_file[MAXFILENAME];
char file_name[MAXFILENAME];
int num_centroids = 0;
int num_dimensions=-1;

int initFilter(void *work, int worksize) {	

	char error[100] = "Coud not open Port";
	get_information_from_work(work, input_file, file_name, &num_centroids , &num_dimensions);

	// gets the input port handler 
	inputP = ahGetInputPortByName("final_centroids_input"); 
	id_inputP = ahGetInputPortByName("final_id_input");

	if (inputP < 0) {
		ahExit(error);
	}
	if (id_inputP < 0) {
		ahExit(error);
	}


	return 0;
}


int processFilter(void *work, int worksize) {

	list_data_type database;
	//int first_local_points;
	//float *coordinates = NULL;
	//int i,j;

	int rank = ahGetMyRank();
	if (rank > 0)
		return 0;

	FILE *file = fopen(file_name, "w");	
	if (file == NULL)
		perror("\nCould not open output file.\n");


	/*unsigned int szMsg = 3*sizeof(int) + sizeof(float)*num_dimensions*num_centroids;
	int *buf = (int *)malloc(szMsg);
	ahReadBuffer(inputP, buf, szMsg);
	*
	 * Message received format:
	 * 0 - number of centroids
	 * 1 - number of dimensions
	 * 2 - number of points from the first local database
	 * 3 - coordinate 1 of centroid 0
	 * 4 - ...
	 *
	num_centroids = buf[0];
	num_dimensions = buf[1];
	first_local_points = buf[2];

	if (first_local_points > 0) {
		coordinates = (float *)&(buf[3]);
		for (i=0; i<num_centroids; i++)
		{
			for (j=0; j<num_dimensions; j++)
			{
				fprintf(file, "%f ", coordinates[i*num_dimensions + j]);
			}
			fprintf(file, "\n");
		}
	} else {
		fprintf(file, "Database file empty. \n");
	}

	free(buf);


	unsigned int szMsgid = 2*sizeof(int)*first_local_points; 
	int *bufid = (int *)malloc(szMsgid);

        while(ahReadBuffer(id_inputP, bufid, szMsgid)!=EOW){	
	
		* message's of centroids
		 * 0 - number of id centroid
		 * 1 - number of id tupla 1
		 * 2 - number of id centroid 
		 * 3 - number of id tupla 2
		 * ...
		 *

		printf("TOTAL POINTS=%d \n",first_local_points);
		for (i=0; i<2*first_local_points;i++)
		{    		fprintf(file, "%d %d \n", bufid[i],bufid[i+1]);
			i++;
	
		}
		
	}
	
	free(bufid);
	*/
	
	create_empty_data_list(&database);
	receive_data(&database, id_inputP);
	print_data_list_final(&database, file);
	fclose(file);
	return 0;
}


int finalizeFilter(void)
{
	//releaseInputPort(inputP);
//	free(file_name);

	return 0;
}
