#ifndef STRUCTS_H_
#define STRUCTS_H_

/*Definitions data */
 typedef struct{
 	char* string;
	int centroid;
 }data_type;

typedef struct data_cell
{
   data_type item;
   struct data_cell *previous;
   struct data_cell *next;

}data_cell, *list_data_pointer;

typedef struct
{
   list_data_pointer first;
   list_data_pointer last;
   int length;
}list_data_type;

#endif
