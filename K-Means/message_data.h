#ifndef MESSAGE_DATA_H
#define MESSAGE_DATA_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include "structs.h"
#include "data.h"
#include "memory_data.h"
#include "anthill.h"

int fill_data_in_message(void **msg, data_type data);
int send_data(data_type *data,  OutputPortHandler port);
int send_data_list(list_data_type *list, OutputPortHandler port);
int receive_data(list_data_type *list, InputPortHandler input_port);
int get_data_from_msg(data_type *data, void *msg);

#endif
