#include <stdlib.h>
#Calcula o intervalo de confiança de cada linha colocando em cada coluna o seu valor
#O intervalo de confiança é calculado considerando-se uma confiança de 95%
{
	media=0.0;
	if (NR != 1)
	{
		printf("\t");
	}
	for (i = NF; i > 0; --i)
	{
		media+=($i); 
	} 
	media/=NF;
	variancia_amostra=0.0;
	for (i = NF; i > 0; --i)
	{
	   variancia_amostra+=($i-media)*($i-media);
	}
	variancia_amostra=variancia_amostra/(NF-1)
	function_factor=0.0;
	# Se eu possuo mais que 30 pontos, usarei a Distribuição normal z para determinar o fator
	if(NF > 30)
	{
		# Distribuição Normal com nível de confiança de 95%, logo z = 1,96
		function_factor=1.96
	}
	else
	{
	   # Calcula o valor da Distribuição t de Student com nível de confiança de 95% e o grau de liberdade desejado
	   grau_liberdade = NF-1;
	   if(grau_liberdade == 1) function_factor = 12.706;
	   else if(grau_liberdade == 2) function_factor = 4.303;
	   else if(grau_liberdade == 3) function_factor = 3.182;
	   else if(grau_liberdade == 4) function_factor = 2.776;
	   else if(grau_liberdade == 5) function_factor = 2.571;
	   else if(grau_liberdade == 6) function_factor = 2.447;
	   else if(grau_liberdade == 7) function_factor = 2.365;
	   else if(grau_liberdade == 8) function_factor = 2.306;
	   else if(grau_liberdade == 9) function_factor = 2.262;
	   else if(grau_liberdade == 10) function_factor = 2.228;
	   else if(grau_liberdade == 11) function_factor = 2.201;
	   else if(grau_liberdade == 12) function_factor = 2.179;
	   else if(grau_liberdade == 13) function_factor = 2.160;
	   else if(grau_liberdade == 14) function_factor = 2.145;
	   else if(grau_liberdade == 15) function_factor = 2.131;
	   else if(grau_liberdade == 16) function_factor = 2.120;
	   else if(grau_liberdade == 17) function_factor = 2.110;
	   else if(grau_liberdade == 18) function_factor = 2.101;
	   else if(grau_liberdade == 19) function_factor = 2.093;
	   else if(grau_liberdade == 20) function_factor = 2.086;
	   else if(grau_liberdade == 21) function_factor = 2.080;
	   else if(grau_liberdade == 22) function_factor = 2.074;
	   else if(grau_liberdade == 23) function_factor = 2.069;
	   else if(grau_liberdade == 24) function_factor = 2.064;
	   else if(grau_liberdade == 25) function_factor = 2.060;
	   else if(grau_liberdade == 26) function_factor = 2.056;
	   else if(grau_liberdade == 27) function_factor = 2.052;
	   else if(grau_liberdade == 28) function_factor = 2.048;
	   else if(grau_liberdade == 29) function_factor = 2.045;
	   else if(grau_liberdade == 30) function_factor = 2.042;
	}
	intervalo_confianca = (function_factor*(sqrt(variancia_amostra)))/(sqrt(NF));
	#printf("Average:%f\n", media);
	#printf("Function Factor: %f\n", function_factor);
	#printf("Sample variance:%f\n", variancia_amostra);
	#printf("Standard Deviation: %f\n", sqrt(variancia_amostra));
	#printf("N: %d sqrt(N): %f\n", NF, sqrt(NF) );
	#printf("IC: (%f , %f)\n", media-intervalo_confianca, media+intervalo_confianca);
	printf("%f", intervalo_confianca);	
}
END{
	printf("\n");
}
