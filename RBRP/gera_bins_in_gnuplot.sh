#!/bin/bash
## argumento1: Nome do diretorio1 onde est�o os arquivos dos bins
## argumento2: Nome do diretorio2 onde est�o os arquivos dos bins

	if [ $# != 1 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "gera_bins_in_gnuplot.sh diretorio" 
        	exit 1
	fi

	DIRETORIO_ANTERIOR=`pwd`
	DIRETORIO=$1
	OUTPUT_FILE=${DIRETORIO}
	EPS_FILE=${OUTPUT_FILE}.eps
	GNUPLOT_FILE=${OUTPUT_FILE}.gp

	if [ ! -d ${DIRETORIO} ] 
	then
      		echo "Erro! Diret�rio ${DIRETORIO} n�o encontrado!"
        	exit 1
	fi

	echo "cd ${DIRETORIO}"
	cd ${DIRETORIO}	

		#Gerando o in�cio do arquivo gnuplot
		echo "
#!/usr/bin/env gnuplot
#reset

#set terminal postscript enhanced eps #color
set terminal png medium
#set term png 25 #color

# Configura��es de codifica��o de caractere
#set encoding default
set encoding iso_8859_1

# Plotando os clusters produzidos sinteticamente para o Algoritmo RBRP 

# Escolhendo onde aparecer a legenda
#set key left top
#set key top right
#set key box
set key outside bottom
#set nokey

set title \"Parti��es geradas em um ambiente bidimensional\"

# Configura��es de escala
set autoscale
#set logscale x 10
#set nologscale

# Definindo o T�tulo dos eixos a serem desenhados
set xlabel \"Eixo X\"
#set xlabel \"Numero de objetos\"
set ylabel \"Eixo Y\"
#set ylabel \"Tempo de Execucao (s)\"

#set parametric
#set grid

# Definindo os limites dos eixos
set xrange []
set yrange []

# Definindo o espa�amento entre dois valores no eixo y
#set xtics 5
#set ytics 5

#set format x \"10^{%L}\"

# Definindo o nome do arquivo de sa�da
set output \"${EPS_FILE}\"

#Plotagem dos pontos

#linetype define a cor dos pontos a serem plotados: 1 - vermelho, 2 - verde, 3 - azul ...

# \"every ::3\" � utilizado para fazer com que o gnuplot n�o plote as 3 primeiras linhas dos dados

# O comando sed � usado para substituir as v�rgulas por espa�os para que o gnuplot entenda o arquivo separado por v�rgulas como colunas
plot [][] \\" > ${GNUPLOT_FILE}

		for FILE in bin_*.ascii
		do
			# Obt�m o n�mero do bin cortando o nome do arquivo utilizando colunas separados por "_" e "." respectivamente
			NUMBER=`echo ${FILE} | cut -f 2 -d "_" | cut -f 1 -d "."`
			echo "Plotando Bin encontrado no arquivo ${FILE}. N�mero do Bin: ${NUMBER}" 

			#Escrevendo no arquivo do Gnuplot os dados do Centr�ide
			# As tr�s primeiras linhas ser�o os valores do Centr�ide e os limites inferiores e superiores do MBR
			echo "	'<grep Real ${FILE}' every ::0::0 using 2:3 title \"Centr�ide ${NUMBER}\" with points pointsize 2.0 linetype ${NUMBER}, \\" >> ${GNUPLOT_FILE}
			
			#echo "	'<grep Real ${FILE}' every ::1::2 using 2:3 title \"MBR ${NUMBER}\" with points pointsize 1.75 linetype ${NUMBER}, \\" >> ${GNUPLOT_FILE}

			#Escrevendo no arquivo do Gnuplot os objetos pertencentes ao Centr�ide, retirando-se os dados do Centr�ide
			NUM_LINES=`grep Real ${FILE} | wc -l`

			# Testa se o Bin possui objetos, se ele possuir, plota esses objetos
			if ((${NUM_LINES} > 3))
			then
				echo "	'<grep Real ${FILE}' every ::3 using 2:3 title \"Parti��o ${NUMBER}\" with points pointsize 1.5 linetype ${NUMBER}, \\" >> ${GNUPLOT_FILE}
			fi
		done
		
		# Roda script python retirando o ", \\"
		echo "Retirando caracteres extras do arquivo ${GNUPLOT_FILE} para prepara��o de gera��o do gr�fico"
		${DIRETORIO_ANTERIOR}/retira_3_caracteres.py ${GNUPLOT_FILE}

		# Gerando o gr�fico com o Gnuplot
		echo "Gerando o gr�fico com o Gnuplot"
		echo "gnuplot ${GNUPLOT_FILE}"
		gnuplot ${GNUPLOT_FILE}

		# Visualizando o gr�fico gerado
		echo "Visualizando o gr�fico gerado"
		echo "gv ${EPS_FILE} &"
		gv ${EPS_FILE} &

	echo "cd -"
	cd -
