#!/bin/bash

BINSIZE_FILE=num_binsize.txt

if [ ! -f $BINSIZE_FILE ];
then
	echo "Nao foi possivel encontrar o arquivo $BINSIZE_FILE!!! Esse arquivo deve existir com os tamanhos dos binsizes a serem testados"  
	exit 1
fi

DATABASE_FILE=bases.txt
if [ ! -f $DATABASE_FILE ];
then
	echo "Nao foi possivel encontrar o arquivo $DATABASE_FILE!!! Esse arquivo deve existir com as bases e os números de atributos a serem testados"  
	exit 1
fi

#Obtendo as bases de dados do arquivo bases.txt como um arranjo de argumentos
ARRANJO_BASES=(`cat $DATABASE_FILE`)

#Calcula o número de bases existentes
NUMERO_ARGUMENTOS=${#ARRANJO_BASES[@]}
NUMERO_DE_BASES=$((${NUMERO_ARGUMENTOS} / 4))

#Guardando os valores de cada base
for NUM_BASE in $(seq 0 $((${NUMERO_DE_BASES} - 1)))
do
	NOVA_BASE=${ARRANJO_BASES[$(($NUM_BASE*4))]}
	NOVO_REAIS=${ARRANJO_BASES[$(($NUM_BASE*4+1))]}
	NOVO_ORDINAIS=${ARRANJO_BASES[$(($NUM_BASE*4+2))]}
	NOVO_CATEGORICOS=${ARRANJO_BASES[$(($NUM_BASE*4+3))]}
	BASES=("${BASES[@]}" "$NOVA_BASE")
	ATRIBUTOS_REAIS=("${ATRIBUTOS_REAIS[@]}" "$NOVO_REAIS")
	ATRIBUTOS_ORDINAIS=("${ATRIBUTOS_ORDINAIS[@]}" "$NOVO_ORDINAIS")
	ATRIBUTOS_CATEGORICOS=("${ATRIBUTOS_CATEGORICOS[@]}" "$NOVO_CATEGORICOS")
done

#Obtendo os binsizes a serem testados do arquivo
VAR_BIN_SIZE=`cat $BINSIZE_FILE`

# Execucao dos testes
#Para cada base de dados existente
for NUM_BASE in $(seq 0 $((${NUMERO_DE_BASES} - 1)))
do
	BASE=${BASES[$NUM_BASE]}
	REAIS=${ATRIBUTOS_REAIS[$NUM_BASE]} 
	ORDINAIS=${ATRIBUTOS_ORDINAIS[$NUM_BASE]}
	CATEGORICOS=${ATRIBUTOS_CATEGORICOS[$NUM_BASE]}

	for BINSIZE in $VAR_BIN_SIZE;
	do
		echo "roda_tempos.sh $BASE NOVOS_EXPERIMENTOS_BINSIZE/$BASE.binsize$BINSIZE $REAIS $ORDINAIS $CATEGORICOS $BINSIZE"
		roda_tempos.sh $BASE NOVOS_EXPERIMENTOS_BINSIZE/$BASE.binsize$BINSIZE $REAIS $ORDINAIS $CATEGORICOS $BINSIZE
		echo "halt" | pvm
		mata_void.sh
	done
done
