#ifndef _CONSTANTS_H_
#define _CONSTANTS_H

/*! \file constants.h
    \brief Definition of all constants of the RBRP algorithm
    
    In this file are defined all constants that modifies the behaviour of the algorithm and other constants used on whole code.
*/

/* ***************************************************************************
 * Contants that defines the behaviour of the RBRP                           *
 * ***************************************************************************/

//!  The maximum size of a BIN, if a bin have more objects than this, it will be splitted 
#define BINSIZE 500

//! The number of partitions used in the k-means algorithm
#define NUM_KMEANS_PARTITIONS 6

//! The maximum number of ITERATIONS used by k-means algorithm
#define MAX_KMEANS_ITERATIONS 10

//! The constant that defines the minimum value of the diagonal MBR for consider splitting a bin if the is greater than binsize. 
// It is necessary to garantee the convergence of the algorithm in a base where the number of identical values is greater than the binsize 
#define MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING 0.0001

// Constant that define pruning objects in bins in the first phase
//#define PRUNE_OBJECTS

// Constant that defines the way how will be get the seed for the random values
//#ifdef SEED_RANK

// Constant that define pruning complete bins in the second phase
// By default, RBRP do not Prune entire partitions.
//#define PRUNE_PARTITIONS

// Order Bins Type
// By default, RBRP do not sort Bins to order the search for outliers.
#define ORDERED_METRIC RANDOM_SORT

//Types of sort the bin list
#define NOT_SORT 0
#define SORT_OBJECTS 1
#define SORT_MBR 2
#define SORT_RADIUS 3
#define SORT_MBR_BASED_DENSITY 4
#define SORT_RADIUS_BASED_DENSITY 5
#define RANDOM_SORT 6

// Metric used to calculate distance between bins
#define DISTANCE_BINS_METRIC DISTANCE_BINS_CENTROID_METRIC

// Constants that define the metric used to calculate distance between bins in second phase.
#define DISTANCE_BINS_MBR_METRIC 0
#define DISTANCE_BINS_CENTROID_METRIC 1

//! The constant that defines the change necessary on the centroids to consider do a new iteration.
/*!
  If the change in the centroid is smaller than the principal diagonal of the MBR of the centroid * ratio, the algorithms had converged.

  Math Definition:
  \f${distance("lower_bound", "upper_bound") * ratio} < distance(new\_centroid, old\_centroid)\f$
*/
#define MAX_RATIO_STOP_KMEANS 0.05

//! The constant that defines the number of objects of a sample that will be get to measure the initial MBR values. 
#define NUMBER_OF_POINTS_OF_SAMPLES 500

//! The number of the best modes found by the instance of Assigner Filter to be sent as local values.
/*!
  This constant defines the number of modes that will be sent by the Assigner Filter to found the global MODE.
  For example:
  If I want to find the global mode of the attribute that have the values: red, blue, gray, green and suposing that NUMBER_OF_MODES was set to 3. The instance of the Filter Assigner will just send the number of occurrences of the best three locally found modes. Ex: Green: 10, Red: 7, Blue: 3.  
*/
#define NUMBER_OF_MODES 8

//! Define the maximum value for real attributes. It is useful for balancing the weight on distance of real values cause it can saturate the difference between two real values
#define MAX_REAL_VALUE 2

//! Define the separators of the values on the database.
#define SEPARATORS ", \n"

//! Constant that define if the time spend on the different phases have to be saved.
#define GET_TIME 1

//! If this constant is set, the execution will be processed in INSTRUMENTATION mode
#define INSTRUMENTATION NORMAL_INST

//Types of instrumentation
#define NORMAL_INST 1
#define DEEP_INST 2

#define NUM_PRINT_INST 10000

//! Constants that defines the existing DEBUG levels
#define LOGGER_VERBOSE 7
#define DEBUGGER 6
#define LOGGER 5
#define INFO 4
#define RECV_MSG_VERBOSE 3
#define RECV_MSG_RESUME 2
#define NORMAL 1
#define NONE 0

//! If this constant is set, the execution will be processed in DEBUG mode
//! You can set this constant with the levels described above
#define DEBUG NONE

//! Constant that defines the number of the initial BIN.
//! Set this constant to a value different by 0 is useful just for debugging purposes.
#define INITIAL_BIN_NUMBER 0

//! If this constant is set, the definitions of the Task will be executed 
//#define TASK_API 1

//! If this constant is set, the bins done are saved on files 
//#define SAVE_BINS 1

//! If this constant is set the bins will be printed with their centroid values.
// This constant depends that SAVE_BINS is set and DEBUG is set to LOGGER_VERBOSE. 
//#define PRINT_CENTROID_VALUES 1

//! If this constant is set, just the first phase will be done
//#define EXECUTE_JUST_FIRST_PHASE

/*****************************************************************************/
//! The maximum length of the name of one file
#define MAX_FILENAME 100

//! The maximum length of a value of an dimension in number of characters

/*!
 * The maximum length of a line on the database is equal to ((MAX_VALUE_LEN+2)*number_of_dimensions+1)
 * 
 * Example: 
 * If in the database there is the line:
 * 1.034, 2.134, 3.489, 4.249, 7.578, 1.000, 1.250, 0.500, 3, 4, 7
 * Then the MAX_VALUE_LEN can be just 5 (the number of chars of the values does not exceed 5)
 */
#define MAX_VALUE_LEN 30  	

//! The constant representing and EMPTY LINE 
#define EMPTY_LINE -3

//! Constants that defines the fields of the messages sent by the Distributor filter to the Assigner Filter.
#define OBJECT_NUMBER_FIELD 0
#define OBJECT_VALUES_FIELD 1

//! Constants that defines the fields of the message sent by Assigner filter to the Bin Filter and by Bin to Bin filter
#define TO_BIN_MSG_TYPE_FIELD 0
#define TO_BIN_BIN_NUMBER_FIELD 1
#define TO_BIN_VALUES_FIELD 2

//! Constants that defines the Bin message types
//! Type of Messages received during the First Phase
#define FIRST_MBR_VALUES_MSG 41
#define BINS_LOCAL_VALUES_MSG 42
#define OBJECT_VALUES_OF_BIN_MSG 43
#define CONCLUDED_BIN_LOCAL_VALUES_MSG 44

//! Type of Messages received during the Second Phase
#define SCORE_NEW_OUTLIER_MSG 45
#define PROBABLE_OUTLIER_MSG 46
#define SEARCH_FINISHED_ON_INSTANCE_MSG 47

//! Constants that defines the fields of the message sent by BIN filter to the Assigner Filter
#define TO_ASSIGNER_MSG_TYPE_FIELD 0
#define TO_ASSIGNER_BIN_NUMBER_FIELD 1
#define TO_ASSIGNER_VALUES_FIELD 2
#define TO_ASSIGNER_PRUNE_RATIO_BIN 2

//! Constants that defines the Assigner message types 
#define CONCLUDED_BIN_MSG 32
#define NEW_ITERATION_MSG 33
#define SPLIT_BIN_MSG 34

//! Constants that defines the fields of the message sent by Bin filter to the Final Filter
#define TO_FINAL_OBJECT_NUMBER_FIELD 0
#define TO_FINAL_SCORE_VALUE_FIELD 1
#define TO_FINAL_NEIGHBOURS_FIELD 2

//! Constants that defines the state of a bin
#define NORMAL_STATE 0

// DO NOT CHANGE THIS VALUE !!! The algorithm assumes that the constant CONCLUDED have the same value of constant MAX_KMEANS_ITERATIONS
#define CONCLUDED MAX_KMEANS_ITERATIONS

#endif
