#!/bin/bash

echo "Procurando por diret�rios com o nome round_* para obten��o dos dados..."
ULTIMA_RODADA=0

DIRETORIO_TEMPOS=NOVOS_EXPERIMENTOS

cd ${DIRETORIO_TEMPOS}
for DIRETORIO in ${DIRETORIO_TEMPOS}/*/*
do
	if [ -d ${DIRETORIO}];
	then
		# Obt�m o n�mero da rodada cortando o nome do diret�rio utilizando colunas separados por "_"
		RODADA=`echo ${DIRETORIO} | cut -f 2 -d "_"`

		# Testa se a vari�vel DIRETORIO realmente est� com um diret�rio assinalado. Isso pode n�o ocorrer caso n�o exista nenhum diretorio round_*
		if [ -d ${DIRETORIO} ];
		then
			# Obt�m a maior rodada existente
			if (( ${RODADA} > ${ULTIMA_RODADA} ));
			then
				ULTIMA_RODADA=${RODADA}
			fi
		else
			echo "ERRO: N�o foram encontrados diret�rios com o nome round_*!!!"
			exit 1;
		fi
	fi
done

# Para cada rodada existente, ele gera o gr�fico com os bins da rodada
for RODADA in `seq 1 ${ULTIMA_RODADA}`
do
	DIRETORIO="round_${RODADA}"
	echo "Processando diretorio: ${DIRETORIO}"
	if [ -d ${DIRETORIO} ];
	then	
		echo "Gerando Bins encontrados na rodada ${RODADA}" 
		./gera_bins_in_gnuplot.sh ${DIRETORIO}
	fi
done
