#!/bin/bash
## argumento1: Nome do diretorio1 onde está o experimento
  for FILE in *bin_0.inst 
	do
	
		echo "REMOVENDO O ARQUIVO ${FILE}.dat"
		rm -f ${FILE}.dat

		echo "CRIANDO O ARQUIVO ${FILE}.dat"
		touch ${FILE}.dat
	
		# Procura os numeros de bins em todos os arquivos
		NUMERO_BINS=`grep "NUMBER OF BINS CREATED" ${FILE} | awk '{num_bins += $6}  END {printf ("%d\n",num_bins);}'`
		echo "${NUMERO_BINS}" > ${FILE}.dat
	
		# Procura os numeros de objetos em todos os arquivos
		NUMERO_OBJETOS=`grep "NUMBER OF POINTS IN ALL BINS OF THIS INSTANCE" ${FILE} | awk '{num_obj += $10}  END {printf ("%d\n",num_obj);}'`
		echo "${NUMERO_OBJETOS}" >> ${FILE}.dat

		# Procura numero eliminacoes no mesmo bin em todos os arquivos
		NUMERO_ELIMINACOES_MESMO_BIN=`grep "NUMBER OF POINTS OF THIS INSTANCE ELIMINATED IN FIRST BIN OF THIS INSTANCE:" ${FILE} | awk '{num_mes_bin += $14}  END {printf ("%d\n",num_mes_bin);}'`
		echo "${NUMERO_ELIMINACOES_MESMO_BIN}" >> ${FILE}.dat

		# Procura numero comparacoes de objetos em todos os arquivos 
		NUMERO_COMPARACOES=`grep "NUMBER OF COMPARATIONS BETWEEN OBJECTS:" ${FILE} | awk '{num_comp += $6}  END {printf ("%d\n",num_comp);}'`
		echo "${NUMERO_COMPARACOES}" >> ${FILE}.dat
	
		# Procura numero de comparacoes no primeiro bin em todos os arquivos
		NUMERO_COMPARACOES_PRIMEIRO_BIN=`grep "NUMBER OF COMPARATIONS BETWEEN OBJECTS IN FIRST BIN:" ${FILE} | awk '{num_comp_bin += $9}  END {printf ("%d\n",num_comp_bin);}'`
		echo "${NUMERO_COMPARACOES_PRIMEIRO_BIN}" >> ${FILE}.dat

		#	Procura numero de comparacoes dos no primeiro bin 
		NUMERO_COMPARACOES_OBJETOS_ELIMINADOS_PRIMEIRO_BIN=`grep "NUMBER OF COMPARATIONS BETWEEN OBJECTS OF OBJECTS PRUNED IN FIRST BIN:" ${FILE} | awk '{num_comp_obj_bin += $12}  END {printf ("%d\n",num_comp_obj_bin);}'`
		echo "${NUMERO_COMPARACOES_OBJETOS_ELIMINADOS_PRIMEIRO_BIN}" >> ${FILE}.dat

		# Procura numero de objetos eliminados pelo mbr
		NUMERO_OBJETOS_ELIMINADOS_MBR=`grep "NUMBER OF OBJECTS FIRST PRUNING USING MBR:" ${FILE} | awk '{num_obj_mbr += $8}  END {printf ("%d\n",num_obj_mbr);}'`
		echo "${NUMERO_OBJETOS_ELIMINADOS_MBR}" >> ${FILE}.dat

		# Procura numero de bins eliminados pelo mbr
		NUMERO_BINS_ELIMINADOS_MBR=`grep "NUMBER OF BINS FIRST PRUNING USING MBR:" ${FILE} | awk '{num_bin_mbr += $8}  END {printf ("%d\n",num_bin_mbr);}'`
		echo "${NUMERO_BINS_ELIMINADOS_MBR}" >> ${FILE}.dat
		
		# Procura numero de objetos eliminados pelo mbr depois com bins vizinhos
		NUMERO_OBJETOS_VIZINHOS_ELIMINADOS_MBR=`grep "NUMBER OF OBJECTS PRUNING IN SEARCH OF NEIGHBOURS:" ${FILE} | awk '{num_obj_viz_mbr += $9}  END {printf ("%d\n",num_obj_viz_mbr);}'`
		echo "${NUMERO_OBJETOS_VIZINHOS_ELIMINADOS_MBR}" >> ${FILE}.dat
	
		# Procura numero de bins eliminados pelo mbr
		NUMERO_BINS_VIZINHOS_ELIMINADOS_MBR=`grep "NUMBER OF BINS PRUNING IN SEARCH OF NEIGHBOURS:" ${FILE} | awk '{num_bin_viz_mbr += $9}  END {printf ("%d\n",num_bin_viz_mbr);}'`
		echo "${NUMERO_BINS_VIZINHOS_ELIMINADOS_MBR}" >> ${FILE}.dat
	
		# Procura numero de objetos eliminados pelo mbr depois com bins vizinhos
		NUMERO_OBJETOS_ELIMINADOS_MBR2=`grep "NUMBER OF OBJECTS SECOND PRUNING USING MAXDIST:" ${FILE} | awk '{num_obj_mbr2 += $8}  END {printf ("%d\n",num_obj_mbr2);}'`
		echo "${NUMERO_OBJETOS_ELIMINADOS_MBR2}" >> ${FILE}.dat
	done
