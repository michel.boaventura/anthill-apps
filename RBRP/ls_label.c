#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "structs.h"

/* extracts the label from the message */
void getLabel(void *msg, int size, char label[]){
	int *msg_int;
	bin_number_type *label_msg;
	bin_number_type *bin_number_msg;
	
	msg_int=(int *)msg;
	label_msg=(bin_number_type *)label;
	
	// Go forward on the message until the Bin Number Field
	bin_number_msg=(bin_number_type *)&((msg_int[TO_BIN_BIN_NUMBER_FIELD]));
	
	// The Bin Number Field is used to define the instance that have to receive the message
	label_msg[0]=bin_number_msg[0];
}

/* Calculates the instance that have to receive the message */
int hash (char *label, int image)
{
	int value;
	bin_number_type *label_msg;
	bin_number_type bin_number;
	
	label_msg=(bin_number_type *)label;
	bin_number = label_msg[0];

	value = bin_number % image;
	return value;		
}
