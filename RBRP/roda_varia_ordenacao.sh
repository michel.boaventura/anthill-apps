#!/bin/bash

OLD_SORT=`grep ORDERED_METRIC constants.h | sed "s/^.*ORDERED_METRIC //g"`

#Obtendo as ordena��es a serem testadas
ORDENACOES="RANDOM_SORT NOT_SORT SORT_OBJECTS SORT_MBR SORT_RADIUS SORT_MBR_BASED_DENSITY SORT_RADIUS_BASED_DENSITY"
#ORDENACOES="NOT_SORT SORT_OBJECTS SORT_DENSITY SORT_MBR"

DATABASE_FILE=bases.txt
if [ ! -f $DATABASE_FILE ];
then
	echo "Nao foi possivel encontrar o arquivo $DATABASE_FILE!!! Esse arquivo deve existir com as bases e os n�meros de atributos a serem testados"  
	exit 1
fi

#Obtendo as bases de dados do arquivo bases.txt como um arranjo de argumentos
ARRANJO_BASES=(`cat $DATABASE_FILE`)

#Define o n�mero de parametros existente para cada base de dados
NUM_PARAMETROS_BASE=5

#Calcula o n�mero de bases existentes
NUMERO_ARGUMENTOS_TOTAL=${#ARRANJO_BASES[@]}
NUMERO_DE_BASES=$((${NUMERO_ARGUMENTOS_TOTAL} / ${NUM_PARAMETROS_BASE}))

#Guardando os valores de cada base
for NUM_BASE in $(seq 0 $((${NUMERO_DE_BASES} - 1)))
do
	NOVA_BASE=${ARRANJO_BASES[$(($NUM_BASE*$NUM_PARAMETROS_BASE))]}
	NOVO_REAIS=${ARRANJO_BASES[$(($NUM_BASE*${NUM_PARAMETROS_BASE}+1))]}
	NOVO_ORDINAIS=${ARRANJO_BASES[$(($NUM_BASE*${NUM_PARAMETROS_BASE}+2))]}
	NOVO_CATEGORICOS=${ARRANJO_BASES[$(($NUM_BASE*${NUM_PARAMETROS_BASE}+3))]}
	NOVO_BINSIZE=${ARRANJO_BASES[$(($NUM_BASE*${NUM_PARAMETROS_BASE}+4))]}
	BASES=("${BASES[@]}" "$NOVA_BASE")
	ATRIBUTOS_REAIS=("${ATRIBUTOS_REAIS[@]}" "$NOVO_REAIS")
	ATRIBUTOS_ORDINAIS=("${ATRIBUTOS_ORDINAIS[@]}" "$NOVO_ORDINAIS")
	ATRIBUTOS_CATEGORICOS=("${ATRIBUTOS_CATEGORICOS[@]}" "$NOVO_CATEGORICOS")
	VALORES_BINSIZE=("${VALORES_BINSIZE[@]}" "$NOVO_BINSIZE")
done

# Execucao dos testes
#Para cada base de dados existente
for NUM_BASE in $(seq 0 $((${NUMERO_DE_BASES} - 1)))
do
	BASE=${BASES[$NUM_BASE]}
	REAIS=${ATRIBUTOS_REAIS[$NUM_BASE]} 
	ORDINAIS=${ATRIBUTOS_ORDINAIS[$NUM_BASE]}
	CATEGORICOS=${ATRIBUTOS_CATEGORICOS[$NUM_BASE]}
	BINSIZE=${VALORES_BINSIZE[$NUM_BASE]}

	for SORT in $ORDENACOES;
	do
		# Altera o m�todo de ordena��o no constants.h
		sed -i "s/^#define.*ORDERED_METRIC.*$/#define ORDERED_METRIC $SORT/g" constants.h
		
		# Verifica se o SORT est� correto
		SORT_USED=`grep ORDERED_METRIC constants.h | sed "s/^.*ORDERED_METRIC //g"`
		if ((SORT_USED != SORT))
		then 
			echo "Erro!!! N�o foi poss�vel alterar a ordena��o!!! SORT desejado: ${SORT} SORT encontrado: ${SORT_USED}"
			echo "Abortando execu��o..."
			exit 1;
		fi

		make clean;
		make;
		prepara_cluster.sh

		echo "roda_tempos.sh $BASE NOVOS_EXPERIMENTOS_SORT/$BASE.sort$SORT.binsize${binsize} $REAIS $ORDINAIS $CATEGORICOS ${BINSIZE}"
		roda_tempos.sh $BASE NOVOS_EXPERIMENTOS_SORT/$BASE.sort$SORT.binsize${BINSIZE} $REAIS $ORDINAIS $CATEGORICOS ${BINSIZE}
		echo halt | pvm
		mata_void.sh
	done
done
		
#Restaurando o valor antigo que estava em ordena��o no constants.h
sed -i 's/^#define.*ORDERED_METRIC.*$/#define ORDERED_METRIC $OLD_SORT/g' constants.h
