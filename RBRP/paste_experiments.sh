#!/bin/bash

if [ $# != 1 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "paste_experiments.sh diretorio_experimentos" 
        exit 1
fi

DIRETORIO_EXPERIMENTOS=$1

DIRETORIO_CORRENTE=`pwd`

echo "Procurando pelos diret�rios com os experimentos para realizar o merge das colunas..."

cd ${DIRETORIO_EXPERIMENTOS}

PRIMEIRO_DIRETORIO=1

if [ ! -d ${PRIMEIRO_DIRETORIO} ];
then
	echo "ERRO: N�o encontrou o diret�rio com o primeiro experimento"
	echo "Tentando encontrar outro diret�rio para considerar o primeiro experimento..."	
	for DIRETORIO in *
	do
		echo "Testando como diret�rio: ${DIRETORIO}"
		if [ -d ${DIRETORIO} ];
		then
			PRIMEIRO_DIRETORIO=${DIRETORIO}
			break	
		else 
			echo "Ignorando ${DIRETORIO}... N�o � um diret�rio!!!"
		fi
	done
fi

echo "Unindo os tempos dos arquivos..."
# Ser� realizado o ``paste'' de todos os arquivos de tempo
#echo "Procurando por arquivos dentro do diret�rio padr�o: ${PRIMEIRO_DIRETORIO}"
cd ${PRIMEIRO_DIRETORIO}
for FILE in *.time
do
	#Assegura de que � um arquivo
#	 echo "Testando como arquivo: ${FILE}"
	if [ -f ${FILE} ];
	then
#		echo "Unindo os tempos do arquivo ${FILE}"
#		echo "Executando paste com o diret�rio corrente: `pwd`"
		if [ -f ../${FILE} ];
		then 
			echo "O arquivo ../${FILE} j� existe... Removendo-o..."
#			echo "rm -fv ../${FILE}"
			rm -f ../${FILE}
		fi

		paste ../*/${FILE} > ../${FILE}
#		echo "paste ../*/${FILE} > ../${FILE}"

		if [ ! -f ../${FILE} ];
		then
			echo "Erro: Falha na cria��o do arquivo com os tempos de todos os diret�rios!!!"
			echo "O arquivo j� existe? Se sim, e voc� deseja recri�-lo, primeiro remova-o!!!"
			echo "Algum problema pode ter ocorrido com o comando paste... Verifique!!!"
			exit 1
		fi
#	else
#		echo "Ignorando ${FILE}... N�o � um arquivo!!!"
	fi
done
cd -

PWD=`pwd`
echo "Processando as m�dias dos arquivos"
echo "Diret�rio corrente: ${PWD}"
for FILE in *.time
do
	#Assegura de que � um arquivo
	if [ -f ${FILE} ];
	then
#		echo "Processando as m�dias do arquivo ${FILE}"
		if [ ! -f $DIRETORIO_CORRENTE/media.awk ];
		then
			echo "Falha ao encontrar o arquivo media.awk utilizado para agregar as m�dias!!!"
			exit 1;
		else
			echo "awk -f $DIRETORIO_CORRENTE/media.awk ${FILE} > ${FILE}.avg"
			awk -f $DIRETORIO_CORRENTE/media.awk ${FILE} > ${FILE}.avg
		fi
		if [ ! -f $DIRETORIO_CORRENTE/intervalo_confianca95.awk ];
		then
			echo "Falha ao encontrar o arquivo intervalo_confianca95.awk utilizado para agregar os intervalos de confian�as!!!"
			exit 1;
		else
			echo "awk -f $DIRETORIO_CORRENTE/intervalo_confianca95.awk ${FILE} > ${FILE}.ic95"
			awk -f $DIRETORIO_CORRENTE/intervalo_confianca95.awk ${FILE} > ${FILE}.ic95
		fi
		if [ ! -f $DIRETORIO_CORRENTE/intervalo_confianca90.awk ];
		then
			echo "Falha ao encontrar o arquivo intervalo_confianca90.awk utilizado para agregar os intervalos de confian�as!!!"
			exit 1;
		else
			echo "awk -f $DIRETORIO_CORRENTE/intervalo_confianca90.awk ${FILE} > ${FILE}.ic90"
			awk -f $DIRETORIO_CORRENTE/intervalo_confianca90.awk ${FILE} > ${FILE}.ic90
		fi
	else
		if [ "${FILE}" =  "*.time" ];
		then
			echo "Erro na cria��o dos arquivos com os tempos unidos!!! Parece n�o existir arquivo .time no diret�rio ${DIRETORIO_CORRENTE}"
			exit 1;
		fi
	fi	
done

