#ifndef _MESSAGES_H
#define _MESSAGES_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"

/* Prototypes */
void *get_object_from_msg(object_type *object, dimension_size_type dimension_size, void *msg);

int fill_mbr_values_in_message(void **msg, dimension_size_type dimension_size, centroid_type centroid);

int send_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size, OutputPortHandler port);

int send_objects_of_bins(bin_list_type *list, dimension_size_type dimension_size, OutputPortHandler port);

bin_list_pointer get_new_centroid_new_bin_from_msg(bin_number_type *msg, bin_list_type *list, dimension_size_type dimension_size);

bin_list_pointer get_new_centroid_for_bin_from_msg(bin_number_type *msg, bin_list_type *list, dimension_size_type dimension_size);

int send_bins_local_values_message(bin_number_type splitted_bin_number, bin_list_pointer *pointer_to_new_bins, dimension_size_type dimension_size, OutputPortHandler port);

int send_concluded_bin_local_values(bin_list_pointer concluded_bin, dimension_size_type dimension_size, OutputPortHandler port);

int get_concluded_bin_local_values_from_msg(int *msg, bin_list_type *list, dimension_size_type dimension_size, unsigned int number_of_assigner_instances);

int send_split_bin_message(bin_number_type splitted_bin_number, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size, OutputPortHandler Assigner_output);

int send_concluded_bin_message(bin_number_type bin_number, float prune_ratio, OutputPortHandler Assigner_output);

int send_new_iteration_message(bin_number_type splitted_bin_number, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size, OutputPortHandler Assigner_output);

int get_probable_outlier_from_msg(message_type message, bin_number_type *bin_real_owner_number, object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours);

int send_new_outlier_message(object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours, OutputPortHandler Final_output);

int send_score_new_outlier_messages(distance_type score, OutputPortHandler Bin_output);

int send_search_finished_on_instance_messages(OutputPortHandler Bin_output, bin_number_type instance_number);

int send_probable_outlier_message(object_type *object, nearest_neighbours_type *nearest_neighbours, int is_last_object_of_the_instance, bin_number_type instance_number, bin_number_type bin_owner_number, dimension_size_type dimension_size, int number_of_neighbours, OutputPortHandler Bin_output);

int get_new_outlier_from_msg(message_type message, object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours);

void print_msg_distributor(void *msg, dimension_size_type dimension_size, FILE *file);

void print_first_mbr_values_of_bin_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_new_iteration_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_new_outlier_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_bins_local_values_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_concluded_bin_local_values_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_object_values_of_bin_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_probable_outlier_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_score_new_outlier_message(void *msg, FILE *file);

void print_search_finished_on_instance_message(void *msg, FILE *file);

void print_split_bin_message(void *msg, dimension_size_type dimension_size, FILE *file);

void print_concluded_bin_message(void *msg, FILE *file);

inline unsigned int get_first_mbr_values_message_size(dimension_size_type dimension_size);

inline unsigned int get_bins_local_values_message_size(dimension_size_type dimension_size);

inline unsigned int get_new_iteration_message_size(dimension_size_type dimension_size);

inline unsigned int get_concluded_bin_local_values_message_size(dimension_size_type dimension_size);

inline unsigned int get_probable_outlier_message_size(dimension_size_type dimension_size, int number_of_neighbours);

inline unsigned int get_new_outlier_message_size(int number_of_neighbours, dimension_size_type dimension_size);

#endif

