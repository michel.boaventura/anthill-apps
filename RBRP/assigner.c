#include "assigner.h"

//==========================================================================================

// INPUT AND OUTPUT PORTS
InputPortHandler Distributor_input, Bin_input;	// input ports
OutputPortHandler Bin_output;			// output port that sends the messages to the filter bin

//INSTRUMENTATION VARIABLES 

#ifdef INSTRUMENTATION
unsigned long long NUMBER_OF_ASSIGNS=0;
unsigned long long NUMBER_OF_OBJECTS_PRUNING=0;
unsigned long long NUMBER_OF_TASKS=0;
unsigned long long NUMBER_OF_OBJECTS_UNSTABLE=0;
unsigned long long NUMBER_OF_OBJECTS_IN_ASSIGN=0;
struct timeval INITIAL_TIME_INST;
#endif

//GLOBAL VARIABLES 
/* Parameters of the program read from the work*/
dimension_size_type dimension_size;			// number of dimensions (attributes) of the database 
char output_file_name[MAX_FILENAME];		// stores the name of the output file

//==========================================================================================

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	/* Parameters of the program read from the work*/
	char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read 
	int number_of_outliers;				// number of n, where n defines the number of outliers to be searched (top-n ouliers)
	int number_of_neighbours;			// number of k, where k is the number of neighbours considered to measure an outlier (k-th nearest neighbour)

	int bin_size;
	char error_msg[100];
	
	// opens input and output ports
	Distributor_input = ahGetInputPortByName ("Distributor_to_Assigner_input");
	Bin_input = ahGetInputPortByName ("Bin_to_Assigner_input");
	Bin_output = ahGetOutputPortByName ("Assigner_to_Bin_output");
	if ( (Distributor_input == -1) || (Bin_input == -1) || Bin_output == -1)
	{
		sprintf(error_msg,"ERROR. Could not open the Assigner filter's ports.\n");
		ahExit (error_msg);
	}
	
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &dimension_size, &number_of_outliers, &number_of_neighbours, &bin_size);
	
	return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int processFilter(void *work, int worksize) 
{
	bin_list_type bin_list;

	/* A pointer to the first bin */
	bin_type first_bin;

	char error_msg[100];
	
#ifdef GET_TIME
	struct timeval initial_time, receive_database_time, first_phase_execution_time, distribution_to_bin_filter_time;
	FILE *fp_time;
	char name_of_fp_time[MAX_FILENAME];
	sprintf(name_of_fp_time, "%s.assigner_%d.time", output_file_name, ahGetMyRank()); 
	
	// Clean the files of times
	fp_time = fopen(name_of_fp_time,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);
#endif

#ifdef GET_TIME	
	gettimeofday(&initial_time, NULL);
#endif

#ifdef INSTRUMENTATION
	gettimeofday(&INITIAL_TIME_INST, NULL);
#endif

	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/
	
	/* Create the list of the BINs initially empty */
	create_empty_bin_list(&bin_list);

	/* Create the First Bin */
	malloc_bin(&first_bin, INITIAL_BIN_NUMBER, dimension_size);

	/* Insert the first bin on the list of the bins */
	insert_in_bin_list(first_bin, &bin_list);

	/* Receive the database from the Distributor Filter */
	receive_database(&bin_list, dimension_size, Distributor_input);

#ifdef GET_TIME	
	gettimeofday(&receive_database_time, NULL);
#endif

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE )
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "RECEIVED DATABASE:\n");
		print_bin_list(&bin_list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/


#ifdef INSTRUMENTATION
	FILE *fp_inst;
	char name_of_fp_inst[MAX_FILENAME];
	sprintf(name_of_fp_inst, "%s.assigner_%d.inst", output_file_name, ahGetMyRank()); 
	// Clean the files of instrumentation
	fp_inst = fopen(name_of_fp_inst,"w");
	fclose(fp_inst);
#endif

	/* Calculate the First MBR Values of the bin to generate the initial centroids */ 
	calculate_first_mbr_values_of_bin(&bin_list, dimension_size);

	/* Send MBR values of a sample to generate the initial centroids */
	send_first_mbr_values_of_bin(&bin_list, dimension_size, Bin_output);

	/* Starts the recursive clustering procedure to generate the bins */
	starts_first_phase(&bin_list, dimension_size, Bin_input, Bin_output);

#ifdef GET_TIME	
	gettimeofday(&first_phase_execution_time, NULL);
#endif

	/* Send the objects of the Bins to the Bin assigner filter when the first phase have finished */
	send_objects_of_bins(&bin_list, dimension_size, Bin_output);
	
	#ifdef INSTRUMENTATION
		fp_inst = fopen(name_of_fp_inst,"a");
		fprintf(fp_inst, "NUMBER OF ASSIGNS IN THIS INSTANCE: %llu\n", NUMBER_OF_ASSIGNS);
		fprintf(fp_inst, "NUMBER OF OBJECTS PRUNING IN THIS INSTANCE: %llu\n", NUMBER_OF_OBJECTS_PRUNING);
		fclose(fp_inst);
	#endif


#ifdef GET_TIME	
	gettimeofday(&distribution_to_bin_filter_time, NULL);
	print_time(&initial_time, &receive_database_time, name_of_fp_time);
	print_time(&receive_database_time, &first_phase_execution_time, name_of_fp_time);
	print_time(&first_phase_execution_time, &distribution_to_bin_filter_time, name_of_fp_time);
#endif

	/* Frees all the memory allocated for the bin list */
	destroy_bin_list(&bin_list, dimension_size);
	
	return 0;
}

int finalizeFilter()
{
        return 0;
}
