/* Inclusoes. */
#include "attribute_type.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Calculate the Euclidian distance between two points with real values */
/**
 * The Euclidian Distance between two bidimensional points \f$(x_1,y_1)\f$ and \f$(x_2,y_2)\f$ is 
 * \f$\sqrt{(x_2-x_1)^2+(y_2-y_1)^2}\f$.
 */
distance_type calculate_distance_between_real_values(real *point1, real *point2, unsigned int dimensions)
{
	distance_type sum = 0;
	distance_type difference=0;
	distance_type distance;
	register unsigned int i;
	
	for(i=0; i < dimensions; i++) 
	{
		distance_type value1, value2;
		/* It is used to saturate the real values to make these no very more significant than the other values (ordinal and categorical)*/
		if (point1[i]>MAX_REAL_VALUE)
				 value1=MAX_REAL_VALUE;
		else
		{
			if (point1[i]<-MAX_REAL_VALUE)
					  value1=-MAX_REAL_VALUE;
			else
				value1=point1[i];
		}
		if (point2[i]>MAX_REAL_VALUE)
				 value2=MAX_REAL_VALUE;
		else
		{
			if (point2[i]<-MAX_REAL_VALUE)
					  value2=-MAX_REAL_VALUE;
			else
				value2=point2[i];
		}
		
		difference=value1-value2;
		sum += difference * difference;
	}
	distance=sqrtf(sum);	
	return distance;
}

/** Calculate the Euclidian distance between two points with ordinal values */
/**
 * The Euclidian Distance between two bidimensional points \f$(x_1,y_1)\f$ and \f$(x_2,y_2)\f$ is 
 * \f$\sqrt{(x_2-x_1)^2+(y_2-y_1)^2}\f$.
 */
distance_type calculate_distance_between_ordinal_values(ordinal *point1, ordinal *point2, unsigned int dimensions)
{
	distance_type sum = 0;
	register unsigned int i;
	
	for(i=0; i < dimensions; i++) 
	{
		sum += (point1[i] - point2[i]) * (point1[i] - point2[i]);
	}
	return sqrtf(sum);
}

/** Calculates the dissimilarity measure between two categorical points */
/**
 * The Dissimilarity measure between two categorical points is known as simple matching.
 * It just calculates the number of atributes where thw two points have different values.
 */
int calculate_distance_between_categorical_values(categorical *point1, categorical *point2, unsigned int dimensions)
{
	int sum = 0;
	register unsigned int i;
	
	for(i=0; i < dimensions; i++) 
	{
		if (point1[i]!=point2[i])
			sum += 1;
	}
	return sum;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Calculate the Euclidian distance between two points */
/**
 * The Euclidian Distance between two bidimensional points \f$(x_1,y_1)\f$ and \f$(x_2,y_2)\f$ is 
 * \f$\sqrt{(x_2-x_1)^2+(y_2-y_1)^2}\f$.
 */
distance_type calculate_distance_between_dimension_values(dimension_values_type dimension_values1, dimension_values_type dimension_values2, dimension_size_type dimension_size)
{
	distance_type sum = 0;
	int distance_between_categorical_values;
	
	sum=calculate_distance_between_real_values(dimension_values1.real_values, dimension_values2.real_values, dimension_size.number_of_real_values);
	
	sum+=calculate_distance_between_ordinal_values(dimension_values1.ordinal_values, dimension_values2.ordinal_values, dimension_size.number_of_ordinal_values);
	
	distance_between_categorical_values=calculate_distance_between_categorical_values(dimension_values1.categorical_values, dimension_values2.categorical_values, dimension_size.number_of_categorical_values);
	sum+=(distance_type)distance_between_categorical_values;
	
	return sum;
}

/** Calculate the distance between a point and an object */
/** The distance calculated is the square of the Euclidian distance */
distance_type distance_between_centroid_and_point(centroid_type centroid, object_type object, dimension_size_type dimension_size)
{
	return (calculate_distance_between_dimension_values(centroid.dimension_values, object.dimension_values, dimension_size));
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** 
 * Print all the elements of an array of real values on a file.
 * @param file Prints the array on this file
 * @param array the array of real values that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 */
int print_real_array(FILE *file, real *array, unsigned int number_of_elements)
{
	register unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "REAL ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, REAL_CONVERSION_STRING, array[i]);
		fprintf(file, " ");
	}
	fprintf(file, "\n");
	
	return 1;
}

/** 
 * Print all the elements of an array of ordinal values on a file.
 * @param file Prints the array on this file
 * @param array the array of ordinal values that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 */
int print_ordinal_array(FILE *file, ordinal *array, unsigned int number_of_elements)
{
	register unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "ORDINAL ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, ORDINAL_CONVERSION_STRING, array[i]);
		fprintf(file, " ");
	}
	fprintf(file, "\n");
	
	return 1;
}

/** 
 * Print all the elements of an array of categorical values on a file.
 * @param file Prints the array on this file
 * @param array the array of categorical values that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 */
int print_categorical_array(FILE *file, categorical *array, unsigned int number_of_elements)
{
	register unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "CATEGORICAL ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, CATEGORICAL_CONVERSION_STRING, array[i]);
		fprintf(file, " ");
	}
	fprintf(file, "\n");
	
	return 1;
}

/* Get a real value from a string */
real get_real_value_from_string(char *temp_string)
{
	return (atof(temp_string));

}

/* Get an ordinal value from a string */
ordinal get_ordinal_value_from_string(char *temp_string)
{
	return (atof(temp_string));

}

/* Get a categorical value from a string */
categorical get_categorical_value_from_string(char *temp_string)
{
	return (atoi(temp_string));
}
