/* Inclusoes. */
#include "nearest_neighbours.h"

/* Print the values of the nearest neighbours, showing the nearest neighbours and its distances */
int print_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, unsigned int object_number, unsigned int number_of_neighbours, FILE *file)
{
	fprintf(file, "Nearest Neighbours of the Object Number: %d\n", object_number);
	if (nearest_neighbours->objects != NULL)
	{
		fprintf(file, "Number of nearest neighbours: %d\n", nearest_neighbours->size);
		
		if (nearest_neighbours->size >0)
		{		  
			/* Print the array that keeps the number of the nearest objects. */
			fprintf(file, "Neighbours Numbers:\n\t");
			print_unsigned_int_array(file, nearest_neighbours->objects, nearest_neighbours->size);
		
			/* Print the array that keeps the distance of the nearest neighbours */
			fprintf(file, "Distance:\n\t");
			print_distance_type_array(file, nearest_neighbours->distance, nearest_neighbours->size);	
		}
	}
	else 
		fprintf(file, "The array of the nearest neighbours is EMPTY!!!\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Frees the memory allocated for the structures of an array of nearest neighbours */ 
int free_nearest_neighbours(nearest_neighbours_type nearest_neighbours)
{
	if (nearest_neighbours.objects!=NULL)
	{
		free(nearest_neighbours.objects);
		free(nearest_neighbours.distance);
		return 1;
	}
	else 
		return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Allocates the memory for the structures of the array of nearest neighbours */
int malloc_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours)
{
	/* Initialize the size */
	nearest_neighbours->size=0;
	
	/* Allocates the structure for the arrays */
	nearest_neighbours->objects=malloc((number_of_neighbours)*sizeof(int));
	nearest_neighbours->distance=malloc((number_of_neighbours)*sizeof(distance_type));

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Insert the neighbour on the nearest neighbours array if it is neareast that the already found neighbours */
int insert_in_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, unsigned int object_number, distance_type distance)
{
	int position, i;
	int already_inserted_neighbours;

	/* Gets the number of neighbours that already have been inserted */
	already_inserted_neighbours=nearest_neighbours->size;

	/* Searches the position where the object have to be inserted */	
	for (position=0; position<already_inserted_neighbours; position++)
	{
		if (nearest_neighbours->distance[position]>distance)
			break;
	}

	/* Tests if the object is a new nearest neighbour */
	if (position!=number_of_neighbours)
	{
		/* Tests if the one nearest neighbour will be removed cause the array already is full*/
		if (already_inserted_neighbours==number_of_neighbours)
		{
			/* Move forward the distance values that is greater that the current distance to save the new value in the position that it have to be inserted */  
			for (i=already_inserted_neighbours-1; i>position; i--)	
			{
				nearest_neighbours->distance[i]=nearest_neighbours->distance[i-1];
				nearest_neighbours->objects[i]=nearest_neighbours->objects[i-1];
			}
		}
		else
		{	
			/* Move forward the distance values that is greater that the current distance to save the new value in the position that it have to be inserted */  
			for (i=already_inserted_neighbours; i>position; i--)	
			{
				nearest_neighbours->distance[i]=nearest_neighbours->distance[i-1];
				nearest_neighbours->objects[i]=nearest_neighbours->objects[i-1];
			}

			/* Updates the number of the neighbours on the array */ 
			nearest_neighbours->size++;
		}

		/* Saves the new values on the nearest neighbours array */
		nearest_neighbours->distance[position]=distance;
		nearest_neighbours->objects[position]=object_number;
	
		return 1;
	}

	return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Checks if an object with this distance would be a nearest neighbour */
int check_distance_for_nearest_neighbours(distance_type distance, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours)
{
	/* If the number of neighbours have not been found, any distance will be a nearest neighbour */ 
	if (nearest_neighbours->size<number_of_neighbours)
		return 1;
	
	return (nearest_neighbours->distance[nearest_neighbours->size-1]>distance);
}

/* Gets the minimum distance of one neighbour on the nearest neighbours */
distance_type get_minimum_distance_in_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours)
{
	/* If the number of neighbours have not been found, we returns 0 */ 
	if (nearest_neighbours->size<number_of_neighbours)
		return 0;
	
	return (nearest_neighbours->distance[nearest_neighbours->size-1]);
}

