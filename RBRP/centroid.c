/* Inclusoes. */
#include "centroid.h"

/* Print the values of a centroid */
void print_centroid(centroid_type centroid, dimension_size_type dimension_size, FILE *fp_log)
{
   unsigned int i;
   fprintf(fp_log, "\tCentroid :\n\t\t");
   print_dimension_values(centroid.dimension_values, dimension_size, fp_log);
		
   fprintf(fp_log, "\tUpper Bound MBR Values: \n\t\t");
   print_dimension_values(centroid.upper_bound_mbr, dimension_size, fp_log);

   fprintf(fp_log, "\tLower Bound MBR Values: \n\t\t");
   print_dimension_values(centroid.lower_bound_mbr, dimension_size, fp_log);

   fprintf(fp_log, "\tList of Occurrences of the Categorical Attributes: \n");

   /* Printing the occurrences list of the categorical attributes */
   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      fprintf(fp_log, "\t\tList of Occurrences of Categorical Attribute:%d \n", i);
      if (!is_empty_occurrences_list(&(centroid.occurrences[i])))
         print_occurrences_list(&(centroid.occurrences[i]), fp_log);
      else 
         fprintf(fp_log, "\t\tThere aren't occurrences on the list\n");
   }

}

/* Compare if two centroids are in the same place on the space */
inline int compare_centroids(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size)
{
	return (distance_between_centroids(centroid1,centroid2, dimension_size)==0);
}

/* Frees the memory allocated for the structures of a centroid */ 
void free_centroid(centroid_type centroid, dimension_size_type dimension_size)
{
   unsigned int i;
   free_dimension_values(centroid.dimension_values);
   free_dimension_values(centroid.new_dimension_values);
   free_dimension_values(centroid.upper_bound_mbr);
   free_dimension_values(centroid.lower_bound_mbr);
  
   /* Frees the memory allocated for the lists of occurrences of the categorical attributes*/
   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      make_occurrences_list_empty(&(centroid.occurrences[i]));
      free(centroid.occurrences[i].first);
   }

   /* Frees the array of occurrences lists */
   if (dimension_size.number_of_categorical_values > 0)
   	free(centroid.occurrences);   
}

/* Allocates the memory for the structures of a centroid */
void malloc_centroid(centroid_type *centroid, dimension_size_type dimension_size)
{
   unsigned int i;
   malloc_dimension_values(&(*centroid).dimension_values, dimension_size);
   malloc_dimension_values(&(*centroid).new_dimension_values, dimension_size);
   malloc_dimension_values(&(*centroid).upper_bound_mbr, dimension_size);
   malloc_dimension_values(&(*centroid).lower_bound_mbr, dimension_size);
   (*centroid).number_of_objects_considered=0;

   /* Allocates the memory to save the lists of occurrences of each categorical value */
   if (dimension_size.number_of_categorical_values > 0)
      (*centroid).occurrences=malloc(dimension_size.number_of_categorical_values*sizeof(occurrences_list_type));

   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      /* Creates the list of the occurrences empty */
      create_empty_occurrences_list(&((*centroid).occurrences[i]));
   }
    
}

/* Calculates the distance between two centroids */
inline distance_type distance_between_centroids(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size)
{
	return (calculate_distance_between_dimension_values(centroid1.dimension_values, centroid2.dimension_values, dimension_size));
}

/* Updates the MBR values if it is necessary */
void compute_object_for_updating_centroid_mbr_values(centroid_type *centroid, object_type *object, dimension_size_type dimension_size)
{
   unsigned int i;
   char error_msg[100];
   
   // Tests if the object is or the centroid is invalid
   if (object==NULL || centroid == NULL)
   {
      sprintf(error_msg,"ERROR. Couldn't update the values of the centroid for the object. Cause one of the pointers is invalid!!!\n");
      ahExit (error_msg);
   }
	
   /* Updates the superior bounds of the MBR */
   get_greater_dimension_values(&(centroid->upper_bound_mbr), &(object->dimension_values), dimension_size);
	
   /* Updates the inferior bounds of the MBR */
   get_smaller_dimension_values(&(centroid->lower_bound_mbr), &(object->dimension_values), dimension_size);

   for (i=0; i<dimension_size.number_of_real_values; i++)
   {		
      centroid->dimension_values.real_values[i]+=object->dimension_values.real_values[i];
   }
   for (i=0; i<dimension_size.number_of_ordinal_values; i++)
   {		
      centroid->dimension_values.ordinal_values[i]+=object->dimension_values.ordinal_values[i];
   }

   /* Computes the occurrence of the values of the categorical values of the object on the occurrences list */
   insert_object_occurrences(centroid, object, dimension_size);
      
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Compute the values of the object from the occurrences list */
void insert_object_occurrences(centroid_type *centroid, object_type *object, dimension_size_type dimension_size)
{
   unsigned int i;
   int value_number;
   char error_msg[100];
   int compute_occurrence=0;

   // Tests if the object is or the centroid is invalid
   if (object==NULL || centroid == NULL)
   {
      sprintf(error_msg,"ERROR. Couldn't update the values of the centroid for the object. Cause one of the pointers is invalid!!!\n");
      ahExit (error_msg);
   }

   /* Compute the occurrence of the values of the object on the occurrences list of this categorical attributes */
   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      value_number=object->dimension_values.categorical_values[i];

      /* The frequency of the attribute will be added by one */
      compute_occurrence=update_frequency_of_item_in_occurrences_list(value_number, 1, &(centroid->occurrences[i]));

      /* Tests if the computing of the occurrence of the categorical attribute worked properly */
      if (!compute_occurrence)
      {
	 sprintf(error_msg,"ERROR. Couldn't compute the occurrence of the attribute!!! Probably the final frequency got negative!!!\n");
	 ahExit (error_msg);
      }
   }
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Remove the values of the object from the occurrences list */
void remove_object_occurrences(centroid_type *centroid, object_type *object, dimension_size_type dimension_size)
{
   unsigned int i;
   int value_number;
   char error_msg[100];
   int compute_occurrence=0;
   
   // Tests if the object is or the centroid is invalid
   if (object==NULL || centroid == NULL)
   {
      sprintf(error_msg,"ERROR. Couldn't update the values of the centroid for the object. Cause one of the pointers is invalid!!!\n");
      ahExit (error_msg);
   }
	
   /* Remove the occurrence of the value on the occurrences list of this categorical attribute */
   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      value_number=object->dimension_values.categorical_values[i];

      /* The frequency of the attribute will be subtracted by one */
      compute_occurrence=update_frequency_of_item_in_occurrences_list(value_number, -1, &(centroid->occurrences[i]));

      /* Tests if the computing of the occurrence of the categorical attribute worked properly */
      if (!compute_occurrence)
      {
         sprintf(error_msg,"ERROR. Couldn't compute the occurrence of the attribute!!! Probably the final frequency got negative!!!\n");
         ahExit (error_msg);
      }
   }
}

/* Get the local number of occurrences of each value of each categorical attribute from an message and update the occurrences listof the centroid of the bin */
void *get_attribute_occurrences_from_msg(centroid_type *centroid, void *msg, dimension_size_type dimension_size)
{
   int *int_msg;
   categorical *categorical_values_msg, value_number;
   unsigned int i, number_of_values;
   int frequency;

   /* If there is no categorical attribute, then won't get any value from the message */
   if (dimension_size.number_of_categorical_values <= 0)
      return msg;

   categorical_values_msg=(categorical *)msg;

   /* For each categorical attribute, we will get the occurrences of each common value */
   for (i=0; i<dimension_size.number_of_categorical_values; i++)
   {
      number_of_values=0;

      /* Initialize the frequency variable with value different than zero */
      frequency=1;

      /* Keep getting the occurrences while there is an occurrence to be get and the number of values sent is not exceeded */
      while(frequency > 0 && number_of_values < NUMBER_OF_MODES)
      {
	 /* Get the value from the message */
	 value_number=categorical_values_msg[0];

	 /* Go forward on the message */
	 int_msg=(int *)&(categorical_values_msg[1]);

	 /* Get the local value number of times that the value had happened */
	 frequency=int_msg[0];

	 /* Go forward on the message */
	 categorical_values_msg=(categorical *)&(int_msg[1]);

	 /* If really there is a new occurrence to be saved, it will be saved and the new values will be get */
	 if (frequency>0)
	 {
	    update_frequency_of_item_in_occurrences_list(value_number, frequency, &(centroid->occurrences[i]));
	    number_of_values++; 
	 }
      }
   }
   return categorical_values_msg;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Get the mbr values received from an message an keep it on the mbr values of the centroid of the bin */
void *get_mbr_values_from_msg(centroid_type *centroid, void *msg, dimension_size_type dimension_size)
{
	void *values_msg = msg;
	
	/* read the Upper bound MBR values from message */
        values_msg=get_dimension_values_from_msg(&(centroid->upper_bound_mbr), values_msg, dimension_size);
        
	/* read the Lower bound MBR values from message */
	values_msg=get_dimension_values_from_msg(&(centroid->lower_bound_mbr), values_msg, dimension_size);

	return values_msg;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Update the mbr values received from an message and keep it on the mbr values of the centroid of the bin */
void *update_mbr_values_from_msg(centroid_type *centroid, void *msg, dimension_size_type dimension_size)
{
	real *upper_bound_real_values_msg, *lower_bound_real_values_msg;
	ordinal *upper_bound_ordinal_values_msg, *lower_bound_ordinal_values_msg;
	categorical *upper_bound_categorical_values_msg, *lower_bound_categorical_values_msg;
	unsigned int i;
	
	upper_bound_real_values_msg=(real *)msg;
	upper_bound_ordinal_values_msg=(ordinal *)&(upper_bound_real_values_msg[dimension_size.number_of_real_values]);
	upper_bound_categorical_values_msg=(categorical *)&(upper_bound_ordinal_values_msg[dimension_size.number_of_ordinal_values]);
	lower_bound_real_values_msg=(real *)&(upper_bound_categorical_values_msg[dimension_size.number_of_categorical_values]);
	lower_bound_ordinal_values_msg=(ordinal *)&(lower_bound_real_values_msg[dimension_size.number_of_real_values]);
	lower_bound_categorical_values_msg=(categorical *)&(lower_bound_ordinal_values_msg[dimension_size.number_of_ordinal_values]);

	/* read the bound MBR real values from message */
	for (i=0; i<dimension_size.number_of_real_values; i++)	
	{
		if (centroid->upper_bound_mbr.real_values[i] < upper_bound_real_values_msg[i])
			centroid->upper_bound_mbr.real_values[i]=upper_bound_real_values_msg[i];
		if (centroid->lower_bound_mbr.real_values[i] > lower_bound_real_values_msg[i])
			centroid->lower_bound_mbr.real_values[i]=lower_bound_real_values_msg[i];
	}

	/* read the bound MBR ordinal values from message */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)	
	{
		if (centroid->upper_bound_mbr.ordinal_values[i] < upper_bound_ordinal_values_msg[i])
			centroid->upper_bound_mbr.ordinal_values[i]=upper_bound_ordinal_values_msg[i];
		if (centroid->lower_bound_mbr.ordinal_values[i] > lower_bound_ordinal_values_msg[i])
			centroid->lower_bound_mbr.ordinal_values[i]=lower_bound_ordinal_values_msg[i];
	}

	/* read the bound MBR categorical values from message */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)	
	{
		if (centroid->upper_bound_mbr.categorical_values[i] < upper_bound_categorical_values_msg[i])
			centroid->upper_bound_mbr.categorical_values[i]=upper_bound_categorical_values_msg[i];
		if (centroid->lower_bound_mbr.categorical_values[i] > lower_bound_categorical_values_msg[i])
			centroid->lower_bound_mbr.categorical_values[i]=lower_bound_categorical_values_msg[i];
	}

	return (&(lower_bound_categorical_values_msg[dimension_size.number_of_categorical_values]));
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/* Get the centroid local values received from an message and save it on the structures of the centroid of the bin.
 */
void *get_centroid_local_values_from_msg(centroid_type *centroid, unsigned int local_length, void *msg, dimension_size_type dimension_size)
{
	real *real_values_msg;
	ordinal *ordinal_values_msg;
	categorical *categorical_values_msg;
	double local_ratio, previous_ratio;  
	unsigned int i, previous_length=centroid->number_of_objects_considered;
	unsigned int total_length=local_length+previous_length;
	occurrences_list_pointer aux;

	real_values_msg=(real *)msg;
	ordinal_values_msg=(ordinal *)&(real_values_msg[dimension_size.number_of_real_values]);
	categorical_values_msg=(categorical *)&(ordinal_values_msg[dimension_size.number_of_ordinal_values]);
	
	/* There are no objects on the bin. Doesn't do anything */
	if(local_length == 0)
		return msg;

	/* Finds the ratio to calculate the centroid value */
	local_ratio=(double)local_length/(double)total_length;
	previous_ratio=(double)previous_length/(double)total_length;

	/* Update the real dimension values of the centroid from message */
	for (i=0; i<dimension_size.number_of_real_values; i++)	
	{
		centroid->new_dimension_values.real_values[i]=centroid->new_dimension_values.real_values[i]*previous_ratio+real_values_msg[i]*local_ratio;
	}
	
	/* Update the ordinal dimension values of the centroid from message */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)	
	{
		centroid->new_dimension_values.ordinal_values[i]=centroid->new_dimension_values.ordinal_values[i]*previous_ratio+ordinal_values_msg[i]*local_ratio;
	}

	/* Get the local number of occurrences of each value of each categorical attribute and go forward on the message */
	real_values_msg=(real *)get_attribute_occurrences_from_msg(centroid, categorical_values_msg, dimension_size);

	/* Tests if the first values are being received */
	if (previous_length == 0)
	{
		/* Get the bound MBR Bound values from message */
		real_values_msg=get_mbr_values_from_msg(centroid, real_values_msg, dimension_size);
	}
	else
	{
		/* Update the bound MBR values from message and go forward on the message */
		real_values_msg=update_mbr_values_from_msg(centroid, real_values_msg, dimension_size);
	}

	/* Update the modes of the each categorical attribute */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		/* Tests if there are occurrences on the list. Just in this case the value could be changed */
		if(get_first_in_occurrences_list(&(centroid->occurrences[i]), &aux))
		{
			centroid->new_dimension_values.categorical_values[i]=get_mode_from_occurrence_list(&(centroid->occurrences[i]));
		}
		else
			centroid->new_dimension_values.categorical_values[i]=centroid->dimension_values.categorical_values[i];
	}

	/* Update the number of objects considered */
	centroid->number_of_objects_considered+=local_length;
	
	return real_values_msg;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Updates the dimension values of the centroid based on the new dimension values, it returns the ratio of changing of the centroid */ 
distance_type update_centroid_dimension_values(centroid_type *centroid, dimension_size_type dimension_size)
{
	distance_type distance_centroid_changing, diagonal_mbr_size;

	// Tests if the bin did not receive any object. In this case, it will keep the old values 
	if (centroid->number_of_objects_considered <= 0)
	{
		distance_centroid_changing=0;
		
		/* Updates the upper bound with the values of the centroid */	
		copy_dimension_values(&(centroid->upper_bound_mbr), &(centroid->dimension_values), dimension_size);

		/* Updates the lower bound with the values of the centroid */	
		copy_dimension_values(&(centroid->lower_bound_mbr), &(centroid->dimension_values), dimension_size);
	}
	else
	{
		/* Calculates the changing of the centroid */
		distance_centroid_changing=calculate_distance_between_dimension_values(centroid->dimension_values, centroid->new_dimension_values, dimension_size);

		/* Updates the dimension values of the centroid */	
		copy_dimension_values(&(centroid->dimension_values), &(centroid->new_dimension_values), dimension_size);
	}
	
	/* Calculates diagonal's size MBR of the bin */
	diagonal_mbr_size=calculate_distance_between_dimension_values(centroid->lower_bound_mbr, centroid->upper_bound_mbr, dimension_size);

	return (distance_centroid_changing>MAX_RATIO_STOP_KMEANS*diagonal_mbr_size);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Fill the local dimension values on a message, getting the values from the structure of the centroid.
 * It will fill on the message the average of real and categorical attributes of the centroid, and the modest occurrences of each value of each categorical attribute. */
void *fill_local_centroid_dimension_values_on_msg(void *msg_dest, centroid_type *src, dimension_size_type dimension_size)
{
	unsigned int i;

        real *real_values_msg;
	ordinal *ordinal_values_msg;
	categorical *categorical_values_msg;
	
	real_values_msg=(real *)msg_dest;

	// Fills the dimension values on the message 
	// Filling the real dimension values 
	for (i=0; i<dimension_size.number_of_real_values; i++)
		real_values_msg[i]=src->dimension_values.real_values[i];
		
	// Go forward on the msg 
	ordinal_values_msg=(ordinal *)&(real_values_msg[dimension_size.number_of_real_values]);

	// Filling the ordinal dimension values
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
		ordinal_values_msg[i]=src->dimension_values.ordinal_values[i];
	
	// Go forward on the msg 
	categorical_values_msg=(categorical *)&(ordinal_values_msg[dimension_size.number_of_ordinal_values]);
	
	// Filling the occurrences of each categorical dimension values
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
	   /* Fill the occurrences of the categorical attribute and go forward on the message.
	    * It will fill the frequency of the more frequent values found on the bin */
	   categorical_values_msg=fill_occurrences_list_on_msg(categorical_values_msg, &(src->occurrences[i]));
	}	

        return (categorical_values_msg);	
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Fill the Centroid values on a message, getting the values from the structure of the centroid */
void *fill_centroid_values_on_msg(void *msg_dest, centroid_type *src, dimension_size_type dimension_size)
{
	void *values_msg=msg_dest;
	
	/* Fills the dimension values of centroid and go forward on the message */
	values_msg=fill_local_centroid_dimension_values_on_msg(values_msg, src, dimension_size);

	/* Fills the Upper Bound values of the centroid and Go forward on the message */
	values_msg=fill_dimension_values_on_msg(values_msg, &(src->upper_bound_mbr), dimension_size);
	
	/* Fills the Lower Bound values of the centroid and Go forward on the message */
	values_msg=fill_dimension_values_on_msg(values_msg, &(src->lower_bound_mbr), dimension_size);
	
	return (values_msg);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Generate a Random dimension values for a centroid, based on the MBR values Bounds of another centroid (father)*/
int generate_random_centroid(centroid_type *dest, centroid_type *src, dimension_size_type dimension_size)
{
	unsigned int i;
	
	/* For the real attributes, the value of the centroid is a random number generated in the interval between upper_bound and lower_bound */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
           dest->dimension_values.real_values[i]=(drand48()*(src->upper_bound_mbr.real_values[i]-src->lower_bound_mbr.real_values[i]))+src->lower_bound_mbr.real_values[i];
	}

	/* For the ordinal attributes, the value of the centroid is a random number generated in the interval between upper_bound and lower_bound */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
	   dest->dimension_values.ordinal_values[i]=(drand48()*(src->upper_bound_mbr.ordinal_values[i]-src->lower_bound_mbr.ordinal_values[i]))+src->lower_bound_mbr.ordinal_values[i];
	}

	/* For the categorical attributes, the value of the centroid is an integer random number generated in the interval between upper_bound and lower_bound */


	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		// Gets the number of different values exists
		unsigned int list_length=get_occurrences_list_length(&((*src).occurrences[i]));

		/* Tests if there are different values to be chosen randomly */
		if ((src->lower_bound_mbr.categorical_values[i])!=(src->upper_bound_mbr.categorical_values[i]) && list_length > 0)
	   {
			// Gets the sum of the frequencies of the modest values. It is used to generate a random categorical value that prefer the modest values, then it is a weighted random value based on the frequencies of each values
			unsigned int sum_frequency_modest_values=get_sum_frequency_modest_values_from_occurrence_list(&((*src).occurrences[i]));
			
			// Generates a random integer number between 0 and sum_frequency_modest_values to established the random value
			unsigned int random_value=lrand48()%sum_frequency_modest_values;
		
			occurrences_list_pointer aux;
			get_first_in_occurrences_list(&((*src).occurrences[i]), &aux);
			while (random_value > aux->item.frequency )
			{
				random_value-=aux->item.frequency;
				go_next_occurrences(&((*src).occurrences[i]), &aux);
   		}
			dest->dimension_values.categorical_values[i]=aux->item.value_number;
		}
		else
			dest->dimension_values.categorical_values[i]=src->upper_bound_mbr.categorical_values[i];
		}
	return 1;
}

void get_representative_min_point_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size, dimension_values_type *dimension_values_aux)
{
	int i = 0;

	for (i = 0; i < dimension_size.number_of_real_values; i++)
	{
			if (dimension_values.real_values[i] < centroid.lower_bound_mbr.real_values[i])
					dimension_values_aux->real_values[i] = centroid.lower_bound_mbr.real_values[i];
			else if (dimension_values.real_values[i] > centroid.upper_bound_mbr.real_values[i])	
					dimension_values_aux->real_values[i] = centroid.upper_bound_mbr.real_values[i];
			else 
					dimension_values_aux->real_values[i] = dimension_values.real_values[i];
	}
	
	for (i = 0; i < dimension_size.number_of_categorical_values; i++)
	{
			if (dimension_values.categorical_values[i] < centroid.lower_bound_mbr.categorical_values[i])
					dimension_values_aux->categorical_values[i] = centroid.lower_bound_mbr.categorical_values[i];
			else if (dimension_values.categorical_values[i] > centroid.upper_bound_mbr.categorical_values[i])	
					dimension_values_aux->categorical_values[i] = centroid.upper_bound_mbr.categorical_values[i];
			else 
					dimension_values_aux->categorical_values[i] = dimension_values.categorical_values[i];
	}
	
	for (i = 0; i < dimension_size.number_of_ordinal_values; i++)
	{
			if (dimension_values.ordinal_values[i] < centroid.lower_bound_mbr.ordinal_values[i])
					dimension_values_aux->ordinal_values[i] = centroid.lower_bound_mbr.ordinal_values[i];
			else if (dimension_values.ordinal_values[i] > centroid.upper_bound_mbr.ordinal_values[i])	
					dimension_values_aux->ordinal_values[i] = centroid.upper_bound_mbr.ordinal_values[i];
			else 
					dimension_values_aux->ordinal_values[i] = dimension_values.ordinal_values[i];
	}

}	

void get_representative_max_point_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size, dimension_values_type *dimension_values_aux)
{
	int i = 0;

	for (i = 0; i < dimension_size.number_of_real_values; i++)
	{
			if (dimension_values.real_values[i] < ((centroid.lower_bound_mbr.real_values[i] + centroid.upper_bound_mbr.real_values[i])/2))
					dimension_values_aux->real_values[i] = centroid.upper_bound_mbr.real_values[i];
			else 
					dimension_values_aux->real_values[i] = centroid.lower_bound_mbr.real_values[i];
	}
	
	for (i = 0; i < dimension_size.number_of_categorical_values; i++)
	{
			if (dimension_values.categorical_values[i] < ((centroid.lower_bound_mbr.categorical_values[i] + centroid.upper_bound_mbr.categorical_values[i])/2))
					dimension_values_aux->categorical_values[i] = centroid.upper_bound_mbr.categorical_values[i];
			else 
					dimension_values_aux->categorical_values[i] = centroid.lower_bound_mbr.categorical_values[i];
	}
	
	for (i = 0; i < dimension_size.number_of_ordinal_values; i++)
	{
			if (dimension_values.ordinal_values[i] < ((centroid.lower_bound_mbr.ordinal_values[i] + centroid.upper_bound_mbr.ordinal_values[i])/2))
					dimension_values_aux->ordinal_values[i] = centroid.upper_bound_mbr.ordinal_values[i];
			else 
					dimension_values_aux->ordinal_values[i] = centroid.lower_bound_mbr.ordinal_values[i];
	}

}

void get_representatives_min_points_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size, dimension_values_type *dimension_values1, dimension_values_type *dimension_values2)
{
	int i = 0;
	for (i = 0; i < dimension_size.number_of_real_values; i++)
	{
			if (centroid2.upper_bound_mbr.real_values[i] < centroid1.lower_bound_mbr.real_values[i])
			{
					dimension_values1->real_values[i] = centroid1.lower_bound_mbr.real_values[i];
					dimension_values2->real_values[i] = centroid2.upper_bound_mbr.real_values[i];
			}		
			else if (centroid2.lower_bound_mbr.real_values[i] > centroid1.upper_bound_mbr.real_values[i])
			{
					dimension_values1->real_values[i] = centroid2.lower_bound_mbr.real_values[i];
					dimension_values2->real_values[i] = centroid1.upper_bound_mbr.real_values[i];
			}
			else 
			{
				dimension_values1->real_values[i] = centroid2.lower_bound_mbr.real_values[i];
				dimension_values2->real_values[i] = centroid2.lower_bound_mbr.real_values[i];
			}			
	}
	
	for (i = 0; i < dimension_size.number_of_categorical_values; i++)
	{
			if (centroid2.upper_bound_mbr.categorical_values[i] < centroid1.lower_bound_mbr.categorical_values[i])
			{
					dimension_values1->categorical_values[i] = centroid1.lower_bound_mbr.categorical_values[i];
					dimension_values2->categorical_values[i] = centroid2.upper_bound_mbr.categorical_values[i];
			}		
			else if (centroid2.lower_bound_mbr.categorical_values[i] > centroid1.upper_bound_mbr.categorical_values[i])
			{
					dimension_values1->categorical_values[i] = centroid2.lower_bound_mbr.categorical_values[i];
					dimension_values2->categorical_values[i] = centroid1.upper_bound_mbr.categorical_values[i];
			}
			else 
			{
				dimension_values1->categorical_values[i] = centroid2.lower_bound_mbr.categorical_values[i];
				dimension_values2->categorical_values[i] = centroid2.lower_bound_mbr.categorical_values[i];
			}			
	}
	
	for (i = 0; i < dimension_size.number_of_ordinal_values; i++)
	{
			if (centroid2.upper_bound_mbr.ordinal_values[i] < centroid1.lower_bound_mbr.ordinal_values[i])
			{
					dimension_values1->ordinal_values[i] = centroid1.lower_bound_mbr.ordinal_values[i];
					dimension_values2->ordinal_values[i] = centroid2.upper_bound_mbr.ordinal_values[i];
			}		
			else if (centroid2.lower_bound_mbr.ordinal_values[i] > centroid1.upper_bound_mbr.ordinal_values[i])
			{
					dimension_values1->ordinal_values[i] = centroid2.lower_bound_mbr.ordinal_values[i];
					dimension_values2->ordinal_values[i] = centroid1.upper_bound_mbr.ordinal_values[i];
			}
			else 
			{
				dimension_values1->ordinal_values[i] = centroid2.lower_bound_mbr.ordinal_values[i];
				dimension_values2->ordinal_values[i] = centroid2.lower_bound_mbr.ordinal_values[i];
			}			
	}
	
}

void get_representatives_max_points_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size, dimension_values_type *dimension_values1, dimension_values_type *dimension_values2)
{
	int i = 0;
	for (i = 0; i < dimension_size.number_of_real_values; i++)
	{
			if (fabs(centroid2.upper_bound_mbr.real_values[i]- centroid1.lower_bound_mbr.real_values[i]) < fabs(centroid1.upper_bound_mbr.real_values[i]- centroid2.lower_bound_mbr.real_values[i]))
			{
					dimension_values1->real_values[i] = centroid1.upper_bound_mbr.real_values[i];
					dimension_values2->real_values[i] = centroid2.lower_bound_mbr.real_values[i];
			}		
			else 
			{
					dimension_values1->real_values[i] = centroid2.upper_bound_mbr.real_values[i];
					dimension_values2->real_values[i] = centroid1.lower_bound_mbr.real_values[i];
			}	
	}
	
	for (i = 0; i < dimension_size.number_of_categorical_values; i++)
	{
			if (fabs(centroid2.upper_bound_mbr.categorical_values[i]-centroid1.lower_bound_mbr.categorical_values[i]) < fabs(centroid1.upper_bound_mbr.categorical_values[i]-centroid2.lower_bound_mbr.categorical_values[i]))
			{
					dimension_values1->categorical_values[i] = centroid1.upper_bound_mbr.categorical_values[i];
					dimension_values2->categorical_values[i] = centroid2.lower_bound_mbr.categorical_values[i];
			}		
			else 
			{
					dimension_values1->categorical_values[i] = centroid2.upper_bound_mbr.categorical_values[i];
					dimension_values2->categorical_values[i] = centroid1.lower_bound_mbr.categorical_values[i];
			}	
	}
	
	for (i = 0; i < dimension_size.number_of_ordinal_values; i++)
	{
			if (fabs(centroid2.upper_bound_mbr.ordinal_values[i]- centroid1.lower_bound_mbr.ordinal_values[i]) < fabs(centroid1.upper_bound_mbr.ordinal_values[i]- centroid2.lower_bound_mbr.ordinal_values[i]))
			{
					dimension_values1->ordinal_values[i] = centroid1.upper_bound_mbr.ordinal_values[i];
					dimension_values2->ordinal_values[i] = centroid2.lower_bound_mbr.ordinal_values[i];
			}		
			else 
			{
					dimension_values1->ordinal_values[i] = centroid2.upper_bound_mbr.ordinal_values[i];
					dimension_values2->ordinal_values[i] = centroid1.lower_bound_mbr.ordinal_values[i];
			}	
	}
}

distance_type distance_min_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size)
{
	dimension_values_type point;
	distance_type distance = 0;

	malloc_dimension_values(&point, dimension_size);
	get_representative_min_point_between_point_mbr(dimension_values, centroid, dimension_size, &point);
	distance=calculate_distance_between_dimension_values(dimension_values, point, dimension_size);
	free_dimension_values(point);


	return distance;
}

distance_type distance_max_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size)
{
	dimension_values_type point;
	distance_type distance = 0;

	malloc_dimension_values(&point, dimension_size);
	get_representative_max_point_between_point_mbr(dimension_values, centroid, dimension_size, &point);
	distance=calculate_distance_between_dimension_values(dimension_values, point, dimension_size);
	free_dimension_values(point);

	return distance;
}

distance_type distance_min_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size)
{
	dimension_values_type dimension_values1, dimension_values2;
	distance_type distance = 0;
	
	malloc_dimension_values(&dimension_values1, dimension_size);
	malloc_dimension_values(&dimension_values2, dimension_size);
	get_representatives_min_points_between_mbrs(centroid1, centroid2, dimension_size, &dimension_values1, &dimension_values2);
	distance=calculate_distance_between_dimension_values(dimension_values1, dimension_values2, dimension_size);
	free_dimension_values(dimension_values1);
	free_dimension_values(dimension_values2);

	return distance;
}		

distance_type distance_max_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size)
{
	dimension_values_type dimension_values1, dimension_values2;
	distance_type distance = 0;
	
	malloc_dimension_values(&dimension_values1, dimension_size);
	malloc_dimension_values(&dimension_values2, dimension_size);
	get_representatives_max_points_between_mbrs(centroid1,centroid2, dimension_size, &dimension_values1, &dimension_values2);
	distance=calculate_distance_between_dimension_values(dimension_values1, dimension_values2, dimension_size);
	free_dimension_values(dimension_values1);
	free_dimension_values(dimension_values2);

	return distance;
}		
