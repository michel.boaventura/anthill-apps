/* Inclusoes. */
#include "occurrences_type.h"

/* Print all the items of the list of occurrences*/
void print_occurrences_list(occurrences_list_type *list, FILE *file)
{
   
	occurrences_list_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_occurrences_list(list, &aux))
	{	
		fprintf(file, "Occurrences List is Empty!!!");
		return;
	}

        /* Prints the number of different occurrences on the list */
	fprintf(file, "Number of occurrences on the list: %d\n", list->length);
	fprintf(file, "Printing the frequency of each occurrence:\n");
	
	/* Prints the First Occurrence of the List*/
	print_occurrences(aux->item, file);

	while (!is_last_occurrences(aux, list))
	{
		go_next_occurrences(list, &aux);
		print_occurrences(aux->item, file);
	}
}

/* Print the NUMBER_OF_MODES more frequent items of the list of occurrences*/
void print_more_frequent_of_occurrences_list(occurrences_list_type *list, FILE *file)
{
        int number_of_occurrences=0;
	occurrences_list_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_occurrences_list(list, &aux))
	{	
		fprintf(file, "Occurrences List is Empty!!!\n");
		return;
	}

        /* Prints the number of different occurrences on the list */
	fprintf(file, "Number of occurrences on the list: %d\n", list->length);
	fprintf(file, "Printing the frequency of each occurrence:\n");
	
	/* Prints the First Occurrence of the List*/
	print_occurrences(aux->item, file);
        number_of_occurrences++;
	
	while (!is_last_occurrences(aux, list) && number_of_occurrences<NUMBER_OF_MODES)
	{
		go_next_occurrences(list, &aux);
		print_occurrences(aux->item, file);
		number_of_occurrences++;
	}
}
/* Procedure that free memory of a cell .
 * It not free the memory for the objects that the cell points */
void free_occurrences_cell(occurrences_list_pointer q)
{
   free(q);
}

/* Print all the occurrences of the list */
void print_occurrences(occurrences_type occurrence, FILE *fp_log)
{
	fprintf(fp_log, "Occurrence value number: %d\tFrequency:%d\n", occurrence.value_number, occurrence.frequency);
}

/* Compare if two occurrences are the same. In practice, it just compares the value number of the occurrence */
int compare_occurrences(occurrences_type occurrence1, occurrences_type occurrence2)
{
	return (occurrence1.value_number==occurrence2.value_number);
}

/* Compare the frequency of two occurrences. Returns the number of times that the first item happened more than the second item. This value can be positive, negative or zero. */
int compare_frequency_occurrences(occurrences_type occurrence1, occurrences_type occurrence2)
{
	return (occurrence1.frequency - occurrence2.frequency);
}
/* This function frees the heap memory allocated of the item of the list. It was kept just for ortoganility. */
void free_occurrences(occurrences_type occurence)
{
}

/* Initializes the structure of the occurrence type */
void malloc_occurrences(occurrences_type *occurrence)
{
   (*occurrence).frequency=0;
}

/* Updates the frequency of the occurrences adding a certain number of frequencies */
void update_frequency_occurrences(occurrences_type *occurrence, int frequency)
{
   (*occurrence).frequency+=frequency;
}

