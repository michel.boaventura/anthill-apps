#ifndef _NEAREST_NEIGHBOURS_H
#define _NEAREST_NEIGHBOURS_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
/* Prototypes */
int print_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, unsigned int object_number, unsigned int number_of_neighbours, FILE *fp_log);
int free_nearest_neighbours(nearest_neighbours_type nearest_neighbours);
int malloc_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours);
int insert_in_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, unsigned int object_number, distance_type distance);
int check_distance_for_nearest_neighbours(distance_type distance, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours);
distance_type get_minimum_distance_in_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, int number_of_neighbours);

#endif

