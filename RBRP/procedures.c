#include "procedures.h"

/*! \file procedures.c
    \brief Definition of all common functions and procedures of all the filters of the RBRP algorithm
*/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Get parameters of the algorithm from the work passed bu the AntHill.
 * This function is responsible to read all the information contained in the 
 * work and to return this information in each of the variables passed by 
 * reference. 
 */

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, dimension_size_type *dimension_size, int *number_of_outliers, int *number_of_neighbours, int *bin_size)
{
	/** Stores the work in a array of char */
	char *work_char;				
	char *temp_string;
	
	/* Transforms the work in a array of char */
	work_char = (char *) work;
	
	/** Reads all the information of the work */
	/** Gets the input file name */
	temp_string = strtok (work_char, ",");		
	strcpy(input_file_name,temp_string);	
	
	/** Gets the output file name */
	temp_string = strtok (NULL, ",");		
	strcpy(output_file_name,temp_string);	

	/** Gets the number of real values */
	temp_string = strtok (NULL, ",");		
	dimension_size->number_of_real_values = atoi (temp_string);		

	/** Gets the number of ordinal values */
	temp_string = strtok (NULL, ",");		
	dimension_size->number_of_ordinal_values = atoi (temp_string);		

	/** Gets the number of categorical values */
	temp_string = strtok (NULL, ",");		
	dimension_size->number_of_categorical_values = atoi (temp_string);		

	/** Gets the number of outliers */
	temp_string = strtok (NULL, ",");		
	*number_of_outliers= atoi (temp_string);		

	/** Gets the number of neighbours */
	temp_string = strtok (NULL, ",");
	*number_of_neighbours = atoi (temp_string);

	/** Gets the number of neighbours */
	temp_string = strtok (NULL, ",");
	*bin_size = atoi (temp_string);		
}
	

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Print all the elements of an array of ints on a file.
 * @param array the array of integers that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 * @param file Prints the array on this file
 */
int print_int_array(FILE* file, int *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "INT ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%d ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/** Print all the elements of an array of unsigned ints on a file.
 * @param file Prints the array on this file
 * @param array the array of unsigned integers that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 */
int print_unsigned_int_array(FILE* file, unsigned int *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "UNSIGNED INT ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%u ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Print all the elements of an array of unsigned longs on a file.
 * @param array the array of longs that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 * @param file Prints the array on this file
 */
int print_unsigned_long_long_array(FILE* file, unsigned long long *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "LONG LONG ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%llu ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** 
 * Print all the elements of an array of distance_type on a file.
 * @param file Prints the array on this file
 * @param array The array of distances that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 */
int print_distance_type_array(FILE *file, distance_type *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "DISTANCE ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, DISTANCE_CONVERSION_STRING, array[i]);
		fprintf(file, " ");
	}
	fprintf(file, "\n");
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** This function find one bin of a list that is the closest bin of one object. */
bin_list_pointer find_closest_bin(list_pointer object, bin_list_pointer *bins, dimension_size_type dimension_size)
{
	int i;
	
	distance_type closest_distance=distance_between_centroid_and_point(bins[0]->item.centroid, object->item, dimension_size);
	bin_list_pointer closest_bin=bins[0];
	distance_type current_distance;
	char error_msg[100];

	// Tests if the object is invalid
	if (object==NULL)
	{
		sprintf(error_msg,"ERROR. Trying to get the closest bin of an invalid object!!!\n");
		ahExit (error_msg);
	}
	// Tests if the list of bins is invalid
	if (bins==NULL)
	{
		sprintf(error_msg,"ERROR. Trying to get the closest bin of an object from an invalid list of bins!!!\n");
		ahExit (error_msg);
	}
	
	for (i=1; i<NUM_KMEANS_PARTITIONS; i++)
	{
		// Tests if the bin on the list is invalid
		if ((bins[i])==NULL)
		{
			sprintf(error_msg,"ERROR. Trying to get the closest bin of an object, but a bin on the list of bins is invalid!!!\n");
			ahExit (error_msg);
		}
	
		current_distance=distance_between_centroid_and_point(bins[i]->item.centroid, object->item, dimension_size);
		if (closest_distance > current_distance)
		{
			closest_distance=current_distance;
			closest_bin=bins[i];	
		}
	}
	return closest_bin;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** This function find the bin on the list of bins that is the closest of one object. */
bin_list_pointer find_closest_bin_in_bin_list(object_type *object, bin_list_type *bin_list, dimension_size_type dimension_size)
{
	bin_list_pointer current_bin;

	distance_type closest_distance;
	bin_list_pointer closest_bin;
	distance_type current_distance;
	char error_msg[100];
 
	// Tests if the object is invalid
	if (object==NULL)
	{
		sprintf(error_msg,"ERROR. Trying to get the closest bin of an invalid object!!!\n");
		ahExit (error_msg);
	}
	// Tests if the list of bins is invalid
	if (bin_list==NULL)
	{
		sprintf(error_msg,"ERROR. Trying to get the closest bin of an object from an invalid list of bins!!!\n");
		ahExit (error_msg);
	}
 
	/* If there is no bin on the list, it returns NULL */
	if(!get_first_in_bin_list(bin_list, &current_bin))
		return NULL;

	/* Initially consider the first bin the closest bin */
	closest_distance=distance_between_centroid_and_point(current_bin->item.centroid, *object, dimension_size);
	closest_bin=current_bin;
	
	/* While there is an next bin, keep searching for a closer bin */
	while (!is_last_bin(current_bin, bin_list))
	{
		go_next_bin(bin_list, &current_bin);
		
		/* Tests if the current bin is the closer that the closest bin already found */
		current_distance=distance_between_centroid_and_point(current_bin->item.centroid, *object, dimension_size);
		if (closest_distance > current_distance)
		{
			closest_distance=current_distance;
			closest_bin=current_bin;	
		}

	}
	
	return closest_bin;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** This functions calculates the local values of the Bin (centroid, upper bound mbr and lower bound mbr) based on the objects in the list.
 * For the real values and ordinal values, it just calculates the average of the values.
 * For the categorical attributes if have to calculate the number of occurrences of all the values */ 
int calculate_bin_local_values(bin_list_pointer bin, dimension_size_type dimension_size)
{
	list_pointer aux;
	occurrences_list_pointer occurrences_aux;
	unsigned int i;
	unsigned int length=bin->item.objects.length;

	list_type *list_of_objects=&(bin->item.objects);
	
	/* If the bin is empty, it just returns */
	if(!get_first_in_list(list_of_objects, &aux))
	{
		/* Initialize the MBR values with the values of the centroid */
		copy_dimension_values(&(bin->item.centroid.upper_bound_mbr), &(bin->item.centroid.dimension_values), dimension_size);
		copy_dimension_values(&(bin->item.centroid.lower_bound_mbr), &(bin->item.centroid.dimension_values), dimension_size);

		return 0; 
	}

	/* Clean the lists of occurrences of the categorical attributes to start computing the new values for the new configuration */
        for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		make_occurrences_list_empty(&(bin->item.centroid.occurrences[i]));
	}
	
	/* Initialize first values of the centroid MBR values */
	copy_dimension_values(&(bin->item.centroid.dimension_values), &(aux->item.dimension_values), dimension_size);
	copy_dimension_values(&(bin->item.centroid.upper_bound_mbr), &(aux->item.dimension_values), dimension_size);
	copy_dimension_values(&(bin->item.centroid.lower_bound_mbr), &(aux->item.dimension_values), dimension_size);
 
	/* Computes the occurrence of the values of the categorical attributes of the first object on the occurrences list */
   	insert_object_occurrences(&(bin->item.centroid), &(aux->item), dimension_size);

	/* While there is an next item, keep calculating the values */
	while (!is_last(aux, list_of_objects))
	{
		go_next(list_of_objects, &aux);
		compute_object_for_updating_centroid_mbr_values(&(bin->item.centroid), &(aux->item), dimension_size);
	}   
   
	/* Calculates the centroid dimension values making the division between the sum by the length. It will be done on the real values and ordinal values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		bin->item.centroid.dimension_values.real_values[i]=bin->item.centroid.dimension_values.real_values[i]/(real)length;
	}

	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		bin->item.centroid.dimension_values.ordinal_values[i]=bin->item.centroid.dimension_values.ordinal_values[i]/(real)length;
	}

	/* Calculates the mode of the categorical values */
        for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		/* Tests if there are occurrences on the list. Just in this case the value could be changed */
		if(get_first_in_occurrences_list(&(bin->item.centroid.occurrences[i]), &occurrences_aux))
		{
			bin->item.centroid.dimension_values.categorical_values[i]=get_mode_from_occurrence_list(&(bin->item.centroid.occurrences[i]));
		}
	}
		
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/**  Verifies if there is a message on the Input, get this message */
int try_getting_message(message_type *message, InputPortHandler port)
{
	/* The maximum size of the message is the size of a probable outlier message */
	int msg_size;
	
	/* Try to receive a message with a non blocking read */	
	msg_size=ahProbe(port);
	
	if (msg_size>0)
	{
		/* Allocates memory for the message with its current size */
		malloc_message(message, msg_size);
	
		ahReadBuffer(port, *message, msg_size);
	
		return 1;
	}
	else
	{
		// If no messages were found at that port at the moment, it returns 0
		return 0;
	}
}

#ifdef GET_TIME

/**
 * Print the time beetween the start and the end of the execution
 */
int print_time(struct timeval *start, struct timeval *end, char *name_of_file)
{
	FILE *file;		// file for time output
	double execution_time;	// gets the execution time

	// opens the file
	file = fopen(name_of_file,"a");

	// calculates the execution time in seconds
	execution_time  = ( end->tv_sec - start->tv_sec )*1000000;
	execution_time += end->tv_usec - start->tv_usec;
	execution_time /= 1000000;

	// print the time
	fprintf(file, "%.3lf\n", execution_time);

	// closes the file
	fclose(file);

	return 1;
}

#endif


