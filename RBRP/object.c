/* Inclusoes. */
#include "object.h"

/* Print all the items of the list */
void print_list(list_type *list, dimension_size_type dimension_size, FILE *file)
{
   
	list_pointer aux;

	fprintf(file, "Number of objects on the list: %d\n", list->length);
	
	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_list(list, &aux))
	{	
		fprintf(file, "Object List is Empty!!!");
		return;
	}

	/* Prints the First Object of the List*/
	print_object(aux->item, dimension_size, file);

	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		print_object(aux->item, dimension_size, file);
	}
}

/* Procedure that free memory of a cell .
 * It not free the memory for the objects that the cell points */
void free_cell(list_pointer q)
{
/*   free_object(q->item);	
   free(q->item);*/
   free(q);
}

/* Print an object */
void print_object(object_type object, dimension_size_type dimension_size, FILE *fp_log)
{
	fprintf(fp_log, "Object Number: %d\n", object.number);
	print_dimension_values(object.dimension_values, dimension_size, fp_log);	
}

int compare_objects(object_type object1, object_type object2)
{
	return (object1.number==object2.number);
}

void free_object(object_type object)
{
	free_dimension_values(object.dimension_values);
}

void malloc_object(object_type *object, dimension_size_type dimension_size)
{
	malloc_dimension_values(&((*object).dimension_values), dimension_size);
}

distance_type distance_between_objects(object_type object1, object_type object2, dimension_size_type dimension_size)
{
	return (calculate_distance_between_dimension_values(object1.dimension_values, object2.dimension_values, dimension_size));
}
