/* Inclusoes. */
#include "bin_type.h"

/* Print all the items of the list */
void print_bin_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file)
{
	bin_list_pointer aux;
   
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_bin_list(list, &aux))
	{
		fprintf(file, "Bin List is EMPTY!!!\n");	   
		return;
	}
   
	/* Prints the First Bin of the list */
	print_bin(aux->item, dimension_size, file);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		print_bin(aux->item, dimension_size, file);
	}
}

/* Print all the items of the list without printing the list of the objects*/
void print_bin_fields_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file)
{
	bin_list_pointer aux;
   
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_bin_list(list, &aux))
	{
		fprintf(file, "Bin List is EMPTY!!!\n");	   
		return;
	}
   
	/* Prints the First Bin of the list */
	print_bin_fields(aux->item, dimension_size, file);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		print_bin_fields(aux->item, dimension_size, file);
	}
}

/* Procedure that free memory of a bin_cell */
void free_bin_cell(bin_list_pointer q)
{
   free(q);
}

/*Print any informations about a bin for instrumentation*/
void print_bin_inst(bin_type bin, dimension_size_type dimension_size, FILE *fp_log)
{
	distance_type diagonal_length = 0;
	distance_type radius_length = 0;
	unsigned int bin_length = 0;

	//Calculate the bin's length or bin's number objects  
	bin_length = get_list_length(&(bin.objects));
	
	// Calculate the length or value of distance between mdr's upper bound and mdr's lower bound
	diagonal_length = calculate_distance_between_dimension_values(bin.centroid.upper_bound_mbr, bin.centroid.lower_bound_mbr, dimension_size);
	
	//Calculate the length of diameter of the bin. This is equal to 2 times the radius where radius represents the greather distance between an object of this bin and the centroid of the Bin
	radius_length = calculate_distance_between_dimension_values(bin.centroid.dimension_values, bin.centroid.new_dimension_values, dimension_size);
	
	fprintf(fp_log, "Bin Number: %llu\tBin Length: %u\tBin Diagonal Length: ", bin.number, bin_length);
	fprintf(fp_log, DISTANCE_CONVERSION_STRING, diagonal_length);
	fprintf(fp_log, "\tBin Diameter: ");
	fprintf(fp_log, DISTANCE_CONVERSION_STRING, 2*radius_length);
	fprintf(fp_log, "\n");
}

/*Print the instrumentations of all bins*/
void print_bin_inst_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file)
{
	bin_list_pointer aux;
	
	/*Print the length of bin list*/
	fprintf(file,"NUMBER OF BINS CREATED : %d\n", get_size_of_bin_list(list));
   
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_bin_list(list, &aux))
	{
		fprintf(file, "Bin List is EMPTY!!!\n");	   
		return;
	}
   
	/* Prints the First Bin of the list */
	print_bin_inst(aux->item, dimension_size, file);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		print_bin_inst(aux->item, dimension_size, file);
	}
}

/* Print a bin */
void print_bin(bin_type bin, dimension_size_type dimension_size, FILE *fp_log)
{
   print_bin_fields(bin, dimension_size, fp_log);
   fprintf(fp_log, "List of Objects: \n");
	
   if (!is_empty(&(bin.objects)))
      print_list(&(bin.objects), dimension_size, fp_log);
   else 
      fprintf(fp_log, "There aren't objects on the list\n");
}

/* Print the fields of the bin */
void print_bin_fields(bin_type bin, dimension_size_type dimension_size, FILE *fp_log)
{
	fprintf(fp_log, "Bin Number: %llu\tState: %d\tLocal values received:%d\n\t", bin.number, bin.state, bin.local_values_received);
        print_centroid(bin.centroid, dimension_size, fp_log);	
}

int compare_bins(bin_type bin1, bin_type bin2)
{
	return (bin1.number==bin2.number);
}


/* Free memory of the structures of a bin */
int free_bin(bin_type bin, dimension_size_type dimension_size)
{
   /* Frees the memory allocated for the list of the objects */ 
   make_list_empty(&((bin).objects));
   free(bin.objects.first);

   /* Frees the memory allocated for the centroid values */
   free_centroid((bin).centroid, dimension_size);

   /* If necessary, frees the memory allocated for the closest bins */
   if (bin.closest_bins.bin_pointers!=NULL)
   {
      free_closest_bins(bin.closest_bins);
   }
   return 1;
}


/* Allocates and initializes the structures of a bin */
void malloc_bin(bin_type *bin, bin_number_type bin_number, dimension_size_type dimension_size_values)
{
   /* Saves the initial values of a Bin */ 
   (*bin).number=bin_number;
   (*bin).state=NORMAL_STATE;
   (*bin).local_values_received=0;
	
   /* Creates the list of the object empty */
   create_empty_list(&((*bin).objects));

   /* Allocates the memory to save of the centroid values */
   malloc_centroid(&((*bin).centroid), dimension_size_values);

   (*bin).closest_bins.bin_pointers=NULL;
   (*bin).closest_bins.distance=NULL;
}

/* Create a bin on memory and initialize these structures */
void create_bin(bin_type **bin, bin_number_type bin_number, dimension_size_type dimension_size)
{
	(*bin)=malloc(sizeof(bin_type));
	malloc_bin((*bin), bin_number, dimension_size);
}

/* Free the memory allocated to the structures of the bin and the bin itself */
void destroy_bin(bin_type *bin, dimension_size_type dimension_size)
{
	free_bin(*bin, dimension_size);
	free(bin);
}

distance_type compare_target_metric_bin(bin_type item2, bin_type item1, int number_of_neighbours, dimension_size_type dimension_size) 
{
	distance_type mbr1 = 0, mbr2 = 0;
	
	mbr1 = calculate_distance_between_dimension_values(item1.centroid.lower_bound_mbr, item1.centroid.upper_bound_mbr, dimension_size);
	mbr2 = calculate_distance_between_dimension_values(item2.centroid.lower_bound_mbr, item2.centroid.upper_bound_mbr, dimension_size);
	
	// returns precedence based on object size : smaller number of objects comes first
	if (ORDERED_METRIC == SORT_OBJECTS)
	{
		if (item1.centroid.number_of_objects_considered == item2.centroid.number_of_objects_considered)
			 return (mbr2-mbr1);	
		else 	
			return (item1.centroid.number_of_objects_considered - item2.centroid.number_of_objects_considered);	
	}
	
	// returns precedence based on mbr size : greater mbr comes first
	if (ORDERED_METRIC == SORT_MBR) 
	{
		if (mbr1 == mbr2)
			return (item1.centroid.number_of_objects_considered - item2.centroid.number_of_objects_considered);	
		else  
			return (mbr2-mbr1);	
	}

	if (ORDERED_METRIC == SORT_MBR_BASED_DENSITY)
	{
		// Used to ensure non zero division
		if (mbr1 < MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING) mbr1 = MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING;
		if (mbr2 < MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING) mbr2 = MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING;

		return (((item1.centroid.number_of_objects_considered-number_of_neighbours+0.5)/mbr1) 
				- ((item2.centroid.number_of_objects_considered-number_of_neighbours+0.5)/mbr2));
	}

	// returns precedence based on radius size : greater radius comes first
	if (ORDERED_METRIC == SORT_RADIUS) 
	{
		distance_type radius1 = 0, radius2 = 0;
		radius1 = calculate_distance_between_dimension_values(item1.centroid.dimension_values, item1.centroid.new_dimension_values, dimension_size);
		radius2 = calculate_distance_between_dimension_values(item2.centroid.dimension_values, item2.centroid.new_dimension_values, dimension_size);
	
		if (radius1 == radius2)
			return (item1.centroid.number_of_objects_considered - item2.centroid.number_of_objects_considered);	
		else  
			return (radius2-radius1);	
	}
	// returns precedence based on radius size : greater radius comes first
	if (ORDERED_METRIC == SORT_RADIUS_BASED_DENSITY) 
	{
		distance_type radius1 = 0, radius2 = 0;
		radius1 = calculate_distance_between_dimension_values(item1.centroid.dimension_values, item1.centroid.new_dimension_values, dimension_size);
		radius2 = calculate_distance_between_dimension_values(item2.centroid.dimension_values, item2.centroid.new_dimension_values, dimension_size);
	
		// Used to ensure non zero division
		if (radius1 < MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING) radius1 = MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING;
		if (radius2 < MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING) radius2 = MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING;

		return (((item1.centroid.number_of_objects_considered-number_of_neighbours+0.5)/radius1) 
				- ((item2.centroid.number_of_objects_considered-number_of_neighbours+0.5)/radius2));
	}
	
	return 0;
}
	
