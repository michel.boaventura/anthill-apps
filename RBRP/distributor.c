#include"distributor.h"
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*************************** INPUT AND OUTPUT PORTS *************************/

OutputPortHandler Assigner_output;		/*!< output port that sends the objects to the assigner filter */


/**************************** GLOBAL VARIABLES ******************************/

/* Parameters of the program read from the work*/

char input_file_name[MAX_FILENAME];		/*!< stores the name of the database that will be read */ 

dimension_size_type dimension_size;		/*!< number of dimensions (attributes) of the database */

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	char output_file_name[MAX_FILENAME];	/*!< stores the name of the output file */
	int number_of_neighbours;		/*!< number of k, where k is the number of neighbours considered to measure an outlier (k-th nearest neighbour) */
	int number_of_outliers;			/*!< number of n, where n defines the number of outliers to be searched (top-n ouliers) */
	char error_msg[100];
	int bin_size;
	
	// opens input and output ports
	Assigner_output = ahGetOutputPortByName("Distributor_to_Assigner_output");
	if ( (Assigner_output == -1) )
	{
		sprintf(error_msg,"ERROR. Could not open the distributor filter's ports.\n");
		ahExit (error_msg);
	}
 
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &dimension_size, &number_of_outliers, &number_of_neighbours, &bin_size);
	
	return 1;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int processFilter(void *work, int workSize)
{
	char error_msg[100];
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_distributor_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/

	// in case there is more than one instance of the distributor filter,
	// the others are killed
	if (ahGetMyRank() > 0)
		return 1;

	FILE *input_file;
	open_database_file(&input_file, input_file_name);
	void *msg;
	int msg_size;
	object_type aux_object;

	// each line of the database file is distributed to one of the
	// assigners (round robin policy)
	// the function get_object_from_file reads a line and fill it in an object	
	while (get_object_from_file(&aux_object, dimension_size, input_file))
	{
		// Allocates the msg and return the message already filled 
		msg_size = fill_object_in_msg (&msg, dimension_size, aux_object);

		// checks for an empty line or a wrong number of dimensions
		if (msg_size == 0)
		{
			sprintf(error_msg,"ERROR. There was an error on reading an object, this seems do not have the right number of dimensions.\nObject number: %d\n", aux_object.number);
			ahExit (error_msg);
		}
		else
		{
			ahWriteBuffer (Assigner_output, msg, msg_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if (DEBUG >= LOGGER_VERBOSE)
			{
				fp_log = fopen(name_of_fp_log,"a");
				print_msg_distributor(msg, dimension_size, fp_log);
				fclose(fp_log);
			}
#endif		
/****************************************************************************/
		}
			
		free(msg);
		free_object(aux_object);
	}
	
	fclose(input_file);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/* */
int finalizeFilter()
{
	return 1;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


