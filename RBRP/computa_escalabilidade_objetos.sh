#!/bin/bash

# O numero de tentativas que o teste seráexecutado
NUM_TENTATIVAS=3

if [ $# != 3 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "gera_escalabilidade_objetos.sh diretorio_tempos base_dados arquivo_saida" 
	exit 1
fi

# Diretorio onde est�o os tempos
DIRETORIO_TEMPOS=$1


# A base de dados que se quer criar o gr�fico da escalabilidade
BASE_DADOS=$2

# Arquivo onde ser�o escritos os dados para entrada no gnuplot
ARQUIVO_SAIDA=$3

DIRETORIO_ATUAL=`pwd`

# Concatena o arquivo de Saida com o PWD para obter o endere�o absoluto
ARQUIVO_SAIDA=$DIRETORIO_ATUAL/$ARQUIVO_SAIDA

if [ ! -d ${DIRETORIO_TEMPOS} ]
then
	echo "ERRO: Diret�rio dos tempos n�o encontrado: ${DIRETORIO_TEMPOS}"
	exit 1
fi

if [ -e ${ARQUIVO_SAIDA} ]
then
	echo "Deletando o arquivo de sa�da j� existente..."
	rm -fv ${ARQUIVO_SAIDA}
fi

# Escreve o cabe�alho no arquivo de dados

echo "0	0	0	0" >> ${ARQUIVO_SAIDA}

#echo "Entrando no diret�rio de tempos: cd ${DIRETORIO_TEMPOS}"
cd $DIRETORIO_TEMPOS

NUM_PONTOS=0
#echo "Procurando por diretorios com nome ${BASE_DADOS}_*"
for DIRETORIO in ${BASE_DADOS}_*
do
	
	if [ -d ${DIRETORIO} ]
	then
		# Obt�m o n�mero de objetos da base de dados a partir do nome do diretorio
		#NUM_OBJETOS=`echo ${DIRETORIO} | cut -f 2 -d "_"`
		NUM_OBJETOS=`echo ${DIRETORIO} | sed "s/^${BASE_DADOS}_//g"`

		# Vari�vel que Salva o n�mero de tentativas encontradas
		NUM_TENTATIVAS_ENCONTRADAS=0

		# Vari�vel que Salva o tempo de execucao m�dio
		TEMPO_EXECUCAO_MINIMO=0
		TEMPO_EXECUCAO_MEDIO=0
		TEMPO_EXECUCAO_MAXIMO=0
	
		# Calcula o tempo m�dio das execu��es
		echo "Entrando no diret�rio com ${NUM_OBJETOS} objetos:"
		#echo "cd ${BASE_DADOS}_${NUM_OBJETOS}"
		cd ${DIRETORIO}
		for DIRETORIO_TENTATIVA in *
		do
			# Computa o n�mero de tentativas
			NUM_TENTATIVAS_ENCONTRADAS=`echo "${NUM_TENTATIVAS_ENCONTRADAS}+1" | bc -l`
			NUM_TEMPOS_TENTATIVA=0
			echo "	Entrando no diret�rio da tentativa ${DIRETORIO_TENTATIVA}:"
			#echo "cd ${DIRETORIO_TENTATIVA}"
			cd ${DIRETORIO_TENTATIVA}
			#echo "		Procurando por arquivos com nome *instancias.time"
			
			for ARQUIVO_TEMPO in `ls *instancias.time`
			do
				echo "		Computando tempo de execu��o do arquivo ${ARQUIVO_TEMPO}..."
				NUM_TEMPOS_TENTATIVA=`echo "${NUM_TEMPOS_TENTATIVA} + 1" | bc -l`
				TEMPO_TENTATIVA=`more ${ARQUIVO_TEMPO}`
		
				ARQUIVO_TEMPO_ASSIGNER=`echo ${ARQUIVO_TEMPO} | sed "s/\.time//g"`
				ARQUIVO_TEMPO_ASSIGNER=`echo $ARQUIVO_TEMPO_ASSIGNER.assigner_0.time`

				if [ ! -e ${ARQUIVO_TEMPO_ASSIGNER} ]
				then
					echo "Encontrado arquivo de tempo do Filtro Final sem o equivalente do Filtro Assigner"
					echo "Final: ${ARQUIVO_TEMPO} Assigner: ${ARQUIVO_TEMPO_ASSIGNER}"
					exit 1
				fi

				# Obtendo o primeiro tempo do arquivo de tempos do Filtro Assigner que indica o
				# Tempo decorrido durante o recebimento da base de dados
				TEMPO_DISTRIBUICAO=`sed "2,3d" ${ARQUIVO_TEMPO_ASSIGNER}`
				
				#echo "TEMPO de Execucao da tentativa: ${TEMPO_TENTATIVA} Tempo de Distribuicao: ${TEMPO_DISTRIBUICAO}"

				# Subtraindo o tempo de distribui��o da base de dados do tempo total de execu��o
				TEMPO_TENTATIVA=`echo "${TEMPO_TENTATIVA} - ${TEMPO_DISTRIBUICAO}" | bc -l`
				
				#echo "TEMPO de Execucao sem distribuicao: ${TEMPO_TENTATIVA}"
	
				# Testa se � a primeira tentativa encontrada
				if (($NUM_TENTATIVAS_ENCONTRADAS == 1))
				then
					#echo "Atualizando tempo m�nimo de $TEMPO_EXECUCAO_MINIMO para $TEMPO_TENTATIVA"
					TEMPO_EXECUCAO_MINIMO=$TEMPO_TENTATIVA
					TEMPO_EXECUCAO_MEDIO=$TEMPO_TENTATIVA
					#echo "Atualizando tempo m�ximo de $TEMPO_EXECUCAO_MAXIMO para $TEMPO_TENTATIVA"
					TEMPO_EXECUCAO_MAXIMO=$TEMPO_TENTATIVA
				else
					# Atualiza o valor m�nimo caso seja necess�rio
					if ((`echo ${TEMPO_EXECUCAO_MINIMO} \> ${TEMPO_TENTATIVA} | bc -l `))
					then
						#echo "Atualizando tempo m�nimo de $TEMPO_EXECUCAO_MINIMO para $TEMPO_TENTATIVA"
						TEMPO_EXECUCAO_MINIMO=$TEMPO_TENTATIVA
					fi

					# Atualiza o tempo de execu��o m�dio somando os valores at� agora. Ao final, ser� feita a divis�o pelo n�mero de tentativas
					#echo "Calculando novo tempo de Execu��o: ${TEMPO_EXECUCAO_MEDIO} + ${TEMPO_TENTATIVA}"
					TEMPO_EXECUCAO_MEDIO=`echo "${TEMPO_EXECUCAO_MEDIO} + $TEMPO_TENTATIVA" | bc -l`
					
					# Atualiza o valor m�ximo caso seja necess�rio
					if ((`echo ${TEMPO_EXECUCAO_MAXIMO} \< ${TEMPO_TENTATIVA} | bc -l `))
					then
						#echo "Atualizando tempo m�ximo de $TEMPO_EXECUCAO_MAXIMO para $TEMPO_TENTATIVA"
						TEMPO_EXECUCAO_MAXIMO=$TEMPO_TENTATIVA
					fi

					#echo "Novo valor m�nimo: ${TEMPO_EXECUCAO_MINIMO}"
					#echo "Nova soma Tempos de execu��o: ${TEMPO_EXECUCAO_MEDIO}"
					#echo "Novo valor m�ximo: ${TEMPO_EXECUCAO_MAXIMO}"
				fi
			done
			cd ..

			if ((${NUM_TEMPOS_TENTATIVA} > 1))
			then
				echo "Encontrados ${NUM_TEMPOS_TENTATIVA} arquivos de tempo no diret�rio ${DIRETORIO_TENTATIVA}"
				exit 1
			fi
		done
		cd ..
		if ((${NUM_TENTATIVAS_ENCONTRADAS} != $NUM_TENTATIVAS ))
		then
			echo "Encontradas ${NUM_TENTATIVAS_ENCONTRADAS} nos arquivos de tempo de ${NUM_OBJETOS}. Quando se esperava encontrar ${NUM_TENTATIVAS} tentativas"
			exit 1
		fi

		#echo "Calculando o tempo m�dio para ${NUM_OBJETOS}: ${TEMPO_EXECUCAO_MEDIO} / ${NUM_TENTATIVAS}"

		# Divide pelo n�mero de tentativas para encontrar o valor m�dio
		TEMPO_EXECUCAO_MEDIO=`echo "${TEMPO_EXECUCAO_MEDIO} / ${NUM_TENTATIVAS}" | bc -l`

		echo "Tempos para ${NUM_OBJETOS}: M�nimo->${TEMPO_EXECUCAO_MINIMO} M�dio->${TEMPO_EXECUCAO_MEDIO} M�ximo->${TEMPO_EXECUCAO_MAXIMO}"

		echo "$NUM_OBJETOS	$TEMPO_EXECUCAO_MEDIO	$TEMPO_EXECUCAO_MINIMO	$TEMPO_EXECUCAO_MAXIMO" >> ${ARQUIVO_SAIDA}

		# Computa o processamento de mais um ponto
		NUM_PONTOS=`echo "${NUM_PONTOS} + 1" | bc -l`
	else
		echo "Encontrado arquivo n�o diret�rio ${DIRETORIO}. Ignorando-o..."
	fi
done
cd ${PWD}

if (($NUM_PONTOS < 2));
then
	echo "N�o foram encontrados pontos suficientes para o grafico. N�mero de Pontos encontrados: $NUM_PONTOS"
else
	sort -n ${ARQUIVO_SAIDA} > ${ARQUIVO_SAIDA}.2
	rm -f ${ARQUIVO_SAIDA}
	echo "#Numero de objetos	Tempo_Execucao_Medio	Tempo_Execucao_Minimo	Tempo_Execucao_Maximo" > ${ARQUIVO_SAIDA}
	echo "`more ${ARQUIVO_SAIDA}.2`" >> ${ARQUIVO_SAIDA}
	
	rm -f ${ARQUIVO_SAIDA}.2

	echo "Execu��o com sucesso! Encontrados ${NUM_PONTOS} pontos para o gr�fico"
fi



