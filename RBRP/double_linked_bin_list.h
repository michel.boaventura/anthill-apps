#ifndef _DOUBLE_LINKED_BIN_LIST_H
#define _DOUBLE_LINKED_BIN_LIST_H
#include "bin_type.h"

/* Prototypes */
void create_empty_bin_list(bin_list_type *list);
inline int is_empty_bin_list(bin_list_type *list);
inline unsigned int get_size_of_bin_list(bin_list_type *list);
void insert_in_bin_list(bin_type item, bin_list_type *list);
void insert_in_begin_bin_list(bin_type item, bin_list_type *list);
bin_type remove_of_bin_list(bin_list_pointer *p, bin_list_type *list);
bin_type remove_first_of_bin_list(bin_list_type *list);
int get_first_in_bin_list(bin_list_type *list, bin_list_pointer *pointer);
int get_last_in_bin_list(bin_list_type *list, bin_list_pointer *pointer);
int go_next_bin(bin_list_type *list, bin_list_pointer *pointer);
inline int is_last_bin(bin_list_pointer p, bin_list_type *list);
bin_list_pointer search_item_in_bin_list(bin_type item, bin_list_type *list);
void swap_bin_items(bin_list_pointer previous, bin_list_pointer next, bin_list_type *list);
inline void make_bin_list_empty(bin_list_type *list, dimension_size_type dimension_size);
void destroy_bin_list(bin_list_type *list, dimension_size_type dimension_size);
int get_number_objects_of_bin_list(bin_list_type *list);

void insert_in_bin_list_on_position(bin_type item, bin_list_pointer *p, bin_list_type *list);
void insert_in_ordered_bin_list(bin_type item, bin_list_type *list, int number_of_neighbours, dimension_size_type dimension_size);

bin_type remove_of_bin_list_on_position(unsigned int position, bin_list_type *list);
#endif

