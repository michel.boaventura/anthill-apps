#ifndef PROCEDURES_DISTRIBUTOR_H
#define PROCEDURES_DISTRIBUTOR_H
#include "rbrp.h"

void open_database_file (FILE **input_file, char *input_file_name);

int check_attributes (char *line, int number_of_attributes);

int fill_object_in_msg (void **msg, dimension_size_type dimension_size, object_type object);

int get_object_from_file(object_type *object, dimension_size_type dimension_size, FILE *input_file);


#endif
