#include <stdlib.h>
#Calcula o intervalo de confiança de cada linha colocando em cada coluna o seu valor
#O intervalo de confiança é calculado considerando-se uma confiança de 95%
{
	media=0.0;
	if (NR != 1)
	{
		printf("\t");
	}
	for (i = NF; i > 0; --i)
	{
		media+=($i); 
	} 
	media/=NF;
	variancia_amostra=0.0;
	for (i = NF; i > 0; --i)
	{
	   variancia_amostra+=($i-media)*($i-media);
	}
	variancia_amostra=variancia_amostra/(NF-1)
	function_factor=0.0;
	# Se eu possuo mais que 30 pontos, usarei a Distribuição normal z para determinar o fator
	if(NF > 30)
	{
		# Distribuição Normal com nível de confiança de 95%, logo z = 1,96
		function_factor=1.645
	}
	else
	{
	   # Calcula o valor da Distribuição t de Student com nível de confiança de 95% e o grau de liberdade desejado
	   grau_liberdade = NF-1;
	   if(grau_liberdade == 1) function_factor = 6.314;
	   else if(grau_liberdade == 2) function_factor = 2.920;
	   else if(grau_liberdade == 3) function_factor = 2.353;
	   else if(grau_liberdade == 4) function_factor = 2.132;
	   else if(grau_liberdade == 5) function_factor = 2.015;
	   else if(grau_liberdade == 6) function_factor = 1.943;
	   else if(grau_liberdade == 7) function_factor = 1.895;
	   else if(grau_liberdade == 8) function_factor = 1.860;
	   else if(grau_liberdade == 9) function_factor = 1.833;
	   else if(grau_liberdade == 10) function_factor = 1.812;
	   else if(grau_liberdade == 11) function_factor = 1.796;
	   else if(grau_liberdade == 12) function_factor = 1.782;
	   else if(grau_liberdade == 13) function_factor = 1.771;
	   else if(grau_liberdade == 14) function_factor = 1.761;
	   else if(grau_liberdade == 15) function_factor = 1.753;
	   else if(grau_liberdade == 16) function_factor = 1.746;
	   else if(grau_liberdade == 17) function_factor = 1.740;
	   else if(grau_liberdade == 18) function_factor = 1.734;
	   else if(grau_liberdade == 19) function_factor = 1.729;
	   else if(grau_liberdade == 20) function_factor = 1.725;
	   else if(grau_liberdade == 21) function_factor = 1.721;
	   else if(grau_liberdade == 22) function_factor = 1.717;
	   else if(grau_liberdade == 23) function_factor = 1.714;
	   else if(grau_liberdade == 24) function_factor = 1.711;
	   else if(grau_liberdade == 25) function_factor = 1.708;
	   else if(grau_liberdade == 26) function_factor = 1.706;
	   else if(grau_liberdade == 27) function_factor = 1.703;
	   else if(grau_liberdade == 28) function_factor = 1.701;
	   else if(grau_liberdade == 29) function_factor = 1.699;
	   else if(grau_liberdade == 30) function_factor = 1.697;
	}
	intervalo_confianca = (function_factor*(sqrt(variancia_amostra)))/(sqrt(NF));
	#printf("Average:%f\n", media);
	#printf("Function Factor: %f\n", function_factor);
	#printf("Sample variance:%f\n", variancia_amostra);
	#printf("Standard Deviation: %f\n", sqrt(variancia_amostra));
	#printf("N: %d sqrt(N): %f\n", NF, sqrt(NF) );
	#printf("IC: (%f , %f)\n", media-intervalo_confianca, media+intervalo_confianca);
	printf("%f", intervalo_confianca);	
}
END{
	printf("\n");
}
