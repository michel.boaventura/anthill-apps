#!/bin/bash
## Executa o rbrp para v�rios arquivos diferentes no diretorio de entrada e armazena 
## os tempos de execu��o no diret�rio de tempos.
## argumento1: Nome do diretorio onde ser�o armazenados os tempos de execu��o
## argumento2: Prefixo dos arquivos de entrada

	## Numero de objetos dos arquivos que ser�o executados
	OBJETOS='50'
	
	## Numero de dimensoes dos arquivos que ser�o executados
	DIMENSOES='2'

	## A Probabilidade de um n�mero no arquivo de entrada ser zero
	PROBABILIDADE_VALOR_ZERO=0.0005

	## O valor m�ximo do m�dulo dos valores no arquivo de entrada
	MODULO=1000

	## Nome do script que testa todas as configura��es
	SCRIPT_CONFS=./computa_todas_confs.sh

	EXECUTAVEL=./main

	## Diret�rio onde ficar�o armazenados os arquivos de entrada
	DIRETORIO_ENTRADA=./IO
	
	if [ $# != 2 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "script.sh diretorio_dos_tempos prefixo_dos_arquivos" 
		echo "Aten��o! Os arquivos armazenados no diretorio nome_diretorio_dos_tempos ser�o apagados."
        	exit 1
	fi

        DIRETORIO_TEMPOS=$1

	PREFIXO=$2
	
	# Preparando os diretorios de entrada e sa�da #
	echo "Preparando os diretorios de entrada e sa�da"

	# Verifica a exist�ncia dos arquivos que o script depende, pois executa-os
	if [ ! -e  ${SCRIPT_CONFS} ]; 
	then
		echo "Erro: N�o foi poss�vel encontrar o script: ${SCRIPT_CONFS}"
		echo "N�o � poss�vel realizar as execu��es sem esse script... Abortando..."
		exit 1
	fi
	if [ ! -e ${EXECUTAVEL} ]; 
	then
		echo "Erro: N�o foi poss�vel encontrar o execut�vel: ${EXECUTAVEL}"
		echo "N�o � poss�vel realizar as execu��es sem esse execut�vel... Abortando..."
		exit 1
	fi

	# Cria o diretorio de entrada se ele ainda n�o existir
	if [ ! -d ${DIRETORIO_ENTRADA} ];
	then
		mkdir -v ${DIRETORIO_ENTRADA};
	fi

	# Cria os diret�rios dos tempos e de sa�da se eles ainda n�o existirem
	if [ ! -d ${DIRETORIO_TEMPOS} ];
	then
		mkdir -v ${DIRETORIO_TEMPOS};
	fi

	for NUM_DIMENSOES in $DIMENSOES
	do
		for NUM_OBJETOS in $OBJETOS
		do
			if [ -e ${DIRETORIO_TEMPOS}/rbrp${NUM_OBJETOS}_${NUM_DIMENSOES}.time ]; 
			then
				# Se j� existem arquivos de tempo no diretorio, aborta-se a execu��o

				echo "O diret�rio de armazenamento dos tempos n�o est� vazio.";
				echo "Fa�a backup do mesmo se quiser e limpe-o ou remova-o.";
				echo "O arquivo: ${DIRETORIO_TEMPOS}/rbrp${NUM_OBJETOS}_${NUM_DIMENSOES}.time j� existe";
				exit 1;
			fi
		done
	done
	
	# Execu��o dos testes
        for NUM_DIMENSOES in $DIMENSOES
	do
		for NUM_OBJETOS in $OBJETOS
		do
			# Atualiza��o do arquivo de entrada
			ARQUIVO_ENTRADA=${PREFIXO}.ascii.${NUM_OBJETOS}.${NUM_DIMENSOES}

			# Prepara��o do diret�rio de entrada e cria��o do arquivo de entrada
			echo "cd ${DIRETORIO_ENTRADA}"
			cd ${DIRETORIO_ENTRADA}
			echo "gera_entrada ${ARQUIVO_ENTRADA} ${NUM_OBJETOS} ${NUM_DIMENSOES} ${PROBABILIDADE_VALOR_ZERO} ${MODULO}"
			gera_entrada ${ARQUIVO_ENTRADA} ${NUM_OBJETOS} ${NUM_DIMENSOES} ${PROBABILIDADE_VALOR_ZERO} ${MODULO}
			echo "cd .."
			cd ..

			# Prepara��o dos diret�rios de tempos
			echo "cd ${DIRETORIO_TEMPOS}"
			cd ${DIRETORIO_TEMPOS}
		
			if [ ! -d ./${NUM_OBJETOS}_objetos_${NUM_DIMENSOES}_dimensoes ];
			then 
				mkdir -v ./${NUM_OBJETOS}_objetos_${NUM_DIMENSOES}_dimensoes	
			else
				rm -f -v ./${NUM_OBJETOS}_objetos_${NUM_DIMENSOES}_dimensoes/*
			fi
			echo "cd .."
			cd ..
	
			if [ ! -e ./${DIRETORIO_ENTRADA}/${ARQUIVO_ENTRADA} ]; 
			then
				echo "Arquivo de entrada ASCII ../${DIRETORIO_ENTRADA}/${ARQUIVO_ENTRADA} n�o encontrado."
			else
				# Chamada do Script computa_todas_instancias.sh para a execu��o do rbrp com todas as configura��es que se deseja
				echo "Executando o RBRP para todas as configura��es com ${NUM_OBJETOS} objetos e ${NUM_DIMENSOES} dimens�es ..."
				echo "${SCRIPT_CONFS} ${ARQUIVO_ENTRADA} ${DIRETORIO_TEMPOS}/${NUM_OBJETOS}_objetos_${NUM_DIMENSOES}_dimensoes/ ${NUM_DIMENSOES}"
				${SCRIPT_CONFS} ${ARQUIVO_ENTRADA} ${DIRETORIO_TEMPOS}/${NUM_OBJETOS}_objetos_${NUM_DIMENSOES}_dimensoes/ ${NUM_DIMENSOES}
	
				echo "Removendo o arquivo de entrada para liberar mem�ria..."
				rm -f -v ${DIRETORIO_ENTRADA}/${ARQUIVO_ENTRADA}
			fi
		done
	done
