#ifndef PROCEDURES_BIN_H
#define PROCEDURES_BIN_H
#include "rbrp.h"

/*Global Variables for Instrumentation*/

#ifdef INSTRUMENTATION
extern unsigned long long MY_POINT_IN_BIN; // This variable will be incremented if a outilier of the proper instance bin don't pass of the local bin
extern unsigned long long MY_POINT_IN_INST; // This variable will be incremented if a outilier of the proper instance bin don't pass of the local instance
extern unsigned long long OTHER_POINT_IN_BIN; // This variable will be incremented if a outilier of the other instance don't pass of the local bin
extern unsigned long long OTHER_POINT_IN_INST; // This variable will be incremented if a outilier of the other instance don't pass of the local instance
extern unsigned long long OTHER_POINT_IN_SCORE;// This variable will be incremented if a outilier of the other instance don't pass of the local score
extern unsigned long long IS_MY_OBJECT; // This variable is a boolean to indicate what instance bin the object belong. 
extern unsigned long long MY_OBJECT_IN_FIRST_BIN; 
extern unsigned long long NUMBER_OF_COMPARATIONS;
extern unsigned long long COUNTER_COMPARATIONS_IN_FIRST_BIN;
extern unsigned long long NUMBER_OF_COMPARATIONS_IN_FIRST_BIN;
extern unsigned long long NUMBER_OF_COMPARATIONS_OF_OBJECTS_ELIMINATED_IN_FIRST_BIN;
extern unsigned long long NUMBER_OF_SPLITS;
extern unsigned long long NUMBER_OF_MYPROB_MSGS;
extern unsigned long long NUMBER_OF_OUTPROB_MSGS;
extern unsigned long long NUMBER_OF_LOCAL_BINS_MSG;
extern unsigned long long PRUNING_COMPLETE_BINS;
extern unsigned long long NUMBER_OF_BINS_ELIMINATED;
extern unsigned long long PRUNING_SEARCH_IN_BINS;
extern unsigned long long NUMBER_OF_BINS_NOT_SEARCH;
extern unsigned long long PRUNING_OBJECTS_USING_BINS;
extern unsigned long long	NUMBER_OF_OBJECTS_CONSIDERED;
extern struct timeval INITIAL_TIME_INST;
#endif

#ifdef GET_TIME
extern struct timeval first_phase_part_time;
#endif 

extern char output_file_name[MAX_FILENAME];

/*Functions*/
int receive_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size , InputPortHandler input_port);

int create_new_bins_in_split(bin_list_type *list, bin_list_pointer pointer_to_splitted_bin, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size );

bin_number_type get_next_bin_number();

int split_bin_action(bin_list_type *bin_list, bin_list_pointer splitted_bin_pointer, dimension_size_type dimension_size , int Assigner_output);

int execute_bin_first_phase(bin_list_type *bin_list, dimension_size_type dimension_size, int number_of_neighbours, int bin_size, OutputPortHandler Assigner_output, InputPortHandler Assigner_input);

int get_bins_local_values_from_msg(int *msg, bin_list_type *list, bin_list_pointer *splitted_bin_pointer, bin_list_pointer **pointer_to_bins, dimension_size_type dimension_size , unsigned int number_of_assigner_instances);

int get_object_values_of_bin_from_msg(int *msg, bin_list_type *list, dimension_size_type dimension_size );

int clean_bin_list(bin_list_type *list, dimension_size_type dimension_size);

int print_bins_done(bin_list_type *list, dimension_size_type dimension_size );

int process_bins_local_values_msg(int *msg, bin_list_type *bin_list, dimension_size_type dimension_size, int number_of_neighbours, unsigned int bin_size, unsigned int number_of_assigner_instances, OutputPortHandler port);

int needs_split_bin_action(bin_type *bin, float *prune_ratio, int number_of_neighbours, int bin_size, dimension_size_type dimension_size);

int is_valid_bin(bin_type *current_bin, distance_type *highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours, dimension_size_type dimension_size); 

int calculate_all_closest_bins(bin_list_type *list, dimension_size_type dimension_size );

int calculate_closest_bins(bin_type *bin, bin_list_type *list, dimension_size_type dimension_size );

int insert_distance_between_bins_in_closest_bins(bin_type *bin, bin_type *bin2, dimension_size_type dimension_size );

int execute_bin_second_phase(bin_list_type *bin_list, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours, InputPortHandler Bin_input, OutputPortHandler Bin_output, OutputPortHandler Final_output);

int process_score_new_outlier_message(distance_type *highest_scores, message_type message, unsigned int number_of_outliers);

int process_probable_outlier_message(message_type message, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours, OutputPortHandler Bin_output, OutputPortHandler Final_output);

int search_for_nearest_neighbours_in_bin_list(nearest_neighbours_type *nearest_neighbours, object_type *object, bin_list_pointer bin_pointer, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size , unsigned int number_of_neighbours, unsigned int number_of_bins);

int search_for_nearest_neighbours_in_bin(nearest_neighbours_type *nearest_neighbours, object_type *object, bin_type *bin, distance_type *highest_scores, dimension_size_type dimension_size , unsigned int number_of_outliers, unsigned int number_of_neighbours);

int new_search_object_as_outlier(list_pointer *previous_object, bin_list_pointer *current_bin, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size , unsigned int number_of_outliers, unsigned int number_of_neighbours, OutputPortHandler Bin_output, OutputPortHandler Final_output);

int check_for_probable_outlier(nearest_neighbours_type *nearest_neighbours, distance_type* highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours);

int sort_bin_list(bin_list_type *bin_list1, bin_list_type *bin_list2, int number_of_neigbours, dimension_size_type dimension_size);

#endif
