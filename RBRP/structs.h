#ifndef _STRUCTS_H_
#define _STRUCTS_H_

/*! \file structs.h
    \brief Definition of all structures of the RBRP algorithm
    
    In this file are defined all the structures manipulated on the RBRP algorithm.
*/

//! Real types of each minerable attribute type
typedef double real;
typedef double ordinal;
typedef int categorical;

//! Real type of the distance type
typedef double distance_type;


//! Type that defines the Bin number
typedef unsigned long long bin_number_type;

//! structure responsible for storing the dimension values
typedef struct
{
	real *real_values;			/*!< Attributes with Real Values */  
	ordinal *ordinal_values;		/*!< Attributes with Ordinal Values */
	categorical *categorical_values;	/*!< Attributes with Categorical Values */
} dimension_values_type;

//! structure responsible for storing the number of values of each type of dimension values 
typedef struct
{
	unsigned int number_of_real_values;		/*!< Number of Attributes with Real Values */  
	unsigned int number_of_ordinal_values;		/*!< Number of Attributes with Ordinal Values */
	unsigned int number_of_categorical_values;	/*!< Number of Attributes with Categorical Values */
} dimension_size_type;

//! structure responsible for storing the closest neighbours and the distance of this bins
typedef struct
{
	int size;
	unsigned int *objects;		/*!< The number of the nearest objects  */
	distance_type *distance;		/*!< The distance of the nearest objects */
} nearest_neighbours_type;

/* Definitions about the structure of an object and the lists of object */
//! structure responsible for storing one object
typedef struct
{
	unsigned int number;			/*!< Object Number */
	dimension_values_type dimension_values;	/*!< dimension values */
} object_type;

typedef struct cell
{
   object_type item;
   struct cell *previous;
   struct cell *next;
} cell, *list_pointer;

typedef struct
{
   list_pointer first;
   list_pointer last;
   unsigned int length;
} list_type;

/* Definitions about the structure of an occurrence type and the lists of occurrences */
//! structure responsible for storing one the number of occurrences of each values of an attribute 
typedef struct
{
	int value_number;					/*!< The Value Number */
	unsigned int frequency;					/*!< Number of occurrences of the value */

} occurrences_type;

typedef struct occurrences_cell
{
   occurrences_type item;
   struct occurrences_cell *previous;
   struct occurrences_cell *next;
} occurrences_cell, *occurrences_list_pointer;

typedef struct
{
   occurrences_list_pointer first;
   occurrences_list_pointer last;
   unsigned int length;
} occurrences_list_type;

//! structure responsible for storing one centroid
typedef struct
{
	dimension_values_type dimension_values;		/*!< Dimension values */  
	dimension_values_type new_dimension_values;	/*!< Partial Dimension values that considered just a few of the parts */
	int number_of_objects_considered;		/*!< Number of objects considered to calculate the new_dimension_values */
	dimension_values_type upper_bound_mbr;
	dimension_values_type lower_bound_mbr;
	occurrences_list_type *occurrences;		/*!< Array that saves the occurrences of each value on categorical attributes */
} centroid_type;

//! structure responsible for storing the closest bins and the distance of this bins
typedef struct
{
	int size;
	struct bin_type_str **bin_pointers;		/*!< array of bin pointers  */
	distance_type *distance;		/*!< The distance of the bin */
} closest_bins_type;


/* Definitions about the structure of a bin and lists of bin */
//! structure responsible for storing one bin
typedef struct bin_type_str
{
	bin_number_type number;			/*!< Bin Number */
	int state;				/*!< The State of the BIN */
	unsigned int local_values_received;	/*!< Controls the number of local values received by the bin filter */
	list_type objects;			/*!< List of objects of the bin  */
	centroid_type centroid;	
	closest_bins_type closest_bins;
} bin_type;

typedef struct bin_cell_str
{
   bin_type item;
   struct bin_cell_str *previous;
   struct bin_cell_str *next;
} bin_cell, *bin_list_pointer;

typedef struct
{
   bin_list_pointer first;
   bin_list_pointer last;
   unsigned int length;
} bin_list_type;


/* Definitions about the structure of an message and the lists of messages */
//! structure responsible for storing one message
typedef void* message_type;

typedef struct msg_cell
{
   message_type item;
   struct msg_cell *previous;
   struct msg_cell *next;
} msg_cell, *msg_list_pointer;

typedef struct
{
   msg_list_pointer first;
   msg_list_pointer last;
   unsigned int length;
} msg_list_type;

typedef struct
{
	list_type objects;			/*!< List of objects of the outliers */
	distance_type *highest_scores;			/*!< Array with the scores of the outliers */
} outliers_type;

#endif
	
