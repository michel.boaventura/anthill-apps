/****************************************************************************/
/*      This file stores the auxiliary functions of the filter Final        */
/****************************************************************************/

#include "procedures_final.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Receives an object candidate to be outlier and process this message.
 * It will verify if the object really is an actual outlier and if it is an outlier save it */

int process_new_outlier_message(message_type message, outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_outliers, int number_of_neighbours)
{
	nearest_neighbours_type aux_nearest_neighbours, temp;
	object_type object;
	int i, inserted_outlier, number_of_outliers_found, outlier_position;
	distance_type score;

	number_of_outliers_found=get_number_of_outliers_found(outliers);
	
	/* Get the object and the nearest neighbour from the message */
	/* It allocates the memory needed for the nearest neighbours fields */
	get_new_outlier_from_msg(message, &object, &aux_nearest_neighbours, dimension_size, number_of_neighbours);

	/* Get the score of the outlier */
	score = get_minimum_distance_in_nearest_neighbours(&aux_nearest_neighbours, number_of_neighbours);

	inserted_outlier = insert_new_outlier(&object, score, outliers, &outlier_position, number_of_outliers);

	/* If really a new outlier was found, we will save the nearest neighbours of this outlier */
	if (inserted_outlier)
	{
		temp=nearest_neighbours[number_of_outliers-1];
			
		/* Move forward the neighbours to insert the new neighbours on the rigth position */  
		for (i=number_of_outliers-1; i>outlier_position; i--)	
		{
			nearest_neighbours[i]=nearest_neighbours[i-1];
		}

		/* Saves the new nearest neighbours on the nearest neighbours array */
		nearest_neighbours[outlier_position]=aux_nearest_neighbours;

		/* Tests if there is a nearest neighbour that was removed from the list */
		if (number_of_outliers_found == number_of_outliers)
		{
			free_nearest_neighbours(temp);
		}
	}
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Print all the outliers of the list  of outliers and the Nearest Neighbours of the Outliers */
void print_results(outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, dimension_size_type dimension_size, FILE *file)
{
	list_pointer aux;

	int outlier_number=0;

	list_type *list=&(outliers->objects);
	
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_list(list, &aux))
	{
		fprintf(file, "There are no Outliers!!!\n");	   
		return;
	}
   
	/* Prints the First Object of the list */
	print_object(aux->item, dimension_size, file);
	fprintf(file, "Score: ");
	fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
	fprintf(file, "\n");
	
	print_nearest_neighbours(&(nearest_neighbours[outlier_number]), aux->item.number, number_of_neighbours, file);
	fprintf(file, "---\t---\t---\t---\t---\t---\t---\n");
	outlier_number++;
	
	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		
		/* Prints the First Object of the list */
		print_object(aux->item, dimension_size, file);
		fprintf(file, "Score: ");
		fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
		fprintf(file, "\n");
		print_nearest_neighbours(&(nearest_neighbours[outlier_number]), aux->item.number, number_of_neighbours, file);
		fprintf(file, "---\t---\t---\t---\t---\t---\t---\n");
		outlier_number++;
	}
}

