#!/bin/bash
## argumento1: Nome do diretorio1 onde est�o os arquivos dos bins
## argumento2: Nome do diretorio2 onde est�o os arquivos dos bins

	if [ $# != 2 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "compara_saida.sh diretorio1 diretorio2" 
        	exit 1
	fi

	DIRETORIO_1=$1
        DIRETORIO_2=$2

	echo "cd ${DIRETORIO_1}"	
	cd ${DIRETORIO_1}	

	# Procura os arquivos do diretorio 1 fazendo diff deles com os arquivos do diret�rio 2 se existirem
	for FILE in outliers.*.out
	do
		# Se o arquivo do diretorio 1 n�o existe no diret�rio 2 reporta-se esse fato
		if [ ! -e ../${DIRETORIO_2}/${FILE} ]; 
		then
			echo "O arquivo ${FILE} foi encontrado no diret�rio 1 e parece n�o existir no diret�rio 2!!!"
		else
			echo "Realizando DIFF do arquivo ${FILE}"
			DIFF= diff ../${DIRETORIO_1}/${FILE} ../${DIRETORIO_2}/${FILE}
			if [ ! DIFF ];
			then
				echo "O arquivo ${FILE} parece ser diferente!!! "
				echo "O diff foi realizado entre os arquivos ../${DIRETORIO_1}/${FILE} e ../${DIRETORIO_2}/${FILE}"
				exit 1
			fi
		fi
	done
	echo "cd .."
	cd ..
	echo "cd ${DIRETORIO_2}"	
	cd ${DIRETORIO_2}	

	for FILE in outliers.*.out
	do
		# Se o arquivo do diretorio 2 n�o existe no diret�rio 1 reporta-se esse fato
		if [ ! -e ../${DIRETORIO_1}/${FILE} ]; 
		then
			echo "O arquivo ${FILE} foi encontrado no diret�rio 2 e parece n�o existir no diret�rio 1!!!"
		fi
	done
	echo "cd .."
	cd ..
