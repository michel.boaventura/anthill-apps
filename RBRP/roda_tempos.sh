#!/bin/bash
## Executa o rbrp para v�rios arquivos diferentes no diretorio de entrada e armazena 
## os tempos de execu��o no diret�rio de tempos.
## argumento1: Nome do arquivo de entrada
## argumento2: Nome do diretorio onde ser�o armazenados os tempos de execu��o
## argumento3: N�mero de atributos reais do arquivo de entrada
## argumento4: N�mero de atributos ordinais do arquivo de entrada
## argumento5: N�mero de categoricos do arquivo de entrada

## O numero de tentativas que se deseja 
NUM_TENTATIVAS=10

if [ $# != 6 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "roda_tempos.sh arquivo_entrada diretorio_tempos numero_de_atributos_reais numero_de_atributos_ordinais numero_de_atributos_categoricos bin_size"
	echo "Aten��o! Os arquivos armazenados no diretorio diretorio_dos_tempos ser�o apagados."
        exit 1
fi

ARQUIVO_ENTRADA=$1

DIRETORIO_TEMPOS=$2

NUM_REAL=$3

NUM_ORDINAL=$4

NUM_CATEGORICAL=$5

BIN_SIZE=$6

# Execu��o dos testes
for TENTATIVA in `seq 1 $NUM_TENTATIVAS`
do
	echo "Executando a tentativa $TENTATIVA"
	computa_todas_confs.sh $ARQUIVO_ENTRADA $DIRETORIO_TEMPOS/$TENTATIVA $NUM_REAL $NUM_ORDINAL $NUM_CATEGORICAL $BIN_SIZE
done
