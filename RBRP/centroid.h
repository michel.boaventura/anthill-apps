#ifndef _CENTROID_H
#define _CENTROID_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
#include "math.h"
#include "dimension_values.h"

/* Prototypes */
void print_centroid(centroid_type centroid, dimension_size_type dimension_size, FILE *fp_log);
inline int compare_centroids(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size);
void free_centroid(centroid_type centroid, dimension_size_type dimension_size);
void malloc_centroid(centroid_type *centroid, dimension_size_type dimension_size);
inline distance_type distance_between_centroids(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size);
void compute_object_for_updating_centroid_mbr_values(centroid_type *centroid, object_type *object, dimension_size_type dimension_size);
void insert_object_occurrences(centroid_type *centroid, object_type *object, dimension_size_type dimension_size);
void remove_object_occurrences(centroid_type *centroid, object_type *object, dimension_size_type dimension_size);
void *get_mbr_values_from_msg(centroid_type *centroid, void *msg, dimension_size_type dimension_size);
void *update_mbr_values_from_msg(centroid_type *centroid, void *msg, dimension_size_type dimension_size);
void *get_centroid_local_values_from_msg(centroid_type *centroid, unsigned int local_length, void *msg, dimension_size_type dimension_size);
distance_type update_centroid_dimension_values(centroid_type *centroid, dimension_size_type dimension_size);
void *fill_centroid_values_on_msg(void *msg_dest, centroid_type *src, dimension_size_type dimension_size);
int generate_random_centroid(centroid_type *dest, centroid_type *src, dimension_size_type dimension_size);
void get_representative_min_point_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size, dimension_values_type *dimension_values_aux);
void get_representative_max_point_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size, dimension_values_type *dimension_values_aux);
void get_representatives_min_points_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size, dimension_values_type *dimension_values1, dimension_values_type *dimension_values2);
void get_representatives_max_points_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size, dimension_values_type *dimension_values1, dimension_values_type *dimension_values2);
distance_type distance_min_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size);
distance_type distance_max_between_point_mbr(dimension_values_type dimension_values, centroid_type centroid, dimension_size_type dimension_size);
distance_type distance_min_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size);
distance_type distance_max_between_mbrs(centroid_type centroid1, centroid_type centroid2, dimension_size_type dimension_size);

//distance_type update_dimension_values_from_local_values(centroid_type *centroid, dimension_size_type dimension_size);


#endif

