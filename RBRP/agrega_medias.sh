#!/bin/bash

#Garante que o ponto ser� interpretado como separador de casa decimal
export LC_NUMERIC="C"

if [ $# != 1 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "agrega_medias.sh diretorio_experimentos" 
        exit 1
fi

DIRETORIO_EXPERIMENTOS=$1

DIRETORIO_CORRENTE=`pwd`

echo "Diretorio corrente: $DIRETORIO_CORRENTE"

cd ${DIRETORIO_EXPERIMENTOS}

# Checking for processed files
for AGR_FILE in *.agr
do 
	if [ -f $AGR_FILE ];
	then
		echo "Parece que existem arquivos j� agregados."
		echo "Removendo arquivo: $AGR_FILE"
		rm -f $AGR_FILE
	fi
done

#Lista as bases encontradas no diret�rio
for DIRETORIO in *
do
	if [ -d $DIRETORIO ];
	then
		#Obtendo a base de dados
		BASE=`echo "${DIRETORIO}" | cut -f 1 -d "."`
		EXISTS=0
		for EXISTING_BASE in $BASES
		do
			if [ $EXISTING_BASE = $BASE ]
			then
				EXISTS=1
			break;
			fi
		done
		if [ "$EXISTS" = "0" ];
		then
		BASES="$BASES $BASE"
		fi
	fi
done

echo "As bases encontradas no diret�rio foram: $BASES"

for BASE in $BASES
do
	echo "Iniciando processamento da base de dados $BASE"
		
	for DIRETORIO in $BASE.*
	do
		if [ -d $DIRETORIO ];
		then 
			#Obtendo o Binsize
			BINSIZE=`echo "${DIRETORIO}" | cut -f 2 -d "." | sed "s/binsize//g"`
				
			echo "Processando os tempos de execu��o para o Binsize $BINSIZE"
     			echo "Gerando os arquivos com as medias"

     			cd $DIRETORIO_CORRENTE
     			echo "paste_experiments.sh ${DIRETORIO_EXPERIMENTOS}/$DIRETORIO"
     			paste_experiments.sh ${DIRETORIO_EXPERIMENTOS}/$DIRETORIO
     			cd -

     			cd $DIRETORIO
     			for FILE in *.avg
     			do
     				if [ -f $FILE ];
     				then
     					echo "Processando dados do arquivo $FILE no diret�rio $DIRETORIO"
     					NAME_FILE=`echo "${FILE}" | sed "s/binsize//g"`
     					AGREGATE_FILE=../$NAME_FILE.agr
     					if [ ! -f $AGREGATE_FILE ];
     					then 
     						echo "Criando o arquivo com os valores agregados $AGREGATE_FILE"
     						touch $AGREGATE_FILE
     						echo -n "$BINSIZE	" > $AGREGATE_FILE
     					else
     						echo "Preenchendo o arquivo com os valores agregados $AGREGATE_FILE"
     						echo -n "$BINSIZE	" >> $AGREGATE_FILE
     					fi
     					cat ${FILE} >> $AGREGATE_FILE
						
						#Reordenando o arquivo agregado com base na primeira coluna (n�mero de bins)
						echo "mv $AGREGATE_FILE temp"
						mv $AGREGATE_FILE temp
						echo "sort -n temp > $AGREGATE_FILE"
						sort -n temp > $AGREGATE_FILE
						rm -f temp
     				else
						if [ "$FILE" = "*.avg" ];
						then
							echo "Ignorando diret�rio: ${DIRETORIO}"
							echo "Esse diret�rio parece n�o ter nenhum arquivo com m�dias..."
     					else
							echo "$FILE n�o � um diret�rio!!! Ignorando..."
						fi
     				fi
     			done
				for FILE in *.ic95
     			do
     				if [ -f $FILE ];
     				then
     					echo "Processando dados do arquivo $FILE no diret�rio $DIRETORIO"
     					NAME_FILE=`echo "${FILE}" | sed "s/binsize//g"`
     					AGREGATE_FILE=../$NAME_FILE.agr
     					if [ ! -f $AGREGATE_FILE ];
     					then 
     						echo "Criando o arquivo com os valores agregados $AGREGATE_FILE"
     						touch $AGREGATE_FILE
     						echo -n "$BINSIZE	" > $AGREGATE_FILE
     					else
     						echo "Preenchendo o arquivo com os valores agregados $AGREGATE_FILE"
     						echo -n "$BINSIZE	" >> $AGREGATE_FILE
     					fi
     					cat ${FILE} >> $AGREGATE_FILE
 
						#Reordenando o arquivo agregado com base na primeira coluna (n�mero de bins)
						echo "mv $AGREGATE_FILE temp"
						mv $AGREGATE_FILE temp
						echo "sort -n temp > $AGREGATE_FILE"
						sort -n temp > $AGREGATE_FILE
						rm -f temp
   				else
						if [ "$FILE" = "*.ic95" ];
						then
							echo "Ignorando diret�rio: ${DIRETORIO}"
							echo "Esse diret�rio parece n�o ter nenhum arquivo com intervalos de confian�a de 95%..."
     					else
							echo "$FILE n�o � um diret�rio!!! Ignorando..."
						fi
     				fi
     			done
				for FILE in *.ic90
     			do
     				if [ -f $FILE ];
     				then
     					echo "Processando dados do arquivo $FILE no diret�rio $DIRETORIO"
     					NAME_FILE=`echo "${FILE}" | sed "s/binsize//g"`
     					AGREGATE_FILE=../$NAME_FILE.agr
     					if [ ! -f $AGREGATE_FILE ];
     					then 
     						echo "Criando o arquivo com os valores agregados $AGREGATE_FILE"
     						touch $AGREGATE_FILE
     						echo -n "$BINSIZE	" > $AGREGATE_FILE
     					else
     						echo "Preenchendo o arquivo com os valores agregados $AGREGATE_FILE"
     						echo -n "$BINSIZE	" >> $AGREGATE_FILE
     					fi
     					cat ${FILE} >> $AGREGATE_FILE
     				 
						#Reordenando o arquivo agregado com base na primeira coluna (n�mero de bins)
						echo "mv $AGREGATE_FILE temp"
						mv $AGREGATE_FILE temp
						echo "sort -n temp > $AGREGATE_FILE"
						sort -n temp > $AGREGATE_FILE
						rm -f temp
					else
						if [ "$FILE" = "*.ic90" ];
						then
							echo "Ignorando diret�rio: ${DIRETORIO}"
							echo "Esse diret�rio parece n�o ter nenhum arquivo com intervalos de confian�a de 90%..."
     					else
							echo "$FILE n�o � um diret�rio!!! Ignorando..."
						fi
     				fi
     			done

     			cd -
     		else
     			echo "$DIRETORIO n�o � um diret�rio!!!"
     		fi

     	done
     	echo "Fim do processamento da base de dados $BASE"
done
