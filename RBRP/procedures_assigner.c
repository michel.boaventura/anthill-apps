/****************************************************************************/
/*      This file stores the auxiliary functions of the filter Assigner     */
/****************************************************************************/

#include "procedures_assigner.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int receive_database(bin_list_type *bin_list, dimension_size_type dimension_size, InputPortHandler input_port)
{
	unsigned int msg_size = sizeof(unsigned int) + get_dimension_values_message_size(dimension_size);	// size of the mesage that will be received
	void *message;
	
	message=malloc(msg_size);	// message that will received

	bin_list_pointer first_bin;

	object_type object;
	
	list_type *object_list;
	
	get_first_in_bin_list(bin_list, &first_bin);

	object_list=&(first_bin->item.objects);

	while (ahReadBuffer (input_port, message, msg_size) != EOW)
	{
		/* Get an object from the message */
		get_object_from_msg(&object, dimension_size, message);
		insert_in_list(object, object_list);

	}

	free(message);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Calculate the minimum and maximum MBR values of a Bin */
/* The values are the minimum and maximum values of the dimensions of all objects present on the bin */
int calculate_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size)
{
	bin_list_pointer first_bin;
	list_pointer aux;
	list_type *object_list;
	int i, number_of_points_measured;
	centroid_type *centroid;

	get_first_in_bin_list(bin_list, &first_bin);

	centroid = &(first_bin->item.centroid);
	
	object_list=&(first_bin->item.objects);

	/* If the first item is not found, it just returns from the function indicating an error */
	if (!get_first_in_list(object_list, &aux))
	{	
		return 0;
	}

	/* Initializes the First values for the MBR values*/
	copy_dimension_values(&(centroid->upper_bound_mbr), &(aux->item.dimension_values), dimension_size);
	copy_dimension_values(&(centroid->lower_bound_mbr), &(aux->item.dimension_values), dimension_size);
	
	copy_dimension_values(&(centroid->dimension_values), &(aux->item.dimension_values), dimension_size);
	copy_dimension_values(&(centroid->new_dimension_values), &(aux->item.dimension_values), dimension_size);

	/* Computes the occurrence of the values of the categorical values of the first object on the occurrences list */
	insert_object_occurrences(centroid, &(aux->item), dimension_size);	

	/* If the list have the number of objects needed in a sample*/
	if (object_list->length>=NUMBER_OF_POINTS_OF_SAMPLES)
		number_of_points_measured=NUMBER_OF_POINTS_OF_SAMPLES;
	else
		/* If there is smaller than NUMBER_OF_POINTS_OF_SAMPLES in the list, we will process all the list */
		number_of_points_measured=object_list->length;
	
	for (i=1;i<number_of_points_measured && !is_last(aux, object_list); i++)
	{
		go_next(object_list, &aux);
	
		/* Updates the values of the MBR bounds if the current object have values that metter on these values*/
		compute_object_for_updating_centroid_mbr_values(centroid, &(aux->item), dimension_size);
	}
	
	/* Calculates the centroid dimension values making the division between the sum by the length. It will be done on the real values and ordinal values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		centroid->dimension_values.real_values[i]=centroid->dimension_values.real_values[i]/(real)number_of_points_measured;
	}
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		centroid->dimension_values.ordinal_values[i]=centroid->dimension_values.ordinal_values[i]/(ordinal)number_of_points_measured;
	}

	return (number_of_points_measured);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Print all the bins of the list each one on a different file*/
int print_objects_in_bins(bin_list_type *list, dimension_size_type dimension_size)
{
	bin_list_pointer aux;
	FILE *file;
	char name_of_the_file[50];
	char directory_name[50];
	char error_msg[100];
	static int round=0;

	// Just computes a new round
	round++;
	
	/* If the first item is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}

     	// The name of the directory
	sprintf(directory_name, "round_%d", round);
   
	/* Creates a directory to save the Bins in the actual round */
	mkdir(directory_name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	/* Prints the First Bin of the list */
	sprintf(name_of_the_file, "%s/bin_%llu.ascii", directory_name, aux->item.number); 

	// Open the file of the bin
	file = fopen(name_of_the_file,"w");

	if (file==NULL)
	{
		sprintf(error_msg,"ERROR. Failed to create the temporary file: %s to keep the results of round %d!!!\nSystem Error: %s\n", name_of_the_file, round, strerror(errno));
		ahExit (error_msg);
	}

#ifdef PRINT_CENTROID_VALUES
	if (aux->item.state == NORMAL_STATE || aux->item.state == CONCLUDED)
	{
		print_bin(aux->item, dimension_size, file);
		fclose(file);
	}
	else
	{
		fclose(file);
		unlink(name_of_the_file);
	}
#else
	// Prints the list just in case this is not empty
	if (is_empty(&(aux->item.objects)))
	{
		fclose(file);
		unlink(name_of_the_file);
	}
	else
	{
		print_list(&(aux->item.objects), dimension_size, file);
		fclose(file);
	}
#endif

	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);

		/* Prints the Bin on the file */
		sprintf(name_of_the_file, "%s/bin_%llu.ascii", directory_name, aux->item.number); 

		// Open the file of the bin
		file = fopen(name_of_the_file,"w");
		if (file==NULL)
		{
			sprintf(error_msg,"ERROR. Failed to create the temporary file: %s to keep the results of round %d!!!\nSystem Error: %s\n", name_of_the_file, round, strerror(errno));
			ahExit (error_msg);
		}

#ifdef PRINT_CENTROID_VALUES
		if (aux->item.state == NORMAL_STATE || aux->item.state == CONCLUDED)
		{
			print_bin(aux->item, dimension_size, file);
			fclose(file);
		}
		else
		{
			fclose(file);
			unlink(name_of_the_file);
		}
#else
		// Prints the list just in case this is not empty
		if (is_empty(&(aux->item.objects)))
		{
			fclose(file);
			unlink(name_of_the_file);
		}
		else
		{
			print_list(&(aux->item.objects), dimension_size, file);
			fclose(file);
		}
#endif
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to execute the whole first phase of the RBRP */
int starts_first_phase(bin_list_type *bin_list, dimension_size_type dimension_size, InputPortHandler Bin_input, OutputPortHandler Bin_output)
{
	// The maximum length of a message that an assigner instance can receive on this phase
	unsigned int msg_size = get_new_iteration_message_size(dimension_size);
	void *message;
	int *int_msg;
	char error_msg[100];
	unsigned int number_of_bins_concluded=0, number_of_bins_splitted=0;

// variables for instrumetation
#ifdef INSTRUMENTATION
		struct timeval READ_BUFFER_INITIAL_TIME, READ_BUFFER_FINAL_TIME;
	 	double parcial_idle_time = 0, total_idle_time = 0, real_time = 0;
		int i = 0;
		//Get o number of objets initial
		NUMBER_OF_OBJECTS_UNSTABLE = get_number_objects_of_bin_list(bin_list);
		
		//open file normal instrumentation
		FILE *fp_inst;
		char name_of_fp_inst[MAX_FILENAME];
		sprintf(name_of_fp_inst, "%s.assigner_%d.dat", output_file_name, ahGetMyRank());
		fp_inst = fopen(name_of_fp_inst, "w");
		fprintf(fp_inst, "#TIME #IDLE_TIME #NUMBER_TASKS #OBJECTS_UNSTABLES\n");

		//open file deep instrumentation
		FILE *fp_deep_inst;
		char name_of_fp_deep_inst[MAX_FILENAME];
		sprintf(name_of_fp_deep_inst, "%s.assigner_%d.dat2", output_file_name, ahGetMyRank());
		if(INSTRUMENTATION > 1)
		{
			fp_deep_inst = fopen(name_of_fp_deep_inst, "w");
			fprintf(fp_deep_inst, "#TIME #OBJECTS_ASSIGN #MESSAGE_TYPE\n");
		}
#endif

	int all_bins_concluded=0;
	message=malloc(msg_size);
	int_msg=(int *)message;

	while (!all_bins_concluded)
	{
		// Get time for instrumentation time IDLE
#ifdef INSTRUMENTATION
			gettimeofday(&READ_BUFFER_INITIAL_TIME, NULL);
			i++;
			if (i == NUM_PRINT_INST)
			{
				real_time  = (READ_BUFFER_INITIAL_TIME.tv_sec - INITIAL_TIME_INST.tv_sec )*1000000;
				real_time += READ_BUFFER_INITIAL_TIME.tv_usec - INITIAL_TIME_INST.tv_usec;
				real_time /= 1000000;
				// get the number of task in this moment
				NUMBER_OF_TASKS =	get_size_of_bin_list(bin_list) - (number_of_bins_concluded + number_of_bins_splitted);

			}		
#endif
			
		/* Gets a message for the MBR bounds */
		if (ahReadBuffer (Bin_input, message, msg_size) == EOW)	
		{
			sprintf(error_msg,"ERROR.The Bin filter have finished abnormaly yet in the First Phase execution!!!\n");
			ahExit (error_msg);
		}

// Get time for instrumentation time IDLE
#ifdef INSTRUMENTATION
			gettimeofday(&READ_BUFFER_FINAL_TIME, NULL);
			parcial_idle_time  = (READ_BUFFER_FINAL_TIME.tv_sec - READ_BUFFER_INITIAL_TIME.tv_sec)*1000000;
			parcial_idle_time += READ_BUFFER_FINAL_TIME.tv_usec - READ_BUFFER_INITIAL_TIME.tv_usec;
			parcial_idle_time /= 1000000;
			total_idle_time += parcial_idle_time;
		
			if (i == NUM_PRINT_INST) 
			{
				fprintf(fp_inst, "%.3lf %.3lf %llu %llu\n", real_time, total_idle_time, NUMBER_OF_TASKS,NUMBER_OF_OBJECTS_UNSTABLE);
				i = 0; 
			}
#endif

		switch(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD])
		{
			case CONCLUDED_BIN_MSG:
			{
				number_of_bins_concluded++;
				process_concluded_bin_msg(bin_list, message, dimension_size, Bin_output);
				
				/* If it concludes all the bin, the first phase have finished */
				if ((number_of_bins_concluded + number_of_bins_splitted)>= get_size_of_bin_list(bin_list)) 
					all_bins_concluded=1;
					
				break;
			}
			case NEW_ITERATION_MSG:
			{
				process_new_iteration_msg(bin_list, message, dimension_size, Bin_output);
#ifdef INSTRUMENTATION
				if(INSTRUMENTATION>1)
				{
					fprintf(fp_deep_inst, "%.3lf %.llu %d\n", real_time, NUMBER_OF_OBJECTS_IN_ASSIGN, int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]);	
				}	
#endif
				break;
			}
			case SPLIT_BIN_MSG:
			{
				number_of_bins_splitted++;
				process_split_bin_msg(bin_list, message, dimension_size, Bin_output);	
#ifdef INSTRUMENTATION
				if(INSTRUMENTATION>1)
				{
					fprintf(fp_deep_inst, "%.3lf %.llu %d\n", real_time, NUMBER_OF_OBJECTS_IN_ASSIGN, int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]);	
				}	
#endif

				break;
			}
			default:
			{
				sprintf(error_msg,"ERROR.The Assigner filter received an invalid message on the First Phase execution!!!\nMessage header: %d\n", int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]);
				ahExit (error_msg);
			}		
		}
#ifdef DEBUG
#ifdef SAVE_BINS
	if (DEBUG >= LOGGER_VERBOSE)
	{
		if (int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]==SPLIT_BIN_MSG || int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]==NEW_ITERATION_MSG)
		{
			print_objects_in_bins(bin_list, dimension_size);
		}
	}
#endif
#endif
	}
	
	free(message);
#ifdef INSTRUMENTATION
	fprintf(fp_inst, "%.3lf %.3lf %llu %llu\n", real_time, total_idle_time, NUMBER_OF_TASKS,NUMBER_OF_OBJECTS_UNSTABLE);
	fclose(fp_inst);
	if (INSTRUMENTATION>1)
		fclose(fp_deep_inst);
#endif	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Split a bin, creating new Bins when splitting */ 
int process_split_bin_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler Bin_output)
{
	bin_number_type splitted_bin_number;
	char error_msg[100];

	int *int_msg;
	bin_number_type *bin_number_msg;
	void *values_msg;
	int i;
	
	bin_type splitted_bin;
	bin_list_pointer pointer_to_splitted_bin, *pointer_to_new_bins;
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_split_bin_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
			
		}
		else
			fprintf(fp_log, "RECEIVING SPLIT BIN MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	int_msg=(int *)message;
	
	// Go forward on the message until the Bin Number field
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_ASSIGNER_BIN_NUMBER_FIELD]));
	
	/* Gets the number of the Bin that will be splitted */
	splitted_bin_number=bin_number_msg[0];
	
	/* Searches the bin that will be splitted */
	splitted_bin.number=splitted_bin_number;	
	pointer_to_splitted_bin=search_item_in_bin_list(splitted_bin, list);
	
	if (pointer_to_splitted_bin==NULL)
	{
		sprintf(error_msg,"ERROR. Couldn't split the Bin: %llu. It was not found on the Bin List!!!\n", splitted_bin_number);
		ahExit (error_msg);
	}

	#ifdef INSTRUMENTATION
	NUMBER_OF_OBJECTS_IN_ASSIGN = pointer_to_splitted_bin->item.objects.length;			
	#endif

	/* Changes its states meaning that it was splitted */
	pointer_to_splitted_bin->item.state++;
	
	// Go forward on the message
	bin_number_msg=&(bin_number_msg[1]);

	/* Allocates memory for the pointer_to_new_bins array */
	pointer_to_new_bins=malloc(sizeof(bin_list_pointer)*NUM_KMEANS_PARTITIONS);
	
	/* It creates the new BINs and get the centroid values for the Bin from the msg */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		/* Points Values message on the position of the msg that will receive the other values */
		values_msg=(void *)&(bin_number_msg[1]);

		/* Get the centroid from the message */
		pointer_to_new_bins[i]=get_new_centroid_new_bin_from_msg(bin_number_msg, list, dimension_size);
	
		/* Go forward on the message */
		bin_number_msg=(bin_number_type *)(values_msg + get_dimension_values_message_size(dimension_size));
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t-----\t-----\t-----\t-----\t-----\t-----\t-----\t-----\t-----\n\n");
		fprintf(fp_log, "New Bins inserted with theirs centroids!\nDATABASE UPDATED:\n");
		print_bin_fields_list(list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n\t\t-----\t-----\t-----\t-----\t-----\t-----\t-----\t-----\t-----\n");
		fclose(fp_log);
	}	
#endif		
/****************************************************************************/
	
	/* Assign the objects of the Splitted Bin to one of the new Bins.
	 * These objects are removed from the Splitted Bin. Then, the Splitted Bin will get empty. */
	assign_objects_to_new_bins(pointer_to_splitted_bin, &pointer_to_new_bins, dimension_size);

#ifdef DEBUG
#ifdef SAVE_BINS
	if (DEBUG >= LOGGER_VERBOSE)
	{
		print_objects_in_bins(list, dimension_size);
	}
#endif
#endif
	
	/* Calculate the local values(centroid, upper bound mbr and lower bound mbr of the new bins */ 
	/* If the bin is empty, no values are changed */
	/* It will compute the ocurrences of each object of the bin to get the modes of each categorical attributes. */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		calculate_bin_local_values(pointer_to_new_bins[i], dimension_size);
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "List of the Bins after assigning the objects to the new centroids: DATABASE UPDATED:\n");
		print_bin_list(list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Send the new values to the Bin Filter */
	send_bins_local_values_message(splitted_bin_number, pointer_to_new_bins, dimension_size, Bin_output);

	/* Frees the memory of the array of pointers to the new bins */
	free(pointer_to_new_bins);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Reassign the objects of the bins using the new centroids */ 
int process_new_iteration_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler port)
{
	bin_number_type splitted_bin_number;

	int *int_msg;
	bin_number_type *bin_number_msg;
	void *values_msg;
	int i;
	
	bin_list_pointer *pointer_to_bins;
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_new_iteration_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else
			fprintf(fp_log, "RECEIVING NEW ITERATION MESSAGE\n");

		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	int_msg=(int *)message;
	
	// Go forward on the message until the Bin Number field
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_ASSIGNER_BIN_NUMBER_FIELD]));
	
	/* Gets the number of the Bin that was splitted to generate the current bins */
	splitted_bin_number=bin_number_msg[0];
	
	// Go forward on the message
	bin_number_msg=&(bin_number_msg[1]);

	/* Allocates memory for the pointer_to_bins array */
	pointer_to_bins=malloc(sizeof(bin_list_pointer)*NUM_KMEANS_PARTITIONS);
	
	/* It creates the new BINs and get the centroid values for the Bin from the msg */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		pointer_to_bins[i]=get_new_centroid_for_bin_from_msg(bin_number_msg, list, dimension_size);
		
		/* Go forward the number of the bin */
		values_msg=(void *)(&(bin_number_msg[1]));

		/* Go forward the dimension values of the centroid */
		bin_number_msg=(bin_number_type *)(values_msg+get_dimension_values_message_size(dimension_size));
	}

	#ifdef INSTRUMENTATION
		NUMBER_OF_OBJECTS_IN_ASSIGN = 0;
		for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
			NUMBER_OF_OBJECTS_IN_ASSIGN+=pointer_to_bins[i]->item.objects.length;
	#endif
		
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "New centroids got from an iteration message!!! DATABASE UPDATED: \n");
		print_bin_fields_list(list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	/* Reassign all the objects of the Bins */ 
	reassign_objects_to_bins(pointer_to_bins, dimension_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "Objects Reassigned with the new centroids!!! DATABASE UPDATED: \n");
		print_bin_list(list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
#ifdef SAVE_BINS
		print_objects_in_bins(list, dimension_size);
#endif
	}
#endif		
/****************************************************************************/

	/* Calculate the local values(centroid, upper bound mbr and lower bound mbr of the new bins */ 
	/* If the bin is empty, no values are changed */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		calculate_bin_local_values(pointer_to_bins[i], dimension_size);
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER_VERBOSE)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "Calculated the new centroids for the bin!!!DATABASE UPDATED: \n");
		print_bin_fields_list(list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	/* Send the new values to the Bin Filter */
	send_bins_local_values_message(splitted_bin_number, pointer_to_bins, dimension_size, port);

	/* Frees the memory of the array of pointers to the new bins */
	free(pointer_to_bins);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Get the concluded bin on the list and sends the local values of this bin to the Bin Filter */ 
int process_concluded_bin_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler port)
{
	int *int_msg;
	float *float_msg; 
	float prune_ratio;
	bin_number_type *bin_number_msg;
	int_msg=(int *)message;
	bin_type concluded_bin;
	bin_list_pointer concluded_bin_pointer;
	char error_msg[100];

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_concluded_bin_message(message, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else
			fprintf(fp_log, "RECEIVING CONCLUDED BIN MESSAGE\n");

		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	// Go forward on the message until the Bin Number field
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_ASSIGNER_BIN_NUMBER_FIELD]));
	
	/* Gets the number of the concluded Bin */
	concluded_bin.number=bin_number_msg[0];
	
	/* Gets a pointer to the concluded bin */
	concluded_bin_pointer=search_item_in_bin_list(concluded_bin, list);	

	if (concluded_bin_pointer==NULL)
	{
		sprintf(error_msg,"ERROR. Received a Concluded Bin message for the Bin: %llu, but it was not found on the Bin List!!!\n", concluded_bin.number);
		ahExit (error_msg);
	}
	
	#ifdef INSTRUMENTATION
		NUMBER_OF_OBJECTS_UNSTABLE -= concluded_bin_pointer->item.objects.length;
		NUMBER_OF_OBJECTS_IN_ASSIGN = concluded_bin_pointer->item.objects.length;
	#endif
	
	// Go forward on the message until the Prune Ratio Bin field
  float_msg = (float *)(&(bin_number_msg[1]));

  // Get the prune ratio of concluded bin
  prune_ratio=float_msg[0];       

	#ifdef PRUNE_OBJECTS
		if (prune_ratio>0)
			prunning_objects_of_bin(concluded_bin_pointer, prune_ratio);			
	#endif 	
	
	/* Send the local values to the concluded Bin to the Bin Filter */
	send_concluded_bin_local_values(concluded_bin_pointer, dimension_size, port);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int assign_objects_to_new_bins(bin_list_pointer pointer_to_splitted_bin, bin_list_pointer **pointers_to_new_bins, dimension_size_type dimension_size)
{
	list_pointer pointer_to_current_object;
	list_type *list_of_objects;
	bin_list_pointer closest_bin;
	object_type current_object;

	list_of_objects=&(pointer_to_splitted_bin->item.objects);

	/* Just get the first of the list and reallocate this object on one of the new bins until the list is not empty */
	while(get_first_in_list(list_of_objects, &pointer_to_current_object))
	{
		#ifdef INSTRUMENTATION
			NUMBER_OF_ASSIGNS++;
		#endif
		/* Finds the closest cluster of this object */
		closest_bin=find_closest_bin(pointer_to_current_object, *pointers_to_new_bins, dimension_size);

		/* Gets the object, this will be inserted on the new Bin */
		current_object=pointer_to_current_object->item;
		
		/* Removes this object from the list of the Bin Splitted */
		remove_of_list(&pointer_to_current_object, list_of_objects);
			
		/* Inserts this object on the new Bin */
		insert_in_list(current_object, &(closest_bin->item.objects));
	}	
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Reassign all the objects of the Bins */ 
int reassign_objects_to_bins(bin_list_pointer *pointers_to_bins, dimension_size_type dimension_size)
{
	list_pointer pointer_to_current_object;
	list_type *list_of_objects;
	list_type **list_of_new_objects;

	bin_list_pointer closest_bin;
	object_type current_object;
	centroid_type *previous_centroid;
	int i, j;
	
	char error_msg[100];
	
	list_of_new_objects=malloc(sizeof(list_type *)*NUM_KMEANS_PARTITIONS);

	/* Create lists to get the new objects that will be reassigned to the bins */
	for(i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		list_of_new_objects[i]=malloc(sizeof(list_type));
		if (list_of_new_objects[i]==NULL)
		{
			sprintf(error_msg,"ERROR. Couldn't malloc the structure. Lack of memory!!!\n");
			ahExit (error_msg);
		}
		create_empty_list((list_of_new_objects[i]));
	}
	
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		list_of_objects=&(pointers_to_bins[i]->item.objects);
                previous_centroid=&(pointers_to_bins[i]->item.centroid);
		
		/* Gets the first object of the list, if it exists, start reassigning the objects */
		if (get_first_in_list(list_of_objects, &pointer_to_current_object))
		{
			while(!is_last(pointer_to_current_object, list_of_objects))
			{
				/* Finds the closest cluster of this object */
				#ifdef INSTRUMENTATION
					NUMBER_OF_ASSIGNS++;
				#endif
				closest_bin=find_closest_bin(pointer_to_current_object, pointers_to_bins, dimension_size);

				/* Tests if the object have to be assigned */
				if (closest_bin!=pointers_to_bins[i])
				{
					/* Finds the position of the closest bin on the list */
					for (j=0; j<NUM_KMEANS_PARTITIONS; j++)
						if (closest_bin==pointers_to_bins[j])
							break;
	
					/* Tests if the position of the closest bin is invalid */
					if (j>=NUM_KMEANS_PARTITIONS || j==i)
					{
						sprintf(error_msg,"ERROR. Trying to reassign an object to an invalid BIN!!!\n");
						ahExit (error_msg);
					}

					/* Gets the object, this will be inserted on the new Bin */
					current_object=pointer_to_current_object->item;
		
					/* Removes this object from the list of the previous Bin */
					remove_of_list(&pointer_to_current_object, list_of_objects);

					/* Inserts this object on the new Bin */
					insert_in_list(current_object, list_of_new_objects[j]);
				}
			
				/* Go forward oh the list of the objects */
				go_next(list_of_objects, &pointer_to_current_object);
			}

			#ifdef INSTRUMENTATION
				NUMBER_OF_ASSIGNS++;
			#endif
			/* Finds the closest cluster of the last object */
			closest_bin=find_closest_bin(pointer_to_current_object, pointers_to_bins, dimension_size);
			
			/* Tests if the object have to be assigned */
			if (closest_bin!=pointers_to_bins[i])
			{
				/* Finds the position of the closest bin on the list */
				for (j=0; j<NUM_KMEANS_PARTITIONS; j++)
					if (closest_bin==pointers_to_bins[j])
						break;
			
				/* Gets the object, this will be inserted on the new Bin */
				current_object=pointer_to_current_object->item;
		
				/* Removes this object from the list of the previous Bin */
				remove_of_list(&pointer_to_current_object, list_of_objects);
                                
				/* Inserts this object on the new Bin */
				insert_in_list(current_object, list_of_new_objects[j]);
			}
		}
	}

	/* Concatenate the lists of the objects with the lists of the reassigned objects and compute the occurrences of the new objects */
	for(i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		list_of_objects=&(pointers_to_bins[i]->item.objects);
		concatenate_lists(list_of_objects, list_of_new_objects[i]);	
		destroy_list(list_of_new_objects[i]);
	}
	free(list_of_new_objects);
	return 1;
}

int prunning_objects_of_bin(bin_list_pointer bin, float prune_ratio)
{
	unsigned long long i=0;
	int number_of_pruning=0;
	list_pointer object_pointer;
	char error_msg[100];
	
	number_of_pruning = (int)(bin->item.objects.length * prune_ratio);

	sprintf(error_msg , "Error : Number of Objects Pruning is bigger than Number of Objects!");
	if (number_of_pruning > bin->item.objects.length) 
		ahExit(error_msg);

	
	get_first_in_list(&(bin->item.objects), &object_pointer);
	
	
	while (i < number_of_pruning )
	{
		remove_of_list(&object_pointer, &(bin->item.objects));
		i++;
		#ifdef INSTRUMENTATION
			NUMBER_OF_OBJECTS_PRUNING++;
		#endif 
		go_next(&(bin->item.objects), &object_pointer);
	}
	
	return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
