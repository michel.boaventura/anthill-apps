#include "bin.h"

//=====================================================================================================
// INPUT AND OUTPUT PORTS 

InputPortHandler Assigner_input, Bin_input;	// input ports
OutputPortHandler Assigner_output, Bin_output, Final_output;	// output ports

// GLOBAL VARIABLES

/* Parameters of the program read from the work*/
dimension_size_type dimension_size;			// number of dimensions (attributes) of the database 
int bin_size;					// this number is the value of bin size
int number_of_outliers;				// number of n, where n defines the number of outliers to be searched (top-n ouliers)
int number_of_neighbours;			// number of k, where k is the number of neighbours considered to measure an outlier (k-th nearest neighbour)
char output_file_name[MAX_FILENAME];		// stores the name of the output file

/*Global Variables for Instrumentation*/
#ifdef INSTRUMENTATION
unsigned long long MY_POINT_IN_BIN = 0; // This variable will be incremented if a outilier of the proper instance bin don't pass of the local bin
unsigned long long MY_POINT_IN_INST = 0; // This variable will be incremented if a outilier of the proper instance bin don't pass of the local instance
unsigned long long OTHER_POINT_IN_BIN = 0; // This variable will be incremented if a outilier of the other instance don't pass of the local bin
unsigned long long OTHER_POINT_IN_INST = 0; // This variable will be incremented if a outilier of the other instance don't pass of the local instance
unsigned long long OTHER_POINT_IN_SCORE = 0; // This variable will be incremented if a outilier of the other instance don't pass of the local score
unsigned long long IS_MY_OBJECT = 0; // This variable is a boolean to indicate what instance bin the object belongs 
unsigned long long MY_OBJECT_IN_FIRST_BIN = 0; 
unsigned long long NUMBER_OF_OBJECTS_IN_INSTANCE = 0;
unsigned long long NUMBER_OF_COMPARATIONS = 0;
unsigned long long COUNTER_COMPARATIONS_IN_FIRST_BIN = 0;
unsigned long long NUMBER_OF_COMPARATIONS_IN_FIRST_BIN = 0;
unsigned long long NUMBER_OF_COMPARATIONS_OF_OBJECTS_ELIMINATED_IN_FIRST_BIN = 0;
unsigned long long NUMBER_OF_SPLITS = 0;
unsigned long long NUMBER_OF_MYPROB_MSGS = 0;
unsigned long long NUMBER_OF_OUTPROB_MSGS = 0;
unsigned long long NUMBER_OF_LOCAL_BINS_MSG = 0;
unsigned long long PRUNING_COMPLETE_BINS = 0;
unsigned long long NUMBER_OF_BINS_ELIMINATED = 0;
unsigned long long PRUNING_SEARCH_IN_BINS = 0;
unsigned long long NUMBER_OF_BINS_NOT_SEARCH = 0;
unsigned long long PRUNING_OBJECTS_USING_BINS = 0;
unsigned long long NUMBER_OF_OBJECTS_CONSIDERED = 0;

struct timeval INITIAL_TIME_INST;
#endif

#ifdef GET_TIME
struct timeval first_phase_part_time;
#endif

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	/* Parameters of the program read from the work*/
	char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read 
	char error_msg[100];
	
	// opens input and output ports
	Assigner_input = ahGetInputPortByName ("Assigner_to_Bin_input");
	Bin_input = ahGetInputPortByName ("Bin_to_Bin_input");
	Assigner_output = ahGetOutputPortByName ("Bin_to_Assigner_output");
	Bin_output = ahGetOutputPortByName ("Bin_to_Bin_output");
	Final_output = ahGetOutputPortByName("Bin_to_Final_output");
	if ( (Assigner_input == -1) || (Bin_input == -1) || Bin_output == -1 || Final_output == -1)
	{
		sprintf(error_msg,"ERROR. Could not open the Bin filter's ports.\n");
		ahExit (error_msg);
	}
	
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &dimension_size, &number_of_outliers, &number_of_neighbours, &bin_size);
	
	return 1;
}

int processFilter(void *work, int worksize) 
{
	/* The list of the bins */
	bin_list_type bin_list;
	bin_list_type bin_list_ordered;
	
	bin_type first_bin;

	char error_msg[100];
		
#ifdef GET_TIME
	struct timeval initial_time, first_phase_time, between_phases_time, final_time;
	struct timeval sort_bin_list_time, clean_bin_list_time;
	FILE *fp_time;
	char name_of_fp_time[MAX_FILENAME];
	sprintf(name_of_fp_time, "%s.bin_%d.time", output_file_name, ahGetMyRank()); 
	
	// Clean the files of times
	fp_time = fopen(name_of_fp_time,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);

#endif
	
#ifdef INSTRUMENTATION
	FILE *fp_inst;
	char name_of_fp_inst[MAX_FILENAME];
	sprintf(name_of_fp_inst, "%s.bin_%d.inst", output_file_name, ahGetMyRank()); 
	// Clean the files of instrumentation
	fp_inst = fopen(name_of_fp_inst,"w");
	fclose(fp_inst);
#endif
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/

#ifdef GET_TIME	
	gettimeofday(&initial_time, NULL);
#endif

#ifdef INSTRUMENTATION
	gettimeofday(&INITIAL_TIME_INST, NULL);
#endif

	/* Initialize the seed of the random generator. It will be used to generate the centroids */
#ifdef SEED_RANK
	srand48(ahGetMyRank());
#else
	struct timeval time;
	gettimeofday(&time, NULL);
	srand48(time.tv_usec * time.tv_sec);
#endif
	
	/* Create the list of the BINs initially empty */
	create_empty_bin_list(&bin_list);
	create_empty_bin_list(&bin_list_ordered);

	/* If it is the instance responsible for the first bin, then it will create the first bin */
	if (ahGetMyRank()==(INITIAL_BIN_NUMBER % ahGetTotalInstances()))
	{
	   	/* Create the First Bin */
		malloc_bin(&first_bin, INITIAL_BIN_NUMBER, dimension_size);

		/* Insert the first bin on the list of the bins */
		insert_in_bin_list(first_bin, &bin_list);
	
		/* Receive the mbr values of samples of all the Assigner's instances to generate the initial centroids*/
		receive_first_mbr_values_of_bin(&bin_list, dimension_size, Assigner_input);
	}
	
	execute_bin_first_phase(&bin_list, dimension_size, number_of_neighbours, bin_size, Assigner_output, Assigner_input);

#ifdef GET_TIME	
	gettimeofday(&first_phase_time, NULL);
#endif

	/* Cleans the list removing the Bins that doesn't belong to this instance, and the bins that were splitted */
	/* The list will get just with concluded Bins */
	clean_bin_list(&bin_list, dimension_size);	

#ifdef GET_TIME	
	gettimeofday(&clean_bin_list_time, NULL);
#endif
	
	//sorting the bins using the criterions NUM_OF_OBJETOS, DENSITY, SPACE_LENGTH.
	sort_bin_list(&bin_list, &bin_list_ordered, number_of_neighbours, dimension_size);

#ifdef GET_TIME	
	gettimeofday(&sort_bin_list_time, NULL);
#endif

	/* Calculate the closest bins */
	calculate_all_closest_bins(&bin_list_ordered, dimension_size);
	
#ifdef GET_TIME	
	gettimeofday(&between_phases_time, NULL);
#endif

#ifdef INSTRUMENTATION
	fp_inst = fopen(name_of_fp_inst,"a");
	/*Print any informations about the bins for instrumentation*/
	print_bin_inst_list(&bin_list_ordered,dimension_size, fp_inst);
	fclose(fp_inst);
#endif

#ifdef SAVE_BINS
	print_bins_done(&bin_list_ordered, dimension_size);
#endif

#ifndef EXECUTE_JUST_FIRST_PHASE
	execute_bin_second_phase(&bin_list_ordered, dimension_size, number_of_outliers, number_of_neighbours, Bin_input, Bin_output, Final_output);
#endif

#ifdef GET_TIME
	gettimeofday(&final_time, NULL);
#endif 
	
#ifdef INSTRUMENTATION
		fp_inst = fopen(name_of_fp_inst,"a");
		fprintf(fp_inst, "**FIRST PHASE** \n");
		fprintf(fp_inst, "NUMBER OF LOCAL BIN MSGS RECEIVED IN THIS INSTANCE: %llu\n", NUMBER_OF_LOCAL_BINS_MSG);
		
		fprintf(fp_inst, "**SECOND PHASE** \n");
		fprintf(fp_inst, "NUMBER OF POINTS OF THIS INSTANCE ELIMINATED IN FIRST BIN OF THIS INSTANCE: %llu\n", MY_POINT_IN_BIN);
		fprintf(fp_inst, "NUMBER OF POINTS OF OTHER INSTANCES ELIMINATED IN FIRST BIN OF THIS INSTANCE: %llu\n", OTHER_POINT_IN_BIN);
		fprintf(fp_inst, "NUMBER OF POINTS OF THIS INSTANCE ELIMINATED IN THIS INSTANCE: %llu\n", MY_POINT_IN_INST);
		fprintf(fp_inst, "NUMBER OF POINTS OF OTHERS INSTANCES ELIMINATED IN THIS INSTANCE: %llu\n", OTHER_POINT_IN_INST);
		fprintf(fp_inst, "NUMBER OF COMPARATIONS BETWEEN OBJECTS: %llu\n", NUMBER_OF_COMPARATIONS);
		fprintf(fp_inst, "NUMBER OF COMPARATIONS BETWEEN OBJECTS IN FIRST BIN: %llu\n", NUMBER_OF_COMPARATIONS_IN_FIRST_BIN);
		fprintf(fp_inst, "NUMBER OF COMPARATIONS BETWEEN OBJECTS OF OBJECTS PRUNED IN FIRST BIN: %llu\n",NUMBER_OF_COMPARATIONS_OF_OBJECTS_ELIMINATED_IN_FIRST_BIN );
		fprintf(fp_inst, "NUMBER OF POINTS OF OTHERS INSTANCES ELIMINATED IN SCORE: %llu\n", OTHER_POINT_IN_SCORE);
		fprintf(fp_inst, "NUMBER OF SPLITS IN BINS OF THIS INSTANCE: %llu\n", NUMBER_OF_SPLITS);
		fprintf(fp_inst, "NUMBER OF PROBABLE OUTLIER MESSAGES OF THIS INSTANCE: %llu\n", NUMBER_OF_MYPROB_MSGS);
		fprintf(fp_inst, "NUMBER OF PROBABLE OUTLIER MESSAGES OF OTHER INSTANCE: %llu\n", NUMBER_OF_OUTPROB_MSGS);
		fprintf(fp_inst, "NUMBER OF OBJECTS PRUNING USING MBR: %llu\n", PRUNING_COMPLETE_BINS);
		fprintf(fp_inst, "NUMBER OF BINS PRUNING USING MBR: %llu\n", NUMBER_OF_BINS_ELIMINATED);
		fprintf(fp_inst, "NUMBER OF OBJECTS PRUNING IN SEARCH OF NEIGHBOURS: %llu\n", PRUNING_SEARCH_IN_BINS);
		fprintf(fp_inst, "NUMBER OF BINS PRUNING IN SEARCH OF NEIGHBOURS: %llu\n", NUMBER_OF_BINS_NOT_SEARCH);
		fprintf(fp_inst, "NUMBER OF OBJECTS SECOND PRUNING USING MAXDIST: %llu\n", PRUNING_OBJECTS_USING_BINS);

		NUMBER_OF_OBJECTS_IN_INSTANCE = get_number_objects_of_bin_list(&bin_list_ordered);
		fprintf(fp_inst, "NUMBER OF POINTS IN ALL BINS OF THIS INSTANCE: %llu\n", NUMBER_OF_OBJECTS_IN_INSTANCE);
		fclose(fp_inst);
#endif
		
#ifdef GET_TIME	
	
	// Time of execution using the first time as first_phase_time, this is without the time between phases	
	print_time(&initial_time, &first_phase_time, name_of_fp_time);
	print_time(&first_phase_time, &final_time, name_of_fp_time);

	// Time of execution using the first time as between_phases_time, this is with the time between phases	
	print_time(&initial_time, &between_phases_time, name_of_fp_time);
	print_time(&between_phases_time, &final_time, name_of_fp_time);

	// Time of execution using the first time as first_phase_part_time 
	print_time(&initial_time, &first_phase_part_time, name_of_fp_time);
	print_time(&first_phase_part_time, &final_time, name_of_fp_time);

	// Extra times
	print_time(&first_phase_part_time, &first_phase_time, name_of_fp_time); // Time of Receive the bins
	print_time(&first_phase_time, &between_phases_time, name_of_fp_time); // Time between phases the bins
	print_time(&first_phase_time, &clean_bin_list_time, name_of_fp_time); // Time of function clean bin list
	print_time(&clean_bin_list_time, &sort_bin_list_time, name_of_fp_time); // Time of function sort bin list
	print_time(&sort_bin_list_time, &between_phases_time, name_of_fp_time); // Time of function calculate closest bin
	

#endif
	
	return 0;
}

int finalizeFilter(void)
{

	return 0;
}
