Essa � a implementa��o do algoritmo RBRP paralelizada.
Informa��es sobre o algoritmo em quest�o podem ser encontrados em "Fast Mining of Distance-Based Outliers in High Dimensional Datasets".

N�o existe a pretens�o da implementa��o da an�lise de componente principal, uma vez que n�o se tem certeza da efici�ncia da mesma assim como a sua implementa��o � muito trabalhosa, principalmente para ser feito em paralelo.
