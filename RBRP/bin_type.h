#ifndef _BIN_TYPE_H
#define _BIN_TYPE_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
#include "double_linked_bin_list.h"
/* Prototypes */
void print_bin(bin_type bin, dimension_size_type dimension_size, FILE *fp_log);

int compare_bins(bin_type bin1, bin_type bin2);

int free_bin(bin_type bin, dimension_size_type dimension_size);

void malloc_bin(bin_type *bin, bin_number_type bin_number, dimension_size_type dimension_size_values);

void print_bin_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file);

void print_bin_fields_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file);

void print_bin_fields(bin_type bin, dimension_size_type dimension_size, FILE *fp_log);

void print_bin_inst(bin_type bin, dimension_size_type dimension_size, FILE *fp_log);

void print_bin_inst_list(bin_list_type *list, dimension_size_type dimension_size, FILE *file);

void free_bin_cell(bin_list_pointer q);

void create_bin(bin_type **bin, bin_number_type bin_number, dimension_size_type dimension_size);

void destroy_bin(bin_type *bin, dimension_size_type dimension_size);

distance_type compare_target_metric_bin(bin_type item2, bin_type item1, int number_of_neighbours, dimension_size_type dimension_size); 

#endif

