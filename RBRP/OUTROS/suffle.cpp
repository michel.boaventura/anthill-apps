// random_shuffle example
#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;

int main () 
{
	// Inicializa a semente randômica com base no tempo
	srand ( unsigned ( time (NULL) ) );

	// Vetor de objetos que terá a base de dados
	vector<string> myvector;
	
	ifstream baseFile("base.txt");

	if (baseFile.is_open())
	{
		char object[400];

		// Reads a seed skiping the white spaces
		baseFile.getline(object, 400);

		while (!baseFile.bad() && !baseFile.eof())
		{
			cout << "Readed Line:" << object << endl;

			string currentObject=string(object);

			// Inserts the Object on the vector
			myvector.push_back(currentObject);
		
			// Reads a seed skiping the white spaces
			baseFile.getline(object, 400);
		}
		baseFile.close();
	}

	// using built-in random generator to shuffle the vector:
	random_shuffle ( myvector.begin(), myvector.end() );

	// Abre o arquivo da nova base para escrita
	ofstream fout("nova_base.txt", fstream::app);
	
	// Declara um iterador para percorrer o vetor de objetos 
	vector<string>::iterator it;

	// Imprime os objetos na nova base
	for (it=myvector.begin(); it!=myvector.end(); ++it)
	{
		// Imprime o objeto
		fout << *it << endl;
	}

	// Fecha o arquivo com a nova base
	fout.close();
	return 0;
}
