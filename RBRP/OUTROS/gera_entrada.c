#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
//#include <math.h>
#include <string.h>

int main (int argc, char *argv[])
{	
	int numero_de_objetos, numero_de_atributos_reais, numero_de_atributos_ordinais, numero_de_atributos_categoricos, numero_de_valores;
	int i, j;
	double probabilidade_de_valor_zero;
	char nome_do_arquivo[100];
	FILE *arquivo;
	double probabilidade, modulo;
	double max_value;
	double min_value;
	double expression_value;
	int semente;
	struct timeval tempo;

	gettimeofday( &tempo, NULL );
	semente=(int)(tempo.tv_usec);
	srand48(semente);

	int valor_categorico;
	
	int sinal_aleatorio;
	if (argc!=10)
	{
		printf("Escreva: ./gera_entrada nome_do_arquivo numero_de_objetos probabilidade_de_valor_zero max_value min_value numero_de_atributos_reais numero_de_atributos_ordinais numero_de_atributos_categoricos numero_de_valores\n");
		printf("Onde probabilidade de Zero significa a probabilidade de que um expression value seja igual a zero.\n");
		printf("Uma probabilidade de 0.5 significa que existe uma probabilidade de que a cada dois valores, um deles seja igual a zero\n");
		printf("Max Value significa o maior valor em m�dulo que se pode ter.\n Um Max Value igual a 10 significa que os valores variar�o de -10 at� 10.\n");
		printf("Max Value significa o maior que se pode ter.\n Um Max Value igual a 10 significa que os valores variar�o at�, no m�ximo, 10.\n");
		printf("Min Value significa o menor que se pode ter.\n Um Min Value igual a -10 significa que os valores variar�o at�, no m�nimo, -10.\n");
		printf("Numero de valores define quantos valores diferentes os atributos ordinais e/ou categ�ricos podem assumir\n");
		printf("./gera_entrada input.ascii.1000.7.3.5 1000 -500 0.05 2 7 3 5 8\n");
		exit(0);
	}

	sprintf(nome_do_arquivo, "%s", argv[1]);
	
	numero_de_objetos= atoi(argv[2]);
	probabilidade_de_valor_zero=atof(argv[3]);
	max_value=atof(argv[4]);
	min_value=atof(argv[5]);
	numero_de_atributos_reais= atoi(argv[6]);
        numero_de_atributos_ordinais=atoi(argv[7]);
	numero_de_atributos_categoricos=atoi(argv[8]);
	numero_de_valores=atoi(argv[9]);

	/* Testa se o n�mero de valores � v�lido*/
	if (numero_de_valores <=0 && (numero_de_atributos_categoricos>0 || numero_de_atributos_ordinais>0))
	{
		printf("Imposs�vel gerar uma base que tenha atributos categ�ricos ou ordinais com %d valores poss�veis!!!\n",numero_de_valores);
		return 1;
	}
	if (min_value > max_value) 
	{
			printf("Imposs�vel gerar uma base que tenha min_value maior que max_value!!!\n",numero_de_valores);
			return 1;
			
	}
	
	/* Abrindo o arquivo de entrada */
	arquivo=fopen(nome_do_arquivo, "w");

	for (i=0;i<numero_de_objetos; i++)
	{
		/* Escreve um inteiro identificador do objeto */
		fprintf(arquivo, "%d", i);

		/* Gerando os atributos reais */
		for (j=0; j<numero_de_atributos_reais; j++)
		{
			/* Insere uma v�rgula precedendo o pr�ximo valor*/
			fprintf(arquivo, ",");

			probabilidade=drand48();
			
			/* Testa a probabilidade do valor ser zero */
			if (probabilidade<probabilidade_de_valor_zero)
			{
				fprintf(arquivo, "0");
			}
			else 
			{
				/* Encontra um n�mero aleat�rio entre -min_value e +max_value diferente de zero */ 

				/* Rejeita o valor zero como valor de m�dulo v�lido */
				do modulo=drand48(); while (modulo==0);
				
				expression_value=modulo*(max_value + abs(min_value));
				
				expression_value += min_value;	
				/* Decide se o n�mero ser� positivo ou negativo */
				//sinal_aleatorio=lrand48()%2;
				//if (sinal_aleatorio==1)
				//expression_value=-expression_value;
				
				fprintf(arquivo, "%lf", expression_value);
			}
		}
		/* Gerando os atributos ordinais */
		for (j=0; j<numero_de_atributos_ordinais; j++)
		{
			/* Insere uma v�rgula precedendo o pr�ximo valor*/
			fprintf(arquivo, ",");

			/* Calcula qual dos valores poss�veis o valor ordinal vai ser */
			modulo=((double)(lrand48()%numero_de_valores))/numero_de_valores;
			
			/* Encontra um n�mero aleat�rio entre -max_value e +max_value para o atributo ordinal*/ 
			expression_value=2*modulo*max_value-max_value;
					
			fprintf(arquivo, "%lf", expression_value);
		}
		/* Gerando os atributos categoricos */
		for (j=0; j<numero_de_atributos_categoricos; j++)
		{
			/* Insere uma v�rgula precedendo o pr�ximo valor*/
			fprintf(arquivo, ",");

			/* Calcula qual dos valores poss�veis o valor categ�rico vai ser */
			valor_categorico=(lrand48()%numero_de_valores);
					
			fprintf(arquivo, "%d", valor_categorico);
		}
		fprintf(arquivo, "\n");
	}
	fclose(arquivo);

	return 1;
	}

