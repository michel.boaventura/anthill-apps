#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(int argc, char ** argv){
int N=0, D=0;
float v,m;
char c_Arg;


  while ((c_Arg = getopt(argc, argv, "n:d:v:h:m:")) != -1) {
    switch(c_Arg) {
    case 'n':
                N = atoi(optarg);
		break;
    case 'd':
                D = atoi(optarg);
		break;
    case 'm':
                m = atof(optarg);
		break;
    case 'v':
	              v = atof(optarg);
		break;
    default:
                fprintf(stderr,"O parametro %c nao existe.\n", c_Arg);
       case 'h':		
		fprintf(stderr,"Generator of normal distributions\n");
		fprintf(stderr,"\t-n <int>    Number of points\n");
		fprintf(stderr,"\t-d <int>  number of dimension\n");
		fprintf(stderr,"\t-m <float>  Mean of X\n");
		fprintf(stderr,"\t-v <float>  Variance of X\n");
		fprintf(stderr,"\t-h          Help\n");
	        exit(1);
    }
  }
 
 
const gsl_rng_type * T;
gsl_rng * r;
  
// Initialize the seed

T = gsl_rng_default;
r = gsl_rng_alloc (T);


//printf("NUMBER OF POINTS :  %d", N);
//printf("NUMBER OF DIMENSIONS :  %d", D);

int i=0, j=0;
float X;

for (i=0;i<N;i++)
{
    printf("%d",i);	  
    for (j=0;j<D;j++)
    {
			X = gsl_ran_gaussian(r, v);
			printf(",%f",X+m);
    }
    printf("\n");
        
}
  return 0;
}

