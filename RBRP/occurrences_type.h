#ifndef _OCCURRENCES_TYPE_H
#define _OCCURRENCES_TYPE_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"

/* Prototypes */
void print_occurrences_list(occurrences_list_type *list, FILE *file);
void print_more_frequent_of_occurrences_list(occurrences_list_type *list, FILE *file);
void free_occurrences_cell(occurrences_list_pointer q);
void print_occurrences(occurrences_type occurrences, FILE *fp_log);
int compare_occurrences(occurrences_type occurrences1, occurrences_type occurrences2);
int compare_frequency_occurrences(occurrences_type occurrence1, occurrences_type occurrence2);
void free_occurrences(occurrences_type occurrence);
void malloc_occurrences(occurrences_type *occurrence);
void update_frequency_occurrences(occurrences_type *occurrence, int frequency);

#endif

