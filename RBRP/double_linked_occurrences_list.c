#include "double_linked_occurrences_list.h"

/* Function alloc_cell allocate the memory need to creating a new cell */
occurrences_list_pointer alloc_occurrences_cell(void)
{
   occurrences_list_pointer aux;
   aux=malloc(sizeof(occurrences_cell));
   return aux;
}

/* Procedure that creates an empty list */
inline void create_empty_occurrences_list(occurrences_list_type *list)
{
   list->first=alloc_occurrences_cell();
   list->last=list->first;
   list->first->next=list->first;
   list->first->previous=list->first;
   list->length=0;
}

/* Answer if a list is empty */
inline int is_empty_occurrences_list(occurrences_list_type *list)
{
   return !(list->length);
}

/* insert_in_occurrences_list inserts a new item on the last position of a list */
void insert_in_occurrences_list(occurrences_type item, occurrences_list_type *list)
{
   list->last->next=alloc_occurrences_cell();
   list->last->next->previous=list->last;
   list->last=list->last->next;
   list->last->item=item;
   list->last->next=list->first;
   list->first->previous=list->last;
   list->length++;
}

/* insert_in_occurrences_list_on_position inserts a new item on the indicated position of a list */
void insert_in_occurrences_list_on_position(occurrences_type item, occurrences_list_pointer *p, occurrences_list_type *list)
{
	occurrences_list_pointer aux=*p;

	if (aux==NULL)
	{
		*p=NULL;
		return;
	}
	/* Tests if the pointer is pointing to the dummy cell.
	 * In this case we will just insert the item on the last of the list */
	if (aux==list->first)
	{
		insert_in_occurrences_list(item, list);
	}
	else
	{
		/* Tests if the pointer is pointing to the real first item of the list.
		 * In this case we will insert the item on the first position of the list */
		
		if(aux==list->first->next)	
			insert_in_begin_occurrences_list(item, list);
		else
		{
			aux->previous->next=alloc_occurrences_cell();
			aux->previous->next->previous=aux->previous;
			aux->previous=aux->previous->next;
			aux->previous->item=item;
			aux->previous->next=aux;
			list->length++;
		}
	}
	*p=aux->previous;
}

/* Inserts a new item on the first position of a list */
void insert_in_begin_occurrences_list(occurrences_type item, occurrences_list_type *list)
{
   occurrences_list_pointer aux;
   aux=alloc_occurrences_cell();
   aux->item=item;
   aux->next=list->first->next;
   aux->next->previous=aux;
   list->first->next=aux;
   aux->previous=list->first;
   if (list->length==0)
	   list->last=aux;
   list->length++;
}   

/* Inserts a new item on the list keeping the list ordered */
void insert_in_ordered_occurrences_list(occurrences_type item, occurrences_list_type *list)
{
   occurrences_list_pointer aux;

   /* If there is no item on the list, just insert the item */
   if(!get_first_in_occurrences_list(list, &aux))
   {
      insert_in_occurrences_list(item, list);
      return; 
   }
   
   /* While there is an next item that the frequency is greater the the frequency of the item that will be inserted, keep running on the list */
   while (!is_last_occurrences(aux, list))
   {
      if (compare_frequency_occurrences(aux->item, item) < 0)
      {
	 insert_in_occurrences_list_on_position(item, &aux, list);
	 return;
      }
      go_next_occurrences(list, &aux);
   }   
   
   /* Tests if the object have to be inserted before the last item */
   if (compare_frequency_occurrences(aux->item, item) < 0)
   {
	 insert_in_occurrences_list_on_position(item, &aux, list);
         return;
   }
   /* If the item is the last one of the list, we will insert this item on the last position */
   else
   {
      insert_in_occurrences_list(item, list);
   }

   return;
}

/* Removes the item on the position where it is located and reinsert the item on the right position on an ordered list. It can be used if the item was updated */
void update_position_in_ordered_occurrences_list(occurrences_list_pointer *old_position, occurrences_list_type *list)
{
   occurrences_type aux;

   /* Remove the occurrence on the previous position on the list */
   aux=remove_of_occurrences_list(old_position, list);
  
   /* Reinserts the item on the right position */
   insert_in_ordered_occurrences_list(aux, list);
   return;
}

/* Remove the item that is pointed.
 * After the remove, the pointer will be pointed to the previous item. 
 * Then, the next list of the item pointed will be the same */
occurrences_type remove_of_occurrences_list(occurrences_list_pointer *p, occurrences_list_type *list)
{
   occurrences_type item;
   occurrences_list_pointer aux;

   /* Tests is the list is empty or the pointer is null */
   if (is_empty_occurrences_list(list) || !(*p) || (*p==list->first))
   {
      printf("Error: List is_empty or position doesn't exists.\n");
      item.frequency=0;
      item.value_number=0;
   }
   else
   {
      item=(*p)->item;
      (*p)->next->previous=(*p)->previous;
      (*p)->previous->next=(*p)->next;
      if ((*p)==list->last)
         list->last=(*p)->previous;
      aux=(*p);
      (*p)=(*p)->previous;
      free_occurrences_cell(aux);
      list->length--;
   }
   return item;
}

/* Remove the first item of the list, and return this*/

occurrences_type remove_first_of_occurrences_list(occurrences_list_type *list)
{
   occurrences_type item;
   occurrences_list_pointer aux=list->first->next;
   item=remove_of_occurrences_list(&aux, list);
   return item;
}

/* Get the first item of the list if it exists */
inline int get_first_in_occurrences_list(occurrences_list_type *list, occurrences_list_pointer *pointer)
{
   if (is_empty_occurrences_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->first->next;
   return 1;
}

/* Get the last item of the list if it exists */
inline int get_last_in_occurrences_list(occurrences_list_type *list, occurrences_list_pointer *pointer)
{
   if (is_empty_occurrences_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->last;
   return 1;
}

/* Go to the next position of the list */
inline int go_next_occurrences(occurrences_list_type *list, occurrences_list_pointer *pointer)
{
	/* Tests if the pointer is valid */
	if (*pointer)
	{
		/* Tests if the object is the last one of the list. In this case it do not have a next item. */
		if ((*pointer)==list->last)
		{
			get_first_in_occurrences_list(list, pointer);
			return 0;
		}
		else
		{
			*pointer=(*pointer)->next;
			return 1;
		}
	}
	else return 0;     
}

/* Tests if the item is the last of the list */
inline int is_last_occurrences(occurrences_list_pointer p, occurrences_list_type *list)
{

   /* If the pointer is NULL, there is an error */
   if (!p)
   {
      printf("Erro: Este apontador n�o aponta para c�lula alguma");
      return 0;
   }
   return (p->next==list->first);
}

/* Searchs an item on the list, returning this position or NULL if the item was not found */
occurrences_list_pointer search_item_in_occurrences_list(occurrences_type item, occurrences_list_type *list)
{
   occurrences_list_pointer aux;

   /* If there is no item on the list, it returns NULL*/
   if(!get_first_in_occurrences_list(list, &aux))
	   return NULL; 

   /* While there is an next item, keep searching */
   do
   {
      if (compare_occurrences(aux->item, item))
      {
	 return aux;
      }
      go_next_occurrences(list, &aux);
   }   
   while (!is_last_occurrences(aux, list));
   
   /* Tests if the object is the last */
   if (compare_occurrences(aux->item, item))
   {
         return aux;
   }

   return NULL;
}

/* Swap two items that are adjacent on the list */
void swap_on_occurrences_list(occurrences_list_pointer previous, occurrences_list_pointer next, occurrences_list_type *list)
{
   if (previous!=list->last && list->length>1)
   {
      if (previous->next==next)
      {
         previous->previous->next=next;
         next->next->previous=previous;
         previous->next=next->next;
         next->previous=previous->previous;
         previous->previous=next;
         next->next=previous;
      }
      if (previous==list->first) 
         list->first=next;
      if (next==list->last) 
         list->last=previous;
   }
   else printf("Error: the swap can�t be completed. \n");
}

/* Make an list empty, it frees the memory allocated*/
inline void make_occurrences_list_empty(occurrences_list_type *list)
{
   occurrences_type aux;
   while (!is_empty_occurrences_list(list))
   {
      aux=remove_first_of_occurrences_list(list);
      free_occurrences(aux);
   }
}

/* Destroys a list. First make this empty, then frees the memory */
void destroy_occurrences_list(occurrences_list_type *list)
{
	make_occurrences_list_empty(list);
	free(list->first);
	free(list);
}

/* Concatenates two lists. The second list structure will get empty on the operation */
int concatenate_occurrences_lists(occurrences_list_type *list1, occurrences_list_type *list2)
{
   occurrences_list_pointer last_of_the_first_list;
   occurrences_list_pointer first_of_the_second_list;
   
   /* If the lists are invalid or the two lists are the same couldn't concatenate them */
   if (list1==NULL || list2==NULL || list1==list2)
	   return 0;
   
   /* If the second list is empty, nothing on the first list will be changed */
   if(!is_empty_occurrences_list(list2))
   {
      if (is_empty_occurrences_list(list1))
      {
         /* Update the fields of the dummy cell of the first list */
         list1->first->next=list2->first->next;
	 list1->first->previous=list2->first->previous;

	 /* Update the fields of the items that points to the dummy cell of the list */
	 list2->first->next->previous=list1->first;
	 list2->last->next=list1->first;
         
	 /* Update the field last of the first list */
	 list1->last=list2->last;
      }
      else
      {
         /* Get a Pointer to the previous last of the first list */
         get_last_in_occurrences_list(list1, &last_of_the_first_list);
      
         /* Get a Pointer to the previous last of the first list */
         get_first_in_occurrences_list(list2, &first_of_the_second_list);
	   
         /* Points the field last of the list to the last item of the second list */
         get_last_in_occurrences_list(list2, &(list1->last));

         /* Points the previous field of the dummy cell to the new last item */
         get_last_in_occurrences_list(list2, &(list1->first->previous));
	
         /* Points the field next of the new last item to the dummy cell of the list 1 */
         list1->last->next=list1->first;
    
         /* Updates the fields of the previous last item of the first list */
         last_of_the_first_list->next=first_of_the_second_list;
     
         /* Updates the fields of the previous first item of the second list */
         first_of_the_second_list->previous=last_of_the_first_list;
      }
 
      /* updates the length of the first list */ 
      list1->length+=list2->length;
            
      /* Make the list2 seems now empty, without remove the items that is now on the list1 */
      list2->last=list2->first;
      list2->first->previous=list2->first;
      list2->first->next=list2->first;
      list2->length=0;
   }
   return 1;
}   

/* Gets the size of the list */
inline unsigned int get_occurrences_list_length(occurrences_list_type *list)
{
	return (list->length);
}

/* Update the frequency of an item on the list, and reorder the list respecting the updating action */
int update_frequency_of_item_in_occurrences_list(categorical value_number, int frequency, occurrences_list_type *list)
{
   int new_frequency;
   occurrences_type aux;
   occurrences_list_pointer aux_pointer;

   aux.value_number=value_number;
   aux.frequency=frequency;

   /* Searches for the previous position on the list */
   aux_pointer=search_item_in_occurrences_list(aux, list);

   /* Tests if the item doesn't already exists on the list. In this case, we will just insert the item on the list */
   if (aux_pointer==NULL)
   {
      if (frequency < 0)
      {
         printf("The item to update can not be updated by the frequency %d, cause it will get a negative frequency: %d!!!\n", frequency, frequency);
         return 0;
      }
      insert_in_ordered_occurrences_list(aux, list);
      return 1;
   }
   else
   {
      /* Calculates the new frequency of the item on the list */
      new_frequency=aux_pointer->item.frequency + frequency;
   
      if (new_frequency < 0)
      {
         printf("The item to update can not be updated by the frequency %d, cause it will get a negative frequency: %d!!!\n", frequency, new_frequency);
         return 0;
      }
      else
      {
         /* Tests if the item will keep on the list */
	 if (new_frequency > 0 )
	 {
            /* Updates the frequency of the item */
            update_frequency_occurrences(&(aux_pointer->item), frequency);
 
            /* Updates the position where the item is located on the list */
            update_position_in_ordered_occurrences_list(&aux_pointer, list);
         }
	 /* If the frequency will get 0, we will just remove the item from the list */
	 else
	 {
            remove_of_occurrences_list(&aux_pointer, list);
	 }
      }
      return 1;
   }
}

/* Get the mode of the attribute from the occurrence list. It assumes that the occurrence list is ordered by the frequency!!! */
inline categorical get_mode_from_occurrence_list(occurrences_list_type *list)
{
   occurrences_list_pointer aux;

   /* If there is no item on the list, just insert the item */
   if(!get_first_in_occurrences_list(list, &aux))
   {
      return 0; 
   }
   else return (aux->item.value_number);
}

/* Get the sum of the occurrences of the NUMBER_OF_MODES modest values from the occurrence list. It assumes that the occurrence list is ordered by the frequency!!! 
Basically it wil be used for generate a random categorical value of the modest values 
*/
unsigned int get_sum_frequency_modest_values_from_occurrence_list(occurrences_list_type *list)
{
   occurrences_list_pointer aux;
	unsigned int num_values;
	unsigned int list_length=get_occurrences_list_length(list);
	unsigned int i;
	unsigned int sum=0;
	
	if (list_length < NUMBER_OF_MODES)
		num_values=list_length;
	else
		num_values=NUMBER_OF_MODES;
	
   get_first_in_occurrences_list(list, &aux);
	for (i=0; i<num_values; i++)
   {
   	sum+=aux->item.frequency;
		go_next_occurrences(list, &aux);
   }
	return sum;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Fill the occurrences list on a message. The occurrences that will be filled will be the NUMBER_OF_MODES local more frequent values happened on the bin. */
void *fill_occurrences_list_on_msg(void *msg_dest, occurrences_list_type *list)
{
   int number_of_occurrences_filled=0;
   int *frequency_msg;
   categorical *value_number_msg;
   occurrences_list_pointer aux;

   value_number_msg=(categorical *)msg_dest;
   
   /* If there is item on the list, it will start filling the occurrences on the list */
   if(get_first_in_occurrences_list(list, &aux))
   {
      /* Filling the first occurrence on the message */
      /* Saves the categorical value of the first occurrence on the message */ 
      value_number_msg[0]=aux->item.value_number;

      /* Go forward on the message */
      frequency_msg=(int *)&(value_number_msg[1]);
    
      /* Saves the frequency of the value on the message */ 
      frequency_msg[0]=aux->item.frequency;

      /* Go forward on the message */
      value_number_msg=(categorical *)&(frequency_msg[1]);
     
      /* Computes that the occurrence was filled */
      number_of_occurrences_filled++;

      /* Filling the occurrences on the message */
      while (!is_last_occurrences(aux, list) && number_of_occurrences_filled < NUMBER_OF_MODES)
      {
         /* Runs the list to the next occurrence value */
         go_next_occurrences(list, &aux);

         /* Saves the categorical value on the message */ 
         value_number_msg[0]=aux->item.value_number;
   
         /* Go forward on the message */
         frequency_msg=(int *)&(value_number_msg[1]);
         
         /* Saves the frequency of the value on the message */ 
         frequency_msg[0]=aux->item.frequency;
   
         /* Go forward on the message */
         value_number_msg=(categorical *)&(frequency_msg[1]);
        
         /* Computes that the occurrence was filled */
         number_of_occurrences_filled++;
      }
   }   
   
   /* If there is no more items on the list, but yet there is space on the message, we save a dummy value with frequency zero. */
   if (number_of_occurrences_filled < NUMBER_OF_MODES)
   {
      /* Filling a dummy value */
      value_number_msg[0]=0;
      
      /* Go forward on the message */
      frequency_msg=(int *)&(value_number_msg[1]);
     
      /* Filling a zero frequency to indicate that the occurrences finished */
      frequency_msg[0]=0;

      /* Go forward on the message */
      value_number_msg=(categorical *)&(frequency_msg[1]);
   }
      
   return (value_number_msg);	
}

