/* Inclusoes. */
#include "messages.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function fills in the msg with the elements of an int array. 
 * Then it returns the number of elements filled in.
 */
int fill_int_array_in_msg (int *array, int *msg, int total_array_size)
{
	int i;
	
	for (i = 0; i < total_array_size; i++)
	{	
		msg[i] = array[i];
	}
	
	return (total_array_size);
}	

/* This function is responsible to get the MBR Bounds of a centroid of a Bin and fill it on an object message
 * that will be sent to the Bin filter.
 * It allocates space in memory for this msg */

int fill_mbr_values_in_message(void **msg, dimension_size_type dimension_size, centroid_type centroid)
{
	int *int_msg;
	bin_number_type *bin_number_msg;
	void *values_msg;
	unsigned int size=get_first_mbr_values_message_size(dimension_size);
	
	*msg=malloc(size);
	
	/* Points int message on the position of the msg that will receive the bin number */
	int_msg=(int *)(*msg);

	/* Fills the header on the message */
	int_msg[TO_BIN_MSG_TYPE_FIELD]=FIRST_MBR_VALUES_MSG;
	
	// Go forward on the message
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_BIN_BIN_NUMBER_FIELD]));
	
	/* The message will be sent to the first bin */
	bin_number_msg[0]=INITIAL_BIN_NUMBER;

	/* Go forward on the message */
	values_msg=(&(bin_number_msg[1]));

	/* Fill the Upper Bound Values on the message */
	values_msg=fill_dimension_values_on_msg(values_msg, &(centroid.upper_bound_mbr), dimension_size);

	/* Fill the Lower Bound Values on the message */
	values_msg=fill_dimension_values_on_msg(values_msg, &(centroid.lower_bound_mbr), dimension_size);

	return size;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send the minimum and maximum MBR values of a sample of the First Bin to the Bin Filter */
/* The values are the minimum and maximum values of the dimensions of all objects present on the sample of the bin */
int send_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size, OutputPortHandler port)
{
	unsigned int msg_size;

	void *msg;

	bin_list_pointer first_bin;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	/* Get the Bin 0 from the list of Bins */
	get_first_in_bin_list(bin_list, &first_bin);

	/* Fill the MBR Values of the Bin 0 on the message */
	msg_size=fill_mbr_values_in_message(&msg, dimension_size, first_bin->item.centroid);
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if( DEBUG >= LOGGER )
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_first_mbr_values_of_bin_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	ahWriteBuffer (port, msg, msg_size);

	free(msg);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Fill the local values of the Bins on the message 
 * MESSAGE:
 * 	campo 0: MESSAGE TYPE to bins_local_values
 * 	campo 1: BIN_NUMBER that was splitted
 * 	campo 2: BIN_Number para o centroide c com n dimensões
 * 	campo 3: Number of objects of the bin c com n dimensões
 * 	campo 4: Valores da dimensão d do novo centróide local c
 * 	campo d+2 to 2d-1: Valor da dimensão d do upper_bound
 * 	campo d+3 to 3d-1: Valor da dimensão d do lower_bound
 */

/* This function is responsible to get the local values of the Bins of a list and fill these values on an message
 * that will be sent to the Assigner filter.
 * It allocates space in memory for this msg */

int fill_bins_local_values_in_message(void **msg, bin_number_type splitted_bin_number, bin_list_pointer *bins, dimension_size_type dimension_size)
{
	/* The message will contains the number of the Splitted Bin, the number and the local values (centroid, upper_bound and lower_bound) of the Bins */
	unsigned int msg_size=get_bins_local_values_message_size(dimension_size);
	int i;
	int *int_msg;
	bin_number_type *bin_number_msg;
	
	/* Allocates the memory for the message*/
	*msg=malloc(msg_size);
	int_msg=(int *)(*msg);

	int_msg[TO_BIN_MSG_TYPE_FIELD]=BINS_LOCAL_VALUES_MSG;
	
	/* Go forward on the message */
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_BIN_MSG_TYPE_FIELD+1]));
	
	/* Fills the number of the splitted bin */
	bin_number_msg[0]=splitted_bin_number;

	/* Go forward on the message */
	bin_number_msg=&(bin_number_msg[1]);

	/* Fill the new local values of the bins on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		/* Fills the number of the bin */
		bin_number_msg[0]=bins[i]->item.number;

		/* Go forward on the message */
		int_msg=(int *)(&(bin_number_msg[1]));

		/* Fill the number of objects of the bin on the message */
		int_msg[0]=bins[i]->item.objects.length;
		
		/* Go forward on the message */
		bin_number_msg=(bin_number_type *)(&(int_msg[1]));
	
		/* Just insert the centroid local values on the message if we have objects inserted on the Bin */
		if (bins[i]->item.objects.length > 0)
		{	
			/* Fills the centroid values of the bin and go forward on the message */
			bin_number_msg=fill_centroid_values_on_msg(bin_number_msg, &(bins[i]->item.centroid), dimension_size);
		}
	}
	
	/* Returns the real size of the message */
	return ((unsigned int)((void *)bin_number_msg - (*msg)));
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send a message with the local values of the list of the Bins to the filter BIN */
int send_bins_local_values_message(bin_number_type splitted_bin_number, bin_list_pointer *pointer_to_new_bins, dimension_size_type dimension_size, OutputPortHandler port)
{
	unsigned int msg_size;
	void *msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fill the values on the message */
	msg_size=fill_bins_local_values_in_message(&msg, splitted_bin_number, pointer_to_new_bins, dimension_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_bins_local_values_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Fill the local values of the concluded Bin on the message 
 * MESSAGE:
 * 	campo 0: Constante definindo o tipo de mensagem para CONCLUDED_BIN_LOCAL_VALUES_MSG
 * 	campo 1: BIN_NUMBER of the concluded bin
 * 	campo 2: Number of the objects on this bin
 * 	campo ?: Valores das dimensões do centróide
 * 	campo ?: Valores das dimensões do upper_bound
 * 	campo ?: Valores das dimensões do lower_bound
 */

/* This function is responsible to get the local values of a concluded Bin and fill these values on an message
 * that will be sent to the Assigner filter.
 * It allocates space in memory for this msg */
int fill_concluded_bin_local_values_in_message(void **msg, bin_list_pointer bin, dimension_size_type dimension_size)
{
	/* Get the maximum size of a Concluded Bin Local Values message */
	unsigned int msg_size=get_concluded_bin_local_values_message_size(dimension_size);
	int *int_msg;
	bin_number_type *bin_number_msg;
	
	/* Allocates the memory for the message*/
	*msg=malloc(msg_size);
	int_msg=(int *)(*msg);

	/* Fills the header of the message */
	int_msg[TO_BIN_MSG_TYPE_FIELD]=CONCLUDED_BIN_LOCAL_VALUES_MSG;

	bin_number_msg=(bin_number_type *)(&(int_msg[TO_BIN_MSG_TYPE_FIELD+1]));
	
	/* Fills the number of the bin */
	bin_number_msg[0]=bin->item.number;
	
	/* Go forward on the message */
	int_msg=(int *)(&(bin_number_msg[1]));

	/* Fills the number of objects of the Bin on the message */
	int_msg[0]=bin->item.objects.length;
		
	/* Go forward on the message */
	int_msg=(&(int_msg[1]));

	/* Just insert the centroid local values on the message if we have objects inserted on the Bin */
	if (bin->item.objects.length > 0)
	{
		/* Fills the centroid values of the bin on the message */
		int_msg=fill_centroid_values_on_msg(int_msg, &(bin->item.centroid), dimension_size);
	}

	/* Returns the real size of the message */
	return ((unsigned int)(((void *)int_msg) - (*msg)));
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send the local values of a concluded Bin to the filter BIN */
int send_concluded_bin_local_values(bin_list_pointer concluded_bin, dimension_size_type dimension_size, OutputPortHandler port)
{
	unsigned int msg_size;

	void *msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fill the values on the message */ 
	msg_size=fill_concluded_bin_local_values_in_message(&msg, concluded_bin, dimension_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= LOGGER )
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \n");
		print_concluded_bin_local_values_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to read a line of a file and fill it on an object message
 * that will be sent to the assigner filter.
 * It allocates space in memory for this msg */

int fill_object_of_bin_in_message(void **msg, bin_number_type bin_number, object_type *object, dimension_size_type dimension_size)
{
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	
	if (((*object).dimension_values.real_values==NULL) && ((*object).dimension_values.categorical_values==NULL) && ((*object).dimension_values.ordinal_values==NULL)) return 0;

	// The message contains the header, the number of the Bin that the object belongs, the object number and their values 
	unsigned int size=2*sizeof(unsigned int)+sizeof(bin_number_type)+get_dimension_values_message_size(dimension_size);

	*msg=malloc(size);
	int_msg=*msg;
	
	// Fills the header of the message
	int_msg[TO_BIN_MSG_TYPE_FIELD]=OBJECT_VALUES_OF_BIN_MSG;

	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[1]));
	
	// Fills the bin number owner of the object
	bin_number_msg[0]=bin_number;

	// Go forward on the message
	int_msg=(unsigned int *)(&(bin_number_msg[1]));
		
	// Fills the object number
	int_msg[0]=(*object).number;

        /* Go forward on the message until the beginning of the dimension values */
	int_msg=&(int_msg[1]);

	/* Fills the dimension values on the message */
	fill_dimension_values_on_msg(int_msg, &((*object).dimension_values), dimension_size);
	
	return size;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send an object to the filter BIN */
int send_object_values_of_bin(bin_number_type bin_number, object_type *object, dimension_size_type dimension_size, OutputPortHandler port)
{
	unsigned int msg_size;

	void *msg=NULL;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_assigner_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fill the values on the message */ 
	msg_size=fill_object_of_bin_in_message(&msg, bin_number, object, dimension_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER) 
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \n");
		print_object_values_of_bin_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Sends the message */
	ahWriteBuffer (port, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Sends all the objects of the list to the filter BIN*/
int send_object_list(bin_number_type bin_number, list_type *list, dimension_size_type dimension_size, OutputPortHandler port)
{
	list_pointer aux;

	/* If the first item is not found, it justs returns */
	if (!get_first_in_list(list, &aux))
	{	
		return 0;
	}

	/* Send the first object of the List*/
	send_object_values_of_bin(bin_number, &(aux->item), dimension_size, port);

	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		send_object_values_of_bin(bin_number, &(aux->item), dimension_size, port);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Send all the objects of a bin to the filter BIN */
int send_objects_of_bin(bin_type bin, dimension_size_type dimension_size, OutputPortHandler port)
{
	/* Tests if the bin is empty */
	if (is_empty(&(bin.objects)))
		return 0;
	else
	{
		send_object_list(bin.number, &(bin.objects), dimension_size, port);
		return 1;
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Sends all the objects of all the bins to the filter BIN */
int send_objects_of_bins(bin_list_type *list, dimension_size_type dimension_size, OutputPortHandler port)
{
	bin_list_pointer aux;
   
	/* If the first item is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}
   
	/* Prints the First Bin of the list */
	send_objects_of_bin(aux->item, dimension_size, port);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		send_objects_of_bin(aux->item, dimension_size, port);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* It creates the new BIN and get the centroid values for the Bin from the msg */
bin_list_pointer get_new_centroid_new_bin_from_msg(bin_number_type *msg, bin_list_type *list, dimension_size_type dimension_size)
{

	bin_type item;
	bin_list_pointer new_bin;
	void *values_msg;
	
	/* Gets the Bin_number from the message */
	bin_number_type bin_number=msg[0];
	
	/* Allocates the bin structures */
	malloc_bin(&item, bin_number, dimension_size);
	
	/* Insert the bin in the list of the bins */
	insert_in_bin_list(item, list);
	
	/* Gets a pointer to the new bin that I had inserted on the final of the list */
	get_last_in_bin_list(list, &new_bin);

	// Go forward on the message
	values_msg=(void *)(&(msg[1]));

	/* Get the dimension values from the message */
	get_dimension_values_from_msg(&(new_bin->item.centroid.dimension_values), (void *)values_msg, dimension_size);

	/* Initializes the MBR bounds with initial values like the centroid */
	copy_dimension_values(&(new_bin->item.centroid.upper_bound_mbr), &(new_bin->item.centroid.dimension_values), dimension_size);
	copy_dimension_values(&(new_bin->item.centroid.lower_bound_mbr), &(new_bin->item.centroid.dimension_values), dimension_size);
	
	return new_bin;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* It updates the centroid values for the Bin, getting the values from the msg */
bin_list_pointer get_new_centroid_for_bin_from_msg(bin_number_type *msg, bin_list_type *list, dimension_size_type dimension_size)
{
	bin_type bin;
	bin_list_pointer bin_pointer;

	void *temp_msg;
		
	/* Gets the Bin_number from the message */
	bin.number=msg[0];

	/* Gets a pointer to the new bin that I had inserted on the final of the list */
	bin_pointer=search_item_in_bin_list(bin, list);

	// Go forward on the message
	temp_msg=(void *)(&(msg[1]));
	
	/* Get the dimension values from the message */
	get_dimension_values_from_msg(&(bin_pointer->item.centroid.dimension_values), (void *)temp_msg, dimension_size);

	return bin_pointer;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that asks for splitting a BIN to the Assigner filter 
 *	send_split_bin
 *
 * 	campo 0: Constante definindo o tipo de mensagem para SPLIT_BIN_MSG
 * 	campo 1: Número do BIN a ser particionado
 * 	campo c*(n+1)+1: Número do BIN para o centroide c com n dimensões
 * 	campo c*(n+1) + 2 + d : Valor da dimensão d do centróide c
*/
int send_split_bin_message(bin_number_type splitted_bin_number, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size, OutputPortHandler Assigner_output)
{
	/* The message will contains the header, the number of the Splitted Bin, the number and the dimension values of the centroid of the new Bins */
	unsigned msg_size=sizeof(int)+sizeof(bin_number_type)*(NUM_KMEANS_PARTITIONS+1)+NUM_KMEANS_PARTITIONS*get_dimension_values_message_size(dimension_size);

	void *msg;
	int *int_msg;
	bin_number_type *bin_number_msg;
	int i;
	
	msg=malloc(msg_size);
	int_msg=(int *)msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	// Fills the header of the message
	int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]=SPLIT_BIN_MSG;
	
	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	
	// Fills the Splitted Bin Number
	bin_number_msg[0]=splitted_bin_number;

	/* Runs the message until the Values fields */
	bin_number_msg=(&(bin_number_msg[1]));
	
	/* Fill the new centroids on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		/* Fills the number of the bin on the message */
		bin_number_msg[0]=new_bins_pointers[i]->item.number;
		
		/* Go forward on the message */
		bin_number_msg=(&(bin_number_msg[1]));

		/* Fills the dimension values on the message */
		bin_number_msg = (bin_number_type *)fill_dimension_values_on_msg(bin_number_msg, &(new_bins_pointers[i]->item.centroid.dimension_values), dimension_size);
	}

	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_split_bin_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	ahWriteBuffer(Assigner_output, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Gets the local values of the concluded bin from the message */
int get_concluded_bin_local_values_from_msg(int *msg, bin_list_type *list, dimension_size_type dimension_size, unsigned int number_of_assigner_instances)
{
	bin_type bin;
	bin_list_pointer bin_pointer;
	void *values_msg;
	int *int_msg;
	bin_number_type *bin_number_msg;
	unsigned int local_length;
	int all_bin_local_values_received;
	char error_msg[100];

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_concluded_bin_local_values_message(msg, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else
		{
			fprintf(fp_log, "RECEIVING CONCLUDED BIN LOCAL VALUES MESSAGE\n");
		}
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	bin_number_msg=(bin_number_type *)(&(msg[TO_BIN_BIN_NUMBER_FIELD]));
	
	/* Gets the number of the concluded Bin from the message */
	bin.number=bin_number_msg[0];
	
	/* Gets a pointer to the bin that are being splitted */
	bin_pointer=search_item_in_bin_list(bin, list);

	/* Tests if the message was destinated to a new bin that doesn't exists until now on this instance. Creates the bin in this case. */
	if (bin_pointer==NULL)
	{
		/* Creates and insert the new bin on the list*/
		malloc_bin(&bin, bin.number, dimension_size);
	
		/* Insert the bin in the list of the bins */
		insert_in_bin_list(bin, list);
	
		/* Gets a pointer to the new bin that I had created */
		get_last_in_bin_list(list, &bin_pointer);
	}
	
	/* Tests if the bin is in a normal state and then can receive a concluded bin local values message */
	if (bin_pointer->item.state!=NORMAL_STATE)
	{
		sprintf(error_msg,"ERROR. Received a Concluded bin local values message to the bin: %llu. But it couldn't receive this cause it isn't in NORMAL state!!!State of the bin: %d\n", bin.number, bin_pointer->item.state);
		ahExit (error_msg);
	}
	
	/* Computes the message received */
	bin_pointer->item.local_values_received++;

	/* Tests if all local values was received */
	all_bin_local_values_received = (bin_pointer->item.local_values_received == number_of_assigner_instances);

	// Go forward on the message
	int_msg=(int *)&(bin_number_msg[1]);

	/* Gets the local length of the Bin */
	local_length=int_msg[0];
	
	values_msg=(void *)(&(int_msg[1]));

	/* Just get the local centroid values if there are objects that belongs to the bin */
	if (local_length > 0)
	{
		/* Get the values from the message and update the values of the centroid. It also go forward on the message. */	
		get_centroid_local_values_from_msg(&(bin_pointer->item.centroid), local_length, values_msg, dimension_size);
	}

	/* If already have received all the local values, updates the dimension values of the centroid */
	if (all_bin_local_values_received)
	{
		/* Update the values of the centroid based on the new members of the bin */
		update_centroid_dimension_values(&(bin_pointer->item.centroid), dimension_size);

		// If the bin already have received all the Concluded Bin Local Values Message 
		// Changes the state of the concluded bin meaning that it was CONCLUDED
		if (all_bin_local_values_received)
			bin_pointer->item.state=CONCLUDED;
	}

	return (all_bin_local_values_received);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that asks for a new iteration on splitting a BIN to the Assigner filter 
 *	send_new_iteration_message
 *
 * 	campo 0: Constante definindo o tipo de mensagem para NEW_ITERATION_MSG
 * 	campo 1: Número do BIN a ser particionado
 * 	campo c*(n+1)+1: Número do BIN para o centroide c com n dimensões
 * 	campo c*(n+1) + 2 + d : Valor da dimensão d do centróide c
*/
int send_new_iteration_message(bin_number_type splitted_bin_number, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size, OutputPortHandler Assigner_output)
{
	/* Get the maximum size of the message */
	unsigned msg_size=get_new_iteration_message_size(dimension_size);
	void *msg;
	int *int_msg;
	bin_number_type *bin_number_msg;
	int i;
	
	msg=malloc(msg_size);
	int_msg=(int *)msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	// Fills the header on the message
	int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]=NEW_ITERATION_MSG;
	
	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	bin_number_msg[0]=splitted_bin_number;

	/* Runs the message until the Values fields */
	bin_number_msg=&(bin_number_msg[1]);

	/* Fill the new centroids on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		bin_number_msg[0]=new_bins_pointers[i]->item.number;
		
		/* Go forward on the message */
		bin_number_msg=&(bin_number_msg[1]);

		/* Fills the dimension values on the message */
		bin_number_msg=(bin_number_type *)fill_dimension_values_on_msg(bin_number_msg, &(new_bins_pointers[i]->item.centroid.dimension_values), dimension_size);
	}

	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG

	if (DEBUG >= LOGGER)
	{	
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_new_iteration_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	ahWriteBuffer(Assigner_output, msg, msg_size);
	
	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that inform that a BIN is concluded to the Assigner filter 
 *	send_concluded_bin_message
 *
 * 	campo 0: Constante definindo o tipo de mensagem para CONCLUDED_BIN_MSG
 * 	campo 1: Número do BIN concluido
 * 	campo 2: Taxa de Eliminação de Objetos do Bin
*/
int send_concluded_bin_message(bin_number_type bin_number, float prune_ratio, OutputPortHandler Assigner_output)
{
	/* The message will contains the number of the Splitted Bin, the number and the dimension values of the centroid of the new Bins */
	unsigned msg_size=sizeof(int)+sizeof(bin_number_type)+sizeof(float);
	void *msg;
	int *int_msg;
	float *prune_ratio_msg;
	bin_number_type *bin_number_msg;
	msg=malloc(msg_size);
	int_msg=(int *)msg;
	

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]=CONCLUDED_BIN_MSG;
	
	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	
	bin_number_msg[0]=bin_number;

	//Assign the elimination ratio of bin
	
	prune_ratio_msg=(float *)(&(bin_number_msg[1]));
	
	prune_ratio_msg[0]=prune_ratio;
       	
	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_concluded_bin_message(msg, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	ahWriteBuffer(Assigner_output, msg, msg_size);

	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Get the values of the object from the message. The structures of the object will be allocated. */ 
void *get_object_from_msg(object_type *object, dimension_size_type dimension_size, void *msg)
{
	unsigned int *msg_int;
	void *msg_values;
	
	malloc_object(object, dimension_size);

	/* read the object number from the message */
	msg_int=(unsigned int *)msg;
	(*object).number=msg_int[OBJECT_NUMBER_FIELD];
	
	msg_values=&(msg_int[OBJECT_VALUES_FIELD]);

	/* read the dimension values from message */
	msg_values=get_dimension_values_from_msg(&((*object).dimension_values), msg_values, dimension_size);
	
	return msg_values;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Gets the local values of the object belonging to a bin from the message */
int get_object_values_of_bin_from_msg(int *msg, bin_list_type *list, dimension_size_type dimension_size)
{
	object_type object;
	bin_type bin;
	bin_list_pointer bin_pointer;
	int *int_msg;
	bin_number_type *bin_number_msg;
	char error_msg[100];
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER )
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "RECEIVING: \t");
		print_object_values_of_bin_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	bin_number_msg=(bin_number_type *)&(msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	/* Gets the Bin_number from the message */
	bin.number=bin_number_msg[0];
	
	/* Gets a pointer to the bin that are being splitted */
	bin_pointer=search_item_in_bin_list(bin, list);

	/* Tests if the message was destinated to a new bin that doesn't exists until now */
	if (bin_pointer==NULL)
	{
		sprintf(error_msg,"ERROR. Received an OBJECT_VALUES_OF_BIN message, but the Bin: %llu was not found on the list of the bins!!!\n", bin.number);
		ahExit (error_msg);
	}

	// Go forward on the message
	int_msg=(int *)(&(bin_number_msg[1]));

	/* Get the object values from the message */	
	get_object_from_msg(&object, dimension_size, int_msg);

	/* Insert the new object on the list of objects of the bin*/
	insert_in_list(object, &(bin_pointer->item.objects));

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Fill the nearest neighbours structure on the message */ 
int fill_nearest_neighbours_in_msg(nearest_neighbours_type *nearest_neighbours, message_type msg)
{
	unsigned int *msg_int;
	distance_type *msg_distance;
	int i, object_number;
	distance_type distance;	
	
	/* read the number of neighbours already found from the message */
	msg_int=(unsigned int *)msg;
	
	/* Fills the number of the neighbours already found */
	msg_int[0]=nearest_neighbours->size;
	
	/* Go forward on the message */	
	msg_int=&msg_int[1];	

	/* fill the neighbours values in message */
	for (i=0; i<nearest_neighbours->size; i++)	
	{
		/* Fills the number of the neighbour */
		object_number=msg_int[0];
		msg_int[0]=(nearest_neighbours->objects)[i];
		
		/*Go forward on the message */
		msg_distance=(distance_type *)&(msg_int[1]);
		
		/* Get the distance of the neighbour */
		distance=msg_distance[0];
		msg_distance[0]=(nearest_neighbours->distance)[i];
		
		/* Go forward on the msg */
		msg_int=(unsigned int *)&(msg_distance[1]);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that asks for searching nearest neighbours of an object to the Bin filter 
 *	send_probable_outlier_message
 *
 * 	campo 0: Constante definindo o tipo de mensagem para PROBABLE_OUTLIER_MSG
 * 	campo 1: Número da instância que receberá a mensagem
 * 	campo 2: Número do bin a qual pertence o objeto
 * 	campo 3: Flag indicando se o objeto é ou não o último da instância
 * 	campo 4: Número do objeto
 * 	campo ?: Dimensões do objeto
 * 	campo ?: Número de vizinhos encontrados
 * 	campo ?: número e distância dos vizinhos mais próximos
*/
int send_probable_outlier_message(object_type *object, nearest_neighbours_type *nearest_neighbours, int is_last_object_of_the_instance, bin_number_type instance_number, bin_number_type bin_owner_number, dimension_size_type dimension_size, int number_of_neighbours, OutputPortHandler Bin_output)
{
   	// Gets the maximum size of a probable outlier message
	unsigned msg_size=get_probable_outlier_message_size(dimension_size, number_of_neighbours);
	void *msg;
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	
	// Allocates space to fill the message
	msg=malloc(msg_size);

	int_msg=(unsigned int *)msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fills the field of the message with the right type */
	int_msg[TO_BIN_MSG_TYPE_FIELD]=PROBABLE_OUTLIER_MSG;

	// Go forward on the message
	bin_number_msg=(bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	/* Fills the number of the instance that the message will be sent */
	bin_number_msg[0]=instance_number;

	/* Fills the number of the bin that the object belongs */
	bin_number_msg[1]=bin_owner_number;

	// Go forward on the message
	int_msg=(unsigned int *)&(bin_number_msg[2]);
	
	/* Fills a flag indicating is it is the last object of the instance */
	int_msg[0]=is_last_object_of_the_instance;	
	
	/* Fills the number of the object */
	int_msg[1]=object->number;

	// Go forward on the message
	int_msg=&(int_msg[2]);

	/* Fills the dimension values of the object on the message and go forward on the message */
	int_msg=fill_dimension_values_on_msg(int_msg, &(object->dimension_values), dimension_size);

	/* Fills the nearest neighbours structure on the message */
	fill_nearest_neighbours_in_msg(nearest_neighbours, int_msg);
	
	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_probable_outlier_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	ahWriteBuffer(Bin_output, msg, msg_size);

	free(msg);
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that tells to the Final filter that a new outlier was found
 *	send_new_outlier_message
 *
 * 	campo 0: OBJECT NUMBER
 * 	campo 1: Score of the object
 * 	campo ?: Object Dimensions
 * 	campo ?: Número de vizinhos encontrados
 * 	campo ?: número e distância dos vizinhos mais próximos
*/
int send_new_outlier_message(object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours, OutputPortHandler Final_output)
{
	unsigned msg_size=get_new_outlier_message_size(number_of_neighbours, dimension_size);
	void *msg;
	int *int_msg;
	distance_type *distance_msg;
	distance_type score;
	msg=malloc(msg_size);
	int_msg=(int *)msg;
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fills the field of the message with the number of the object */
	int_msg[TO_FINAL_OBJECT_NUMBER_FIELD]=object->number;

	/* Fills this score */
	distance_msg=(distance_type *)&(int_msg[TO_FINAL_SCORE_VALUE_FIELD]);

	/* Fills the dimension values of the object on the message and go forward on the message */
	distance_msg=(distance_type*)fill_dimension_values_on_msg((void *)distance_msg, &(object->dimension_values), dimension_size);

	/* Gets the score of the object */
	score=get_minimum_distance_in_nearest_neighbours(nearest_neighbours, number_of_neighbours);
	
	distance_msg[0]=score;
	
	/* Go forward on the message */
	distance_msg=(distance_type *)&(distance_msg[1]);

	/* Fills the nearest neighbours structure on the message */
	fill_nearest_neighbours_in_msg(nearest_neighbours, (void *)distance_msg);
	
	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= LOGGER )
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "SENDING: \t");
		print_new_outlier_message(msg, dimension_size, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	ahWriteBuffer(Final_output, msg, msg_size);
	
	free(msg);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that tells to the other instances of the Bin filter that a new outlier was found to update the scores
 * 	send_score_new_outlier
 * 	campo 0: Constante definindo o tipo da mensagem
 * 	campo 1: Número do Bin utilizado pela política label stream para entregar a mensagem
 * 	campo 2: score do novo Outlier encontrado *
*/
int send_score_new_outlier_messages(distance_type score, OutputPortHandler Bin_output)
{
	// The message have the header, the number of the instance (type bin_number_type) and the score
	unsigned msg_size=sizeof(int)+sizeof(bin_number_type)+sizeof(distance_type);
	void *msg;
	int *int_msg;
	bin_number_type *bin_number_msg;
	distance_type *distance_type_msg;
	unsigned int total_instances;
	bin_number_type i, instance_number;
	msg=malloc(msg_size);
	int_msg=(int *)msg;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fills the field of the message with the type of the message */
	int_msg[TO_BIN_MSG_TYPE_FIELD]=SCORE_NEW_OUTLIER_MSG;

	// Go forward on the message until the bin number field
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_BIN_BIN_NUMBER_FIELD]));

	// Go forward on the message	
	distance_type_msg=(distance_type *)(&(bin_number_msg[1]));
	/* Fills the new score on the message */
	distance_type_msg[0]=score;

	// Gets the total of instances to know the number of messages that will be sent
	total_instances=ahGetTotalInstances();
	
	// Gets the number of the instance to be used for do not send the message for itself
	instance_number=ahGetMyRank();
	
	/* Send the new Score to whole other instances */
	for (i=0; i<total_instances; i++)
	{
		bin_number_msg[0]=i;
		
		// Do not send the message to itself	
		if (instance_number != i)
		{
			/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if (DEBUG >= LOGGER)
			{
				fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
				fprintf(fp_log, "SENDING: \t");
				print_score_new_outlier_message(msg, fp_log);
				fprintf(fp_log, "END OF MESSAGE\n");
				fclose(fp_log);
			}
#endif		
/****************************************************************************/
			ahWriteBuffer(Bin_output, msg, msg_size);
		}
	}	
	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*
 * MESSAGE: Message that tells to the instance 0 of the Bin filter that all the objects of an instance already have been considered to be outliers
 * 	send_search_finished_on_instance
 * 	campo 0: Constante definindo o tipo da mensagem
 * 	campo 1: Campo indicando a instância que receberá a mensagem
*/
int send_search_finished_on_instance_messages(OutputPortHandler Bin_output, bin_number_type number_of_instance)
{
	unsigned msg_size=sizeof(int)+2*sizeof(bin_number_type);
	void *msg;
	int *int_msg;
	bin_number_type *bin_number_msg;
	int i, total_instances;

	msg=malloc(msg_size);
	int_msg=(int *)msg;
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Fills the field of the message with the type of the message */
	int_msg[TO_BIN_MSG_TYPE_FIELD]=SEARCH_FINISHED_ON_INSTANCE_MSG;

	bin_number_msg=(bin_number_type *)&(int_msg[TO_BIN_MSG_TYPE_FIELD+1]);	
	
	/* Fills the number of the instance that have whole theirs objects already searched */
	bin_number_msg[1]=number_of_instance;
	
	total_instances=ahGetTotalInstances();
	
	/* Send the new Score to whole instances */
	for (i=0; i<total_instances; i++)
	{
		bin_number_msg[0]=i;
		
		/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if (DEBUG >= LOGGER)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "SENDING: \t");
			print_search_finished_on_instance_message(msg, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
			fclose(fp_log);
		}
#endif		
/****************************************************************************/
		ahWriteBuffer(Bin_output, msg, msg_size);
	}	
	free(msg);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Get the values of the object from the message. The structures of the object will be allocated. */ 
int get_nearest_neighbours_from_msg(nearest_neighbours_type *nearest_neighbours, message_type msg, int number_of_neighbours)
{
	unsigned int *msg_int;
	distance_type *msg_distance_type;
	unsigned int number_of_neighbours_found, i;
	unsigned int object_number;
	distance_type distance;	
	
	malloc_nearest_neighbours(nearest_neighbours, number_of_neighbours);

	/* read the number of neighbours already found from the message */
	msg_int=(unsigned int *)msg;
	number_of_neighbours_found=msg_int[0];
	
	/* Go forward on the message */	
	msg_int=&msg_int[1];	

	/* read the dimension values from message */
	for (i=0; i<number_of_neighbours_found; i++)	
	{
		/* Gets the number of the neighbour */
		object_number=msg_int[0];	
		
		/*Go forward on the message */
		msg_distance_type=(distance_type *)&(msg_int[1]);

		/* Get the distance of the neighbour */
		distance=msg_distance_type[0];

		/* Insert the neighbour on the nearest neighbour array */
		insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);
		
		/* Go forward on the msg */
		msg_int=(unsigned int *)&(msg_distance_type[1]);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Gets the local values of the bins from the message */
int get_probable_outlier_from_msg(message_type message, bin_number_type *bin_real_owner_number, object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours)
{
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	distance_type *distance_type_msg;	
	message_type nearest_neighbours_message;
	int flag_is_last_object_of_the_instance;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_probable_outlier_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else 
			fprintf(fp_log, "RECEIVING PROBABLE OUTLIER MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	int_msg=(unsigned int *)message;
	
	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[1]));

	/* Gets the number of the bin that the object belongs */
	/* This information can be necessary to know if the neigbours of the object already have been searched in all instances */
	*bin_real_owner_number=bin_number_msg[1];

	// Go forward on the message
	int_msg = (unsigned int *)(&(bin_number_msg[2]));
	
	flag_is_last_object_of_the_instance=int_msg[0];
	
	/* Go forward the message to the object values */
	int_msg=&(int_msg[1]);

	/* Get the object values from the message and Go forward on the message */	
	distance_type_msg=(distance_type *)get_object_from_msg(object, dimension_size, int_msg);

	nearest_neighbours_message = (message_type)distance_type_msg;
		
	/* Get the nearest neighbours from the message */
	get_nearest_neighbours_from_msg(nearest_neighbours, nearest_neighbours_message, number_of_neighbours);
	
	return flag_is_last_object_of_the_instance;
}

int get_new_outlier_from_msg(message_type message, object_type *object, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_neighbours)
{
	unsigned int *int_msg=(unsigned int *)message;
	distance_type *distance_type_msg;
	distance_type score;
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_final_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	int_msg=&(int_msg[TO_FINAL_OBJECT_NUMBER_FIELD]);
	
	/* Fills the field of the message with the object  and Go Forward on the message */
	distance_type_msg=(distance_type *)get_object_from_msg(object, dimension_size, (void *)int_msg);

	/* Fills this score */
	score=distance_type_msg[0];

	/* Go forward on the message */
	distance_type_msg=(distance_type *)&(distance_type_msg[1]);

	/* Get the nearest neighbours structure from the message */
	get_nearest_neighbours_from_msg(nearest_neighbours, (void *)distance_type_msg, number_of_neighbours);
	
	/* Sends the message */
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_new_outlier_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else
			fprintf(fp_log, "RECEIVING NEW OUTLIER MESSAGE\n");
			
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print a msg sent by the distributor filter to the assigner filter on a file.
 */
void print_msg_distributor(void *msg, dimension_size_type dimension_size, FILE *file)
{
	unsigned int *msg_int;
	distance_type *values_msg;
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	msg_int=(unsigned int *)msg;
	fprintf(file, "OBJECT NUMBER: %d\n", msg_int[OBJECT_NUMBER_FIELD]);
	
	/* Go forward on the message */
	values_msg=(distance_type *)&(msg_int[OBJECT_VALUES_FIELD]);

	get_dimension_values_from_msg(&dimension_values, values_msg, dimension_size);

	/* Print the dimension values that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);
	
	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
	
}

/* This function is responsible to print the msg with the first mbr values sent by the assigner filter to the bin filter on a file.
 */
void print_first_mbr_values_of_bin_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	int *int_msg=(int *)msg;
	bin_number_type *bin_number_msg;
	void *values_msg;
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	/* If the message is not a First Mbr Values Message doesn't print anything */
	if (int_msg[TO_BIN_MSG_TYPE_FIELD]!=FIRST_MBR_VALUES_MSG)
		return;
	
	fprintf(file, "MESSAGE: FIRST_MBR_VALUES\t");
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_BIN_MSG_TYPE_FIELD+1]));
	bin_number_msg[0]=0;
	fprintf(file, "To Bin Number: %llu\n", bin_number_msg[0]);

	/* Go forward on the message */
	values_msg=(&(bin_number_msg[1]));

	fprintf(file, "UPPER BOUND MBR: ");
	
	values_msg=get_dimension_values_from_msg(&dimension_values, values_msg, dimension_size);

	/* Print the dimension values that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);

	fprintf(file, "LOWER BOUND MBR: ");
	
	values_msg=get_dimension_values_from_msg(&dimension_values, values_msg, dimension_size);

	/* Print the dimension values that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
	
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print the split bin msg sent from the Bin filter to the Assigner filter on a file. */
void print_split_bin_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	int *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(int *)msg;
	int i;
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	/* If the message is not a Split Bin Message doesn't print anything */
	if (int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]!=SPLIT_BIN_MSG)
		return;
	
	fprintf(file, "MESSAGE: SPLIT_BIN\t");

	// Go forward on the message
	bin_number_msg = (bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	
	fprintf(file, "Splitted Bin Number: %llu\n", bin_number_msg[0]);

	/* Runs the message until the Values fields */
	bin_number_msg=(&(bin_number_msg[1]));

	/* Prints the new centroids on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		fprintf(file, "Bin Number: %llu\tValues: ", bin_number_msg[0]);
		
		/* Go forward on the message */
		bin_number_msg=(&(bin_number_msg[1]));
	
		/* Get the dimension values from the message and Go forward on the message */
		bin_number_msg = get_dimension_values_from_msg(&dimension_values, bin_number_msg, dimension_size);

		/* Print the dimension values that were get from the message */
		print_dimension_values(dimension_values, dimension_size, file);
	}

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print the concluded bin msg sent from the Bin filter to the Assigner filter on a file. */
void print_concluded_bin_message(void *msg, FILE *file)
{
	int *int_msg;
	float *float_msg;
	bin_number_type *bin_number_msg;
	int_msg=(int *)msg;
	
	/* If the message is not a Conclude Bin Message doesn't print anything */
	if (int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]!=CONCLUDED_BIN_MSG)
		return;

	// Go forward on the message
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	float_msg = (float *)(&(bin_number_msg[1]));
	fprintf(file, "MESSAGE: CONCLUDED_BIN\t");
	fprintf(file, "Bin Number: %llu\t, Ratio Eliminated : %f\n", bin_number_msg[0], float_msg[0]);
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print the new iteration msg sent from the Bin filter to the Assigner filter on a file. */
void print_new_iteration_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	int *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(int *)msg;
	int i;
	
	dimension_values_type dimension_values;

	/* Create a dummy dimension values just to print them later */
	malloc_dimension_values(&dimension_values, dimension_size);
	
	/* If the message is not a new iteration Message doesn't print anything */
	if (int_msg[TO_ASSIGNER_MSG_TYPE_FIELD]!=NEW_ITERATION_MSG)
		return;
	
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_ASSIGNER_MSG_TYPE_FIELD+1]));
	
	fprintf(file, "MESSAGE: NEW_ITERATION_MSG\t");
	
	fprintf(file, "Splitted Bin Number: %llu\n", bin_number_msg[0]);

	/* Runs the message until the Values fields */
	bin_number_msg=&(bin_number_msg[1]);

	/* Prints the new centroids on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		fprintf(file, "Bin Number: %llu\tValues: ", bin_number_msg[0]);
		
		/* Go forward on the message */
		bin_number_msg=(&(bin_number_msg[1]));

		/* Get the dimension values from the message and Go forward on the message */
		bin_number_msg=(bin_number_type *)get_dimension_values_from_msg(&dimension_values, bin_number_msg, dimension_size);

		/* Print the dimension values that were get from the message */
		print_dimension_values(dimension_values, dimension_size, file);
	}
	
	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

void *print_modes_values_from_msg(FILE *file, categorical *categorical_values_msg)
{
	int *frequency_msg, frequency,number_of_occurrences;
	categorical *value_number_msg, value_number;
  
	frequency=1;
	number_of_occurrences=0;
	value_number_msg=categorical_values_msg;
   
	/* Printing the occurrences while there is a new occurrence on the message and the limit of occurrence was not exceeded */
	while (frequency>0 && number_of_occurrences < NUMBER_OF_MODES)
	{
		/* Saves the categorical value on the message */ 
		value_number=value_number_msg[0];
   
		/* Go forward on the message */
		frequency_msg=(int *)&(value_number_msg[1]);
         
		/* Saves the frequency of the value on the message */ 
		frequency=frequency_msg[0];
   
		/* Go forward on the message */
		value_number_msg=(categorical *)&(frequency_msg[1]);
	
		/* Tests if really there is a new occurrence or it is a dummy occurrence (frequency 0) */
		if(frequency>0) 
		{
		        /* Printing the Value Number and the frequency on the file */
			fprintf(file, "\t\t\tValue number: ");
			fprintf(file, CATEGORICAL_CONVERSION_STRING, value_number);
			fprintf(file, "\tFrequency:%10.1d\n", frequency);

			/* Computes that the occurrence was filled */
			number_of_occurrences++;
		}
        
	}

	/* Tests if there aren't filled occurrences */
	if (number_of_occurrences==0)
		fprintf(file, "\t\t\tThere isn't occurrences on the list!!!\n");
	
	return(value_number_msg);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print the Local Centroid Dimension Values from the message on a file. */
/* It prints the local dimension values and the modes of the categorical attributes */
void *print_local_centroid_dimension_values_from_msg(FILE *file, void *msg, dimension_size_type dimension_size)
{
	int *int_msg;
	int_msg=(int *)msg;
	unsigned int i;
	
        real *real_values_msg;
	ordinal *ordinal_values_msg;
	categorical *categorical_values_msg;
	
	real_values_msg=(real *)msg;

	// Print the dimension values  
	// Printing the real dimension values 
        fprintf(file, "\t\tReal: ");
	if (dimension_size.number_of_real_values>0)
		print_real_array(file, real_values_msg, dimension_size.number_of_real_values);
	else fprintf(file, "There aren't Real Values!!!\n");
	
	/* Go forward on the message */
	ordinal_values_msg=(ordinal *)&(real_values_msg[dimension_size.number_of_real_values]);
	
	// Printing the ordinal dimension values 
	fprintf(file, "\t\tOrdinal: ");
	if (dimension_size.number_of_ordinal_values>0)
		print_ordinal_array(file, ordinal_values_msg, dimension_size.number_of_ordinal_values);
	else fprintf(file, "There aren't Ordinal Values!!!\n");
	
	/* Go forward on the message */
	categorical_values_msg=(categorical *)&(ordinal_values_msg[dimension_size.number_of_ordinal_values]);
	
	// Printing the categorical modes values 
	fprintf(file, "\t\tCategorical Modes:\n");
	if (dimension_size.number_of_categorical_values>0)
	{
		for (i=0; i<dimension_size.number_of_categorical_values; i++)
		{       
			fprintf(file, "\t\tModes for Categorical Attribute %d:\n",i);
		        
			/* Print the modes of one attribute on a file and Go forward on the message */
			categorical_values_msg=print_modes_values_from_msg(file, categorical_values_msg);
		}
	}
	else fprintf(file, "There aren't Categorical Values!!!\n");
	return (categorical_values_msg);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Bins Local Values message sent from the Assigner filter to the Bin filter on a file. */
void print_bins_local_values_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(unsigned int *)msg;
	int i, number_of_objects;
	
	/* Create a dummy dimension values just to print them later */
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	bin_number_msg=(bin_number_type *)(&(int_msg[TO_BIN_BIN_NUMBER_FIELD]));
	
	fprintf(file, "MESSAGE: BINS_LOCAL_VALUES\t");
	fprintf(file, "Splitted Bin Number: %llu\n", bin_number_msg[0]);

	/* Runs the message until the Values fields */
	bin_number_msg=&(bin_number_msg[1]);

	/* Prints the new local values of the bins from the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
	   	int_msg=(unsigned int *)(&(bin_number_msg[1]));
	   	fprintf(file, "Bin Number: %llu\tNumber of objects: %d\n\t", bin_number_msg[0], int_msg[0]);
		
		// Get the number of objects and keep this value
		number_of_objects=int_msg[0];
	
		// Go forward on the message
		int_msg=(&(int_msg[1]));
		
		if (number_of_objects==0)
		{
			fprintf(file, "There are no Dimension Values!!!\n");
			
			// Go forward on the message
			bin_number_msg=(bin_number_type *)int_msg;
		}
		else
		{
			fprintf(file, "Dimension Values: \n");
			
			/* Print the local centroid dimension values from the message and Go forward on the message */
			int_msg=print_local_centroid_dimension_values_from_msg(file, int_msg, dimension_size);
		
			/* Get the Upper Bound MBR dimension values from the message and Go forward on the message */
			int_msg=get_dimension_values_from_msg(&dimension_values, int_msg, dimension_size);
			
			/* Prints the upper Bound MBR values of the bin */
			fprintf(file, "\tUpper Bound MBR values: ");
			print_dimension_values(dimension_values, dimension_size, file);

			/* Get the Lower Bound MBR dimension values from the message and Go forward on the message */
			bin_number_msg=get_dimension_values_from_msg(&dimension_values, int_msg, dimension_size);

			/* Prints the lower Bound MBR values of the bin */
			fprintf(file, "\tLower Bound MBR values: ");
			print_dimension_values(dimension_values, dimension_size, file);
		}
	}
	
	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Concluded Bin Local Values message sent from the Assigner filter to the Bin filter on a file. */
void print_concluded_bin_local_values_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	int number_of_objects, *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(int *)msg;

	/* Create a dummy dimension values just to print them later */
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	fprintf(file, "MESSAGE: CONCLUDED_BIN_LOCAL_VALUES\t");
	
	bin_number_msg=(bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	fprintf(file, "Concluded Bin Number: %llu\t", bin_number_msg[0]);

	/* Runs the message until the Values fields */
	int_msg=(int *)&(bin_number_msg[1]);

	number_of_objects=int_msg[0];
	fprintf(file, "Number of objects: %d\n", number_of_objects);
		
	int_msg=(&(int_msg[1]));

	if (number_of_objects == 0)
		fprintf(file, "There are no Dimension Values!!!\n");
	else
	{
		fprintf(file, "\tDimension Values: \n");

		/* Print the local centroid dimension values from the message and Go forward on the message */
		int_msg=print_local_centroid_dimension_values_from_msg(file, int_msg, dimension_size);
		
		/* Get the Upper Bound MBR dimension values from the message and Go forward on the message */
		int_msg=get_dimension_values_from_msg(&dimension_values, int_msg, dimension_size);

		/* Prints the upper Bound MBR values of the bin */
		fprintf(file, "\tUpper Bound MBR values: ");
		print_dimension_values(dimension_values, dimension_size, file);

		/* Get the Lower Bound MBR dimension values from the message and Go forward on the message */
		int_msg=get_dimension_values_from_msg(&dimension_values, int_msg, dimension_size);

		/* Prints the lower Bound MBR values of the bin */
		fprintf(file, "\tLower Bound MBR values: ");
		print_dimension_values(dimension_values, dimension_size, file);
	}

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Object Values of Bin message sent from the Assigner filter to the Bin filter on a file. */
void print_object_values_of_bin_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(unsigned int *)msg;
	
	/* Create a dummy dimension values just to print them later */
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);

	bin_number_msg = (bin_number_type *) &(int_msg[TO_BIN_BIN_NUMBER_FIELD]);
	int_msg=(unsigned int *)&(bin_number_msg[1]);  
	
	fprintf(file, "MESSAGE: OBJECT_VALUES_OF_BIN\t");
	fprintf(file, "Bin Number: %llu\t", bin_number_msg[0]);
	fprintf(file, "OBJECT NUMBER: %d\t", int_msg[0]);

	// Go forward on the message
	int_msg=(&(int_msg[1]));
	
	/* Prints the object values from the message */
	fprintf(file, "Dimension Values: ");

	/* Get the dimension values from the message */
	get_dimension_values_from_msg(&dimension_values, int_msg, dimension_size);

	/* Print the dimension values that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Probable Outlier message sent from the Bin filter to the Bin filter on a file. */
void print_probable_outlier_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	unsigned int *int_msg;
	bin_number_type *bin_number_msg;
	distance_type *distance_type_msg;
	int_msg=(unsigned int *)msg;
	int i, size;
	
	/* Create a dummy dimension values just to print them later */
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	fprintf(file, "MESSAGE: PROBABLE OUTLIER\t");

	bin_number_msg = (bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	/* Prints the number of the instance that the message will be sent */
	fprintf(file, "INSTANCE DESTINATED: %llu\t", bin_number_msg[0]);
	
	/* Prints the number of the bin that the object belongs */
	fprintf(file, "BIN OWNER INSTANCE: %llu\n", bin_number_msg[1]);

	// Go forward on the message
	int_msg=(unsigned int *)&(bin_number_msg[2]);
	
	/* Prints a flag indicating is it is the last object of the instance */
	fprintf(file, "FLAG: %d\t", int_msg[0]);	
	       
	/* Prints the number of the object */
	fprintf(file, "OBJECT NUMBER: %d\n", int_msg[1]);

	/* Go forward on the message */
	distance_type_msg=(distance_type *)&(int_msg[2]);

	/* Prints the dimension values of the object */
	fprintf(file, "DIMENSIONS: ");
	
	/* Get the dimension values from the message and Go forward on the message */
	int_msg=(unsigned int *)get_dimension_values_from_msg(&dimension_values, distance_type_msg, dimension_size);

	/* Print the dimension values of the object that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);

	/* Get the number of neighbours already found */
	size=int_msg[0];
	
	/* Prints the nearest neighbours */
	fprintf(file, "NUMBER OF NEIGHBOURS: %d\n", size);

	/* Go forward on the message */
	int_msg=(unsigned int *)&(int_msg[1]);
			
	for (i=0; i<size; i++)
	{
		fprintf(file, "\tNEIGHBOUR: %d\t", int_msg[0]);
		
		/* Go forward on the message */
		distance_type_msg=(distance_type *)&(int_msg[1]);

		fprintf(file, "DISTANCE: ");
		fprintf(file, DISTANCE_CONVERSION_STRING, distance_type_msg[0]);
		fprintf(file, "\n"); 
		
		/* Go forward on the message */
		int_msg=(unsigned int *)&(distance_type_msg[1]);
	}
	fprintf(file, "\n");

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print New Outlier message sent from the Bin filter to the Final filter on a file. */
void print_new_outlier_message(void *msg, dimension_size_type dimension_size, FILE *file)
{
	unsigned int *int_msg;
	distance_type *distance_type_msg;
	int_msg=(unsigned int *)msg;
	int i, size;
	
	/* Create a dummy dimension values just to print them later */
	dimension_values_type dimension_values;
	malloc_dimension_values(&dimension_values, dimension_size);
	
	/* Prints the type of the message */
	fprintf(file, "MESSAGE: NEW OUTLIER\n");
	
	/* Prints the number of the object that the message will be sent */
	fprintf(file, "OBJECT NUMBER: %d\t", int_msg[TO_FINAL_OBJECT_NUMBER_FIELD]);

	/* Go forward on the message */
	distance_type_msg=(distance_type *)&(int_msg[TO_FINAL_SCORE_VALUE_FIELD]);
	
	/* Prints the dimension values of the object */
	fprintf(file, "DIMENSIONS: ");

	/* Get the dimension values from the message and Go forward on the message */
	distance_type_msg=(distance_type *)get_dimension_values_from_msg(&dimension_values, distance_type_msg, dimension_size);

	/* Print the dimension values of the object that were get from the message */
	print_dimension_values(dimension_values, dimension_size, file);
	
	/* Prints the number of the bin that the object belongs */
	fprintf(file, "SCORE: ");
	fprintf(file,DISTANCE_CONVERSION_STRING, distance_type_msg[0]);
	fprintf(file,"\n");
	/* Go forward on the message */
	int_msg=(unsigned int *)(&(distance_type_msg[1]));

	size = int_msg[0];
	
	/* Prints the nearest neighbours */
	fprintf(file, "NUMBER OF NEIGHBOURS: %d\n", size);

	/* Go forward on the message */
	int_msg=(unsigned int *)&(int_msg[1]);
			
	for (i=0; i<size; i++)
	{
		fprintf(file, "\tNEIGHBOUR: %d\t", int_msg[0]);
		
		/* Go forward on the message */
		distance_type_msg=(distance_type *)&(int_msg[1]);

		fprintf(file, "DISTANCE: ");
		fprintf(file,DISTANCE_CONVERSION_STRING, distance_type_msg[0]);
		fprintf(file,"\n");
		
		/* Go forward on the message */
		int_msg=(unsigned int *)&(distance_type_msg[1]);
	}
	fprintf(file, "\n");

	/* Frees the memory allocated for the dimension values */
	free_dimension_values(dimension_values);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Score New Outlier message sent from the Bin filter to the Bin filter on a file. */
void print_score_new_outlier_message(void *msg, FILE *file)
{
	int *int_msg;
	distance_type *distance_type_msg;
	int_msg=(int *)msg;

	/* Prints the type of the message */
	fprintf(file, "MESSAGE: SCORE NEW OUTLIER\t");

	fprintf(file, "INSTANCE DESTINATED:%d\t", int_msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	/* Go forward until the field where the new score was saved */
	distance_type_msg=(distance_type *)&(int_msg[TO_BIN_VALUES_FIELD]);
	
	/* Prints the new score */
	fprintf(file, "SCORE: ");
	fprintf(file,DISTANCE_CONVERSION_STRING, distance_type_msg[0]);
	fprintf(file,"\n");

}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This function is responsible to print Search Finished on Instance message sent from the Bin filter to the Bin filter on a file. */
void print_search_finished_on_instance_message(void *msg, FILE *file)
{
	int *int_msg;
	bin_number_type *bin_number_msg;
	int_msg=(int *)msg;

	/* Prints the type of the message */
	fprintf(file, "MESSAGE: SEARCH FINISHED ON INSTANCE\n");
	
	bin_number_msg=(bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);

	fprintf(file, "INSTANCE DESTINATED:%llu\t", bin_number_msg[0]);
	
	/* Prints the new score */
	fprintf(file, "SEARCH FINISHED ON INSTANCE : %llu\n", bin_number_msg[1]);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Return the size of a FIRST_MBR_VALUES message */
inline unsigned int get_first_mbr_values_message_size(dimension_size_type dimension_size)
{
	/* The message will contains the header, the number of the Splitted Bin, the number and the local values (centroid, upper_bound and lower_bound) of the Bins */
	int head_msg_size=sizeof(int)+sizeof(bin_number_type);
	int mbr_values_msg_size=2*(get_dimension_values_message_size(dimension_size));
	return (head_msg_size+mbr_values_msg_size);
}

/* Return the maximum size of a BINS LOCAL VALUES message */
inline unsigned int get_bins_local_values_message_size(dimension_size_type dimension_size)
{
	/* The message will contains the header, the number of the Splitted Bin, the number and the local values (centroid, upper_bound and lower_bound) of the Bins */
	int head_msg_size=sizeof(int)+sizeof(bin_number_type);
	int bin_head_msg_size=2*NUM_KMEANS_PARTITIONS*sizeof(bin_number_type);
	int mbr_bouns_values_msg_size=2*NUM_KMEANS_PARTITIONS*(get_dimension_values_message_size(dimension_size));
        int centroid_values_msg_size=NUM_KMEANS_PARTITIONS*(get_local_dimension_values_message_size(dimension_size));
	return (head_msg_size+bin_head_msg_size+centroid_values_msg_size+mbr_bouns_values_msg_size);
}

/* Return the maximum size of a New Iteration message */
inline unsigned int get_new_iteration_message_size(dimension_size_type dimension_size)
{
	/* The message will contains the header, the number of the Splitted Bin, the number and centroid of the Bins */
	int head_msg_size=sizeof(int)+sizeof(bin_number_type);
	int bin_head_msg_size=NUM_KMEANS_PARTITIONS*sizeof(bin_number_type);
	int centroid_values_msg_size=NUM_KMEANS_PARTITIONS*(get_dimension_values_message_size(dimension_size));
	return (head_msg_size+bin_head_msg_size+centroid_values_msg_size);
}

/* Return the maximum size of a CONCLUDED BIN LOCAL VALUES message */
inline unsigned int get_concluded_bin_local_values_message_size(dimension_size_type dimension_size)
{
	/* The message will contains the header, the number of the concluded bin, the number of objects inserted on the bin and the local values (centroid, upper_bound and lower_bound) of this Bin */
	int head_msg_size=sizeof(int)*2+sizeof(bin_number_type);
	int centroid_values_msg_size=get_local_dimension_values_message_size(dimension_size);
	int mbr_bouns_values_msg_size=2*(get_dimension_values_message_size(dimension_size));
	return (head_msg_size+centroid_values_msg_size+mbr_bouns_values_msg_size);
}

/* Return the maximum size of a PROBABLE OUTLIER message */
inline unsigned int get_probable_outlier_message_size(dimension_size_type dimension_size, int number_of_neighbours)
{
	/* The message will contain the header, the number of the instance that will receive the message, the number of the bin that the object belongs, a flag, the number of the object, the number of neighbours found, the dimension values of the object, the number and the distance of the neighbours */
	
	// Header, flag, object number and number of neighbours already found
	int int_msg_size=4*sizeof(unsigned int);

	//Instance Destinated and Bin Owner
	int bin_number_msg_size=2*sizeof(bin_number_type);
		
	// The values of the probable outlier
	int object_values_msg_size = get_dimension_values_message_size(dimension_size);

	// The number of the neighbours
	int neighbours_number_msg_size=number_of_neighbours * sizeof(unsigned int);
	
	// The distances of the neighbours
	int distance_msg_size=number_of_neighbours * sizeof(distance_type);

	return (int_msg_size+bin_number_msg_size+neighbours_number_msg_size+distance_msg_size+object_values_msg_size);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Return the size of a NEW OUTLIER message */
inline unsigned int get_new_outlier_message_size(int number_of_neighbours, dimension_size_type dimension_size)
{
	unsigned msg_size = sizeof(unsigned int) * ( 2 + number_of_neighbours) + sizeof(distance_type)*(number_of_neighbours+1) + get_dimension_values_message_size(dimension_size);

	return (msg_size);
}
