#ifndef PROCEDURES_ASSIGNER_H
#define PROCEDURES_ASSIGNER_H
#include "rbrp.h"

#ifdef INSTRUMENTATION
extern unsigned long long NUMBER_OF_ASSIGNS;
extern unsigned long long NUMBER_OF_OBJECTS_PRUNING;
extern unsigned long long NUMBER_OF_TASKS;
extern unsigned long long NUMBER_OF_OBJECTS_UNSTABLE;
extern unsigned long long NUMBER_OF_OBJECTS_IN_ASSIGN;
extern struct timeval INITIAL_TIME_INST;
#endif 

extern char output_file_name[MAX_FILENAME];

int receive_database(bin_list_type *bin_list, dimension_size_type dimension_size, InputPortHandler input_port);

int calculate_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size);

int starts_first_phase(bin_list_type *bin_list, dimension_size_type dimension_size, InputPortHandler Bin_input, OutputPortHandler Bin_output);

int process_split_bin_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler Bin_output);

int assign_objects_to_new_bins(bin_list_pointer pointer_to_splitted_bin, bin_list_pointer **pointers_to_new_bins, dimension_size_type dimension_size);

int reassign_objects_to_bins(bin_list_pointer *pointers_to_bins, dimension_size_type dimension_size);

int process_new_iteration_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler port);

int process_concluded_bin_msg(bin_list_type *list, void *message, dimension_size_type dimension_size, OutputPortHandler port);

int prunning_objects_of_bin(bin_list_pointer bin, float prune_ratio);

#endif
