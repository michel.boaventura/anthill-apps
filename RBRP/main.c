#include "rbrp.h"

int main (int argc, char *argv[])
{	
	char *work;
	int work_size;
	
	char *input_file_name=NULL, *output_file_name=NULL;
	char *number_of_real_values=NULL;
	char *number_of_ordinal_values=NULL;
	char *number_of_categorical_values=NULL;
	char *number_of_outliers=NULL;
	char *number_of_neighbours=NULL;
	char *bin_size=NULL;

	Layout *console;
	char confFile[] = "./conf.xml";
	
	// check the number of arguments 
	if (argc != 17)
  {
  	printf ("Number of parameter incorrects: %d", argc);
		printf ("Usage: ./main -i <database_file> -o <output_file> -r <#real_values> -d <#ordinal_values> -c <#categorical_values> -n <#number of outliers> -k <#number_of_neighbours> -s <#bin_size>\n");
		printf("Example: \n./main -i ./IO/input.ascii.random.10000.7 -o ./OUT/outliers.random.10000.7 -r 7 -d 0 -c 0 -n 10 -k 4 -s 1500\n");
    exit(1);
  }

	int option;	
  // loop to receive information from the command line
  while ( (option = getopt (argc, argv, "i:o:r:d:c:n:k:s:")) != EOF ) 
	{
    switch (option) 
		{
			// input file
                        case 'i':
                                input_file_name = strdup(optarg);
			break;
			
			// output file 
                        case 'o':
                                output_file_name = strdup(optarg);
			break;
                        
			// number of real values
			case 'r':
                                number_of_real_values = strdup(optarg);
			break;
	
			// number of ordinal values
			case 'd':
                                number_of_ordinal_values = strdup(optarg);
			break;
	
			// number of categorical values
			case 'c':
                                number_of_categorical_values = strdup(optarg);
			break;
			
        		// number of outliers to be find
			case 'n':
                                number_of_outliers = strdup(optarg);
			break;
             	
			// number of neighbours
			case 'k':
                                number_of_neighbours = strdup(optarg);
			break;

			// bin size
			case 's':
                                bin_size = strdup(optarg);
			break;

			// error message
			default:
				printf ("\nError in the command line: \nOption found: %d -> %c", option, (char)option );
			        printf ("Usage: ./main -i <database_file> -o <output_file> -r <#real_values> -d <#ordinal_values> -c <#categorical_values> -n <#number of outliers -k <#number_of_neighbours> -s <#bin_size>\n");
				printf("Example: \n./main -i ./IO/input.ascii.random.10000.7 -o ./OUT/outliers.random.10000.7 -r 7 -d 0 -c 0 -n 10 -k 4 -s 1500\n");
				exit(1);
                }
        }

		
	if ( atoi(number_of_real_values) < 0 || atoi(number_of_ordinal_values) < 0 || atoi(number_of_categorical_values) < 0 || (atoi(number_of_real_values) + atoi(number_of_ordinal_values) + atoi(number_of_categorical_values)) <= 0 || atoi(number_of_outliers) <= 0 || atoi(number_of_neighbours) <= 0 || atoi(bin_size)<=0 )
        {
                printf ("\tERROR. The parameters passed are invalid.\n");
		printf("\tNumber of real values: %d\n", atoi(number_of_real_values));
		printf("\tNumber of ordinal values: %d\n", atoi(number_of_ordinal_values));
		printf("\tNumber of categorical values: %d\n", atoi(number_of_categorical_values));
		printf("\tNumber of outliers: %d\t Number of neighbours: %d\n", atoi(number_of_outliers), atoi(number_of_neighbours));
		printf("\tBin Size: %d\n", atoi(bin_size));
		exit(1);
        }

	// fills in the work with the all the parameters received

	/****************************************
	 * 	WORK			        *
	 *				        * 	
	 * 	1. input file name	        *
	 * 	2. output file name             *
	 * 	3. number of real values        *
	 * 	4. number of ordinal values     *
	 * 	5. number of categorical values *
	 * 	6. number of outliers           *
	 *	7. number of neighbours         *
	 ***************************************/	

	
	work_size = strlen(input_file_name) + strlen(output_file_name) + 
		+ strlen(number_of_real_values) + strlen(number_of_ordinal_values) + strlen(number_of_categorical_values)
		+ strlen(number_of_outliers)+ strlen(number_of_neighbours) + strlen(bin_size) + 8 /*extra space*/;
	
	work = (char *) malloc (work_size);
	sprintf(work, "%s,%s,%s,%s,%s,%s,%s,%s", input_file_name, output_file_name,
		number_of_real_values, number_of_ordinal_values, number_of_categorical_values, number_of_outliers, number_of_neighbours, bin_size);
	
	
	console = initAh(confFile, argc, argv); 
	appendWork(console, (void *)work, work_size);
	finalizeAh(console);
	
	return 0;
}
