#ifndef PROCEDURES_H
#define PROCEDURES_H

#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include "constants.h"
#include "structs.h"
#include "outliers_type.h"
#include "object.h"
#include "double_linked_list.h"
#include "dimension_values.h"
#include "nearest_neighbours.h"
#include "highest_scores.h"

extern unsigned long long NUMBER_OF_COMPARATIONS;
extern char output_file_name[MAX_FILENAME];

void open_database_file (FILE **input_file, char *input_file_name);
int check_attributes (char *line, int number_of_attributes);
int get_object_from_file(object_type *object, dimension_size_type dimension_size, FILE *input_file);
int search_for_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, object_type *object, list_type *list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours);
int check_for_probable_outlier(nearest_neighbours_type *nearest_neighbours, distance_type* highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours);
int *create_samples_neighbours(unsigned int number_of_objects);
int search_for_nearest_neighbours_bayesian(nearest_neighbours_type *nearest_neighbours, object_type *object, list_type *list, distance_type *highest_scores, int *object_samples, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours);
int print_int_array(FILE* file, int *array, unsigned int number_of_elements);
int print_unsigned_int_array(FILE* file, unsigned int *array, unsigned int number_of_elements);
int print_unsigned_long_long_array(FILE* file, unsigned long long *array, unsigned int number_of_elements);
int print_distance_type_array(FILE *file, distance_type *array, unsigned int number_of_elements);
void print_results(outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, dimension_size_type dimension_size, FILE *file);
int process_new_outlier(object_type *object, nearest_neighbours_type *aux_nearest_neighbours, outliers_type *outliers, nearest_neighbours_type *nearest_neighbours_outliers, dimension_size_type dimension_size, int number_of_outliers, int number_of_neighbours);
#ifdef GET_TIME
int print_time(struct timeval *start, struct timeval *end, char *name_of_file);
#endif

#endif
