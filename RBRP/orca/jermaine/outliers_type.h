#ifndef _OUTLIERS_TYPE_H
#define _OUTLIERS_TYPE_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
/* Prototypes */

void print_outliers(outliers_type *outliers, dimension_size_type dimension_size, FILE *file);
int insert_new_outlier(object_type *object, distance_type score, outliers_type *outliers, int *insert_position, unsigned int number_of_outliers);
int free_outliers(outliers_type outliers);
void malloc_outliers(outliers_type *outliers, unsigned int number_of_outliers);
int get_number_of_outliers_found(outliers_type *outliers);

#endif

