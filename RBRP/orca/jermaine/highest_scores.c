/* Inclusoes. */
#include "highest_scores.h"
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Gets the smallest score of the outliers found */
inline distance_type get_worst_highest_score(distance_type *highest_scores, unsigned int number_of_outliers)
{
	return (highest_scores[number_of_outliers-1]);
}

/* Gets the better score of the outliers found */
inline distance_type get_highest_score(distance_type *highest_scores)
{
	return (highest_scores[0]);
}

/* Save a new score on the Highest scores array */
int save_new_score(distance_type *highest_scores, distance_type score, unsigned int number_of_outliers)
{
	unsigned int position, i;
	
	/* Searches the position where the object have to be inserted */
	for (position=0; position<number_of_outliers; position++)
	{
		if (highest_scores[position]<score)
			break;
	}

	/* Tests if the score is a really new highest score */
	if (position<number_of_outliers)
	{
		/* Move forward the scores that is smaller than the current score to save the new score in the right position */  
		for (i=number_of_outliers-1; i>position; i--)	
		{
			highest_scores[i]=highest_scores[i-1];
		}

		/* Saves the new score on the highest scores array */
		highest_scores[position]=score;
	
		return 1;
	}

	return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* Prints the scores of the outliers found */
int print_highest_scores(distance_type *highest_scores, unsigned int number_of_outliers, FILE *file)
{
	fprintf(file, "HIGHEST SCORES:\t");
	print_distance_type_array(file, highest_scores, number_of_outliers);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
