#ifndef _OBJECT_H
#define _OBJECT_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
#include "attribute_type.h"


/* Prototypes */
void print_list(list_type *list, dimension_size_type dimension_size, FILE *file);
void free_cell(list_pointer q);

void print_object(object_type object, dimension_size_type dimension_size, FILE *fp_log);
int compare_objects(object_type object1, object_type object2);
void free_object(object_type object);
void malloc_object(object_type *object, dimension_size_type dimension_size);
distance_type distance_between_objects(object_type object1, object_type object2, dimension_size_type dimension_size);

#endif

