#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include "constants.h"
#include "structs.h"
#include "procedures.h"
#include "highest_scores.h"
#include "outliers_type.h"
#include "nearest_neighbours.h"
#include "object.h"
#include "double_linked_list.h"


#ifdef INSTRUMENTATION
unsigned long long NUMBER_OF_COMPARATIONS = 0;
#endif

#ifdef GET_TIME
struct timeval start_read_data;
struct timeval finish_read_data;
struct timeval start_sorting_method;
struct timeval finish_sorting_method;
struct timeval start_search_outliers;
struct timeval finish_search_outliers;
#endif

int main(int argc , char* argv[])
{

	char *input_file_name=NULL, *output_file_name=NULL;
	unsigned int number_of_real_values;
	unsigned int number_of_ordinal_values;
	unsigned int number_of_categorical_values;
	unsigned int number_of_outliers;
	unsigned int number_of_neighbours;

	dimension_size_type dimension_size;

		// check the number of arguments 
	if (argc != 15)
  {
  	printf ("Number of parameter incorrects: %d\n\n", argc);
		printf ("Usage: ./main -i <database_file> -o <output_file> -r <#real_values> -d <#ordinal_values> -c <#categorical_values> -n <#number of outliers> -k <#number_of_neighbours> \n");
		printf("Example: \n./main -i ./IO/input.ascii.random.10000.7 -o ./OUT/outliers.random.10000.7 -r 7 -d 0 -c 0 -n 10 -k 4\n");
    exit(1);
  }

	int option;	
  // loop to receive information from the command line
  while ( (option = getopt (argc, argv, "i:o:r:d:c:n:k:")) != EOF ) 
	{
    switch (option) 
		{
			// input file
      case 'i':
	  												    input_file_name = strdup(optarg);
			break;
			
			// output file 
      case 'o':
                                output_file_name = strdup(optarg);
			break;
                        
			// number of real values
			case 'r':
                                number_of_real_values = atoi(optarg);
			break;
	
			// number of ordinal values
			case 'd':
                                number_of_ordinal_values = atoi(optarg);
			break;
	
			// number of categorical values
			case 'c':
                                number_of_categorical_values = atoi(optarg);
			break;
			
        		// number of outliers to be find
			case 'n':
                                number_of_outliers = atoi(optarg);
			break;
             	
			// number of neighbours
			case 'k':
                                number_of_neighbours = atoi(optarg);
			break;

			// error message
			default:
				printf ("\nError in the command line: \nOption found: %d -> %c", option, (char)option );
			        printf ("Usage: ./main -i <database_file> -o <output_file> -r <#real_values> -d <#ordinal_values> -c <#categorical_values> -n <#number of outliers -k <#number_of_neighbours>\n");
				printf("Example: \n./main -i ./IO/input.ascii.random.10000.7 -o ./OUT/outliers.random.10000.7 -r 7 -d 0 -c 0 -n 10 -k 4\n");
				exit(1);
                }
        }

		
	if ( (number_of_real_values) < 0 || (number_of_ordinal_values) < 0 || (number_of_categorical_values) < 0 || ((number_of_real_values) + (number_of_ordinal_values) + (number_of_categorical_values)) <= 0 || (number_of_outliers) <= 0 ||(number_of_neighbours) <= 0)
        {
                printf ("\tERROR. The parameters passed are invalid.\n");
		printf("\tNumber of real values: %d\n",(number_of_real_values));
		printf("\tNumber of ordinal values: %d\n", (number_of_ordinal_values));
		printf("\tNumber of categorical values: %d\n", (number_of_categorical_values));
		printf("\tNumber of outliers: %d\t Number of neighbours: %d\n", (number_of_outliers), (number_of_neighbours));
		exit(1);
        }

	dimension_size.number_of_real_values = (number_of_real_values);
	dimension_size.number_of_categorical_values = (number_of_categorical_values);
	dimension_size.number_of_ordinal_values = (number_of_ordinal_values);

	//=================================================================================================================//
	// Read the data base
	// ================================================================================================================//
	
	#ifdef GET_TIME
	gettimeofday(&start_read_data, NULL);
	#endif

	FILE *input_file;

	FILE *fp_out;
	FILE *fp_inst;
	FILE *fp_score;
	
	char fp_out_name[300];
	char fp_inst_name[300];
	char fp_score_name[300];
	char fp_time_name[300];
	
	sprintf(fp_out_name, "%s.bin_0.out", output_file_name); 
	sprintf(fp_inst_name, "%s.bin_0.inst", output_file_name); 
	sprintf(fp_score_name, "%s.bin_0.score", output_file_name); 
	sprintf(fp_time_name, "%s.bin_0.time", output_file_name); 
	
	open_database_file(&input_file, input_file_name);
	fp_out = fopen(fp_out_name, "w");
	fp_inst = fopen(fp_inst_name, "w");
	fp_score = fopen(fp_score_name, "w");

	/*create the object list*/
	list_type object_list;
	create_empty_list(&object_list);

	object_type aux_object;
	
	while (get_object_from_file(&aux_object, dimension_size, input_file))
	{
		insert_in_list(aux_object, &object_list);
	}

	fclose(input_file);

	printf("\tNumber of real values: %d\n",(number_of_real_values));
	printf("\tNumber of ordinal values: %d\n", (number_of_ordinal_values));
	printf("\tNumber of categorical values: %d\n", (number_of_categorical_values));
	printf("\tNumber of outliers: %d\t Number of neighbours: %d\n", (number_of_outliers), (number_of_neighbours));


	//=================================================================================================================//
	// Bayesian ordered Method
	// ================================================================================================================//

	#ifdef GET_TIME
	gettimeofday(&finish_read_data, NULL);
	print_time(&start_read_data, &finish_read_data, fp_time_name);
	gettimeofday(&start_sorting_method, NULL);
	#endif	

	#ifndef SEED_TIME
	srand48(0);
	#else
	struct timeval timeseed;
	gettimeofday(&timeseed, NULL);
	srand48(timeseed.tv_usec * timeseed.tv_sec);
	#endif

	list_pointer current_object;
	distance_type *highest_scores_bayesian;
	highest_scores_bayesian=calloc(get_list_length(&object_list), sizeof(distance_type));	
	nearest_neighbours_type nearest_neighbours;
	nearest_neighbours_type *nearest_neighbours_bayesian;

	/* Creates the array that will keep the neighbours of the outliers */
	nearest_neighbours_bayesian=malloc(get_list_length(&object_list)*sizeof(nearest_neighbours_type));
	int *object_samples = create_samples_neighbours(get_list_length(&object_list));
	outliers_type list_of_outliers_bayesian;	
	
	double score = 0;
	int new_outlier_found = 0;

	/* Creates the list of the outliers initially empty */
	malloc_outliers(&list_of_outliers_bayesian, get_list_length(&object_list));
	print_int_array(fp_inst, object_samples, get_list_length(&object_list));

 	/* If there is no item on the list of objects, it returns 1 meaning that the object continue being an probable outlier */
	if(!get_first_in_list(&object_list, &current_object))
	{
		printf("The list is empty!\n");
		return 1; 
	}
	do
	{
		/* Allocates the structure to save the neighbours */
		malloc_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

		new_outlier_found = search_for_nearest_neighbours_bayesian(&nearest_neighbours, &(current_object->item), &object_list, highest_scores_bayesian, object_samples, dimension_size, get_list_length(&object_list), number_of_neighbours);

		if (new_outlier_found)	
		{
			/* Get the score of the outlier */
			score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

			/* Saves the new score on the highest scores array */
			save_new_score(highest_scores_bayesian, score,  get_list_length(&object_list));

			process_new_outlier(&(current_object->item), &nearest_neighbours, &list_of_outliers_bayesian, nearest_neighbours_bayesian, dimension_size,  get_list_length(&object_list), number_of_neighbours);
		}
		//free_nearest_neighbours(nearest_neighbours);
		printf("estimating score for object number : %u, score : %lf\n", current_object->item.number,score);

		go_next(&object_list, &current_object);
	}   
	while (!is_last(current_object, &object_list));
  
	/* If the last item of the list was not cheched yet, we will do it */
	if (get_list_length(&object_list)>1)
	{
			/* Allocates the structure to save the neighbours */
			malloc_nearest_neighbours(&nearest_neighbours, number_of_neighbours);
	
			new_outlier_found = search_for_nearest_neighbours_bayesian(&nearest_neighbours, &(current_object->item), &object_list, highest_scores_bayesian, object_samples, dimension_size, get_list_length(&object_list), number_of_neighbours);

		if (new_outlier_found)	
		{
			/* Get the score of the outlier */
			score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

			/* Saves the new score on the highest scores array */
			save_new_score(highest_scores_bayesian, score, number_of_outliers);

			process_new_outlier(&(current_object->item), &nearest_neighbours, &list_of_outliers_bayesian, nearest_neighbours_bayesian, dimension_size,  get_list_length(&object_list), number_of_neighbours);
		}
		//free_nearest_neighbours(nearest_neighbours);
		printf("estimating score for object number : %u, score : %lf\n", current_object->item.number,score);
	}


	//=================================================================================================================//
	// Search for outliers
	// ================================================================================================================//


	#ifdef GET_TIME
	gettimeofday(&finish_sorting_method, NULL);
	print_time(&start_sorting_method, &finish_sorting_method, fp_time_name);
	gettimeofday(&start_search_outliers, NULL);
	#endif	

	
	fprintf(fp_score,"#object #score\n");

//	list_pointer current_object; /*declared in top*/
	distance_type *highest_scores;
	highest_scores=calloc(number_of_outliers, sizeof(distance_type));	
//	nearest_neighbours_type nearest_neighbours; /*declared in top*/
	nearest_neighbours_type *nearest_neighbours_outliers;

	/* Creates the array that will keep the neighbours of the outliers */
	nearest_neighbours_outliers=malloc(number_of_outliers*sizeof(nearest_neighbours_type));
	outliers_type list_of_outliers;	
	
//	double score = 0; /*declared in top*/
//	int new_outlier_found = 0; /*declared in top*/

	/* Creates the list of the outliers initially empty */
	malloc_outliers(&list_of_outliers, number_of_outliers);

 	/* If there is no item on the list of objects, it returns 1 meaning that the object continue being an probable outlier */
	if(!get_first_in_list(&(list_of_outliers_bayesian.objects), &current_object))
	{
		printf("The list is empty!\n");
		return 1; 
	}

	unsigned int counter = 1;

	do
	{
		/* Allocates the structure to save the neighbours */
		malloc_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

		new_outlier_found = search_for_nearest_neighbours(&nearest_neighbours, &(current_object->item), &(list_of_outliers_bayesian.objects), highest_scores, dimension_size, number_of_outliers,number_of_neighbours);

		if (new_outlier_found)	
		{
			/* Get the score of the outlier */
			score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

			/* Saves the new score on the highest scores array */
			save_new_score(highest_scores, score, number_of_outliers);

			printf("new outlier found!\n number outlier : %d, score : %f\n", current_object->item.number,score);
			fprintf(fp_score,"%u %lf\n",  counter, get_worst_highest_score(highest_scores, number_of_outliers));
			process_new_outlier(&(current_object->item), &nearest_neighbours, &list_of_outliers, nearest_neighbours_outliers, dimension_size, number_of_outliers, number_of_neighbours);
		}
		//free_nearest_neighbours(nearest_neighbours);
	
		go_next(&(list_of_outliers_bayesian.objects), &current_object);
		counter++;
	}   
	while (!is_last(current_object, &(list_of_outliers_bayesian.objects)));
  
	/* If the last item of the list was not cheched yet, we will do it */
	if (get_list_length(&(list_of_outliers_bayesian.objects))>1)
	{
		//	printf("check if this object is an outlier! number object : %d \n", current_object->item.number);

			/* Allocates the structure to save the neighbours */
			malloc_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

			new_outlier_found = search_for_nearest_neighbours(&nearest_neighbours, &(current_object->item), &(list_of_outliers_bayesian.objects), highest_scores, dimension_size, number_of_outliers, number_of_neighbours);
		if (new_outlier_found)	
		{
			/* Get the score of the outlier */
			score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

			/* Saves the new score on the highest scores array */
			save_new_score(highest_scores, score, number_of_outliers);

			printf("new outlier found!\n number outlier : %u, score : %lf\n", current_object->item.number,score);
			fprintf(fp_score,"%u %lf\n",  counter, get_worst_highest_score(highest_scores, number_of_outliers));


			process_new_outlier(&(current_object->item), &nearest_neighbours, &list_of_outliers, nearest_neighbours_outliers, dimension_size, number_of_outliers, number_of_neighbours);
		}
		//free_nearest_neighbours(nearest_neighbours);
	}


	fprintf(fp_inst, "%llu", NUMBER_OF_COMPARATIONS);
	print_results(&(list_of_outliers), nearest_neighbours_outliers, number_of_neighbours, dimension_size, fp_out);

	fflush(fp_score);
	fflush(fp_out);
	fflush(fp_inst);

	fclose(fp_inst);
	fclose(fp_score);
	fclose(fp_out);

	#ifdef GET_TIME
	gettimeofday(&finish_search_outliers, NULL);
	print_time(&start_search_outliers, &finish_search_outliers, fp_time_name);
	#endif

	return 1;
}

