#include "procedures.h"

//! This function is responsible to open the database and checks if any errors occured.
void open_database_file (FILE **input_file, char *input_file_name)
{
	
	// opens the file and checks if any errors occured
	if ((*input_file = fopen (input_file_name, "r")) == NULL)
	{
		printf("ERROR. Could not open the file: %s !!!\n", input_file_name);
		exit(1);
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/*! This function is responsible to check if the number of attributes of the 
 * current object matches the number of attributes of the database */
int check_attributes (char *line, int number_of_attributes)
{
	char *temp_string; 				// auxiliary to store temporarily a string of the line
	char *temp_line;			// auxiliary to store temporarily the line
	int temp_num_attributes = 0;			// auxiliary to store the number of attributes

	int max_line_len=((MAX_VALUE_LEN+2)*number_of_attributes+1);
	temp_line=malloc(max_line_len*sizeof(char));
	
	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	while (temp_string != NULL)
	{
		temp_num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}

	// an empty line was found
	if (temp_num_attributes == 0)
	{
		return EMPTY_LINE;
	}
	else
	{
		// gives an error message if the number of attributes is different 
		// from the number of samples
		if ( (temp_num_attributes != number_of_attributes) && (temp_num_attributes != 0) )
		return 0;
	}

	free(temp_line);
	return temp_num_attributes;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

//! Gets the values of a tuple and keeps it on the structure object
int get_object_from_file(object_type *object, dimension_size_type dimension_size, FILE *input_file)
{
	unsigned int i;
	int check;
	char *file_line, *temp_string;

	int num_attributes = 0;			// auxiliary to store the number of attributes

	/* The number of columns is equal ID column ID add to the number of total attributes */
	int num_columns = dimension_size.number_of_real_values+dimension_size.number_of_ordinal_values+dimension_size.number_of_categorical_values+1;
	int max_line_len=(MAX_VALUE_LEN+2)*num_columns;
	
	/* Allocate the memory of an line with an object */
	file_line=malloc(max_line_len*sizeof(char));

	/* read the file until gets a new line with an object or find the end of file */
	do
	{
		/* If was not possible to read the file, e.g end of the file, returns 0 */
		if (fgets(file_line, max_line_len-1, input_file)==NULL)
			return 0;
		
		/* Check if there are on the file the ID and the right number of dimensions */
		check = check_attributes(file_line, num_columns);
	}
	while (check==EMPTY_LINE);
	if (!check)
	{
		printf("ERROR. Inconsistent number of attributes.\n");
		exit(1);
	}
	
	// starts reading the line getting this ID
	temp_string = (char *) strtok(file_line, SEPARATORS);

	// Get the ID of the object
	(*object).number=atoi(temp_string);

	/* Allocate the structure to keep the values of the object */
	malloc_object(object, dimension_size);

	// starts reading the dimension values from the line
	temp_string = (char *) strtok(NULL, SEPARATORS);
	
	// Getting the real dimension values */
	for (i=0; i<dimension_size.number_of_real_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.real_values[i]=get_real_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;
	
	// Getting the ordinal dimension values */
	for (i=0; i<dimension_size.number_of_ordinal_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.ordinal_values[i]=get_ordinal_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;

	// Getting the categorical dimension values */
	for (i=0; i<dimension_size.number_of_categorical_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.categorical_values[i]=get_categorical_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;

	/* Free the memory previously allocated for the line */
	free(file_line);
	return (num_attributes==num_columns-1);
}


/* Search for nearest neighbours of the object in the whole bin on the list */
int search_for_nearest_neighbours(nearest_neighbours_type *nearest_neighbours, object_type *object, list_type *list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	list_pointer current_object;
	distance_type distance;
	int object_number, new_nearest_neighbour;

	/* If there is no item on the list of objects, it returns 1 meaning that the object continue being an probable outlier */
	if(!get_first_in_list(list, &current_object))
	{
		printf("The list is empty!\n");
		exit(1); 
	}
		/* While there is an next object, keep searching */
	do
	{
		/* Test if the current object on the list is not the object itself. Just in this case the current object can be a nearest neighbour.*/
		if (!compare_objects(current_object->item, *object))
		{
			/* Get the distance between the objects */
			distance=distance_between_objects(current_object->item, *object, dimension_size);
			//printf("distance object %d to %d : %f\n", current_object->item.number, object->number, distance);

			#ifdef INSTRUMENTATION
			/*Incremental the counter of comparations*/
			NUMBER_OF_COMPARATIONS++;
			#endif
			object_number=current_object->item.number;

			/* Insert in the nearest neighbours array. If it is not a nearest neighbour it just returns 0 */
			new_nearest_neighbour=insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);
			/* If a new nearest neighbour was found, check if the object keep being a probable outlier */
			if (new_nearest_neighbour)
			{
				/* Checks if the object keep being a probable outlier */
				/* If it can not be an outlier, we can stop searching */
				if (!check_for_probable_outlier(nearest_neighbours, highest_scores, number_of_outliers, number_of_neighbours))
				{
//					printf("this object number %d isn't an outlier, score %f\n ", object->number, nearest_neighbours->distance[number_of_neighbours-1]);
					return 0;
				}	
			}
		}
		go_next(list, &current_object);
	}   
	while (!is_last(current_object, list));
  
	/* If the last item of the list was not cheched yet, we will do it */
	if (get_list_length(list)>1)
	{
		/* Test if the last object on the list is a nearest neighbour.*/
		if (!compare_objects(current_object->item, *object))
		{
			/* Get the distance between the objects */
			distance=distance_between_objects( current_object->item,*object, dimension_size);
			
			#ifdef INSTRUMENTATION
			/*Incremental the counter of comparations*/
			NUMBER_OF_COMPARATIONS++;	
			#endif

			object_number=current_object->item.number;

			/* Insert in the nearest neighbours array. If it is not a nearest neighbour it just returns 0 */
			new_nearest_neighbour=insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);

			/* If a new nearest neighbour was found, check if the object keep being a probable outlier */
			if (new_nearest_neighbour)
			{
				/* Checks if the object keep being a probable outlier */
				/* If it can not be an outlier, we can stop searching */
				if (!check_for_probable_outlier(nearest_neighbours, highest_scores, number_of_outliers, number_of_neighbours))
				{
//					printf("this object number %d isn't an outlier, score %f\n ", object->number, nearest_neighbours->distance[number_of_neighbours-1]);
					return 0;
				}	
			}
		}
	}
	return 1;
}

int *create_samples_neighbours(unsigned int number_of_objects)
{
	int *object_samples = malloc(sizeof(int)*NUM_SAMPLES);
	int neighbor_number_sample, i, k;

	if (NUM_SAMPLES > number_of_objects)
	{
		printf("The number of sample is higher than list list length!!\n");
		exit(1); 
	}

	/* Select the first sample of neighbour*/
	neighbor_number_sample = object_samples[0] = rand()%number_of_objects;
	printf("new sample: %d\n", neighbor_number_sample);

	/* Select numbers of samples of neighbour*/
	int new_sample;
	for (i = 1; i < NUM_SAMPLES; i++) 
	{
		new_sample = i;
		neighbor_number_sample = rand()%number_of_objects;
		for (k = 0; k < i; k++) 
		{
			if (object_samples[k]==neighbor_number_sample)
			{
				i--;
				break;
			}
		}
		if (i==new_sample) 
		{
			object_samples[i] = neighbor_number_sample;
			printf("new sample: %d\n", neighbor_number_sample);
			new_sample = 0;
		}	
	}
	return object_samples;
}

/* Search for nearest neighbours of the object using the bayesian method */
int search_for_nearest_neighbours_bayesian(nearest_neighbours_type *nearest_neighbours, object_type *object, list_type *list, distance_type *highest_scores, int *object_samples, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	list_pointer current_object;
	distance_type distance;
	int object_number, new_nearest_neighbour, i;

	/* If there is no item on the list of objects, it returns 1 meaning that the object continue being an probable outlier */
	if(!get_first_in_list(list, &current_object))
	{
		printf("The list is empty!\n");
		exit(1); 
	}

	if (NUM_SAMPLES > get_list_length(list))
	{
		printf("The number of sample is higher than list list length!!\n");
		exit(1); 
	}


	for (i = 0; i < NUM_SAMPLES; i++) 
	{
		go_item_on_position(list, &current_object, object_samples[i]);

		/* Get the distance between the objects */
		distance=distance_between_objects(current_object->item, *object, dimension_size);
	
		object_number=current_object->item.number;

		/* Insert in the nearest neighbours array. If it is not a nearest neighbour it just returns 0 */
		new_nearest_neighbour=insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);
	}
	return 1;
}


/* Checks if the neighbours found for one object could not garantee that this object is not an outier */
int check_for_probable_outlier(nearest_neighbours_type *nearest_neighbours, distance_type* highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	int is_probable_outlier;
	
	/* Get the smaller score of an actual outlier */
	distance_type worst_highest_score=get_worst_highest_score(highest_scores, number_of_outliers);

	/* Check if the worst highest score (minimum distance of the less outlier of the outliers) would be a nearest neighbour on the nearest neighbours array. If it is true, the object yet is a probable outlier */
	is_probable_outlier=check_distance_for_nearest_neighbours(worst_highest_score, nearest_neighbours, number_of_neighbours);
	return is_probable_outlier;
}

/** Print all the elements of an array of ints on a file.
 * @param array the array of integers that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 * @param file Prints the array on this file
 */
int print_int_array(FILE* file, int *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "INT ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%d ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/** Print all the elements of an array of unsigned ints on a file.
 * @param file Prints the array on this file
 * @param array the array of unsigned integers that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 */
int print_unsigned_int_array(FILE* file, unsigned int *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "UNSIGNED INT ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%u ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Print all the elements of an array of unsigned longs on a file.
 * @param array the array of longs that will be printed
 * @param number_of_elements the number of elements in the array that will be printed
 * @param file Prints the array on this file
 */
int print_unsigned_long_long_array(FILE* file, unsigned long long *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "LONG LONG ARRAY NULL!!!\n");
		return 0;
	}

	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; i < number_of_elements; i++)
	{
		fprintf(file, "%llu ", array[i]);
	}
	fprintf(file, "\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** 
 * Print all the elements of an array of distance_type on a file.
 * @param file Prints the array on this file
 * @param array The array of distances that will be printed.
 * @param number_of_elements the number of elements in the array that will be printed.
 */
int print_distance_type_array(FILE *file, distance_type *array, unsigned int number_of_elements)
{
	unsigned int i;

	if (array==NULL) 
	{
		fprintf(file, "DISTANCE ARRAY NULL!!!\n");
		return 0;
	}
		
	/* Prints the array until it finds an empty position or the whole array got run*/
	for(i=0; (i < number_of_elements); i++)
	{
		fprintf(file, DISTANCE_CONVERSION_STRING, array[i]);
		fprintf(file, " ");
	}
	fprintf(file, "\n");
	
	return 1;
}
#ifdef GET_TIME

/**
 * Print the time beetween the start and the end of the execution
 */
int print_time(struct timeval *start, struct timeval *end, char *name_of_file)
{
	FILE *file;		// file for time output
	double execution_time;	// gets the execution time

	// opens the file
	file = fopen(name_of_file,"a");

	// calculates the execution time in seconds
	execution_time  = ( end->tv_sec - start->tv_sec )*1000000;
	execution_time += end->tv_usec - start->tv_usec;
	execution_time /= 1000000;

	// print the time
	fprintf(file, "%.3lf\n", execution_time);

	// closes the file
	fclose(file);

	return 1;
}

#endif

/** Receives an object candidate to be outlier and process this message.
 * It will verify if the object really is an actual outlier and if it is an outlier save it */

int process_new_outlier(object_type *object, nearest_neighbours_type *aux_nearest_neighbours, outliers_type *outliers, nearest_neighbours_type *nearest_neighbours_outliers, dimension_size_type dimension_size, int number_of_outliers, int number_of_neighbours)
{
	nearest_neighbours_type temp;
	int i, inserted_outlier, number_of_outliers_found, outlier_position;
	distance_type score;

	number_of_outliers_found=get_number_of_outliers_found(outliers);
	
	/* Get the score of the outlier */
	score = get_minimum_distance_in_nearest_neighbours(aux_nearest_neighbours, number_of_neighbours);

	inserted_outlier = insert_new_outlier(object, score, outliers, &outlier_position, number_of_outliers);

	/* If really a new outlier was found, we will save the nearest neighbours of this outlier */
	if (inserted_outlier)
	{
		temp=nearest_neighbours_outliers[number_of_outliers-1];
			
		/* Move forward the neighbours to insert the new neighbours on the rigth position */  
		for (i=number_of_outliers-1; i>outlier_position; i--)	
		{
			nearest_neighbours_outliers[i]=nearest_neighbours_outliers[i-1];
		}

		/* Saves the new nearest neighbours on the nearest neighbours array */
		nearest_neighbours_outliers[outlier_position]=(*aux_nearest_neighbours);

		/* Tests if there is a nearest neighbour that was removed from the list */
		if (number_of_outliers_found == number_of_outliers)
		{
			free_nearest_neighbours(temp);
		}
	}
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Print all the outliers of the list  of outliers and the Nearest Neighbours of the Outliers */
void print_results(outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, dimension_size_type dimension_size, FILE *file)
{
	list_pointer aux;

	int outlier_number=0;

	list_type *list=&(outliers->objects);
	
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_list(list, &aux))
	{
		fprintf(file, "There are no Outliers!!!\n");	   
		return;
	}
   
	/* Prints the First Object of the list */
	print_object(aux->item, dimension_size, file);
	fprintf(file, "Score: ");
	fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
	fprintf(file, "\n");
	
	print_nearest_neighbours(&(nearest_neighbours[outlier_number]), aux->item.number, number_of_neighbours, file);
	fprintf(file, "---\t---\t---\t---\t---\t---\t---\n");
	outlier_number++;
	
	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		
		/* Prints the First Object of the list */
		print_object(aux->item, dimension_size, file);
		fprintf(file, "Score: ");
		fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
		fprintf(file, "\n");
		print_nearest_neighbours(&(nearest_neighbours[outlier_number]), aux->item.number, number_of_neighbours, file);
		fprintf(file, "---\t---\t---\t---\t---\t---\t---\n");
		outlier_number++;
	}
}

