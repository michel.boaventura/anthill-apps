#ifndef _HIGHEST_SCORES_H
#define _HIGHEST_SCORES_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"

/* Prototypes */
inline distance_type get_worst_highest_score(distance_type *highest_scores, unsigned int number_of_outliers);
inline distance_type get_highest_score(distance_type *highest_scores);
int save_new_score(distance_type *highest_scores, distance_type score, unsigned int number_of_outliers);
int print_highest_scores(distance_type *highest_scores, unsigned int number_of_outliers, FILE *file);

#endif

