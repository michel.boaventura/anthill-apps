#ifndef _DIMENSION_VALUES_H
#define _DIMENSION_VALUES_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
#include "math.h"

/* Prototypes */
void print_dimension_values(dimension_values_type dimension_values, dimension_size_type dimension_size, FILE *fp_log);
void copy_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size);
void get_greater_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size);
void get_smaller_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size);
int compare_dimension_values(dimension_values_type dimension_values1, dimension_values_type dimension_values2, dimension_size_type dimension_size);
void *get_dimension_values_from_msg(dimension_values_type *dest, void *msg_src, dimension_size_type dimension_size);
void *fill_dimension_values_on_msg(void *msg_dest, dimension_values_type *src, dimension_size_type dimension_size);
void free_dimension_values(dimension_values_type dimension_values);
void malloc_dimension_values(dimension_values_type *dimension_values, dimension_size_type dimension_size);
int get_dimension_values_message_size(dimension_size_type dimension_size);
int get_local_dimension_values_message_size(dimension_size_type dimension_size);
#endif

