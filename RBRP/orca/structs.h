#ifndef STRUCTS_H
#define STRUCTS_H
/*! \file structs.h
    \brief Definition of all structures of the RBRP algorithm
    
    In this file are defined all the structures manipulated on the RBRP algorithm.
*/

//! Real types of each minerable attribute type
typedef double real;
typedef double ordinal;
typedef int categorical;

//! Real type of the distance type
typedef double distance_type;


//! Type that defines the Bin number
typedef unsigned long long bin_number_type;

//! structure responsible for storing the dimension values
typedef struct
{
	real *real_values;			/*!< Attributes with Real Values */  
	ordinal *ordinal_values;		/*!< Attributes with Ordinal Values */
	categorical *categorical_values;	/*!< Attributes with Categorical Values */
} dimension_values_type;

//! structure responsible for storing the number of values of each type of dimension values 
typedef struct
{
	unsigned int number_of_real_values;		/*!< Number of Attributes with Real Values */  
	unsigned int number_of_ordinal_values;		/*!< Number of Attributes with Ordinal Values */
	unsigned int number_of_categorical_values;	/*!< Number of Attributes with Categorical Values */
} dimension_size_type;

//! structure responsible for storing the closest neighbours and the distance of this bins
typedef struct
{
	int size;
	unsigned int *objects;		/*!< The number of the nearest objects  */
	distance_type *distance;		/*!< The distance of the nearest objects */
} nearest_neighbours_type;

/* Definitions about the structure of an object and the lists of object */
//! structure responsible for storing one object
typedef struct
{
	unsigned int number;			/*!< Object Number */
	dimension_values_type dimension_values;	/*!< dimension values */

} object_type;

typedef struct cell
{
   object_type item;
   struct cell *previous;
   struct cell *next;
} cell, *list_pointer;

typedef struct
{
   list_pointer first;
   list_pointer last;
   unsigned int length;
} list_type;

typedef struct
{
	list_type objects;			/*!< List of objects of the outliers */
	distance_type *highest_scores;			/*!< Array with the scores of the outliers */
} outliers_type;

#endif
