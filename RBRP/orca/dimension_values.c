/* Inclusoes. */
#include "dimension_values.h"

/** Print the dimension values */
void print_dimension_values(dimension_values_type dimension_values, dimension_size_type dimension_size, FILE *fp_log)
{
	fprintf(fp_log, "Dimension Values: ");

	/* Checks if there is any value */
	if (dimension_values.real_values!=NULL || dimension_values.ordinal_values!=NULL || dimension_values.categorical_values!=NULL)
	{
		fprintf(fp_log, "\n\tReal: ");
		if (dimension_values.real_values!=NULL)
			print_real_array(fp_log, dimension_values.real_values, dimension_size.number_of_real_values);
		else fprintf(fp_log, "There aren't Real Values!!!\n");
	
		fprintf(fp_log, "\tOrdinal: ");
		if (dimension_values.ordinal_values!=NULL)
			print_ordinal_array(fp_log, dimension_values.ordinal_values, dimension_size.number_of_ordinal_values);
		else fprintf(fp_log, "There aren't Ordinal Values!!!\n");
		
		fprintf(fp_log, "\tCategorical: ");
		if (dimension_values.categorical_values!=NULL)
			print_categorical_array(fp_log, dimension_values.categorical_values, dimension_size.number_of_categorical_values);
		else fprintf(fp_log, "There aren't Categorical Values!!!\n");
	}
	else 
		fprintf(fp_log, "There aren't Dimension Values!!!\n");
}

/** Copy the dimension values from the source to the destination dimension values */
void copy_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size)
{
	unsigned int i;

	/* Copy the real values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		dest->real_values[i]=src->real_values[i];
	}
	
	/* Copy the ordinal values */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		dest->ordinal_values[i]=src->ordinal_values[i];
	}

	/* Copy the categorical values */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		dest->categorical_values[i]=src->categorical_values[i];
	}
}

/** Add two dimension values and save then on the destination dimension values */
void add_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size)
{
	unsigned int i;

	/* Copy the real values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		dest->real_values[i]+=src->real_values[i];
	}
	
	/* Copy the ordinal values */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		dest->ordinal_values[i]=src->ordinal_values[i];
	}

	/* Copy the categorical values */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		dest->categorical_values[i]=src->categorical_values[i];
	}
}

/** Compares the destination values and the source values dimension, updating the destination values with the greater values of the two dimension values.
 * The result are the greater dimension values of source and destination, saved on the destination dimension values */
void get_greater_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size)
{
	unsigned int i;

	/* Copy the real values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		if (src->real_values[i]>dest->real_values[i])
			dest->real_values[i]=src->real_values[i];
	}
	
	/* Copy the ordinal values */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		if (src->ordinal_values[i]>dest->ordinal_values[i])
			dest->ordinal_values[i]=src->ordinal_values[i];
	}

	/* Copy the categorical values */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		if (src->categorical_values[i]>dest->categorical_values[i])
			dest->categorical_values[i]=src->categorical_values[i];
	}
}

/** Compares the destination values and the source values dimension, updating the destination values with the smaller values of the two dimension values.
 * The result are the smaller dimension values of source and destination, saved on the destination dimension values */
void get_smaller_dimension_values(dimension_values_type *dest, dimension_values_type *src, dimension_size_type dimension_size)
{
	unsigned int i;

	/* Copy the real values */
	for (i=0; i<dimension_size.number_of_real_values; i++)
	{
		if (src->real_values[i]<dest->real_values[i])
			dest->real_values[i]=src->real_values[i];
	}
	
	/* Copy the ordinal values */
	for (i=0; i<dimension_size.number_of_ordinal_values; i++)
	{
		if (src->ordinal_values[i]<dest->ordinal_values[i])
			dest->ordinal_values[i]=src->ordinal_values[i];
	}

	/* Copy the categorical values */
	for (i=0; i<dimension_size.number_of_categorical_values; i++)
	{
		if (src->categorical_values[i]<dest->categorical_values[i])
			dest->categorical_values[i]=src->categorical_values[i];
	}
}

/** Compare if two dimension values have all identical values */
int compare_dimension_values(dimension_values_type dimension_values1, dimension_values_type dimension_values2, dimension_size_type dimension_size)
{
	return (calculate_distance_between_dimension_values(dimension_values1, dimension_values2, dimension_size)==0);
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/** Frees the memory allocated for the dimension values */ 
void free_dimension_values(dimension_values_type dimension_values)
{
	if (dimension_values.real_values!=NULL)
	{
		free(dimension_values.real_values);
		dimension_values.real_values=NULL;
	}
	if (dimension_values.ordinal_values!=NULL)
	{
		free(dimension_values.ordinal_values);
		dimension_values.real_values=NULL;
	}
	if (dimension_values.categorical_values!=NULL)
	{
		free(dimension_values.categorical_values);
		dimension_values.real_values=NULL;
	}
}

/** Allocates the memory for the dimension_values */
void malloc_dimension_values(dimension_values_type *dimension_values, dimension_size_type dimension_size)
{
	if (dimension_size.number_of_real_values>0)
		(*dimension_values).real_values=malloc(dimension_size.number_of_real_values*sizeof(real));
	else
		(*dimension_values).real_values=NULL;
	
	if (dimension_size.number_of_ordinal_values>0)
		(*dimension_values).ordinal_values=malloc(dimension_size.number_of_ordinal_values*sizeof(ordinal));
	else
		(*dimension_values).ordinal_values=NULL;
	
	if (dimension_size.number_of_categorical_values>0)
		(*dimension_values).categorical_values=malloc(dimension_size.number_of_categorical_values*sizeof(categorical));
	else
		(*dimension_values).categorical_values=NULL;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

