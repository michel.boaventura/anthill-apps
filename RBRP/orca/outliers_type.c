/* Inclusoes. */
#include "outliers_type.h"

/* Print all the outliers of the list */
void print_outliers(outliers_type *outliers, dimension_size_type dimension_size, FILE *file)
{
	list_pointer aux;

	int outlier_number=0;

	list_type *list=&(outliers->objects);
	
	/* If the first item is not found, it prints that the list is empty*/
	if (!get_first_in_list(list, &aux))
	{
		fprintf(file, "There are no Outliers!!!\n");	   
		return;
	}
   
	/* Prints the First Object of the list */
	print_object(aux->item, dimension_size, file);
	fprintf(file, "Score: ");
	fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
	fprintf(file, "\n---\t---\t---\t---\t---\t---\t---\n");
	outlier_number++;
	
	while (!is_last(aux, list))
	{
		go_next(list, &aux);
		
		/* Prints the First Object of the list */
		print_object(aux->item, dimension_size, file);
		fprintf(file, "Score: ");
		fprintf(file, DISTANCE_CONVERSION_STRING, (outliers->highest_scores)[outlier_number]);
		fprintf(file, "\n---\t---\t---\t---\t---\t---\t---\n");
		outlier_number++;
	}
}

/* Inserts the object as an outlier on the list of outliers. This functions also returns if the object is really an outlier or not */
int insert_new_outlier(object_type *object, distance_type score, outliers_type *outliers, int *insert_position, unsigned int number_of_outliers)
{
	list_pointer current_object;

	list_type *list=&(outliers->objects);
	distance_type *highest_scores=outliers->highest_scores;

	unsigned int number_of_outliers_already_found;

	unsigned int position, i;

	object_type aux;

	/* Searches the position where the object have to be inserted */
	for (position=0; position<number_of_outliers; position++)
	{
		if (highest_scores[position]<score)
			break;
	}

	/* Saves the position where the outlier will be inserted */
	*insert_position=position;

	/* Tests if the score is a really a new outlier */
	if (position<number_of_outliers)
	{

		/* Move forward the scores that is smaller than the current score to save the new score in the right position */  
		for (i=number_of_outliers-1; i>position; i--)	
		{
			highest_scores[i]=highest_scores[i-1];
		}

		/* Saves the new score on the highest scores array */
		highest_scores[position]=score;

		number_of_outliers_already_found=get_list_length(list);

	        /* Tests if the outlier have to be inserted on the last position of the list */
		if (number_of_outliers_already_found==0 || (number_of_outliers_already_found==position && number_of_outliers_already_found<number_of_outliers))
		{
			insert_in_list(*object, list);
		}
		else
		{
			get_first_in_list(list, &current_object);
	
			/* Runs the list of outliers until the right position of the outlier */
			for (i=0; i<position; i++)	
			{	
				go_next(list, &current_object);
			}

			insert_in_list_on_position(*object, &current_object, list);
		
			/* Tests if an outlier have to be removed of the list */
			if (get_list_length(list) > number_of_outliers)
			{
				/* Remove the last outlier of the list */
				get_last_in_list(list, &current_object);
				aux=remove_of_list(&current_object, list);
			}
		}
		return 1;
	}
	return 0;
}

/* Free memory of the structures of the outliers list */
int free_outliers(outliers_type outliers)
{
	/* Frees the memory allocated for the list of the objects */ 
	make_list_empty(&((outliers).objects));
	free(outliers.objects.first);

	/* If necessary, frees the memory allocated for the highest scores */
	if (outliers.highest_scores!=NULL)
	{
		free(outliers.highest_scores);
	}
	return 1;
}


/* Allocates and initializes the structures of outliers list */
void malloc_outliers(outliers_type *outliers, unsigned int number_of_outliers)
{
	/* Allocates the scores of the outliers */ 
	(*outliers).highest_scores=calloc(number_of_outliers, sizeof(distance_type));
	
	/* Creates the list of the object empty */
	create_empty_list(&((*outliers).objects));
}

/* Return the number of outliers found */
int get_number_of_outliers_found(outliers_type *outliers)
{
	return (get_list_length(&(outliers->objects)));
}
