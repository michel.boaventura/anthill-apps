#ifndef CONSTANTS_H
#define CONSTANTS_H
//! The maximum length of the name of one file
#define MAX_FILENAME 100

//! The maximum length of a value of an dimension in number of characters

/*!
 * The maximum length of a line on the database is equal to ((MAX_VALUE_LEN+2)*number_of_dimensions+1)
 * 
 * Example: 
 * If in the database there is the line:
 * 1.034, 2.134, 3.489, 4.249, 7.578, 1.000, 1.250, 0.500, 3, 4, 7
 * Then the MAX_VALUE_LEN can be just 5 (the number of chars of the values does not exceed 5)
 */
#define MAX_VALUE_LEN 30  	

//! The constant representing and EMPTY LINE 
#define EMPTY_LINE -3

//! Define the maximum value for real attributes. It is useful for balancing the weight on distance of real values cause it can saturate the difference between two real values
#define MAX_REAL_VALUE 2

//! Define the separators of the values on the database.
#define SEPARATORS ",\n"

#define INSTRUMENTATION

#define GET_TIME

#define NUM_SAMPLES 500

#define SEED_TIME

#endif
