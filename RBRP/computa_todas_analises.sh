#!/bin/bash

## argumento1: Nome do diretorio onde est�oo os arquivos de instrumentacao dos bins
## argumento2: Nome do arquivo da base de dados
## argumento3: Nome do arquivo .dat
test -e

if [ $# != 3 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "./analisa_bins.sh diretorio arquivo_de_entrada arquivo_de_saida" 
        	exit 1
	fi

	DIRETORIO=$1
	ARQUIVO_ENTRADA=$2
	ARQUIVO_SAIDA=$3
	INSTANCIAS='12 10 8 6 4 2 1'

	#echo "cd ${DIRETORIO}"	
	#cd ${DIRETORIO}

echo "Procurando por diret�rios com o nome *.inst para obten��o dos dados..."

# Gera as todas as analises
for NUM_INSTANCIAS in $INSTANCIAS
do
	echo "Processando diretorio: ${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.inst"
	if [ -d ${DIRETORIO} ];
	then	
		echo "Gerando resultados para o numero de instancia ${NUM_INSTANCIAS}" 
		echo "./analise_bins.sh ${DIRETORIO} . ${ARQUIVO_ENTRADA} ${NUM_INSTANCIAS}"
		./analisa_bins.sh ${DIRETORIO} . ${ARQUIVO_ENTRADA} ${NUM_INSTANCIAS}
	fi
done


echo "REMOVENDO ${ARQUIVO_SAIDA}.dat"
rm ${ARQUIVO_SAIDA}.dat

# Gera os Arquivos .dat para o gnuplot
# Procura os numeros de bins em todos os arquivos
for NUM_INSTANCIAS in $INSTANCIAS
do
	NUMERO_MEDIO_OBJETOS_POR_BIN=`grep "NUMERO_MEDIO_OBJETOS_POR_BIN" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' ` 
	NUMERO_MEDIO_COMPARACOES_POR_BIN=`grep "NUMERO_MEDIO_COMPARACOES_POR_BIN" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' `
	PORCENTAGEM_ELIMINACOES_MESMO_BIN=`grep "PORCENTAGEM_ELIMINACOES_MESMO_BIN" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' `
	NUMERO_MEDIO_BINS_POR_INSTANCIA=`grep "NUMERO_MEDIO_BINS_POR_INSTANCIA" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' `
	NUMERO_MEDIO_COMPARACOES_POR_INSTANCIA=`grep "NUMERO_MEDIO_COMPARACOES_POR_INSTANCIA" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' `
	PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA=`grep "PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA" ${DIRETORIO}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.analise | awk '{num_bins = $2}  END {printf ("%f\n",num_bins);}' `

	echo "${NUM_INSTANCIAS} ${NUMERO_MEDIO_OBJETOS_POR_BIN} ${NUMERO_MEDIO_COMPARACOES_POR_BIN} ${PORCENTAGEM_ELIMINACOES_MESMO_BIN} ${NUMERO_MEDIO_BINS_POR_INSTANCIA} ${NUMERO_MEDIO_COMPARACOES_POR_INSTANCIA} ${PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA}" >> ${ARQUIVO_SAIDA}.dat

done 
