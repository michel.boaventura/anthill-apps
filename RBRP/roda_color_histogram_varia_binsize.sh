#!/bin/bash

BINSIZE_FILE=num_binsize.txt

if [ ! -f $BINSIZE_FILE ];
then
        echo "Nao foi possivel encontrar o arquivo $BINSIZE_FILE!!! Esse arquivo deve existir com os tamanhos dos binsizes a serem testados"   
	exit 1
fi

VAR_BIN_SIZE=`cat $BINSIZE_FILE`

# Execucao dos testes
for BINSIZE in $VAR_BIN_SIZE;
do
	echo "roda_tempos.sh ColorHistogram_normalizado.csv NOVOS_EXPERIMENTOS/color_histogram.binsize$BINSIZE 32 0 0 $BINSIZE"
	roda_tempos.sh ColorHistogram_normalizado.csv NOVOS_EXPERIMENTOS/color_histogram.binsize$BINSIZE 32 0 0 $BINSIZE
done
