/****************************************************************************/
/*      This file stores the auxiliary functions of the filter Bin          */
/****************************************************************************/

#include "procedures_bin.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/** Receive the first MBR (minimum bound rectangle) of a small number of samples (NUMBER_OF_POINTS_OF_SAMPLE) to measure the MBR Values and generate centroids randomly INSIDE this MBR.
 * In practice, it calculates the greatest and smallest values of all dimensions.   */
int receive_first_mbr_values_of_bin(bin_list_type *bin_list, dimension_size_type dimension_size, InputPortHandler input_port)
{
	unsigned int i;
	char error_msg[100];
	void *message;
	int *int_msg;
	bin_number_type *bin_number_msg;
	unsigned int number_of_assigner_instances=ahGetNumWriters(input_port);
	unsigned int msg_size=get_first_mbr_values_message_size(dimension_size);	// size of the message that will be received 
	message=malloc(msg_size);	// message that will received

	bin_list_pointer first;

	list_type *object_list;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	get_first_in_bin_list(bin_list, &first);

	object_list=&(first->item.objects);

	/* Gets the first values for the MBR bounds */
	ahReadBuffer (input_port, message, msg_size);

	int_msg=(int *)message;
	bin_number_msg=(bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);

	/* Tests if the message fields are valid */
	if (int_msg[TO_BIN_MSG_TYPE_FIELD]!=FIRST_MBR_VALUES_MSG || bin_number_msg[0]!=INITIAL_BIN_NUMBER)
	{
		sprintf(error_msg,"ERROR. Trying to receive the first MBR values, but the message received is invalid!!!\nMessage header: %d Bin Number Destinated: %llu\n", int_msg[TO_BIN_MSG_TYPE_FIELD], bin_number_msg[0]);
		ahExit (error_msg);
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_first_mbr_values_of_bin_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else 
			fprintf(fp_log, "RECEIVING FIRST MBR VALUES OF BIN MESSAGE\n");

		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Points Values message on the position of the msg that its saved */
	int_msg=(int *)(&(bin_number_msg[1]));

	// Get the initial MBR values from the message	
	get_mbr_values_from_msg(&(first->item.centroid), int_msg, dimension_size);
	
	for (i=1; i<number_of_assigner_instances; i++)
	{
		/* Updates the MBR bound values if the values received are more significant than the previous */
		ahReadBuffer (input_port, message, msg_size);
	
		int_msg=(int *)message;
		bin_number_msg = (bin_number_type *)&(int_msg[TO_BIN_BIN_NUMBER_FIELD]);
		
		/* Tests if the message fields are valid */
		if (int_msg[TO_BIN_MSG_TYPE_FIELD]!=FIRST_MBR_VALUES_MSG || bin_number_msg[0]!=INITIAL_BIN_NUMBER)
		{
			sprintf(error_msg,"ERROR. Trying to receive the first MBR values, but the message received is invalid!!!\nMessage header: %d Bin Number Destinated: %llu\n", int_msg[TO_BIN_MSG_TYPE_FIELD], bin_number_msg[0]);
			ahExit (error_msg);
		}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_first_mbr_values_of_bin_message(message, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else 
			fprintf(fp_log, "RECEIVING FIRST MBR VALUES OF BIN MESSAGE\n");

		fclose(fp_log);
	}
#endif		
/****************************************************************************/
		
		/* Points Values message on the position of the msg that its saved */
		int_msg=(&(int_msg[TO_BIN_VALUES_FIELD]));
	
		// Updates the MBR Values reading the message	
		update_mbr_values_from_msg(&(first->item.centroid), int_msg, dimension_size);		
	}
		
	free(message);
	return 1;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Create the new Bins when splitting a Bin */
int create_new_bins_in_split(bin_list_type *list, bin_list_pointer pointer_to_splitted_bin, bin_list_pointer *new_bins_pointers, dimension_size_type dimension_size)
{
	bin_type item;
	unsigned int i;
	bin_number_type generated_bin_number;

	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		/* Generate a number for the New Bin */
		generated_bin_number=get_next_bin_number();
	
		/* Create the Bin */
		/* Allocates the bin structures */
		malloc_bin(&item, generated_bin_number, dimension_size);
	
		/* Insert the bin in the list of the bins */
		insert_in_bin_list(item, list);
	
		/* Gets a pointer to the new bin that I had inserted on the final of the list */
		get_last_in_bin_list(list, &(new_bins_pointers[i]));

		/* Generate the initial centroid for this Bin */
		generate_random_centroid(&(new_bins_pointers[i]->item.centroid), &(pointer_to_splitted_bin->item.centroid), dimension_size);
	}
	
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Finds a number that will be used to create a new bin. This instance garatees that different instances could not create the same bin number and will not be created two bins with the same number */ 
bin_number_type get_next_bin_number()
{
   char error_msg[100];
   unsigned int total_instances=ahGetTotalInstances();
   unsigned int my_rank=ahGetMyRank();
   static bin_number_type last_bin_number=INITIAL_BIN_NUMBER;
   bin_number_type next_bin_number;

   // Tests if we just have to initialize the last bin number with the first valid Bin Number of this instance
   if ((last_bin_number == INITIAL_BIN_NUMBER) && (((INITIAL_BIN_NUMBER/total_instances)%total_instances) != my_rank))
   {
         last_bin_number=my_rank*total_instances;
   }
   else
   {
      next_bin_number = last_bin_number + 1;

      // Tests if the new next bin number is not bound on the blocks of this instance, in this case the number will be added until gets in the new block of this instance
      if (((next_bin_number/total_instances)%total_instances) != my_rank)
      {
	 // Go forward on the blocks of bin number until hit on an available block (block that belongs to this instance)
	 next_bin_number+=(total_instances-1)*total_instances;
      }	
      if (next_bin_number <= last_bin_number)	
      {
	 sprintf(error_msg,"ERROR. Could not create next bin number! It seems that there aren't enough numbers available!!! Last Bin Number: %llu Next Bin Number: %llu\n", last_bin_number, next_bin_number);
	 ahExit (error_msg);
      }
      last_bin_number=next_bin_number;
   }
   return (last_bin_number);	
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int execute_bin_first_phase(bin_list_type *bin_list, dimension_size_type dimension_size, int number_of_neighbours, int bin_size, OutputPortHandler Assigner_output, InputPortHandler Assigner_input)
{
	/* The maximum size of a message is the size of a Bins Local Values Message */
	unsigned int msg_size=get_bins_local_values_message_size(dimension_size);
	void *message;
	unsigned int number_of_assigner_instances=ahGetNumReaders(Assigner_input);
	
	char error_msg[100];
	int *int_msg;	
	bin_list_pointer splitted_bin_pointer;

	// variables for instrumetation
	#ifdef INSTRUMENTATION
		struct timeval READ_BUFFER_INITIAL_TIME, READ_BUFFER_FINAL_TIME;
	 	double parcial_idle_time = 0, total_idle_time = 0, real_time = 0;
		int i = 0;
		//open file
		FILE *fp_inst;
		char name_of_fp_inst[MAX_FILENAME];
		sprintf(name_of_fp_inst, "%s.bin_%d.dat", output_file_name, ahGetMyRank()); 
		fp_inst = fopen(name_of_fp_inst, "w");
		fprintf(fp_inst, "#TIME #IDLE_TIME\n");
	#endif
	
	#ifdef GET_TIME
	 int first_msg = 1;
	#endif 	
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

/********************************** TASK API ********************************/
#ifdef TASK_API
	// Creates the First Task that represents the whole First Phase
	// The task has number 1, and do not has dependencies neither metadata
	ahCreateTask(1, NULL, 0, NULL, 0);
#endif
/****************************************************************************/	
	
	/* If this is the first instance, it will split the first BIN partition */
	if (ahGetMyRank() == ((long)INITIAL_BIN_NUMBER % (long)ahGetTotalInstances()))
	{
		get_first_in_bin_list(bin_list, &splitted_bin_pointer);

		/* Split the first bin, generate centroids for the new bins and send split bin message */
		split_bin_action(bin_list, splitted_bin_pointer, dimension_size, Assigner_output);
		
		#ifdef INSTRUMENTATION
		NUMBER_OF_SPLITS++;	
		#endif
	 	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if (DEBUG >= DEBUGGER)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "Splitted the first BIN! DATABASE UPDATED:\n");
			print_bin_fields_list(bin_list, dimension_size, fp_log);
			fprintf(fp_log, "END OF DATABASE\n");
			fclose(fp_log);
		}
#endif		
/****************************************************************************/
	}

	//Allocates space in memory to the message
	message=malloc(msg_size);

	int_msg=(int *)message;

	// Get time for instrumentation time IDLE
	#ifdef INSTRUMENTATION
		gettimeofday(&READ_BUFFER_INITIAL_TIME, NULL);
		i++;
		if (i == NUM_PRINT_INST)
		{
			real_time  = (READ_BUFFER_INITIAL_TIME.tv_sec - INITIAL_TIME_INST.tv_sec )*1000000;
			real_time += READ_BUFFER_INITIAL_TIME.tv_usec - INITIAL_TIME_INST.tv_usec;
			real_time /= 1000000;
		}		
	#endif
	
	/* Gets the next message */
	while (ahReadBuffer (Assigner_input, message, msg_size) != EOW)	
	{
		// Get time for instrumentation time IDLE
		#ifdef INSTRUMENTATION
			gettimeofday(&READ_BUFFER_FINAL_TIME, NULL);
			parcial_idle_time  = (READ_BUFFER_FINAL_TIME.tv_sec - READ_BUFFER_INITIAL_TIME.tv_sec )*1000000;
			parcial_idle_time += READ_BUFFER_FINAL_TIME.tv_usec - READ_BUFFER_INITIAL_TIME.tv_usec;
			parcial_idle_time /= 1000000;
			total_idle_time += parcial_idle_time;
			if (i == NUM_PRINT_INST) 
			{
				fprintf(fp_inst, "%.3lf %.3lf\n", real_time, total_idle_time);
				i = 0; 
			}		
		#endif
		
		switch(int_msg[TO_BIN_MSG_TYPE_FIELD])
		{
			case BINS_LOCAL_VALUES_MSG:
			{
				#ifdef INSTRUMENTATION	
					NUMBER_OF_LOCAL_BINS_MSG++;
				#endif 
				process_bins_local_values_msg(int_msg, bin_list, dimension_size, number_of_neighbours, bin_size, number_of_assigner_instances, Assigner_output);
				break;
			}
			case CONCLUDED_BIN_LOCAL_VALUES_MSG:
			{
				/* Get the local values of a concluded bin */
				get_concluded_bin_local_values_from_msg(int_msg, bin_list, dimension_size, number_of_assigner_instances);
				break;
			}
			case OBJECT_VALUES_OF_BIN_MSG:
			{
				// Saves the object received of the bin on the list of objects of this BIN
				#ifdef GET_TIME
				if (first_msg) 
				{
					gettimeofday(&first_phase_part_time, NULL);
					first_msg = 0;
				}
				#endif 
				get_object_values_of_bin_from_msg(int_msg, bin_list, dimension_size); 
				break;
			}
			default: 
			{
				sprintf(error_msg,"ERROR.The BIN filter received an invalid message on the First Phase execution!!!\nCode of the Message type received: %d\n", int_msg[TO_BIN_MSG_TYPE_FIELD]);
				ahExit (error_msg);
			}	
		}
		// Get time for instrumentation time IDLE
		#ifdef INSTRUMENTATION
			gettimeofday(&READ_BUFFER_INITIAL_TIME, NULL);
			i++;
			if (i == NUM_PRINT_INST)
			{
				real_time  = (READ_BUFFER_INITIAL_TIME.tv_sec - INITIAL_TIME_INST.tv_sec )*1000000;
				real_time += READ_BUFFER_INITIAL_TIME.tv_usec - INITIAL_TIME_INST.tv_usec;
				real_time /= 1000000;
			}		
		#endif
	}

	free(message);
#ifdef INSTRUMENTATION
	fprintf(fp_inst, "%.3lf %.3lf\n", real_time, total_idle_time);
	fclose(fp_inst);
#endif	

/********************************** TASK API ********************************/
#ifdef TASK_API
	// Terminates the First Task that represents the whole First Phase.
	ahEndTask(1);
#endif
/****************************************************************************/	

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Gets the local values of the bins from the message */
int get_bins_local_values_from_msg(int *msg, bin_list_type *list, bin_list_pointer *splitted_bin_pointer, bin_list_pointer **pointer_to_bins, dimension_size_type dimension_size, unsigned int number_of_assigner_instances)
{
	bin_type bin;
	bin_list_pointer bin_pointer;
	void *values_msg;
	int *int_msg;	
	bin_number_type *bin_number_msg;
	unsigned int i, local_length, local_values_received;
	int all_bin_local_values_received;
	int changing=0, creating_bins=0;
	char error_msg[100];

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if ( DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if ( DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_bins_local_values_message(msg, dimension_size, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else 
			fprintf(fp_log, "RECEIVING BINS LOCAL VALUES MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Gets the Bin_number from the message */
	bin_number_msg=(bin_number_type *)&(msg[TO_BIN_BIN_NUMBER_FIELD]);
	
	bin.number=bin_number_msg[0];
	
	/* Gets a pointer to the bin that are being splitted */
	*splitted_bin_pointer=search_item_in_bin_list(bin, list);

	/* Tests if the message was destinated to a new bin that doesn't exists until now */
	if (*splitted_bin_pointer==NULL)
	{
		/* Creates and insert on the list the new bin*/
		malloc_bin(&bin, bin.number, dimension_size);
	
		/* Insert the bin in the list of the bins */
		insert_in_bin_list(bin, list);
	
		/* Gets a pointer to the new bin that I had created */
		get_last_in_bin_list(list, splitted_bin_pointer);

		/* Changes the state of the splitted bin meaning that it was SPLITTED*/
		(*splitted_bin_pointer)->item.state++;
		
		/* Marks that new bins have to be created when receiving its values */
		creating_bins=1;
	}
	else
	{
		/* If the bin was just created being a child and have not been splitted yet, we will have to create their childs */
		if ((*splitted_bin_pointer)->item.state==NORMAL_STATE)
		{
			/* Changes the state of the splitted bin meaning that it was SPLITTED*/
			(*splitted_bin_pointer)->item.state++;

			/* Marks that new bins have to be created when receiving its values */
			creating_bins=1;
		}
	}

	/* Computes the message received */
	(*splitted_bin_pointer)->item.local_values_received++;
	local_values_received=(*splitted_bin_pointer)->item.local_values_received;

	/* Tests if all local values was received */
	all_bin_local_values_received=(local_values_received == number_of_assigner_instances);
	
	// Go forward on the message
	bin_number_msg=&(bin_number_msg[1]);

	/* Fill the new local values of the bins on the message */
	for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
	{
		/* Get the number of the bins */
		bin.number=bin_number_msg[0];

		/* Test if the bins have to be created */
		if (creating_bins)
		{
			/* Creates and insert on the list the new bin*/
			malloc_bin(&bin, bin.number, dimension_size);
	
			/* Insert the bin in the list of the bins */
			insert_in_bin_list(bin, list);
	
			/* Gets a pointer to the new bin that I had created */
			get_last_in_bin_list(list, &bin_pointer);
		}
		else 
		{
			/* Gets a pointer to the Bin */
			bin_pointer=search_item_in_bin_list(bin, list);
			
			/* Tests if the bin was not found, in this case, there is a fatal error */
			if (bin_pointer==NULL)
			{
				sprintf(error_msg,"ERROR. Trying to find the BIN %llu on the list, but it wasn't found!!!\nCouldn't get the bins local values from the message!!!\n", bin.number);
				ahExit (error_msg);
			}
		}

		(*pointer_to_bins)[i]=bin_pointer;
	
		// Go forward on the message
		int_msg=(int *)&(bin_number_msg[1]);
		
		/* Gets the local length of the Bin */
		local_length=int_msg[0];
	
		values_msg=(void *)(&(int_msg[1]));

		/* Just in case that there are objects, that we have to get the local centroid values from the message */ 
		
		if (local_length > 0)	
		{
			/* Get the values from the message and update the values of the centroid. It also go forward on the message. */	
			values_msg=get_centroid_local_values_from_msg(&(bin_pointer->item.centroid), local_length, values_msg, dimension_size);
		}

		/* Go forward on the message */
		bin_number_msg=(bin_number_type *)values_msg;
	
	}

	/* If already have received all the local values, updates the dimension values of the centroid */
	if (all_bin_local_values_received)
	{
		for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
		{
			/* This function returns if the centroid had a changing */
			changing+=update_centroid_dimension_values(&(((*pointer_to_bins)[i])->item.centroid), dimension_size);
		}
//		for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
//		{
			//TODO: Implementar funcionalidade para reposicionar bins vazios
			//bin_list_pointer pointer_to_empty_bins[NUM_KMEANS_PARTITIONS];
			//bin_list_pointer pointer_to_greater_bins[NUM_KMEANS_PARTITIONS];
	
			/* This function returns if the centroid had a changing */
//			changing+=update_centroid_dimension_values(&(((*pointer_to_bins)[i])->item.centroid), dimension_size);
//		}

	}
	
	/* Checks if a new iteration is necessary or not */
	if (all_bin_local_values_received)
	{
		/* If didn't changed the centroid values, there is no more iteration */
		if (!changing)
			(*splitted_bin_pointer)->item.state=CONCLUDED;
	}	
	return (all_bin_local_values_received);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Split the bin, generate centroids for the new bins and send split bin message */
int split_bin_action(bin_list_type *bin_list, bin_list_pointer splitted_bin_pointer, dimension_size_type dimension_size, int Assigner_output)
{
	bin_list_pointer new_bins_pointers[NUM_KMEANS_PARTITIONS];
	
	/* Changes the state of the splitted bin meaning that it was SPLITTED*/
	splitted_bin_pointer->item.state++;

	/* Creates the bins on the bin list */
	create_new_bins_in_split(bin_list, splitted_bin_pointer, new_bins_pointers, dimension_size);
	
	/* Send the split message to the Assigner filter asking for the Split */
	send_split_bin_message(splitted_bin_pointer->item.number, new_bins_pointers, dimension_size, Assigner_output);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Cleans the list removing the Bins that doesn't belong to this instance, and the bins that were splitted */
/* The list will get just with concluded Bins */
int clean_bin_list(bin_list_type *list, dimension_size_type dimension_size)
{
	bin_list_pointer aux;
	bin_type removed_bin;
	
	/* If the first item is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}
   
	/* Clean all the Bins that not belong to this instance or that was splitted*/
	/* These Bins are empty now, then we test if the Bin is empty and, in this case we remove the from the list */
	if (is_empty(&(aux->item.objects)))
	{
		removed_bin=remove_of_bin_list(&aux, list);
		free_bin(removed_bin, dimension_size);	
	}

	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		if (is_empty(&(aux->item.objects)))
		{
			removed_bin=remove_of_bin_list(&aux, list);
			free_bin(removed_bin, dimension_size);	
		}
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Print all the bins of the list each one on a different file*/
int print_bins_done(bin_list_type *list, dimension_size_type dimension_size)
{
	bin_list_pointer aux;
	FILE *file;
	char name_of_file[50];

	/* If the first item is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}
   
	/* Prints the First Bin of the list */
	sprintf(name_of_file, "bin_%llu.ascii", aux->item.number); 

	// Open the file of the bin
	file = fopen(name_of_file,"w");
	
	//print_bin(aux->item, dimension_size, file);
	print_list(&(aux->item.objects), dimension_size, file);
	fclose(file);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);

		/* Prints the Bin on the file */
		sprintf(name_of_file, "bin_%llu.ascii", aux->item.number); 

		// Open the file of the bin
		file = fopen(name_of_file,"w");
		//print_bin(aux->item, dimension_size, file);
		print_list(&(aux->item.objects), dimension_size, file);
	
		fclose(file);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Tests if a bin needs to be splitted or it can be concluded */
int needs_split_bin_action(bin_type *bin, float *prune_ratio, int number_of_neighbours, int bin_size, dimension_size_type dimension_size)
{
	distance_type diagonal_mbr_size;
	int return_value;

	diagonal_mbr_size = calculate_distance_between_dimension_values(bin->centroid.lower_bound_mbr, bin->centroid.upper_bound_mbr, dimension_size);
	
	// Tests if the diagonal's size MBR is greater than the minimum value for splitting a bin
	// This is necessary for garantee the convergence of the algorithm for bases where the number of identical values is greater than the Bin_size
	if (diagonal_mbr_size < MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING)
	{
		if (bin->centroid.number_of_objects_considered > number_of_neighbours)
	       		(*prune_ratio) = ((float)(bin->centroid.number_of_objects_considered)-number_of_neighbours)/(float)(bin->centroid.number_of_objects_considered);
		else (*prune_ratio)=0;
		return_value = 0;
	}
	else 
	{
		(*prune_ratio) = 0;
		if (bin->centroid.number_of_objects_considered > bin_size) 
			return_value=1;
		else return_value = 0;
	}	
		
	return return_value;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int process_bins_local_values_msg(int *msg, bin_list_type *bin_list, dimension_size_type dimension_size, int number_of_neighbours, unsigned int bin_size, unsigned int number_of_assigner_instances, OutputPortHandler port)
{
	unsigned int i, j;
	float prune_ratio;
	int do_action;
	bin_list_pointer *pointer_to_bins;
	bin_list_pointer splitted_bin_pointer;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Allocates memory for the pointer_to_bins array */
	pointer_to_bins=malloc(sizeof(bin_list_pointer)*NUM_KMEANS_PARTITIONS);
	
	do_action=get_bins_local_values_from_msg(msg, bin_list, &splitted_bin_pointer, &pointer_to_bins, dimension_size, number_of_assigner_instances); 

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= DEBUGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "Received bins local values of an instance! DATABASE UPDATED:\n");
		print_bin_fields_list(bin_list, dimension_size, fp_log);
		fprintf(fp_log, "END OF DATABASE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	/* Tests if is necessary to do an action cause the last instance sent the message with its local values */
	if (do_action)
	{

		if(splitted_bin_pointer->item.state==CONCLUDED)	
		{
			for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
			{
				/* If the Bin is bigger than BINSIZE, we have to split it again */
				if (needs_split_bin_action(&(pointer_to_bins[i]->item), &prune_ratio, number_of_neighbours, bin_size, dimension_size))
				{
					split_bin_action(bin_list, pointer_to_bins[i], dimension_size, port);
					#ifdef INSTRUMENTATION
						NUMBER_OF_SPLITS++;	
					#endif 

/********************************** DEBUG ***********************************/
#ifdef DEBUG
					if (DEBUG >= INFO)
					{
						distance_type diagonal_mbr_size = calculate_distance_between_dimension_values(pointer_to_bins[i]->item.centroid.lower_bound_mbr, pointer_to_bins[i]->item.centroid.upper_bound_mbr, dimension_size);
	
						fp_log = fopen(name_of_fp_log,"a");
						fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n");
						fprintf(fp_log, "Deciding to split the Bin: %llu\tNumber of objects of the bin: %d\tDiagonal MBR: ", pointer_to_bins[i]->item.number, pointer_to_bins[i]->item.centroid.number_of_objects_considered);
						fprintf(fp_log, DISTANCE_CONVERSION_STRING, diagonal_mbr_size);
						fprintf(fp_log, "\tBin Father: %llu\n", splitted_bin_pointer->item.number);
						fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n");
						fclose(fp_log);
					}
#endif		
/****************************************************************************/

				}
				// The bin can be concluded
				else
				{
					
					// Send a message to Assigner Filter notifying the conclusion of the bin
					send_concluded_bin_message(pointer_to_bins[i]->item.number, prune_ratio, port);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
					if (DEBUG >= INFO)
					{
						distance_type diagonal_mbr_size = calculate_distance_between_dimension_values(pointer_to_bins[i]->item.centroid.lower_bound_mbr, pointer_to_bins[i]->item.centroid.upper_bound_mbr, dimension_size);
	
						fp_log = fopen(name_of_fp_log,"a");
						fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n");
						fprintf(fp_log, "Deciding to conclude the Bin: %llu\tNumber of objects of the bin: %d\tDiagonal MBR: ", pointer_to_bins[i]->item.number, pointer_to_bins[i]->item.centroid.number_of_objects_considered);
						fprintf(fp_log, DISTANCE_CONVERSION_STRING, diagonal_mbr_size);
						fprintf(fp_log, "\tBin Father: %llu\n", splitted_bin_pointer->item.number);
						fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n");
						fclose(fp_log);
					}
#endif		
/****************************************************************************/
				}
			}
		}
		else
		{
			send_new_iteration_message(splitted_bin_pointer->item.number, pointer_to_bins, dimension_size, port);
		}

		/* Counts the new iteration message to satisfy the convergence test */
		splitted_bin_pointer->item.state++;	
		
		/* Restart the length of the bin */
		splitted_bin_pointer->item.centroid.number_of_objects_considered=0;
	
		for (i=0; i<NUM_KMEANS_PARTITIONS; i++)
		{
			pointer_to_bins[i]->item.centroid.number_of_objects_considered=0;
			
			/* Cleaning the lists of occurrences of the categorical attributes */
			for (j=0; j<dimension_size.number_of_categorical_values; j++)
			{
				make_occurrences_list_empty(&(pointer_to_bins[i]->item.centroid.occurrences[j]));
			}
		}
			
		/* Restart the number of local values received to do the new iteration */
		splitted_bin_pointer->item.local_values_received=0;	
	}
	
	/* Frees the memory of the array of pointers to bins */
	free(pointer_to_bins);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Calculates the closest bins of all the bins on the list of bins.
 * The result is saved on the matrix of the closest bins */
int calculate_all_closest_bins(bin_list_type *list, dimension_size_type dimension_size)
{
	bin_list_pointer aux;
  
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* If the first item of the bin list is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}
   
	/* Calculates the closest bins of the First Bin of the list */
	calculate_closest_bins(&(aux->item), list, dimension_size);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "Closest Bins calculated: \n");
		print_closest_bins(&(aux->item.closest_bins), aux->item.number, list->length, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);

		/* Calculates the closest bins of the Bin */
		calculate_closest_bins(&(aux->item), list, dimension_size);
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if (DEBUG >= LOGGER)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "All Closest Bins calculated: \t");
			print_closest_bins(&(aux->item.closest_bins), aux->item.number, list->length, fp_log);
			fclose(fp_log);
		}
#endif		
/****************************************************************************/

	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Calculate the closest bins of a bin. It runs list of bins calculating the distance between the Bins. */
int calculate_closest_bins(bin_type *bin, bin_list_type *list, dimension_size_type dimension_size)
{
	bin_list_pointer aux;
	unsigned int number_of_bins=list->length;
	
	/* If the first item of the bin list is not found, it just returns*/
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}
 
	/* Allocates the memory for the structure of the closest bins */
	malloc_closest_bins(&(bin->closest_bins), number_of_bins);

	// Insert the distance of the bin on the closest bins
	insert_distance_between_bins_in_closest_bins(bin, &(aux->item), dimension_size);
	
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);

		// Insert the distance of the bin on the closest bins */
		insert_distance_between_bins_in_closest_bins(bin, &(aux->item), dimension_size);
	}
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Insert the distance between the bins on the closest bins of the Bin1. */
int insert_distance_between_bins_in_closest_bins(bin_type *bin, bin_type *bin2, dimension_size_type dimension_size)
{
	bin_number_type bin_number;
	distance_type distance = 0;
	
	/* Gets the number of the bin that will get its values inserted */
	bin_number=bin2->number;
	
	/* Calculates the distance between the bins */
	if (DISTANCE_BINS_METRIC == DISTANCE_BINS_CENTROID_METRIC)
		distance=distance_between_centroids(bin->centroid, bin2->centroid, dimension_size);
	else
	{
		if (DISTANCE_BINS_METRIC == DISTANCE_BINS_MBR_METRIC) 
			distance=distance_min_between_mbrs(bin->centroid, bin2->centroid, dimension_size);
		else
		{
			char error_msg[100];
			sprintf(error_msg,"ERROR. There is no metric defined to calculate the distance between Bins!!!\n");
			ahExit (error_msg);
		}	
	}

	// Hacked operation to ensure that the closest neighbour of a bin is the bin itself
	if (bin_number == bin->number)
			  distance=-MIN_DIAGONAL_MBR_SIZE_FOR_SPLITTING;
	
	/* Insert the new values on the closest bins */
	insert_in_closest_bins(&(bin->closest_bins), bin2, distance);

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This functions verifies if there are messages on the buffer, put them on the list of messages and return the prior message that have to be processed */
int get_next_message(msg_list_type *msg_list, message_type *message, dimension_size_type dimension_size, InputPortHandler Bin_input)
{	
	int type_of_the_message;
	char error_msg[100];

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	/* Gets the next message */
	while (try_getting_message(message, Bin_input))
	{
		type_of_the_message=((int *)(*message))[TO_BIN_MSG_TYPE_FIELD];
		switch(type_of_the_message)
		{
			case PROBABLE_OUTLIER_MSG:
			case SEARCH_FINISHED_ON_INSTANCE_MSG:
			{	
				/* If a probable outlier message is received, we just insert this on the list of messages */
				/* It will be processed when it gets the first position on the list and there is no more score new outlier on the buffer */
				insert_in_msg_list(*message, msg_list);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
				if (DEBUG >= INFO )
				{

					fp_log = fopen(name_of_fp_log,"a");
					fprintf(fp_log, "\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
					if (type_of_the_message==PROBABLE_OUTLIER_MSG)
					{
						fprintf(fp_log, "RECEIVED A PROBABLE OUTLIER_MSG AND PUT IT ON THE LIST: \n");
						print_probable_outlier_message(*message, dimension_size, fp_log);
					}
					if (type_of_the_message==SEARCH_FINISHED_ON_INSTANCE_MSG)
					{
						fprintf(fp_log, "RECEIVED A SEARCH_FINISHED_ON_INSTANCE_MSG AND PUT IT ON THE LIST: \n");
						print_search_finished_on_instance_message(*message, fp_log);
					}	
					fprintf(fp_log, "END OF MESSAGE\n");
					fclose(fp_log);
				}
#endif		
/****************************************************************************/
				
				break;
			}
			case SCORE_NEW_OUTLIER_MSG:
			{
				/* This message have priority and then is processed soon it is received */
				return 1;
				break;
			}

			default: 
			{
				sprintf(error_msg,"ERROR.The BIN filter received an invalid message on the Second Phase execution!!!\nCode of the Message type received: %d\n", type_of_the_message);
				ahExit (error_msg);
			}	
		}
	}

	/* If there is no SCORE_NEW_OUTLIER messages on the buffer, we can get any other messages on the list of messages */

	/* Tests if the list of messages is empty */
	if (is_empty_msg_list(msg_list))
	{
		/* there are no messages to be processed, then we just returns 0 */ 
		return 0;	
	}	
	else 
	{
		/* We get the first message of the list and returns 1 indicating that we will process this message now */
		*message=remove_first_of_msg_list(msg_list);
		return 1;	
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

int execute_bin_second_phase(bin_list_type *bin_list, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours, InputPortHandler Bin_input, OutputPortHandler Bin_output, OutputPortHandler Final_output)
{
	int new_message;
	char error_msg[100];
	msg_list_type msg_list;
	message_type message;
	int type_of_the_message;
	
	distance_type *highest_scores;
	
	list_pointer previous_object=NULL;

	bin_list_pointer current_bin=NULL;

	int number_of_instances_finished=0, total_instances;

	int remaining_objects=1;
	
#ifdef INSTRUMENTATION
	//open file
	FILE *fp_dat;
	char name_of_fp_dat[MAX_FILENAME];
	sprintf(name_of_fp_dat, "%s.bin_%d.score", output_file_name, ahGetMyRank()); 
	fp_dat = fopen(name_of_fp_dat, "w");
	fprintf(fp_dat, "#TIME #SCORE #WORST_SCORE #OBJECTS_CONSIDERED\n");
	fclose(fp_dat);
#endif
	
/********************************** TASK API ********************************/
#ifdef TASK_API
	// Creates the Second Task that represents the whole Second Phase
	// The task has number 2, and do not has dependencies neither metadata
	ahCreateTask(2, NULL, 0, NULL, 0);
#endif
/****************************************************************************/	

	// Alocates the structure to save the highest scores of the outliers, it initialize with zero values //
	highest_scores=calloc(number_of_outliers, sizeof(distance_type));	

	/* Create the list of the messages initially empty */
	create_empty_msg_list(&msg_list);

	total_instances=ahGetTotalInstances();

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= INFO)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "STARTING SECOND PHASE OF THE RBRP!!!\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	while(1)
	{
		new_message=get_next_message(&msg_list, &message, dimension_size, Bin_input);
		if (new_message)
		{
			type_of_the_message=((int*)message)[TO_BIN_MSG_TYPE_FIELD];
			switch(type_of_the_message)
			{
				case PROBABLE_OUTLIER_MSG:
				{
					#ifdef INSTRUMENTATION
					// The object don't belongs this instance
					IS_MY_OBJECT = 0;
					#endif
					
					/* If we get a probable outlier message, we process this message */
					process_probable_outlier_message(message, bin_list, highest_scores, dimension_size, number_of_outliers, number_of_neighbours, Bin_output, Final_output);
					
					/* Frees the space allocated for this message */
					free_message(message);
					break;
				}
				case SCORE_NEW_OUTLIER_MSG:
				{
					process_score_new_outlier_message(highest_scores, message, number_of_outliers);
				
					/* Frees the space allocated for this message */
					free_message(message);
					break;
				}

				case SEARCH_FINISHED_ON_INSTANCE_MSG:
				{
/********************************** DEBUG ***********************************/
#ifdef DEBUG
					if (DEBUG >= RECV_MSG_RESUME )
					{
						fp_log = fopen(name_of_fp_log,"a");
						if (DEBUG >= RECV_MSG_VERBOSE )
						{
							fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
							fprintf(fp_log, "RECEIVING: \t");
							print_search_finished_on_instance_message(message, fp_log);
							fprintf(fp_log, "END OF MESSAGE\n");
						}
						else 
							fprintf(fp_log, "RECEIVING SEARCH FINISHED ON INSTANCE MESSAGE\n");

						fclose(fp_log);
					}
#endif		
/****************************************************************************/
					
					/* Frees the space allocated for this message */
					free_message(message);
					
					/* Computes the search on all the objects of one instance have been finished */ 
					number_of_instances_finished++;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
					if (DEBUG >= INFO)
					{
						fp_log = fopen(name_of_fp_log,"a");
						fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
						fprintf(fp_log, "NUMBER OF INSTANCES FINISHED:%d\t TOTAL NUMBER OF INSTANCES: %d\n", number_of_instances_finished, total_instances);
						fclose(fp_log);
					}
#endif		
/****************************************************************************/
	
					if (number_of_instances_finished==total_instances)
					{

/********************************** TASK API ********************************/
						
#ifdef TASK_API
						// Terminates the Second Task that represents the whole Second Phase
						ahEndTask(2);
#endif
/****************************************************************************/	
					
						return 0;
					}
					break;
				}
				default: 
				{
					sprintf(error_msg,"ERROR.The BIN filter received an invalid message on the Second Phase execution!!!\nCode of the Message type received: %d", type_of_the_message);
					ahExit (error_msg);
				}	
			}
		}
		else
		{
			/* If there are remaining objects to be searched we get a new one. */
			if (remaining_objects)
			{
				#ifdef INSTRUMENTATION
				// The object don't belongs this instance
				IS_MY_OBJECT = 1;
				#endif
				remaining_objects=new_search_object_as_outlier(&previous_object, &current_bin, bin_list, highest_scores, dimension_size, number_of_outliers, number_of_neighbours, Bin_output, Final_output);
			}
		}
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Receives an object candidate to be outlier and process this message searching for nearest neighbours of the object.
 * I will send messages telling if a new outlier have been found */
int process_probable_outlier_message(message_type message, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours, OutputPortHandler Bin_output, OutputPortHandler Final_output)
{
	nearest_neighbours_type nearest_neighbours;
	object_type object;
	int is_probable_outlier, number_of_bin_instances;
	bin_number_type next_instance = 0;
	
	/* the pointer to the closest bin of this instance */
	bin_list_pointer closest_local_bin_pointer;
	
	/* the number of the bin that the objects belongs */
	bin_number_type bin_real_owner_number;
	
	int is_last_object_of_the_instance, new_outlier_found;

	distance_type score;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
	
	/* Get the object and the nearest neighbour from the message */
	is_last_object_of_the_instance=get_probable_outlier_from_msg(message, &bin_real_owner_number, &object, &nearest_neighbours, dimension_size, number_of_neighbours);

	/* Check if the nearest neighbours already found can not guarantee that the object is not an outlier */
	is_probable_outlier=check_for_probable_outlier(&nearest_neighbours, highest_scores, number_of_outliers, number_of_neighbours);

	/* If the object really already is a probable outlier, we will search for nearest neighbours locally */
	if (is_probable_outlier)
	{
		/* Find the closest bin of the object, the object will be considered belonging to this bin when the search for nearest neighbours will be done */
		/* If the bin list is empty, there is no closest bin and the local search for nearest bin will not be done */
		closest_local_bin_pointer=find_closest_bin_in_bin_list(&object, bin_list, dimension_size);
		if (closest_local_bin_pointer!=NULL)
		{
			/* Search for nearest neighbours running all the bin list, respecting the closest bin of the closest_local_bin_pointer on the search */
			is_probable_outlier=search_for_nearest_neighbours_in_bin_list(&nearest_neighbours, &object, closest_local_bin_pointer, bin_list, highest_scores, dimension_size, number_of_outliers, number_of_neighbours);
		}
		
		/* Gets the number of the instance that will receive the probable_outlier_message */
		number_of_bin_instances=ahGetTotalInstances();
		
		next_instance=(ahGetMyRank()+1)%number_of_bin_instances;
		
		/* Check if a new outlier was found */
		new_outlier_found=(is_probable_outlier && (next_instance==(bin_real_owner_number%number_of_bin_instances)));
	}
	else
	{
		new_outlier_found=0;
		#ifdef INSTRUMENTATION
			/*Incremental the counters if the object isn't probable outlier in the score*/
			if (!IS_MY_OBJECT) OTHER_POINT_IN_SCORE++;
		#endif


/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if (DEBUG >= INFO)
			{
				fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "THE CURRENT OBJECT RECEIVED CAN NOT BE AN OUTLIER ACCORDING THE CURRENT HIGHEST SCORES!!!\n");
				fprintf(fp_log, "FOUND A NEW OUTLIER WITHOUT DOING THE LOCAL SEARCH!!!\n");
				fclose(fp_log);
			}
#endif		
/****************************************************************************/
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= INFO)
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= LOGGER)
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "LOCAL SEARCH FINISHED:\t");
			fprintf(fp_log, "Bin Number: %llu\tObject Number: %d\n", bin_real_owner_number, object.number);
			print_nearest_neighbours(&nearest_neighbours, object.number, number_of_neighbours, fp_log);
			print_highest_scores(highest_scores, number_of_outliers, fp_log);
		}
		if (new_outlier_found)
		{
			fprintf(fp_log, "A NEW OUTLIER WAS FOUND!!!\n");
		}
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Tests if the next instance is the instance that originally started the search for outlier.
	 * if it is true we have found a new outlier */ 
	if (new_outlier_found)	
	{
		/* Get the score of the outlier */
		score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

		/* Saves the new score on the highest scores array */
		save_new_score(highest_scores, score, number_of_outliers);

		/* Send the message to the final filter with the new outlier */
		send_new_outlier_message(&object, &nearest_neighbours, dimension_size, number_of_neighbours, Final_output);
		
		/* Send the new score to the other instances */
		send_score_new_outlier_messages(score, Bin_output);

		/* Tests if the search on the object was finished and this is the last object of their instance.
	 	 * In this case we have to sent a search_finished_on_instance message */
		if (is_last_object_of_the_instance)
		{
			send_search_finished_on_instance_messages(Bin_output, bin_real_owner_number);
		}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if (DEBUG >= LOGGER)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "NEW HIGH SCORES AFTER SAVING THE NEW SCORE: ");
			fprintf(fp_log, DISTANCE_CONVERSION_STRING, score);
			fprintf(fp_log, "\n");
			print_highest_scores(highest_scores, number_of_outliers, fp_log);
			fclose(fp_log);
		}
#endif		
/****************************************************************************/
	}
	else
	{
		if (is_probable_outlier)	
		{
			send_probable_outlier_message(&object, &nearest_neighbours, is_last_object_of_the_instance, (unsigned long long)next_instance, bin_real_owner_number, dimension_size, number_of_neighbours, Bin_output);
			#ifdef INSTRUMENTATION
			NUMBER_OF_OUTPROB_MSGS++;
			#endif
		}
		else
		{
			/* Tests if the search on the object was finished and this is the last object of the instance.
			 * In this case we have to sent a search_finished_on_instance message */

/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if ( DEBUG >= INFO )
			{
				fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "THE CURRENT OBJECT IS NOT AN OUTLIER!!!\n");
				fclose(fp_log);
			}
#endif		
/****************************************************************************/

			if (is_last_object_of_the_instance)
				send_search_finished_on_instance_messages(Bin_output, bin_real_owner_number);
		}
	}
	free_nearest_neighbours(nearest_neighbours);
	return 1;
}

/* Search for nearest neighbours of the object in all bins on the list */
int search_for_nearest_neighbours_in_bin_list(nearest_neighbours_type *nearest_neighbours, object_type *object, bin_list_pointer bin_owner_pointer, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	bin_type *current_bin_pointer;
	int is_probable_outlier;
	int number_of_bins=bin_list->length;
	int bins_considerate = 0;

#ifdef PRUNE_PARTITIONS
 int neighbours_considerate =  0; 
 distance_type max_distance_bin = 0; 
#endif	
 
	/* Starts the search on the bin that owns the object */
	current_bin_pointer=&(bin_owner_pointer->item);
	
#ifdef INSTRUMENTATION
 /* Set 1 if the bin owns the object*/
	if (IS_MY_OBJECT) {
		MY_OBJECT_IN_FIRST_BIN=1;	
	}
#endif
 
	/* Search for the nearest neighbours on the initial bin */ 
	is_probable_outlier=search_for_nearest_neighbours_in_bin(nearest_neighbours, object, current_bin_pointer, highest_scores, dimension_size, number_of_outliers, number_of_neighbours);


	/* If the object can not be an outlier, we just return 0 */
	if(!is_probable_outlier)
	{
		#ifdef INSTRUMENTATION
			NUMBER_OF_COMPARATIONS_OF_OBJECTS_ELIMINATED_IN_FIRST_BIN+=COUNTER_COMPARATIONS_IN_FIRST_BIN;
	    NUMBER_OF_COMPARATIONS_IN_FIRST_BIN+=COUNTER_COMPARATIONS_IN_FIRST_BIN;
			/*Incremental the counters if the object isn't probable outlier still in first bin*/
			if (IS_MY_OBJECT) {
				MY_POINT_IN_BIN++;
				MY_POINT_IN_INST++;
			}
			else {
				OTHER_POINT_IN_BIN++;
				OTHER_POINT_IN_INST++;
			}		
		#endif
		return 0;
	}
	
#ifdef INSTRUMENTATION
	 NUMBER_OF_COMPARATIONS_IN_FIRST_BIN+=COUNTER_COMPARATIONS_IN_FIRST_BIN;
	 MY_OBJECT_IN_FIRST_BIN=0;
#endif
	 
	//go next closest bin
	bins_considerate++;
	current_bin_pointer=bin_owner_pointer->item.closest_bins.bin_pointers[bins_considerate];
	
	while(bins_considerate!=number_of_bins)
	{
		
#ifdef PRUNE_PARTITIONS
		neighbours_considerate = get_list_length(&(current_bin_pointer->objects));
		max_distance_bin = distance_max_between_point_mbr(object->dimension_values, current_bin_pointer->centroid, dimension_size);
		if (((get_worst_highest_score(highest_scores, number_of_outliers)) > max_distance_bin) && (neighbours_considerate >= number_of_neighbours)) 
 		{
				#ifdef INSTRUMENTATION
				/*Incremental the counters if the object isn't probable outlier still in first bin*/
			  PRUNING_OBJECTS_USING_BINS ++;
				if (IS_MY_OBJECT) MY_POINT_IN_INST++;
				else OTHER_POINT_IN_INST++;
				#endif
				return 0;
		}
#endif 

		/* Search for the nearest neighbours on the bin */ 
		is_probable_outlier=search_for_nearest_neighbours_in_bin(nearest_neighbours, object, current_bin_pointer, highest_scores, dimension_size, number_of_outliers, number_of_neighbours);
		
		/* If the object can not be an outlier, we stop searching for nearest neighbours */
		if (!is_probable_outlier){
			#ifdef INSTRUMENTATION
				/*Incremental the counters if the object isn't probable outlier still in first bin*/
				if (IS_MY_OBJECT) MY_POINT_IN_INST++;	
				else OTHER_POINT_IN_INST++;	
				
			#endif
			return 0;
		}
		//go next closest bin
		bins_considerate++;
		current_bin_pointer=bin_owner_pointer->item.closest_bins.bin_pointers[bins_considerate];
	}
	
	return 1;
}

/* Search for nearest neighbours of the object in the whole bin on the list */
int search_for_nearest_neighbours_in_bin(nearest_neighbours_type *nearest_neighbours, object_type *object, bin_type *bin, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	list_type *list=&(bin->objects);
	list_pointer current_object;
	distance_type distance;
	int object_number, new_nearest_neighbour;

#ifdef INSTRUMENTATION
	COUNTER_COMPARATIONS_IN_FIRST_BIN = 0;
#endif	
	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/
	
	/* If there is no item on the list of objects, it returns 1 meaning that the object continue being an probable outlier */
	if(!get_first_in_list(list, &current_object))
	{
#ifdef DEBUG
		if (DEBUG>=INFO)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "/-------------------/\n Empty Bin number: %llu\n /--------------/\n", bin->number);
			fclose(fp_log);
		}
#endif
		return 1; 
	}
#ifdef PRUNE_PARTITIONS	
	// If the minimum possible distance of an object of this bin could not be a nearest neighbour of this object, we
	// just stop searching on this bin
	distance_type min_distance = distance_min_between_point_mbr(object->dimension_values, bin->centroid, dimension_size);
 	if (!check_distance_for_nearest_neighbours(min_distance, nearest_neighbours, number_of_neighbours))
	{
#ifdef DEBUG
		if (DEBUG>=INFO)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "/-------------------/\nPRUNING Searching in BIN number: %llu Minimum Distance from Object: ", bin->number);
			fprintf(fp_log, DISTANCE_CONVERSION_STRING, min_distance);
			fprintf(fp_log, "/--------------/\n");
			fclose(fp_log);
		}
#endif
#ifdef INSTRUMENTATION
	PRUNING_SEARCH_IN_BINS+=bin->objects.length;
	NUMBER_OF_BINS_NOT_SEARCH++;
#endif 		
		return 1;
	}	
#endif
	
	//TODO 
	//implementar a poda usando o centroid e o raio
	
	/* While there is an next object, keep searching */
	do
	{
		/* Test if the current object on the list is not the object itself. Just in this case the current object can be a nearest neighbour.*/
		if (!compare_objects(current_object->item, *object))
		{
			/* Get the distance between the objects */
			distance=distance_between_objects(*object, current_object->item, dimension_size);
			#ifdef INSTRUMENTATION
			/*Incremental the counter of comparations*/
			NUMBER_OF_COMPARATIONS++;
			if (MY_OBJECT_IN_FIRST_BIN)
			{
					COUNTER_COMPARATIONS_IN_FIRST_BIN++;
			}		
			#endif

			object_number=current_object->item.number;

			/* Insert in the nearest neighbours array. If it is not a nearest neighbour it just returns 0 */
			new_nearest_neighbour=insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);

			/* If a new nearest neighbour was found, check if the object keep being a probable outlier */
			if (new_nearest_neighbour)
			{
				/* Checks if the object keep being a probable outlier */
				/* If it can not be an outlier, we can stop searching */
				if (!check_for_probable_outlier(nearest_neighbours, highest_scores, number_of_outliers, number_of_neighbours))
				{
					return 0;
				}	
			}
		}
		go_next(list, &current_object);
	}   
	while (!is_last(current_object, list));
  
	/* If the last item of the list was not cheched yet, we will do it */
	if (get_list_length(list)>1)
	{
		/* Test if the last object on the list is a nearest neighbour.*/
		if (!compare_objects(current_object->item, *object))
		{
			/* Get the distance between the objects */
			distance=distance_between_objects(*object, current_object->item, dimension_size);
			#ifdef INSTRUMENTATION
			/*Incremental the counter of comparations*/
			NUMBER_OF_COMPARATIONS++;	
			if (MY_OBJECT_IN_FIRST_BIN)
			{
					COUNTER_COMPARATIONS_IN_FIRST_BIN++;
			}		
			#endif

			object_number=current_object->item.number;

			/* Insert in the nearest neighbours array. If it is not a nearest neighbour it just returns 0 */
			new_nearest_neighbour=insert_in_nearest_neighbours(nearest_neighbours, number_of_neighbours, object_number, distance);
			/* If a new nearest neighbour was found, check if the object keep being a probable outlier */
			if (new_nearest_neighbour)
			{
				/* Checks if the object keep being a probable outlier */
				/* If it can not be an outlier, we can stop searching */
				if (!check_for_probable_outlier(nearest_neighbours, highest_scores, number_of_outliers, number_of_neighbours))
				{
					return 0;
				}	
			}
		}
	}
	return 1;
}

/* Checks if the neighbours found for one object could not garantee that this object is not an outier */
int check_for_probable_outlier(nearest_neighbours_type *nearest_neighbours, distance_type* highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours)
{
	int is_probable_outlier;
	
	/* Get the smaller score of an actual outlier */
	distance_type worst_highest_score=get_worst_highest_score(highest_scores, number_of_outliers);

	/* Check if the worst highest score (minimum distance of the less outlier of the outliers) would be a nearest neighbour on the nearest neighbours array. If it is true, the object yet is a probable outlier */
	is_probable_outlier=check_distance_for_nearest_neighbours(worst_highest_score, nearest_neighbours, number_of_neighbours);
	return is_probable_outlier;
}	

int is_valid_bin(bin_type *current_bin, distance_type *highest_scores, unsigned int number_of_outliers, unsigned int number_of_neighbours, dimension_size_type dimension_size) 
{
#ifdef PRUNE_PARTITIONS
	distance_type mbr_value = calculate_distance_between_dimension_values(current_bin->centroid.lower_bound_mbr, current_bin->centroid.upper_bound_mbr, dimension_size);
#endif

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	if (is_empty(&(current_bin->objects)))
		return 0;
	
#ifdef DEBUG
	if (DEBUG >= INFO)
	{	
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n/-------------/\nCHECK IF IS INVALID BIN. NUMBER: %llu \n /-------------/\n",current_bin->number);
		fclose(fp_log);
	}
#endif	

#ifdef PRUNE_PARTITIONS
	if (((get_worst_highest_score(highest_scores, number_of_outliers))>mbr_value) && (number_of_neighbours <= current_bin->centroid.number_of_objects_considered ))
	{
		#ifdef INSTRUMENTATION
		PRUNING_COMPLETE_BINS +=current_bin->centroid.number_of_objects_considered;
		NUMBER_OF_OBJECTS_CONSIDERED+=current_bin->centroid.number_of_objects_considered;
		NUMBER_OF_BINS_ELIMINATED ++;
		#endif

		#ifdef DEBUG
			if (DEBUG >= INFO)
			{	
				fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "\n/-------------/\nBIN INVALID : %llu\n Highest Score: ",current_bin->number);
				fprintf(fp_log, DISTANCE_CONVERSION_STRING, highest_scores[number_of_outliers-1]);
				fprintf(fp_log, "MBR Value:");
				fprintf(fp_log, DISTANCE_CONVERSION_STRING, mbr_value);
				fprintf(fp_log, "\n/-------------/\n");
				fclose(fp_log);
			}
		#endif	
		return 0;
	}	
#endif
	
	return 1;
}

int new_search_object_as_outlier(list_pointer *previous_object, bin_list_pointer *current_bin, bin_list_type *bin_list, distance_type *highest_scores, dimension_size_type dimension_size, unsigned int number_of_outliers, unsigned int number_of_neighbours, OutputPortHandler Bin_output, OutputPortHandler Final_output)
{
	nearest_neighbours_type nearest_neighbours;
	list_pointer current_object;
	int is_probable_outlier, next_instance, number_of_bin_instances;

	/* the number of the bin that the objects belongs */
	int is_last_object_of_the_instance, new_outlier_found;

	distance_type score;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	

	// variables for instrumetation
#ifdef INSTRUMENTATION
	struct timeval SCORE_TIME;
 	double real_time = 0;
	//open file
	FILE *fp_dat;
	char name_of_fp_dat[MAX_FILENAME];
	sprintf(name_of_fp_dat, "%s.bin_%d.score", output_file_name, ahGetMyRank());
#endif

	
	/* If the current have not been initialized, we will initialize this. And starts searching on the first object */
	if (*current_bin==NULL && *previous_object==NULL)
	{
		/* Gets the first bin on the list. If there are no bins we just send the search finished on instance message */
		if (!get_first_in_bin_list(bin_list, current_bin))
		{
/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if (DEBUG >= INFO)
			{	fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "THIS INSTANCE DO NOT HAVE ANY BINS TO SEARCH FOR OBJECT!!!\n");
				fclose(fp_log);
			}
#endif		
/****************************************************************************/
			send_search_finished_on_instance_messages(Bin_output, ahGetMyRank());
			return 0;
		}
		
		/* Gets the first object on the list of objects of the bin */
		/* If a bin is valid we just go to the next bin */
		while (!is_valid_bin(&((*current_bin)->item), highest_scores, number_of_outliers, number_of_neighbours, dimension_size))
		{
					if (!go_next_bin(bin_list, current_bin))
					{
/********************************** DEBUG ***********************************/
#ifdef DEBUG
						if (DEBUG >= INFO)
						{	fp_log = fopen(name_of_fp_log,"a");
							fprintf(fp_log, "THE LAST BIN IS INVALID! SENDING SEARCH FINISHED ON INSTANCE!!!\n");
							fclose(fp_log);
						}
#endif		
/****************************************************************************/

						send_search_finished_on_instance_messages(Bin_output, ahGetMyRank());
						return 0;
					}
	
		
		}
		get_first_in_list(&(((*current_bin)->item).objects), &current_object);
#ifdef INSTRUMENTATION
			NUMBER_OF_OBJECTS_CONSIDERED++;
#endif 		
	}
	else
	{
		
		/* Tests if the previous object is the last object of a bin. In this case we will start searching for an object of the next bin */
		if(is_last(*previous_object, &(((*current_bin)->item).objects)))
		{
			/* Tests if the bin is the last bin of the list*/
			if (is_last_bin(*current_bin, bin_list))
			{
				/* All the objects of this instance already have been searched for nearest neighbours */
				return 0;
			}
			else
			{
				go_next_bin(bin_list, current_bin);
				
				#ifdef DEBUG
					if (DEBUG >= INFO)
					{	
						fp_log = fopen(name_of_fp_log,"a");
						fprintf(fp_log, "GO NEXT BIN : %llu\n",(*current_bin)->item.number);
						fclose(fp_log);
					}
				#endif	

				/* Gets the first object of the new bin */
				/* If a bin empty or the mbr of the bin is shorter than score we just go to the next bin */
				while (!is_valid_bin(&((*current_bin)->item), highest_scores, number_of_outliers, number_of_neighbours, dimension_size))
				{
					/* Go to the next bin, if there isn't a next bin, we just returns */
					
					if (!go_next_bin(bin_list, current_bin))
					{
/********************************** DEBUG ***********************************/
#ifdef DEBUG
						if (DEBUG >= INFO)
						{	fp_log = fopen(name_of_fp_log,"a");
							fprintf(fp_log, "THE LAST BIN IS INVALID! SENDING SEARCH FINISHED ON INSTANCE!!!\n");
							fclose(fp_log);
						}
#endif		
/****************************************************************************/


						send_search_finished_on_instance_messages(Bin_output, ahGetMyRank());
						return 0;
					}
/********************************** DEBUG ***********************************/
#ifdef DEBUG
					if (DEBUG >= INFO)
					{	
						fp_log = fopen(name_of_fp_log,"a");
						fprintf(fp_log, "GO NEXT BIN : %llu\n",(*current_bin)->item.number);
						fclose(fp_log);
					}
#endif
/****************************************************************************/

				}
				get_first_in_list(&(((*current_bin)->item).objects), &current_object);
#ifdef INSTRUMENTATION
				NUMBER_OF_OBJECTS_CONSIDERED++;
#endif 		
			}
		}
		else
		{
			current_object=*previous_object;
			go_next(&(((*current_bin)->item).objects), &current_object);
#ifdef INSTRUMENTATION
			NUMBER_OF_OBJECTS_CONSIDERED++;
#endif 			
		}
	}
	
	/* The object is the last object of the instance if the bin is the last of the list and the object is the last of the bin */
	is_last_object_of_the_instance=(is_last_bin(*current_bin, bin_list) && is_last(current_object, &(((*current_bin)->item).objects)));
	
	/* Allocates the structure to save the neighbours */
	malloc_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "STARTING A NEW SEARCH OBJECT AS OUTLIER: \t");
		fprintf(fp_log, "Bin Number: %llu Object Number: %d\n", (*current_bin)->item.number, current_object->item.number);
		fprintf(fp_log, "PRINTING INFO BEFORE START SEARCHING\n");
		print_nearest_neighbours(&nearest_neighbours, current_object->item.number, number_of_neighbours, fp_log);
		print_highest_scores(highest_scores, number_of_outliers, fp_log);
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* Search for nearest neighbours running all the bin list, respecting the closest bins structure of the current bin on the search */
	is_probable_outlier=search_for_nearest_neighbours_in_bin_list(&nearest_neighbours, &(current_object->item), *current_bin, bin_list, highest_scores, dimension_size, number_of_outliers, number_of_neighbours);

	/* Gets the number of the instance that will receive the probable_outlier_message */
	number_of_bin_instances=ahGetTotalInstances();
	next_instance=(ahGetMyRank()+1)%number_of_bin_instances;

	/* check if a new outlier was found. It will happen just if there is just one Bin instance and the object is a probable outlier */
	new_outlier_found=(is_probable_outlier && (number_of_bin_instances==1));

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= INFO)
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= LOGGER)
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "LOCAL SEARCH FINISHED:\t");
			fprintf(fp_log, "Bin Number: %llu\tObject Number: %d\n", (*current_bin)->item.number, current_object->item.number);
			print_nearest_neighbours(&nearest_neighbours, current_object->item.number, number_of_neighbours, fp_log);
			print_highest_scores(highest_scores, number_of_outliers, fp_log);
		}
		if (new_outlier_found)
			fprintf(fp_log, "A NEW OUTLIER WAS FOUND!!!\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	/* if we have found a new outlier */ 
	if (new_outlier_found)	
	{
		/* get the score of the outlier */
		score=get_minimum_distance_in_nearest_neighbours(&nearest_neighbours, number_of_neighbours);

#ifdef INSTRUMENTATION
		distance_type worst_score = get_worst_highest_score(highest_scores,number_of_outliers);
		gettimeofday(&SCORE_TIME, NULL);
		real_time  = (SCORE_TIME.tv_sec - INITIAL_TIME_INST.tv_sec )*1000000;
		real_time += SCORE_TIME.tv_usec - INITIAL_TIME_INST.tv_usec;
		real_time /= 1000000;
		fp_dat = fopen(name_of_fp_dat,"a");
		fprintf(fp_dat, "%lf ", real_time); 
		fprintf(fp_dat, DISTANCE_CONVERSION_STRING , score);
		fprintf(fp_dat, " "); 
		fprintf(fp_dat, DISTANCE_CONVERSION_STRING , worst_score);
		fprintf(fp_dat, " "); 
		fprintf(fp_dat, "%llu" , NUMBER_OF_OBJECTS_CONSIDERED);
		fprintf(fp_dat, "\n");
		fclose(fp_dat);
#endif		

		/* saves the new score on the highest scores array */
		save_new_score(highest_scores, score, number_of_outliers);

		/* send the message to the final filter with the new outlier */
		send_new_outlier_message(&(current_object->item), &nearest_neighbours, dimension_size, number_of_neighbours, Final_output);
		
		/* Send the new score to the other instances */
		send_score_new_outlier_messages(score, Bin_output);
	
		/* Tests if the search on the object was finished and this is the last object of the instance.
	 	* In this case we have to sent a search_finished_on_instance message */
		if (is_last_object_of_the_instance)
		{
			send_search_finished_on_instance_messages(Bin_output, ahGetMyRank());
		}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if (DEBUG >= LOGGER)
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "NEW HIGH SCORES AFTER SAVING THE NEW SCORE: ");
			fprintf(fp_log, DISTANCE_CONVERSION_STRING, score);
			fprintf(fp_log, "\n");
			print_highest_scores(highest_scores, number_of_outliers, fp_log);
			fclose(fp_log);
		}
#endif		
/****************************************************************************/
	
	}
	else
	{
		/* Tests if the search on the object was finished and this is the last object of the instance.
		 * In this case we have to sent a search_finished_on_instance message */
		if (is_probable_outlier)	
		{
			send_probable_outlier_message(&(current_object->item), &nearest_neighbours, is_last_object_of_the_instance, next_instance, (((*current_bin)->item).number), dimension_size, number_of_neighbours, Bin_output);
			#ifdef INSTRUMENTATION
			NUMBER_OF_MYPROB_MSGS++;
			#endif

		}
		else
		{

/********************************** DEBUG ***********************************/
#ifdef DEBUG
			if (DEBUG >= INFO)
			{
				fp_log = fopen(name_of_fp_log,"a");
				fprintf(fp_log, "THE CURRENT OBJECT IS NOT AN OUTLIER!!!\n");
				fclose(fp_log);
			}
#endif		
/****************************************************************************/

			/* If the object can not be an outlier and it is the last object of the instance, I have to send a search finished on instance */
			if (is_last_object_of_the_instance)
				send_search_finished_on_instance_messages(Bin_output, ahGetMyRank());
		}
	}

	/* Saves that the object was searched for updating the previous object like the current object */
	*previous_object=current_object;
	
	free_nearest_neighbours(nearest_neighbours);
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Process a Score New Outlier Message */
int process_score_new_outlier_message(distance_type *highest_scores, message_type message, unsigned int number_of_outliers)
{
	bin_number_type *bin_number_msg;
	distance_type *distance_msg;
	distance_type score, worst_score;
	int high_score_saved=0;

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_bin_%d.txt", ahGetMyRank()); 
#endif
/****************************************************************************/	
/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= RECV_MSG_RESUME )
	{
		fp_log = fopen(name_of_fp_log,"a");
		if (DEBUG >= RECV_MSG_VERBOSE )
		{
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "RECEIVING: \t");
			print_score_new_outlier_message(message, fp_log);
			fprintf(fp_log, "END OF MESSAGE\n");
		}
		else
			fprintf(fp_log, "RECEIVING SCORE NEW OUTLIER MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/

	/* Runs the message until the new score */
	bin_number_msg=(bin_number_type *)&(((int *)message)[TO_BIN_BIN_NUMBER_FIELD]);
	distance_msg=(distance_type *)&(bin_number_msg[1]);
	
	/* Gets the new score received */
	score = distance_msg[0];	

	/* Gets the worst score on the highest scores */
	worst_score=get_worst_highest_score(highest_scores, number_of_outliers);

	/* Checks if the score received is really better that the worst score of an outlier */
	if (score > worst_score)
	{
		/* Saves the new score on the highest scores array */
		high_score_saved=save_new_score(highest_scores, score, number_of_outliers);
	}

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	if (DEBUG >= LOGGER)
	{
		fp_log = fopen(name_of_fp_log,"a");
		fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
		fprintf(fp_log, "NEW SCORE SAVED: ");
		fprintf(fp_log, DISTANCE_CONVERSION_STRING, score);
		fprintf(fp_log, "\n");
		print_highest_scores(highest_scores, number_of_outliers, fp_log);
		fprintf(fp_log, "END OF MESSAGE\n");
		fclose(fp_log);
	}
#endif		
/****************************************************************************/
	
	return (high_score_saved);
}

// Sort an bin list using insertion method
int sort_bin_list(bin_list_type *bin_list1, bin_list_type *bin_list2, int number_of_neighbours, dimension_size_type dimension_size)
{

	bin_type current_bin;
	bin_list_pointer pointer_list;
	unsigned int random_position;

	if (!get_first_in_bin_list(bin_list1, &pointer_list))
		return 0;

	while (!is_empty_bin_list(bin_list1))
	{
		if (ORDERED_METRIC!=RANDOM_SORT)
		{
			current_bin = remove_first_of_bin_list(bin_list1);
		
			if (ORDERED_METRIC!=NOT_SORT)
				insert_in_ordered_bin_list(current_bin, bin_list2, number_of_neighbours, dimension_size);
			else 
				insert_in_bin_list(current_bin, bin_list2);
		
		}
		else 
		{
			random_position = drand48() * (get_size_of_bin_list(bin_list1)-1);
			current_bin = remove_of_bin_list_on_position(random_position, bin_list1);
			insert_in_bin_list(current_bin, bin_list2);
		}

	}

	return 1;
}
