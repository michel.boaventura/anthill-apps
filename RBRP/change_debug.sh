#!/bin/bash

## ajeita o initScript rodar uma aplica��o AntHill em uma nova m�quina
## argumento 1: 1 - debug ativado, 0 - debug desativado
## argumento 2: 1 - checagem de mem�ria ativada (Valgrind), 0 - checagem desativada

if [ $# != 2 ] 
then
	echo "Erro dos parametros."
	echo "Execute o script com os seguintes par�metros respectivamente:"
	echo "debug memcheck"
	echo "Exemplo para rodar da m�quina ares sem debug nem memory check: ./script.sh 0 0"
	exit 1
fi

# atualizacao do arquivo conf.xml (host)
sed "4c\		<host name=\"`hostname`\">" <conf.xml >temp.xml
cp -f temp.xml conf.xml
rm -f temp.xml

# atualizacao do arquivo initScript (display e debug)
# Atualiza��o do Display no initScript
sed "13c\DISPLAY=`hostname`:0.0" <initScript >temp.txt

#Atualiza��o da constante de DEBUG do initScript
if [ $1 != 0 ]
then
	sed "16c\DEBUG=1" <temp.txt >temp1.txt
else
	sed "16c\#DEBUG=1" <temp.txt >temp1.txt
fi

#Atualiza��o da constante de MEMCHECK do initScript para a depura��o com o Valgrind
if [ $2 != 0 ]
then
	sed "19c\MEMCHECK=1" <temp1.txt>temp2.txt
else
	sed "19c\#MEMCHECK=1" <temp1.txt>temp2.txt
fi

cp -f temp2.txt initScript 
rm -f temp*.txt

killall -9 gdb
killall -9 ddd
killall -9 main
killall -9 initScript
