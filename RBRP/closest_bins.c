/* Inclusoes. */
#include "closest_bins.h"

/* Print the values of the closest bins, showing the closest bins and the distances of a bin */
int print_closest_bins(closest_bins_type *closest_bins, bin_number_type bin_number, unsigned int number_of_bins, FILE *fp_log)
{
	int i;
	fprintf(fp_log, "Closest Bins of the Bin: %llu\n", bin_number);
	if (closest_bins->bin_pointers!=NULL)
	{
		fprintf(fp_log, "Bin Numbers: ");
		
		/* Print the array that keeps the number of the closest bins.
		 * Its length is the total number of bins.
		 * This array includes the bin itself like your neighbour*/ 
		for(i = 0; i < number_of_bins; i++)
			fprintf(fp_log, "%llu ", (closest_bins->bin_pointers[i])->number);

		
		fprintf(fp_log, "\n");
		fprintf(fp_log, "Distance: ");
		
		/* Print the array that keeps the distance of the closest bins */
		print_distance_type_array(fp_log, closest_bins->distance, number_of_bins);	
		
		fprintf(fp_log, "\n");
	}
	else 
		fprintf(fp_log, "The array of the closest bins is EMPTY!!!\n");
	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Frees the memory allocated for the structures of an array of closest bins */ 
int free_closest_bins(closest_bins_type closest_bins)
{
	if (closest_bins.bin_pointers!=NULL)
	{
		free(closest_bins.bin_pointers);
		free(closest_bins.distance);
		return 1;
	}
	else 
		return 0;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Allocates the memory for the structures of the array of closest bins */
int malloc_closest_bins(closest_bins_type *closest_bins, unsigned int number_of_bins)
{
	/* Initialize the size */
	closest_bins->size=0;
	
	/* The bin is considered a neighbour of itself, then the number_of_bins is equal the number of neighbours of a bin */ 
	closest_bins->bin_pointers=malloc((number_of_bins)*sizeof(bin_type*));
	closest_bins->distance=malloc((number_of_bins)*sizeof(distance_type));

	return 1;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* Insert new Values in the closest bins */
int insert_in_closest_bins(closest_bins_type *closest_bins, bin_type *bin_pointer, distance_type distance)
{
	int position, i;
	int already_inserted_bins;
	
	/* Gets the number of bins that already have been inserted */
	already_inserted_bins=closest_bins->size;
	
	/* Searches for the position that the bin will be inserted the bin*/
	for (position=0; position<already_inserted_bins; position++)
	{
		if (closest_bins->distance[position] > distance)
			break;
	}

	/* Move forward the distance values that is greater that the current distance to save the new value in the position that it have to be inserted */  
	for (i=already_inserted_bins; i>position; i--)	
	{
		closest_bins->distance[i]=closest_bins->distance[i-1];
		closest_bins->bin_pointers[i]=closest_bins->bin_pointers[i-1];
	}

	/* Saves the new values on the closest bins */
	closest_bins->distance[position]=distance;
	closest_bins->bin_pointers[position]=bin_pointer;
		
	/* Updates the size of the current bins */ 
	closest_bins->size++;
	
	return 1;
}

