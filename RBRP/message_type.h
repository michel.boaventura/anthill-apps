#ifndef _MESSAGE_TYPE_H
#define _MESSAGE_TYPE_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"

/* Prototypes */
void print_msg_list(msg_list_type *list, FILE *file);
void free_msg_cell(msg_list_pointer q);
void print_message(message_type message, FILE *fp_log);
int compare_messages(message_type msg1, message_type msg2);
void free_message(message_type message);
int malloc_message(message_type *message, int size);

#endif

