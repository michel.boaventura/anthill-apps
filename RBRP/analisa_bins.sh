#!/bin/bash
## argumento1: Nome do diretorio1 onde estão os arquivos de instrumentacao dos bins
## argumento2: Nome do diretorio2 onde estão será gravado o arquivo de analise
## argumento3: Nome do arquivo de entrada
## argumento4: Numero de instancias bins

test -e

if [ $# != 4 ] 
	then
      		echo "Erro nos parâmetros do script."
		echo "Execute o script da seguinte forma: "
		echo "analisa_bins.sh diretorio1 diretorio2 arquivo_entrada numero_instancias_bin" 
        	exit 1
	fi

	DIRETORIO_1=$1
        DIRETORIO_2=$2
	FILE_ENTRADA=$3	
	NUMERO_INSTANCIAS=$4

	echo "cd ${DIRETORIO_1}"	
	cd ${DIRETORIO_1}
	echo "REMOVENDO OS ARQUIVOS ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.analise"
	rm ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise

	echo "REMOVENDO OS ARQUIVOS ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.balanceamento"
	rm ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.balanceamento

	# Procura os numeros de bins em todos os arquivos e realiza a soma.
	NUMERO_BINS_TOTAL=`grep "NUMBER OF BINS CREATED" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_bins += $6}  END {printf ("%d\n",num_bins);}'`

	# Calcula o numero medio de bins por instancia
	MEDIA_BINS_POR_INSTANCIA=`echo "${NUMERO_BINS_TOTAL} / ${NUMERO_INSTANCIAS}" | bc -l` 

	# Procura os numeros de objetos em todos os arquivos e realiza a soma.
	NUMERO_OBJETOS_TOTAL=`grep "NUMBER OF POINTS IN ALL BINS OF THIS INSTANCE" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_obj += $10}  END {printf ("%d\n",num_obj);}'`

	# Calcula o numero medio de objetos por instancia
	MEDIA_OBJETOS_POR_INSTANCIA=`echo "${NUMERO_OBJETOS_TOTAL} / ${NUMERO_INSTANCIAS}" | bc -l`

	# Calcula o numero medio de objetos por bin	
	MEDIA_OBJETOS_POR_BIN=`echo "${NUMERO_OBJETOS_TOTAL} / ${NUMERO_BINS_TOTAL}" | bc -l`

	# Procura as diagonais de bins em todos os arquivos e realiza a soma.
	NUMERO_DIAGONAL_TOTAL=`grep "Bin Diagonal Length:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_diag += $10}  END {printf ("%d\n",num_diag);}'`

	# Calcula o numero medio de diagonal por bin
	MEDIA_DIAGONAL_POR_BIN=`echo "${NUMERO_DIAGONAL_TOTAL} / ${NUMERO_BINS_TOTAL}" | bc -l`

	# Procura numero comparacoes de objetos em todos os arquivos e realiza a soma.
	NUMERO_COMPARACOES_TOTAL=`grep "NUMBER OF COMPARATIONS BETWEEN OBJECTS:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_comp += $6}  END {printf ("%d\n",num_comp);}'`
	
	# Calcula o numero medio de comparacoes por bin
	MEDIA_COMPARACOES_POR_BIN=`echo "${NUMERO_COMPARACOES_TOTAL} / ${NUMERO_BINS_TOTAL}" | bc -l`
	
	# Calcula o numero medio de comparacoes por instancia
	MEDIA_COMPARACOES_POR_INSTANCIA=`echo "${NUMERO_COMPARACOES_TOTAL} / ${NUMERO_INSTANCIAS}" | bc -l`

	# Calcula o numero medio de comparacoes por objeto
	MEDIA_COMPARACOES_POR_OBJETO=`echo "${NUMERO_COMPARACOES_TOTAL} / ${NUMERO_OBJETOS_TOTAL}" | bc -l`

	# Procura numero eliminacoes no mesmo bin em todos os arquivos e realiza a soma.
	NUMERO_ELIMINACOES_MESMO_BIN_TOTAL=`grep "NUMBER OF POINTS OF THIS INSTANCE ELIMINATED IN FIRST BIN OF THIS INSTANCE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_mes_bin += $14}  END {printf ("%d\n",num_mes_bin);}'`
	
	# Calcula porcentagem de eliminacoes no mesmo bin em relacao ao total de objetos
	PORCENTAGEM_ELIMINACOES_MESMO_BIN=`echo "${NUMERO_ELIMINACOES_MESMO_BIN_TOTAL} / ${NUMERO_OBJETOS_TOTAL} * 100" | bc -l`
	# Procura numero eliminacoes na mesma instancia em todos os arquivos e realiza a soma.
	NUMERO_ELIMINACOES_MESMA_INSTANCIA_TOTAL=`grep "NUMBER OF POINTS OF THIS INSTANCE ELIMINATED IN THIS INSTANCE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_mes_inst += $11}  END {printf ("%d\n",num_mes_inst);}'`

	# Calcula porcentagem de eliminacoes na mesma instancia em relacao ao total de objetos
	PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA=`echo "${NUMERO_ELIMINACOES_MESMA_INSTANCIA_TOTAL} / ${NUMERO_OBJETOS_TOTAL} * 100" | bc -l`
	
	# Procura numero eliminacoes no primeiro bin de outras instancias em todos os arquivos e realiza a soma.
	NUMERO_ELIMINACOES_OUTRO_BIN_TOTAL=`grep "NUMBER OF POINTS OF OTHER INSTANCES ELIMINATED IN FIRST BIN OF THIS INSTANCE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_out_bin += $14}  END {printf ("%d\n",num_out_bin);}'`
	
	# Calcula porcentagem de eliminacoes no primeiro bin de outra instancia em relacao ao total de objetos
	PORCENTAGEM_ELIMINACOES_OUTRO_BIN=`echo "${NUMERO_ELIMINACOES_OUTRO_BIN_TOTAL} / ${NUMERO_OBJETOS_TOTAL} * 100" | bc -l`
	# Procura numero eliminacoes em outra instancia em todos os arquivos e realiza a soma.
	NUMERO_ELIMINACOES_OUTRA_INSTANCIA_TOTAL=`grep "NUMBER OF POINTS OF OTHERS INSTANCES ELIMINATED IN THIS INSTANCE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_out_inst += $11}  END {printf ("%d\n",num_out_inst);}'`

	# Calcula porcentagem de eliminacoes em outra instancia em relacao ao total de objetos
	PORCENTAGEM_ELIMINACOES_OUTRA_INSTANCIA=`echo "${NUMERO_ELIMINACOES_OUTRA_INSTANCIA_TOTAL} / ${NUMERO_OBJETOS_TOTAL} * 100" | bc -l`

	# Procura numero eliminacoes em score em todos os arquivos e realiza a soma.
	NUMERO_ELIMINACOES_SCORE_TOTAL=`grep "NUMBER OF POINTS OF OTHERS INSTANCES ELIMINATED IN SCORE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_score += $10}  END {printf ("%d\n",num_score);}'`

	# Calcula porcentagem de eliminacoes em outra instancia em relacao ao total de objetos
	PORCENTAGEM_ELIMINACOES_SCORE=`echo "${NUMERO_ELIMINACOES_SCORE_TOTAL} / ${NUMERO_OBJETOS_TOTAL} * 100" | bc -l`
	
	# Procura numero de splits em todos os arquivos e realiza a soma.
	NUMERO_SPLITS_TOTAL=`grep "NUMBER OF SPLITS IN BINS OF THIS INSTANCE:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_*.inst | awk '{num_splits += $9}  END {printf ("%d\n",num_splits);}'`

	# Calcula media de splits por instancia
	MEDIA_SPLITS_POR_INSTANCIA=`echo "${NUMERO_SPLITS_TOTAL} / ${NUMERO_INSTANCIAS}" | bc -l`

	# Calcula media de splits por bin
	MEDIA_SPLITS_POR_BIN=`echo "${NUMERO_SPLITS_TOTAL} / ${NUMERO_BINS_TOTAL}" | bc -l`

	# echo "cd .."	
	# cd ${DIRETORIO_2}

	echo "********VALORES TOTAIS*********" > ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_INSTANCIAS_TOTAL ${NUMERO_INSTANCIAS}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_BINS_TOTAL ${NUMERO_BINS_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_OBJETOS_TOTAL ${NUMERO_OBJETOS_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_COMPARACOES_TOTAL ${NUMERO_COMPARACOES_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_ELIMINACOES_MESMO_BIN_TOTAL ${NUMERO_ELIMINACOES_MESMO_BIN_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_ELIMINACOES_OUTRO_BIN_TOTAL ${NUMERO_ELIMINACOES_OUTRO_BIN_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_ELIMINACOES_MESMA_INSTANCIA_TOTAL ${NUMERO_ELIMINACOES_MESMA_INSTANCIA_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_ELIMINACOES_OUTRA_INSTANCIA_TOTAL ${NUMERO_ELIMINACOES_OUTRA_INSTANCIA_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_SPLITS_TOTAL ${NUMERO_SPLITS_TOTAL}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
 
	echo "********ANALISE POR INSTANCIA*********" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_BINS_POR_INSTANCIA ${MEDIA_BINS_POR_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_OBJETOS_POR_INSTANCIA ${MEDIA_OBJETOS_POR_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_COMPARACOES_POR_INSTANCIA ${MEDIA_COMPARACOES_POR_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_SPLITS_POR_INSTANCIA ${MEDIA_SPLITS_POR_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise

	echo "********ANALISE POR BIN*********" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_OBJETOS_POR_BIN ${MEDIA_OBJETOS_POR_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_DIAGONAL_POR_BIN ${MEDIA_DIAGONAL_POR_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_COMPARACOES_POR_BIN ${MEDIA_COMPARACOES_POR_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_SPLITS_POR_BIN ${MEDIA_SPLITS_POR_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise

	echo "********ANALISE POR OBJETO*********" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "NUMERO_MEDIO_COMPARACOES_POR_OBJETO ${MEDIA_COMPARACOES_POR_OBJETO}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise

	echo "********EFICIENCIA PODA*********" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "PORCENTAGEM_ELIMINACOES_MESMO_BIN ${PORCENTAGEM_ELIMINACOES_MESMO_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "PORCENTAGEM_ELIMINACOES_OUTRO_PRIMEIRO_BIN ${PORCENTAGEM_ELIMINACOES_OUTRO_BIN}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA ${PORCENTAGEM_ELIMINACOES_MESMA_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise
	echo "PORCENTAGEM_ELIMINACOES_OUTRA_INSTANCIA ${PORCENTAGEM_ELIMINACOES_OUTRA_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.analise

	
	#criando arquivo de analise de balanceamento
	for NUM_INSTANCIA in `seq 0 $((NUMERO_INSTANCIAS - 1))`
	do
		# numero comparacoes de objetos por instancia sem a soma.
		NUMERO_COMPARACOES_DE_CADA_INSTANCIA=`grep "NUMBER OF COMPARATIONS BETWEEN OBJECTS:" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.inst | awk '{num_bins = $6}  END {printf ("%d\n",num_bins);}' `
		# Procura os numeros de objetos em todos os arquivos e realiza a soma.
		NUMERO_OBJETOS_DE_CADA_INSTANCIA=`grep "NUMBER OF POINTS IN ALL BINS OF THIS INSTANCE" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.inst | awk '{num_bins = $10}  END {printf ("%d\n",num_bins);}' ` 
		NUMERO_BINS_DE_CADA_INSTANCIA=`grep "NUMBER OF BINS CREATED :" ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.inst | awk '{num_bins = $6}  END {printf ("%d\n",num_bins);}' `
		echo "${NUM_INSTANCIA} ${NUMERO_COMPARACOES_DE_CADA_INSTANCIA} ${NUMERO_OBJETOS_DE_CADA_INSTANCIA} ${NUMERO_BINS_DE_CADA_INSTANCIA}" >> ${FILE_ENTRADA}.${NUMERO_INSTANCIAS}_instancias.balanceamento
	done
