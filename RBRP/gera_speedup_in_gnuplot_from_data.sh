#!/bin/bash

	if [ $# != 2 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "gera_bins_in_gnuplot.sh arquivo_de_dados arquivo_de_saida" 
        	exit 1
	fi

	EPS_FILE=$2.eps
	GNUPLOT_FILE=$2.gp
	DATA_FILE=$1


	#Gerando o in�cio do arquivo gnuplot
	echo "
#!/usr/bin/env gnuplot
#reset

set terminal postscript enhanced eps# color
#set terminal png medium
#set term png 25 #  color

#set key left top
set key top right

# Configura��es de codifica��o de caractere
set encoding default
set encoding iso_8859_1

# Plotando os clusters produzidos sinteticamente para o Algoritmo RBRP 

# Escolhendo onde aparecer a legenda
set key box
#set key outside
set title \"KddcupBINSIZE500 PRUNING\"

# Configura��es de escala
set autoscale
#set logscale x 10
#set nologscale

# Definindo o T�tulo dos eixos a serem desenhados
set xlabel \"N�mero de M�quinas\"
#set ylabel \"Tempo de Execucao (s)\"

#set parametric
#set grid

# Definindo os limites dos eixos
set xrange [0:]
set yrange [0:]

# Definindo o espa�amento entre dois valores no eixo y
#set xtics 5
#set ytics 5

#set format x \"10^{%L}\"

# Defini��o dos par�metros das fun��es
a=1
b=0 

#Defini��o de uma fun��o linear
f(x)= a*x + b

#Ajuste linear da fun��o definida
#fit f(x) \"${DATA_FILE}\" using 1:((\$8-\$2)/(\$9-\$3)) via a,b

# Definindo o nome do arquivo de sa�da
set output \"${EPS_FILE}\"

#Plotagem dos pontos

plot [][] \\" > ${GNUPLOT_FILE}

	# Plotando o speedup do tempo de Distribui��o da base de Dados
#	echo "	'${DATA_FILE}' using 1:((\$2/\$3)) title \"Speedup Distribui��o\" with linespoints pointsize 1.0, \\" >> ${GNUPLOT_FILE}
	# Plotando o speedup do tempo de execu��o da Primeira Fase do Algoritmo
	echo "	'${DATA_FILE}' using 1:((\$4/\$5)) title \"Speedup Fase 1\" with linespoints pointsize 2.0, \\" >> ${GNUPLOT_FILE}
	# Plotando o speedup do tempo de Distribui��o da base de Dados do Filtro Assigner para o Filtro Bin
#	echo "	'${DATA_FILE}' using 1:((\$6/\$7)) title \"Speedup Distribui��o Assigner-Bin\" with linespoints pointsize 1.0, \\" >> ${GNUPLOT_FILE}


	# Plotando o speedup do tempo de execu��o da Segunda Fase do Algoritmo
	echo "	'${DATA_FILE}' using 1:((\$8/\$9)) title \"Speedup Fase 2\" with linespoints pointsize 2.0, \\" >> ${GNUPLOT_FILE}
	# Plotando o speedup do tempo de execu��o do Algoritmo RETIRANDO os tempos de distribui��o da base de dados
	#echo "	'${DATA_FILE}' using 1:(((\$4+\$8)/(\$5+\$9))) title \"Speedup Minera��o\" with linespoints pointsize 1.0, \\" >> ${GNUPLOT_FILE}
	# Plotando o speedup do tempo de execu��o do Algoritmo RETIRANDO apenas o tempo de distribui��o da base de dados inicial
	echo "	'${DATA_FILE}' using 1:((\$4+\$6+\$8)/(\$5+\$7+\$9)) title \"Speedup Algoritmo\" with linespoints pointsize 2.0, \\" >> ${GNUPLOT_FILE}
	# Plotando o speedup do tempo de execu��o do Algoritmo total CONTANDO os tempos de distribui��o da base de dados
	#echo "	'${DATA_FILE}' using 1:((\$10/\$11)) title \"Speedup Total\" with linespoints pointsize 2.0, \\" >> ${GNUPLOT_FILE}
	echo "	f(x) t \"Speedup Linear\"" >> ${GNUPLOT_FILE}

	# Gerando o gr�fico com o Gnuplot
	echo "Gerando o gr�fico com o Gnuplot"
	echo "gnuplot ${GNUPLOT_FILE}"
	gnuplot ${GNUPLOT_FILE}

	# Visualizando o gr�fico gerado
	echo "Visualizando o gr�fico gerado"
	echo "gv ${EPS_FILE} &"
	gv ${EPS_FILE} &

