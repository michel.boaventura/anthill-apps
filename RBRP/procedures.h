#ifndef PROCEDURES_H
#define PROCEDURES_H
#include "rbrp.h"

void get_information_from_work(void *work, char *input_file_name, char *output_file_name, dimension_size_type *dimension_size, int *number_of_outliers, int *number_of_neighbours, int *bin_size);

int print_int_array(FILE* file, int *array, unsigned int number_of_elements);
int print_unsigned_int_array(FILE* file, unsigned int *array, unsigned int number_of_elements);
int print_unsigned_long_long_array(FILE* file, unsigned long long *array, unsigned int number_of_elements);
int print_distance_type_array(FILE *file, distance_type *array, unsigned int number_of_elements);
bin_list_pointer find_closest_bin(list_pointer object, bin_list_pointer *bins, dimension_size_type dimension_size);
bin_list_pointer find_closest_bin_in_bin_list(object_type *object, bin_list_type *bin_list, dimension_size_type dimension_size);
int calculate_bin_local_values(bin_list_pointer bin, dimension_size_type dimension_size);
int try_getting_message(message_type *new_message, InputPortHandler port);

#ifdef GET_TIME
int print_time(struct timeval *start, struct timeval *end, char *name_of_file);
#endif

#endif

