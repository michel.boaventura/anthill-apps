#ifndef _CLOSEST_BINS_H
#define _CLOSEST_BINS_H
#include "structs.h"
#include "stdio.h"
#include "stdlib.h"
#include "procedures.h"
/* Definitions */
#define NEXT_CLOSEST_BIN_NOT_FOUND -1

/* Prototypes */
int print_closest_bins(closest_bins_type *closest_bins_type, bin_number_type bin_number, unsigned int number_of_bins, FILE *fp_log);
int free_closest_bins(closest_bins_type closest_bins);
int malloc_closest_bins(closest_bins_type *closest_bins, unsigned int number_of_bins);
int insert_in_closest_bins(closest_bins_type *closest_bins, bin_type *bin_pointer, distance_type distance);
bin_number_type next_closest_bin(closest_bins_type *closest_bins, bin_number_type **pointer_to_current_bin_number);
bin_number_type get_closest_bin(closest_bins_type *closest_bins, bin_number_type **pointer_to_current_bin_number);
int is_last_closest_bin(closest_bins_type *closest_bins, bin_number_type current_bin, unsigned int number_of_bins);

#endif

