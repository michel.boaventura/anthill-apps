#!/bin/bash
## Executa o rbrp para v�rios arquivos diferentes no diretorio de entrada e armazena 
## os tempos de execu��o no diret�rio de tempos.
## argumento1: Nome do arquivo de entrada
## argumento2: Nome do diretorio onde ser�o armazenados os tempos de execu��o
## argumento3: N�mero de atributos reais do arquivo de entrada
## argumento4: N�mero de atributos ordinais do arquivo de entrada
## argumento5: N�mero de categoricos do arquivo de entrada

## Numero de maquinas de processamento
INSTANCIAS='10 8 6 4 2 1'

## O numero de tentativas que o teste seráexecutado
NUM_TENTATIVAS=3

if [ $# != 3 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "computa_todas_instancias.sh diretorio_tempos arquivo_entrada arquivo_saida" 
	exit 1
fi

DIRETORIO_TEMPOS=$1

ARQUIVO_ENTRADA=$2

ARQUIVO_SAIDA=$3

if [ -e ${ARQUIVO_SAIDA} ]
then
	echo "Deletando o arquivo de sa�da j� existente..."
	rm -fv ${ARQUIVO_SAIDA}
fi

#Obtem os valores dos tempos com uma �nica instancia
echo "-------------------------------------------------------------------\n"
echo "Obtendo os valores para a execu��o com uma �nica inst�ncia..."

NUM_INSTANCIAS=1
NUM_INSTANCIA=0

ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.assigner_${NUM_INSTANCIA}.time
if [ ! -e ${ARQUIVO_TEMPOS} ]
then
	echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
	exit 1;
fi		

# Obtendo o primeiro tempo do arquivo de tempos do Filtro Assigner que indica o
# Tempo decorrido durante o recebimento da base de dados
MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO=0
TEMPO=`sed "2,3d" ${ARQUIVO_TEMPOS}`
if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO} | bc -l `))
then
	MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO=${TEMPO}
fi

# Obtendo o segundo tempo do arquivo de tempos do Filtro Assigner que indica o
# Tempo decorrido entre o final da distribui��o e o t�rmino da primeira fase do algoritmo, sem a distribui��o dos objetos para o Filtro Bin
MAIOR_TEMPO_ASSIGNER_FASE1=0
TEMPO=`sed -e "1d" -e "3d" ${ARQUIVO_TEMPOS}`
if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_FASE1} | bc -l`))
then
	MAIOR_TEMPO_ASSIGNER_FASE1=${TEMPO}
fi

# Obtendo o terceiro tempo do arquivo de tempos do Filtro Assigner que indica o
# Tempo decorrido durante a distribui��o dos objetos do Filtro Assigner ao Filtro Bin. 
# Ou seja, t�rmino da minera��o da primeira fase e t�rmino da distribui��o dos objetos ao Filtro Bin e termina��o da execu��o do Filtro Assigner
MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN=0
TEMPO=`sed "1,2d" ${ARQUIVO_TEMPOS}`

if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN} | bc -l`))
then
	MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN=${TEMPO}
fi

ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.time
if [ ! -e ${ARQUIVO_TEMPOS} ]
then
	echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
	exit 1;
fi		

# Obtendo o primeiro tempo do arquivo de tempos do Filtro Bin que indica o
# Tempo decorrido desde o in�cio da execu��o at� o final da primeira fase
MAIOR_TEMPO_BIN_FASE1=0
TEMPO=`sed "2,8d" ${ARQUIVO_TEMPOS}`
if ((`echo  ${TEMPO} \> ${MAIOR_TEMPO_BIN_FASE1} | bc -l`))
then
	MAIOR_TEMPO_BIN_FASE1=${TEMPO}
fi

#Retirando os tempos de distribui��o da base de dados para o Filtro Assigner e de distribui��o da base de dados para o Filtro Bin
echo "Retirando os tempos de distribui��o do MAIOR TEMPO BIN FASE1: $MAIOR_TEMPO_BIN_FASE1 Distribuicao Inicial: ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO} Distribuicao Assigner to Bin: ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN}"
MAIOR_TEMPO_BIN_FASE1=`echo "${MAIOR_TEMPO_BIN_FASE1} - ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO}" - ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN} | bc -l`

echo "MAIOR TEMPO BIN FASE 1: ${MAIOR_TEMPO_BIN_FASE1}"
MAIOR_TEMPO_FASE1=${MAIOR_TEMPO_BIN_FASE1}

# Considera o maior tempo da Fase 1 como o maior tempo de qualquer instancia do Filtro Bin e do Filtro Assigner
if ((`echo ${MAIOR_TEMPO_ASSIGNER_FASE1} \> ${MAIOR_TEMPO_FASE1} | bc -l`))
then
	MAIOR_TEMPO_FASE1=${MAIOR_TEMPO_ASSIGNER_FASE1}
	echo "Assigner Fase 1 � maior, atualizando Tempo Fase 1 para ${MAIOR_TEMPO_FASE1}"
fi

echo "TEMPO BIN FASE1: ${MAIOR_TEMPO_BIN_FASE1} TEMPO ASSIGNER FASE 1: ${MAIOR_TEMPO_ASSIGNER_FASE1} MAIOR TEMPO FASE 1 = ${MAIOR_TEMPO_FASE1}"

MAIOR_TEMPO_BIN_FASE2=0

ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.time
if [ ! -e ${ARQUIVO_TEMPOS} ]
then
	echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
	exit 1;
fi		

# Obtendo o segundo tempo do arquivo de tempos do Filtro Bin que indica o
# Tempo decorrido durante a segunda fase do algoritmo
TEMPO=`sed -e "1d" -e "3,8d" ${ARQUIVO_TEMPOS}`
if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_BIN_FASE2} | bc -l`))
then
	MAIOR_TEMPO_BIN_FASE2=${TEMPO}
fi

ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.time
if [ ! -e ${ARQUIVO_TEMPOS} ]
then
	echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
	exit 1;
fi

# Obtendo o tempo do arquivo de tempos do Filtro Final que indica o
# Tempo decorrido durante toda a execu��o do algoritmo
TEMPO_FINAL_FASE2=`more ${ARQUIVO_TEMPOS}`

echo "MAIORES TEMPOS DE EXECU��O PARA O SEQ�ENCIAL: "
echo "Tempo Distribui��o da Base de Dados para o Filtro Assigner: $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO"
echo "PRIMEIRA FASE:"
echo "Filtro Assigner: ${MAIOR_TEMPO_ASSIGNER_FASE1} Filtro Bin: ${MAIOR_TEMPO_BIN_FASE1} Fase1: ${MAIOR_TEMPO_FASE1}"
echo "Tempo Distribui��o da Base de Dados para o Filtro Bin: $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN"
echo "SEGUNDA FASE:"
echo "Filtro Bin: ${MAIOR_TEMPO_BIN_FASE2}"
echo "Tempo Total: ${TEMPO_FINAL_FASE2}"
echo "-------------------------------------------------------------------\n"

# Salvando os valores do Tempo Sequencial
MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_SEQUENCIAL=${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO}
MAIOR_TEMPO_FASE1_SEQUENCIAL=${MAIOR_TEMPO_FASE1}
MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN_SEQUENCIAL=${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN}
MAIOR_TEMPO_BIN_FASE2_SEQUENCIAL=${MAIOR_TEMPO_BIN_FASE2}
TEMPO_FINAL_FASE2_SEQUENCIAL=${TEMPO_FINAL_FASE2}

echo "#N�mero de instancias	Tempo_Distribuicao_Sequencial	Tempo_Distribuicao	Tempo_Fase1_Sequencial	Tempo_Fase1	Tempo_Distribuicao_Bin_Sequencial	Tempo_Distribuicao_Bin	Tempo_Fase2_Sequencial	Tempo_Fase2	Tempo_Total_Sequencial	Tempo_Total" > ${ARQUIVO_SAIDA}

# Obtendo os tempos de execu��o para cada n�mero de inst�ncias existente
for NUM_INSTANCIAS in $INSTANCIAS
do
	echo "-------------------------------------------------------------------\n"
	echo "Computando os dados da execu��o com ${NUM_INSTANCIAS} instancias..."

	MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO=0
	#echo "Calculando o maior tempo de execu��o da primeira fase do Filtro Assigner"
	for NUM_INSTANCIA in `seq 0 $((NUM_INSTANCIAS -1))`
	do 
		ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.assigner_${NUM_INSTANCIA}.time
		if [ ! -e ${ARQUIVO_TEMPOS} ]
		then
			echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
			exit 1;
		fi		
		
		# Obtendo o primeiro tempo do arquivo de tempos do Filtro Assigner que indica o
		# Tempo decorrido durante o recebimento da base de dados
		TEMPO=`sed "2,3d" ${ARQUIVO_TEMPOS}`
		#echo "Comparando os tempos: TEMPO:${TEMPO} > MAIOR_TEMPO:${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO}"
		#echo "valor: $((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO} | bc -l`))"
		if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO} | bc -l`))
		then
			#echo "Atualizando o maior tempo para ${TEMPO}"
			MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO=${TEMPO}
		fi
	done

	MAIOR_TEMPO_ASSIGNER_FASE1=0
	#echo "Calculando o maior tempo de execu��o da primeira fase do Filtro Assigner"
	for NUM_INSTANCIA in `seq 0 $((NUM_INSTANCIAS -1))`
	do 
		ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.assigner_${NUM_INSTANCIA}.time
		if [ ! -e ${ARQUIVO_TEMPOS} ]
		then
			echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
			exit 1;
		fi		

		# Obtendo o segundo tempo do arquivo de tempos do Filtro Assigner que indica o
		# Tempo decorrido entre o final da distribui��o e o t�rmino da primeira fase do algoritmo, sem a distribui��o dos objetos para o Filtro Bin
		TEMPO=`sed -e "1d" -e "3d" ${ARQUIVO_TEMPOS}`
		if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_FASE1} | bc -l`))
		then
			MAIOR_TEMPO_ASSIGNER_FASE1=${TEMPO}
		fi
	done

	MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN=0
	for NUM_INSTANCIA in `seq 0 $((NUM_INSTANCIAS -1))`
	do
		ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.assigner_${NUM_INSTANCIA}.time
		if [ ! -e ${ARQUIVO_TEMPOS} ]
		then
			echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
			exit 1;
		fi	

		# Obtendo o terceiro tempo do arquivo de tempos do Filtro Assigner que indica o
		# Tempo decorrido durante a distribui��o dos objetos do Filtro Assigner ao Filtro Bin. 
		# Ou seja, t�rmino da minera��o da primeira fase e t�rmino da distribui��o dos objetos ao Filtro Bin e termina��o da execu��o do Filtro Assigner
		TEMPO=`sed "1,2d" ${ARQUIVO_TEMPOS}`
		if ((`echo  ${TEMPO} \> ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN} | bc -l`))
		then
			MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN=${TEMPO}
		fi
 	done

	MAIOR_TEMPO_BIN_FASE1=0
	#echo "Calculando o maior tempo de execu��o da primeira fase do Filtro Bin"
	for NUM_INSTANCIA in `seq 0 $((NUM_INSTANCIAS -1))`
	do 
		ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.time
		if [ ! -e ${ARQUIVO_TEMPOS} ]
		then
			echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
			exit 1;
		fi		
	
		# Obtendo o primeiro tempo do arquivo de tempos do Filtro Bin que indica o
		# Tempo decorrido desde o in�cio da execu��o at� o final da primeira fase
		TEMPO=`sed "2,8d" ${ARQUIVO_TEMPOS}`
		if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_BIN_FASE1} | bc -l`))
		then
			MAIOR_TEMPO_BIN_FASE1=${TEMPO}
		fi
	done

	#Retirando os tempos de distribui��o da base de dados para o Filtro Assigner e de distribui��o da base de dados para o Filtro Bin
	MAIOR_TEMPO_BIN_FASE1=`echo ${MAIOR_TEMPO_BIN_FASE1} - ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO} - ${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN} | bc -l`

	MAIOR_TEMPO_FASE1=${MAIOR_TEMPO_BIN_FASE1}

	# Considera o maior tempo da Fase 1 como o maior tempo de qualquer instancia do Filtro Bin e do Filtro Assigner
	if ((`echo ${MAIOR_TEMPO_ASSIGNER_FASE1} \> ${MAIOR_TEMPO_FASE1} | bc -l`))
	then
		MAIOR_TEMPO_FASE1=${MAIOR_TEMPO_ASSIGNER_FASE1}
	fi

	echo "MAIOR TEMPO BIN FASE1: ${MAIOR_TEMPO_BIN_FASE1} MAIOR TEMPO ASSIGNER FASE 1: ${MAIOR_TEMPO_ASSIGNER_FASE1} MAIOR TEMPO FASE 1 = ${MAIOR_TEMPO_FASE1}"
	
	MAIOR_TEMPO_BIN_FASE2=0
	#echo "Calculando o maior tempo de execu��o da primeira fase pelo Filtro Bin"
	for NUM_INSTANCIA in `seq 0 $((NUM_INSTANCIAS - 1))`
	do 
		ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.bin_${NUM_INSTANCIA}.time
		if [ ! -e ${ARQUIVO_TEMPOS} ]
		then
			echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
			exit 1;
		fi		
	
		# Obtendo o segundo tempo do arquivo de tempos do Filtro Bin que indica o
		# Tempo decorrido durante a segunda fase do algoritmo
		TEMPO=`sed -e "1d" -e "3,8d" ${ARQUIVO_TEMPOS}`
		if ((`echo ${TEMPO} \> ${MAIOR_TEMPO_BIN_FASE2} | bc -l`))
		then
			MAIOR_TEMPO_BIN_FASE2=${TEMPO}
		fi
	done

	ARQUIVO_TEMPOS=${DIRETORIO_TEMPOS}/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias.time
	if [ ! -e ${ARQUIVO_TEMPOS} ]
	then
		echo "Arquivo ${ARQUIVO_TEMPOS} com o tempo da execu��o com ${NUM_INSTANCIAS} n�o encontrado!!!"
		exit 1;
	fi
	
	# Obtendo o tempo do arquivo de tempos do Filtro Final que indica o
	# Tempo decorrido durante toda a execu��o do algoritmo
	TEMPO_FINAL_FASE2=`more ${ARQUIVO_TEMPOS}`

	echo "Maiores tempos de execu��o:"
	echo "Tempo Distribui��o da Base de Dados para o Filtro Assigner: $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO"
	echo "Primeira Fase:"
	echo "Filtro Assigner: ${MAIOR_TEMPO_ASSIGNER_FASE1} Filtro Bin: ${MAIOR_TEMPO_BIN_FASE1} Fase1: ${MAIOR_TEMPO_FASE1}"
	echo "Tempo Distribui��o da Base de Dados para o Filtro Bin: $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN"
	echo "Segunda Fase:"
	echo "Filtro Bin: ${MAIOR_TEMPO_BIN_FASE2}"
	echo "Tempo Distribui��o da Base de Dados: $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO"
	echo "Tempo Total: ${TEMPO_FINAL_FASE2}"
	echo "-------------------------------------------------------------------\n"

	echo "${NUM_INSTANCIAS}	${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_SEQUENCIAL}	${MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO}	${MAIOR_TEMPO_FASE1_SEQUENCIAL}	${MAIOR_TEMPO_FASE1}	$MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN_SEQUENCIAL $MAIOR_TEMPO_ASSIGNER_DISTRIBUICAO_ASSIGNER_TO_BIN ${MAIOR_TEMPO_BIN_FASE2_SEQUENCIAL}	${MAIOR_TEMPO_BIN_FASE2}	${TEMPO_FINAL_FASE2_SEQUENCIAL}	${TEMPO_FINAL_FASE2}" >> ${ARQUIVO_SAIDA}

done

