#!/bin/bash

if [ $# != 1 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "paste_experiments.sh diretorio_experimentos" 
        exit 1
fi

DIRETORIO_CORRENTE=`pwd`
DIRETORIO_EXPERIMENTOS=$1

DIRETORIO_CORRENTE=`pwd`

echo "Procurando pelos diret�rios com os experimentos para criar o arquivo de instrumentacao"

cd ${DIRETORIO_EXPERIMENTOS}


for DIRETORIO in *
do
	echo "Testando como diret�rio: ${DIRETORIO}"
	if [ -d ${DIRETORIO} ];
	then
		cd ${DIRETORIO}
		echo "Criando arquivo inst para experimento ${DIRETORIO}"
		echo "criando_inst.sh"
		${DIRETORIO_CORRENTE}/criando_inst.sh
		cd ..
	else 
		echo "Ignorando ${DIRETORIO}... N�o � um diret�rio!!!"
	fi
done
