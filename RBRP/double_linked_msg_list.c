/* Arquivo: lists.c */

/* Inclusoes. */
#include "double_linked_msg_list.h"

/* Function alloc_cell allocs memory necessary to creating a new cell */
msg_list_pointer alloc_msg_cell(void)
{
   msg_list_pointer aux;
   aux=malloc(sizeof(msg_cell));
   return aux;
}

/* Procedure that creates an empty list */

void create_empty_msg_list(msg_list_type *list)
{
   list->first=alloc_msg_cell();
   list->last=list->first;
   list->first->next=list->first;
   list->first->previous=list->first;
   list->length=0;
}

/* Answer if a list is empty */
inline int is_empty_msg_list(msg_list_type *list)
{
   return !(list->length);
}

/* insert_in_list inserts a new item on the last position of a list */
void insert_in_msg_list(message_type item, msg_list_type *list)
{
   list->last->next=alloc_msg_cell();
   list->last->next->previous=list->last;
   list->last=list->last->next;
   list->last->item=item;
   list->last->next=list->first;
   list->first->previous=list->last;
   list->length++;
}

/* Inserts a new item on the first position of a list */
void insert_in_begin_msg_list(message_type item, msg_list_type *list)
{
   msg_list_pointer aux;
   aux=alloc_msg_cell();
   aux->item=item;
   aux->next=list->first->next;
   aux->next->previous=aux;
   list->first->next=aux;
   aux->previous=list->first;
   if (list->length==0)
	   list->last=aux;
   list->length++;
}   

/* Remove the item that is pointed.
 * After the remove, the pointer will be pointed to the previous item. 
 * Then, the next list of the item pointed will be the same
 * THIS FUNCTION SHOULD NOT HAVE BEEN CALLED ON AN EMPTY LIST */

message_type remove_of_msg_list(msg_list_pointer *p, msg_list_type *list)
{
	message_type item;
	msg_list_pointer aux;

	/* Tests is the list is empty or the pointer is null */
	if (is_empty_msg_list(list) || !(*p) || (*p==list->first))
	{
		printf("Error: List is_empty or position doesn't exists.\n");
		return NULL;
	}   
	else
	{
		item=(*p)->item;
		(*p)->next->previous=(*p)->previous;
		(*p)->previous->next=(*p)->next;
		if ((*p)==list->last)
		list->last=(*p)->previous;
		aux=(*p);
		(*p)=(*p)->previous;
		free_msg_cell(aux);
		list->length--;
	}
	return item;
}

/* Remove the first item of the list, and return this*/
message_type remove_first_of_msg_list(msg_list_type *list)
{
   message_type item;
   msg_list_pointer aux=list->first->next;
   item=remove_of_msg_list(&aux, list);
   return item;
}

/* Get the first item of the list if it exists */
inline int get_first_in_msg_list(msg_list_type *list, msg_list_pointer *pointer)
{
   if (is_empty_msg_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->first->next;
   return 1;
}

/* Get the last item of the list if it exists */
inline int get_last_in_msg_list(msg_list_type *list, msg_list_pointer *pointer)
{
   if (is_empty_msg_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->last;
   return 1;
}

/* Go to the next position of the list */
inline int go_next_msg(msg_list_type *list, msg_list_pointer *pointer)
{
	/* Tests if the pointer is valid */
	if (*pointer)
	{
		/* Tests if the object is the last one of the list. In this case it do not have a next item. */
		if ((*pointer)==list->last)
		{
			get_first_in_msg_list(list, pointer);
			return 0;
		}
		else
		{
			*pointer=(*pointer)->next;
			return 1;
		}
	}
	else return 0;
}

/* Tests is the item is the last of the list */
inline int is_last_msg(msg_list_pointer p, msg_list_type *list)
{

   /* If the pointer is NULL, there is an error */
   if (!p)
   {
      printf("Erro: Este apontador n�o aponta para c�lula alguma");
      return 0;
   }
   return (p->next==list->first);
}

/* Searchs an item on the list, returning this position or NULL if the item was not found */
msg_list_pointer search_item_in_msg_list(message_type item, msg_list_type *list)
{
   msg_list_pointer aux;

   /* If there is no item on the list, it returns NULL*/
   if(!get_first_in_msg_list(list, &aux))
	   return NULL; 

   /* While there is an next item, keep searching */
   do
   {
      if (compare_messages(aux->item, item))
      {
	 return aux;
      }
      go_next_msg(list, &aux);
   }   
   while (!is_last_msg(aux, list));
   
   /* Tests if the object is the last */
   if (compare_messages(aux->item, item))
   {
         return aux;
   }

   return NULL;
}

/* Swap two items that are adjacent on the list */
void swap_msg_items(msg_list_pointer previous, msg_list_pointer next, msg_list_type *list)
{
   if (previous!=list->last && list->length>1)
   {
      if (previous->next==next)
      {
         previous->previous->next=next;
         next->next->previous=previous;
         previous->next=next->next;
         next->previous=previous->previous;
         previous->previous=next;
         next->next=previous;
      }
      if (previous==list->first) 
         list->first=next;
      if (next==list->last) 
         list->last=previous;
   }
   else printf("Error: the swap can�t be completed. \n");
}

/* Make an list empty, is frees the memory allocated*/
inline void make_msg_list_empty(msg_list_type *list)
{
   message_type aux;
   while (!is_empty_msg_list(list))
   {
      aux=remove_first_of_msg_list(list);
      free_message(aux);
   }
}

/* Destroys a list. First make this empty, then frees the memory */
void destroy_msg_list(msg_list_type *list)
{
	make_msg_list_empty(list);
	free(list->first);
	free(list);
}

