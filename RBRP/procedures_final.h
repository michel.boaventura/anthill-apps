#ifndef PROCEDURES_FINAL_H
#define PROCEDURES_FINAL_H
#include "rbrp.h"

int process_new_outlier_message(message_type message, outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, dimension_size_type dimension_size, int number_of_outliers, int number_of_neighbours);
void print_results(outliers_type *outliers, nearest_neighbours_type *nearest_neighbours, int number_of_neighbours, dimension_size_type dimension_size, FILE *file);

#endif
