#ifndef _DOUBLE_LINKED_OCCURRENCES_LIST_H
#define _DOUBLE_LINKED_OCCURRENCESLIST_H
#include "occurrences_type.h"

/* Prototypes */
occurrences_list_pointer alocc_occurrences_cell(void);
void create_empty_occurrences_list(occurrences_list_type *list);
inline int is_empty_occurrences_list(occurrences_list_type *list);
void insert_in_occurrences_list(occurrences_type item, occurrences_list_type *list);
void insert_in_begin_occurrences_list(occurrences_type item, occurrences_list_type *list);
void insert_in_occurrences_list_on_position(occurrences_type item, occurrences_list_pointer *p, occurrences_list_type *list);
occurrences_type remove_of_occurrences_list(occurrences_list_pointer *p, occurrences_list_type *list);
occurrences_type remove_first_of_occurrences_list(occurrences_list_type *list);
inline int get_first_in_occurrences_list(occurrences_list_type *list, occurrences_list_pointer *pointer);
inline int get_last_in_occurrences_list(occurrences_list_type *list, occurrences_list_pointer *pointer);
inline int go_next_occurrences(occurrences_list_type *list, occurrences_list_pointer *pointer);
inline int is_last_occurrences(occurrences_list_pointer p, occurrences_list_type *list);
occurrences_list_pointer search_item_in_occurrences_list(occurrences_type item, occurrences_list_type *list);
void swap_on_occurrences_list(occurrences_list_pointer previous, occurrences_list_pointer next, occurrences_list_type *list);
void make_occurrences_list_empty(occurrences_list_type *list);
void destroy_occurrences_list(occurrences_list_type *list);
int concatenate_occurrences_lists(occurrences_list_type *list1, occurrences_list_type *list2);
unsigned int get_occurrences_list_length(occurrences_list_type *list);
int update_frequency_of_item_in_occurrences_list(categorical value_number, int frequency, occurrences_list_type *list);
categorical get_mode_from_occurrence_list(occurrences_list_type *list);
unsigned int get_sum_frequency_modest_values_from_occurrence_list(occurrences_list_type *list);
void *fill_occurrences_list_on_msg(void *msg_dest, occurrences_list_type *list);

#endif

