#include "procedures_distributor.h"
/**********************************************************************************************/
/*! \file procedures_distributor.c
    \brief This file stores the auxiliary functions of the filter Distributor.
***********************************************************************************************/    

//! This function is responsible to open the database and checks if any errors occured.
void open_database_file (FILE **input_file, char *input_file_name)
{
	char error_msg[100];				// auxiliary to store the message printed when a 
							// error occurs
	
	// opens the file and checks if any errors occured
	if ((*input_file = fopen (input_file_name, "r")) == NULL)
	{
		sprintf(error_msg,"ERROR. Could not open the file: %s !!!\n", input_file_name);
		ahExit (error_msg);
	}
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/*! This function is responsible to check if the number of attributes of the 
 * current object matches the number of attributes of the database */
int check_attributes (char *line, int number_of_attributes)
{
	char *temp_string; 				// auxiliary to store temporarily a string of the line
	char *temp_line;			// auxiliary to store temporarily the line
	int temp_num_attributes = 0;			// auxiliary to store the number of attributes

	int max_line_len=((MAX_VALUE_LEN+2)*number_of_attributes+1);
	temp_line=malloc(max_line_len*sizeof(char));
	
	// copies the line to a auxiliary variable
	strcpy(temp_line, line);
	
	// starts reading and counting the attributes of the line
	temp_string = (char *) strtok(temp_line, SEPARATORS);
	while (temp_string != NULL)
	{
		temp_num_attributes++;
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}

	// an empty line was found
	if (temp_num_attributes == 0)
	{
		return EMPTY_LINE;
	}
	else
	{
		// gives an error message if the number of attributes is different 
		// from the number of samples
		if ( (temp_num_attributes != number_of_attributes) && (temp_num_attributes != 0) )
		return 0;
	}

	free(temp_line);
	return temp_num_attributes;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

//! Gets the values of a tuple and keeps it on the structure object
int get_object_from_file(object_type *object, dimension_size_type dimension_size, FILE *input_file)
{
	unsigned int i;
	int check;
	char error_msg[100];		/*!< auxiliary to store the message printed when an error occur */
	char *file_line, *temp_string;

	int num_attributes = 0;			// auxiliary to store the number of attributes

	/* The number of columns is equal ID column ID add to the number of total attributes */
	int num_columns = dimension_size.number_of_real_values+dimension_size.number_of_ordinal_values+dimension_size.number_of_categorical_values+1;
	int max_line_len=(MAX_VALUE_LEN+2)*num_columns;
	
	/* Allocate the memory of an line with an object */
	file_line=malloc(max_line_len*sizeof(char));

	/* read the file until gets a new line with an object or find the end of file */
	do
	{
		/* If was not possible to read the file, e.g end of the file, returns 0 */
		if (fgets(file_line, max_line_len-1, input_file)==NULL)
			return 0;
		
		/* Check if there are on the file the ID and the right number of dimensions */
		check = check_attributes(file_line, num_columns);
	}
	while (check==EMPTY_LINE);
	if (!check)
	{
		sprintf(error_msg,"ERROR. Inconsistent number of attributes.\n");
		ahExit(error_msg);
	}
	
	// starts reading the line getting this ID
	temp_string = (char *) strtok(file_line, SEPARATORS);

	// Get the ID of the object
	(*object).number=atoi(temp_string);

	/* Allocate the structure to keep the values of the object */
	malloc_object(object, dimension_size);

	// starts reading the dimension values from the line
	temp_string = (char *) strtok(NULL, SEPARATORS);
	
	// Getting the real dimension values */
	for (i=0; i<dimension_size.number_of_real_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.real_values[i]=get_real_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;
	
	// Getting the ordinal dimension values */
	for (i=0; i<dimension_size.number_of_ordinal_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.ordinal_values[i]=get_ordinal_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;

	// Getting the categorical dimension values */
	for (i=0; i<dimension_size.number_of_categorical_values && temp_string !=NULL; i++)
	{
	   	(*object).dimension_values.categorical_values[i]=get_categorical_value_from_string(temp_string);
		temp_string = (char *) strtok(NULL, SEPARATORS);
	}
	
	num_attributes+=i;

	/* Free the memory previously allocated for the line */
	free(file_line);
	return (num_attributes==num_columns-1);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*! This function is responsible to read a line of a file and fill it on an object message
 * that will be sent to the assigner filter.
 * It allocates space in memory for this msg */
int fill_object_in_msg (void **msg, dimension_size_type dimension_size, object_type object)
{
	unsigned int *msg_int;
	void *values_msg;

	if (object.dimension_values.real_values==NULL && object.dimension_values.ordinal_values==NULL && object.dimension_values.categorical_values==NULL) return 0;

	// The message have the number of the object and its dimension values
	unsigned int size=sizeof(unsigned int) + get_dimension_values_message_size(dimension_size);

	*msg=malloc(size);
	msg_int=*msg;
	
	// Fills the object number on the message
	msg_int[OBJECT_NUMBER_FIELD]=object.number;

	// Go forward on the message
	values_msg=(void *)&(msg_int[OBJECT_VALUES_FIELD]);

	/* Fill the dimension values on the message */
	fill_dimension_values_on_msg(values_msg, &(object.dimension_values), dimension_size);
	
	return size;
}

