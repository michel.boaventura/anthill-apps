#ifndef _DOUBLE_LINKED_LIST_H
#define _DOUBLE_LINKED_LIST_H
#include "object.h"

/* Prototypes */
list_pointer alocc_cell(void);
void create_empty_list(list_type *list);
int is_empty(list_type *list);
void insert_in_list(object_type item, list_type *list);
void insert_in_begin_list(object_type item, list_type *list);
void insert_in_list_on_position(object_type item, list_pointer *p, list_type *list);
object_type remove_of_list(list_pointer *p, list_type *list);
object_type remove_first_of_list(list_type *list);
int get_first_in_list(list_type *list, list_pointer *pointer);
int get_last_in_list(list_type *list, list_pointer *pointer);
int go_next(list_type *list, list_pointer *pointer);
int is_last(list_pointer p, list_type *list);
list_pointer search_item_in_list(object_type item, list_type *list);
void swap_items(list_pointer previous, list_pointer next, list_type *list);
void make_list_empty(list_type *list);
void destroy_list(list_type *list);
int concatenate_lists(list_type *list1, list_type *list2);
unsigned int get_list_length(list_type *list);

#endif

