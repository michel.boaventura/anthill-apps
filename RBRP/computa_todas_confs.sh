#!/bin/bash
## Executa o rbrp para v�rios arquivos diferentes no diretorio de entrada e armazena 
## os tempos de execu��o no diret�rio de tempos.
## argumento1: Nome do arquivo de entrada
## argumento2: Nome do diretorio onde ser�o armazenados os tempos de execu��o
## argumento3: N�mero de atributos reais do arquivo de entrada
## argumento4: N�mero de atributos ordinais do arquivo de entrada
## argumento5: N�mero de categoricos do arquivo de entrada

## Numero de maquinas de processamento
INSTANCIAS='1'

## O numero de excecoes que se deseja encontrar
NUM_EXCECOES=30

## O n�mero de vizinhos a serem considerados na determina��o dos objetos exce��es
NUM_VIZINHOS=5

## Diretorios de entrada e saida
DIRETORIO_SAIDA=./OUT
DIRETORIO_ENTRADA=/scratch/carlos/svn/OUTPUT

if [ $# != 6 ] 
then
	echo "Erro nos par�metros do script."
	echo "Execute o script da seguinte forma: "
	echo "computa_todas_instancias.sh arquivo_entrada diretorio_tempos numero_de_atributos_reais numero_de_atributos_ordinais numero_de_atributos_categoricos bin_size" 
	echo "Aten��o! Os arquivos armazenados no diretorio diretorio_dos_tempos ser�o apagados."
	echo "Aten��o! Os arquivos armazenados no diretorio $DIRETORIO_SAIDA ser�o apagados."
	exit 1
fi

ARQUIVO_ENTRADA=$1

DIRETORIO_TEMPOS=$2

NUM_REAL=$3

NUM_ORDINAL=$4

NUM_CATEGORICAL=$5

BIN_SIZE=$6

# Cria os diretorios de saida e dos tempos se eles ainda nao existirem
if [ ! -d ${DIRETORIO_TEMPOS} ];
then
	# Preparando os diretorios de entrada e sa�da #
	echo "Preparando os diretorios de tempos"
	mkdir -pv ${DIRETORIO_TEMPOS};
else
	echo "Apagando arquivos existentes no diretorio de tempos"
	rm -rfv ${DIRETORIO_TEMPOS}/*
fi
if [ ! -d ${DIRETORIO_SAIDA} ];
then
	# Preparando o diretorio de saida 
	echo "Preparando o diretorio de saida"
	mkdir -pv ${DIRETORIO_SAIDA};
else
	echo "Apagando arquivos existentes no diretorio de saida"
	rm -rfv ${DIRETORIO_SAIDA}/*;
fi

# Execucao dos testes
for NUM_INSTANCIAS in $INSTANCIAS
do
	echo "Atualizando o arquivo conf.xml com o numero de instancias do filtro BIN"
	# atualizacao do arquivo conf.xml (host)
	sed -i "s#libbin.so\" instances=.*#libbin.so\" instances=\"${NUM_INSTANCIAS}\">#" conf.xml
	
	# Atualiza o nome do arquivo de saida 
	ARQUIVO_SAIDA=./OUT/${ARQUIVO_ENTRADA}.${NUM_INSTANCIAS}_instancias 

	echo "Atualizando o arquivo conf.xml com o numero de instancias do filtro Assigner"
	# atualizacao do arquivo conf.xml (host)
	sed -i "s#libassigner.so\" instances=.*#libassigner.so\" instances=\"${NUM_INSTANCIAS}\">#" conf.xml

	echo "Preparando o Cluster para Execucao... Realizando a consistencia de arquivos..."
	~/bin/prepara_cluster.sh
	
	echo "Obtendo arquivos criados localmente... Realizando a consistencia de arquivos..."
	~/bin/pega_logs.sh


	echo "./initScript -i ${DIRETORIO_ENTRADA}/${ARQUIVO_ENTRADA} -o ${ARQUIVO_SAIDA} -r ${NUM_REAL} -d ${NUM_ORDINAL} -c ${NUM_CATEGORICAL} -n ${NUM_EXCECOES} -k ${NUM_VIZINHOS} -s ${BIN_SIZE}"
	./initScript -i ${DIRETORIO_ENTRADA}/${ARQUIVO_ENTRADA} -o ${ARQUIVO_SAIDA} -r ${NUM_REAL} -d ${NUM_ORDINAL} -c ${NUM_CATEGORICAL} -n ${NUM_EXCECOES} -k ${NUM_VIZINHOS} -s ${BIN_SIZE} 
		  
	echo "Obtendo arquivos criados localmente... Realizando a consistencia de arquivos..."
	~/bin/pega_logs.sh

	echo "Movendo os arquivos de tempos para o diretorio dos tempos..."
	mv -v ${ARQUIVO_SAIDA}* ./${DIRETORIO_TEMPOS}/
done
