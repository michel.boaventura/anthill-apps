/* Arquivo: lists.c */

/* Inclusoes. */
#include "double_linked_bin_list.h"

/* Function alloc_bin_cell allocs memory necessary to creating a new bin_cell */

bin_list_pointer alloc_bin_cell(void)
{
   return(malloc(sizeof(bin_cell)));
}

/* Procedure that creates an empty list with a dummy cell*/

void create_empty_bin_list(bin_list_type *list)
{
   list->first=alloc_bin_cell();
   list->last=list->first;
   list->first->next=list->first;
   list->first->previous=list->first;
   list->length=0;
}

/* Answer if a list is empty */
inline int is_empty_bin_list(bin_list_type *list)
{
	return !(list->length);
}

/* Gets the number of elements inserted on the list */
inline unsigned int get_size_of_bin_list(bin_list_type *list)
{
	return (list->length);
}

/* Gets the number of objects inserted on the bins in list */
int get_number_objects_of_bin_list(bin_list_type *list)
{
	bin_list_pointer aux;
	int number_of_objects=0;
	
	if (!get_first_in_bin_list(list, &aux))
	{
		return 0;
	}

	number_of_objects = aux->item.objects.length;
		
	while (!is_last_bin(aux, list))
	{
		go_next_bin(list, &aux);
		number_of_objects += aux->item.objects.length;

	}

	return number_of_objects;

}

/* insert_in_list inserts a new item on the last position of a list */
void insert_in_bin_list(bin_type item, bin_list_type *list)
{
   list->last->next=alloc_bin_cell();
   list->last->next->previous=list->last;
   list->last=list->last->next;
   list->last->item=item;
   list->last->next=list->first;
   list->first->previous=list->last;
   list->length++;
}

/* Inserts a new item on the first position of a list */
void insert_in_begin_bin_list(bin_type item, bin_list_type *list)
{
   bin_list_pointer aux;
   aux=alloc_bin_cell();
   aux->item=item;
   aux->next=list->first->next;
   aux->next->previous=aux;
   list->first->next=aux;
   aux->previous=list->first;
   if (list->length==0)
	   list->last=aux;
   list->length++;
}   

/* Remove the item that is pointed.
 * After the remove, the pointer will be pointed to the previous item. 
 * Then, the next list of the item pointed will be the same 
 * THIS FUNCTION SHOULD NOT HAVE BEEN CALLED ON AN EMPTY LIST */
bin_type remove_of_bin_list(bin_list_pointer *p, bin_list_type *list)
{
	bin_type item;
	bin_list_pointer aux;

	/* Tests is the list is empty or the pointer is null */
	if (is_empty_bin_list(list) || !(*p) || (*p==list->first))
	{
		ahExit("Error: List is_empty or position doesn't exists.\n");
	}
	else
	{
		item=(*p)->item;
		(*p)->next->previous=(*p)->previous;
		(*p)->previous->next=(*p)->next;
		if ((*p)==list->last)
		list->last=(*p)->previous;
		aux=(*p);
		(*p)=(*p)->previous;
		free_bin_cell(aux);
		list->length--;
	}
	return item;
}

/* Remove the first item of the list, and retornn this*/
bin_type remove_first_of_bin_list(bin_list_type *list)
{
   bin_type item;
   bin_list_pointer aux=list->first->next;
   item=remove_of_bin_list(&aux, list);
   return item;
}

/* Get the first item of the list if it exists */
int get_first_in_bin_list(bin_list_type *list, bin_list_pointer *pointer)
{
   if (is_empty_bin_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->first->next;
   return 1;
}

/* Get the last item of the list if it exists */
int get_last_in_bin_list(bin_list_type *list, bin_list_pointer *pointer)
{
   if (is_empty_bin_list(list))
   {
      (*pointer)=NULL;
      return 0;
   }
   (*pointer)=list->last;
   return 1;
}

/* Go to the next position of the list */
int go_next_bin(bin_list_type *list, bin_list_pointer *pointer)
{
	/* Tests if the pointer is valid */
	if (*pointer)
	{
		/* Tests if the object is the last one of the list. In this case it do not have a next item. */
		if ((*pointer)==list->last)
		{
			get_first_in_bin_list(list, pointer);
			return 0;
		}
		else
		{
			*pointer=(*pointer)->next;
			return 1;
		}
	}
	else return 0;
}

/* Tests is the item is the last of the list */
inline int is_last_bin(bin_list_pointer p, bin_list_type *list)
{

   /* If the pointer is NULL, there is an error */
   if (!p)
   {
      printf("Erro: Este apontador n�o aponta para c�lula alguma");
      return 0;
   }
   return (p->next==list->first);
}

/* Searchs an item on the list, returning this position or NULL if the item was not found */

bin_list_pointer search_item_in_bin_list(bin_type item, bin_list_type *list)
{
   bin_list_pointer aux;
   
   /* If there is no bin on the list, it returns NULL*/
   if(!get_first_in_bin_list(list, &aux))
	   return NULL;

   /* While there is an next item, keep searching */
   do
   {
      /* If the bin was found, it returns the pointer to this bin */
      if (compare_bins(aux->item, item))
      {
         return aux;
      }
      go_next_bin(list, &aux);
   }
   while (!is_last_bin(aux, list));
 
   /* Tests if the bin is the last */ 
   if (compare_bins(aux->item, item))
   {
         return aux;
   }
  
   /* If the item was not found on the list, it returns NULL */
   return NULL;
}

/* Swap two items that are adjacent on the list */
void swap_bin_items(bin_list_pointer previous, bin_list_pointer next, bin_list_type *list)
{
   if (previous!=list->last && list->length>1)
   {
      if (previous->next==next)
      {
         previous->previous->next=next;
         next->next->previous=previous;
         previous->next=next->next;
         next->previous=previous->previous;
         previous->previous=next;
         next->next=previous;
      }
      if (previous==list->first) 
         list->first=next;
      if (next==list->last) 
         list->last=previous;
   }
   else printf("Error: the swap can�t be completed. \n");
}

/* Make an list empty, is frees the memory allocated*/
void make_bin_list_empty(bin_list_type *list, dimension_size_type dimension_size)
{
   bin_type aux;
   while (!is_empty_bin_list(list))
   {
      aux=remove_first_of_bin_list(list);
      free_bin(aux, dimension_size);
   }
}

/* Destroys a list. First make this empty, then frees the memory of the dummy cell*/
void destroy_bin_list(bin_list_type *list, dimension_size_type dimension_size)
{
	make_bin_list_empty(list, dimension_size);
	free(list->first);
}

/*inserts a new item on the indicated position of a list */
void insert_in_bin_list_on_position(bin_type item, bin_list_pointer *p, bin_list_type *list)
{
	bin_list_pointer aux=*p;

	if (aux==NULL)
	{
		*p=NULL;
		return;
	}
	/* Tests if the pointer is pointing to the dummy cell.
	 * In this case we will just insert the item on the last of the list */
	if (aux==list->first)
	{
		insert_in_bin_list(item, list);
	}
	else
	{
		/* Tests if the pointer is pointing to the real first item of the list.
		 * In this case we will insert the item on the first position of the list */
		
		if(aux==list->first->next)	
			insert_in_begin_bin_list(item, list);
		else
		{
			aux->previous->next=alloc_bin_cell();
			aux->previous->next->previous=aux->previous;
			aux->previous=aux->previous->next;
			aux->previous->item=item;
			aux->previous->next=aux;
			list->length++;
		}
	}
	*p=aux->previous;
}


/* Inserts a new item on the list keeping the list ordered */
void insert_in_ordered_bin_list(bin_type item, bin_list_type *list, int number_of_neighbours, dimension_size_type dimension_size)
{
   bin_list_pointer aux;

   /* If there is no item on the list, just insert the item */
   if(!get_first_in_bin_list(list, &aux))
   {
      insert_in_bin_list(item, list);
      return; 
   }
   
   /* While there is an next item that the frequency is greater of the frequency of the item that will be inserted, keep running on the list */
   while (!is_last_bin(aux, list))
   {
      if (compare_target_metric_bin(aux->item, item, number_of_neighbours, dimension_size) < 0)
      {
	 			insert_in_bin_list_on_position(item, &aux, list);
	 			return;
      }
      go_next_bin(list, &aux);
   }   
   
   /* Tests if the object have to be inserted before the last item */
   if (compare_target_metric_bin(aux->item, item, number_of_neighbours, dimension_size) < 0)
   {
	 		insert_in_bin_list_on_position(item, &aux, list);
      return;
   }
   /* If the item is the last one of the list, we will insert this item on the last position */
   else
   {
      insert_in_bin_list(item, list);
   }

   return;
}

bin_type remove_of_bin_list_on_position(unsigned int position, bin_list_type *list)
{
   bin_list_pointer aux;
   int i = 0;

   /* If there is no bin on the list, it returns NULL*/
   if(!get_first_in_bin_list(list, &aux))
		ahExit("Error: List is_empty or position doesn't exists.\n");

   /* While there is an next item, keep searching */
   while (!is_last_bin(aux, list))
   {	
		 if (position == i)
				break;

		 go_next_bin(list, &aux);
	   i++;
   }
	
	 return (remove_of_bin_list(&aux, list));

}

