#ifndef _DOUBLE_LINKED_MSG_LIST_H
#define _DOUBLE_LINKED_MSG_LIST_H
#include "message_type.h"

/* Prototypes */
msg_list_pointer alocc_msg_cell(void);
void create_empty_msg_list(msg_list_type *list);
int is_empty_msg_list(msg_list_type *list);
void insert_in_msg_list(message_type item, msg_list_type *list);
void insert_in_begin_msg_list(message_type item, msg_list_type *list);
message_type remove_of_msg_list(msg_list_pointer *p, msg_list_type *list);
message_type remove_first_of_msg_list(msg_list_type *list);
int get_first_in_msg_list(msg_list_type *list, msg_list_pointer *pointer);
int go_next_msg(msg_list_type *list, msg_list_pointer *pointer);
int is_last_msg(msg_list_pointer p, msg_list_type *list);
msg_list_pointer search_item_in_msg_list(message_type item, msg_list_type *list);
void swap_items_message(msg_list_pointer previous, msg_list_pointer next, msg_list_type *list);
void make_msg_list_empty(msg_list_type *list);
void destroy_msg_list(msg_list_type *list);

#endif

