#include "final.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/


/*************************** INPUT AND OUTPUT PORTS *************************/

InputPortHandler Bin_input;			// input ports

/**************************** GLOBAL VARIABLES ******************************/

/* Parameters of the program read from the work*/
dimension_size_type dimension_size;			// number of dimensions (attributes) of the database 
int number_of_outliers;				// number of n, where n defines the number of outliers to be searched (top-n ouliers)
int number_of_neighbours;			// number of k, where k is the number of neighbours considered to measure an outlier (k-th nearest neighbour)
char output_file_name[MAX_FILENAME];		// stores the name of the output file
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/* This module is responsible for opening the ports of this filter and 
 * getting the information from work  */

int initFilter (void *work, int workSize)
{		
	/* Parameters of the program read from the work*/
	char input_file_name[MAX_FILENAME];		// stores the name of the database that will be read 
	char error_msg[100];
	int bin_size;
	// opens input and output ports
	Bin_input = ahGetInputPortByName ("Bin_to_Final_input");
	if ( Bin_input == -1)
	{
		sprintf(error_msg,"ERROR. Could not open the Final filter's ports.\n");
		ahExit (error_msg);
	}
	
	// gets the necessary information from work
	get_information_from_work (work, input_file_name, output_file_name, &dimension_size, &number_of_outliers, &number_of_neighbours, &bin_size);
	
	return 1;
}

int processFilter(void *work, int worksize) 
{
	outliers_type list_of_outliers;	
	nearest_neighbours_type *nearest_neighbours;
	
	FILE *fp_out;
	char name_of_fp_out[MAX_FILENAME];
	sprintf(name_of_fp_out, "%s.out", output_file_name);
	
	char error_msg[100];
	int i;
	
#ifdef GET_TIME
	struct timeval initial_time, final_time;
	FILE *fp_time;
	char name_of_fp_time[MAX_FILENAME];
	sprintf(name_of_fp_time, "%s.time", output_file_name); 
	
	// Clean the files of times
	fp_time = fopen(name_of_fp_time,"w");
	if (!fp_time)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}
	fclose(fp_time);

#endif

#ifdef GET_TIME	
	gettimeofday(&initial_time, NULL);
#endif

	/* Get the length of a new outlier message */
	int msg_size=get_new_outlier_message_size(number_of_neighbours, dimension_size);
	
	message_type msg[msg_size];

/********************************** DEBUG ***********************************/
#ifdef DEBUG
	FILE *fp_log;
	char name_of_fp_log[50];
	sprintf(name_of_fp_log, "log_final_%d.txt", ahGetMyRank()); 

	// Clean the files of logs
	fp_log = fopen(name_of_fp_log,"w");
	fclose(fp_log);
#endif
/****************************************************************************/	

	/* Creates the list of the outliers initially empty */
	malloc_outliers(&list_of_outliers, number_of_outliers);

	/* Creates the array that will keep the neighbours of the outliers */
	nearest_neighbours=malloc(number_of_outliers*sizeof(nearest_neighbours_type));
	
	while (ahReadBuffer (Bin_input, msg, msg_size) != EOW)
	{
		process_new_outlier_message(msg, &list_of_outliers, nearest_neighbours, dimension_size, number_of_outliers, number_of_neighbours);
/********************************** DEBUG ***********************************/
#ifdef DEBUG
		if ( DEBUG >= LOGGER )
		{
			fp_log = fopen(name_of_fp_log,"a");
			fprintf(fp_log, "\n\t\t----------\t----------\t----------\t----------\t----------\t----------\n\n");
			fprintf(fp_log, "NEW OUTLIER MESSAGE PROCESSED: \n");
			print_outliers(&(list_of_outliers), dimension_size, fp_log);
			fprintf(fp_log, "END OF ACTUAL OUTLIERS\n");
			fclose(fp_log);
		}
#endif		
/****************************************************************************/
	}

	// Open the Output File to write on this
	fp_out = fopen(name_of_fp_out,"w");
	if (!fp_out)
	{
		sprintf(error_msg,"ERROR. Could not create the output files.\n");
		ahExit (error_msg);
	}

	print_results(&(list_of_outliers), nearest_neighbours, number_of_neighbours, dimension_size, fp_out);

	// Close the Output File
	fclose(fp_out);

#ifdef GET_TIME
	gettimeofday(&final_time, NULL);	
	print_time(&initial_time, &final_time, name_of_fp_time);
#endif

	/* Frees the memory of the nearest neighbours */	
	for (i=0; i<get_number_of_outliers_found(&list_of_outliers); i++)
	{
		free_nearest_neighbours(nearest_neighbours[i]);
	}
	free(nearest_neighbours);

	/* Free the memory of the outliers */
	free_outliers(list_of_outliers);

	return 0;
}


int finalizeFilter(void)
{

	return 0;
}
