#!/bin/bash

	if [ $# != 2 ] 
	then
      		echo "Erro nos par�metros do script."
		echo "Execute o script da seguinte forma: "
		echo "gera_escalabilidade_objetos.sh arquivo_de_dados arquivo_de_saida" 
        	exit 1
	fi

	EPS_FILE=$2.eps
	GNUPLOT_FILE=$2.gp
	DATA_FILE=$1

	#Gerando o in�cio do arquivo gnuplot
	echo "
#!/usr/bin/env gnuplot
#reset

set terminal postscript enhanced eps# color
#set terminal png medium
#set term png 25 #  color

set key left top
#set key top right

# Configura��es de codifica��o de caractere
#set encoding default
set encoding iso_8859_1

# Plotando os clusters produzidos sinteticamente para o Algoritmo RBRP 

# Escolhendo onde aparecer a legenda
set key box
#set key outside

set notitle

# Configura��es de escala
set autoscale
set logscale x
set logscale y
#set nologscale

# Definindo o T�tulo dos eixos a serem desenhados
set xlabel \"N�mero de Objetos\"
set ylabel \"Tempo de Execu��o (s)\"

#set parametric
#set grid

# Definindo o espa�amento entre dois valores no eixo y
#set xtics 5
#set ytics 5

set format x \"10^{%L}\"
set format y \"10^{%L}\"

# Defini��o dos par�metros das fun��es
a=1
b=1

#Defini��o de uma fun��o linear
f(x)= a*x

# Defini��o de uma fun��o quadr�tica
g(x)= b*x*x

#Ajuste linear da fun��o definida
fit f(x) \"${DATA_FILE}\" every::0::1 using 1:2 via a
fit g(x) \"${DATA_FILE}\" every::0::1 using 1:2 via b

# Definindo os limites dos eixos a serem plotados. 
# Se isso for definido antes dos ajustes ele interferir� os ajustes tamb�m
set xrange [1000:]
#set yrange [:]

# Definindo o nome do arquivo de sa�da
set output \"${EPS_FILE}\"

#Plotagem dos pontos

plot [][] \\" > ${GNUPLOT_FILE}

	# Plotando a fun��o linear	
	echo "	f(x) t \"Fun��o Linear\", \\" >> ${GNUPLOT_FILE}

	# Plotando a fun��o quadr�tica
	echo "	g(x) t \"Fun��o Quadr�tica\", \\" >> ${GNUPLOT_FILE}
	
	# Plotando a curva com os tempos de execu��o do Algoritmo
	#echo "	'<sort -n ${DATA_FILE}' using 1:2 title \"Tempo de Execu��o\" with linespoints pointsize 2.0, \\" >> ${GNUPLOT_FILE}
	echo "	'<sort -n ${DATA_FILE}' using 1:2 title \"Tempo de Execu��o\" with linespoints pointsize 1." >> ${GNUPLOT_FILE}

	#Plotando os errorbars	
	#echo "	'<sort -n ${DATA_FILE}' using 1:2:3:4 title \"Tempo de Execu��o\" with errorbars" >> ${GNUPLOT_FILE}

	# Gerando o gr�fico com o Gnuplot
	echo "Gerando o gr�fico com o Gnuplot"
	echo "gnuplot ${GNUPLOT_FILE}"
	gnuplot ${GNUPLOT_FILE}

	# Visualizando o gr�fico gerado
	echo "Visualizando o gr�fico gerado"
	echo "gv ${EPS_FILE} &"
	gv ${EPS_FILE} &

