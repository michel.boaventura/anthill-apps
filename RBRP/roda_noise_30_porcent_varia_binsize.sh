#!/bin/bash

BINSIZE_FILE=num_binsize.txt

if [ ! -f $BINSIZE_FILE ];
then
	echo "Nao foi possivel encontrar o arquivo $BINSIZE_FILE!!! Esse arquivo deve existir com os tamanhos dos binsizes a serem testados"  
	exit 1
fi

#Obtendo os binsizes a serem testados do arquivo
VAR_BIN_SIZE=`cat $BINSIZE_FILE`

# Execucao dos testes
for BINSIZE in $VAR_BIN_SIZE;
do
	echo "roda_tempos.sh noise_30_porcent_1000000.csv NOVOS_EXPERIMENTOS/noise_30_porcent.binsize$BINSIZE 30 0 0 $BINSIZE"
	roda_tempos.sh noise_30_porcent_1000000.csv NOVOS_EXPERIMENTOS/noise_30_porcent.binsize$BINSIZE 30 0 0 $BINSIZE
done
