#ifndef _DISTANCE_H
#define _DISTANCE_H
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "structs.h" 
#include "dimension_values.h" 

/* Definitions */
#define REAL_CONVERSION_STRING "%.8lf"
#define ORDINAL_CONVERSION_STRING "%.8lf"
#define CATEGORICAL_CONVERSION_STRING "%d"
#define DISTANCE_CONVERSION_STRING "%.8lf"

/* Prototypes */
distance_type calculate_distance_between_real_values(real *point1, real *point2, unsigned int dimensions);

distance_type calculate_distance_between_ordinal_values(ordinal *point1, ordinal *point2, unsigned int dimensions);

int calculate_distance_between_categorical_values(categorical *point1, categorical *point2, unsigned int dimensions);

distance_type calculate_distance_between_dimension_values(dimension_values_type dimension_values1, dimension_values_type dimension_values2, dimension_size_type dimension_size);

distance_type distance_between_centroid_and_point(centroid_type centroid, object_type object, dimension_size_type dimension_size);

int print_real_array(FILE *file, real *array, unsigned int number_of_elements);

int print_ordinal_array(FILE *file, ordinal *array, unsigned int number_of_elements);

int print_categorical_array(FILE *file, categorical *array, unsigned int number_of_elements);

real get_real_value_from_string(char *temp_string);

ordinal get_ordinal_value_from_string(char *temp_string);

categorical get_categorical_value_from_string(char *temp_string);

#endif

