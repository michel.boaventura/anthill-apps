/* Inclusoes. */
#include "message_type.h"

/* Print all the items of the list */
void print_msg_list(msg_list_type *list, FILE *file)
{
   
	msg_list_pointer aux;

	/* If the first item is not found, it prints that the list is EMPTY */
	if (!get_first_in_msg_list(list, &aux))
	{	
		fprintf(file, "Object List is Empty!!!");
		return;
	}

	/* Prints the First Message of the List*/
	print_message(aux->item, file);

	while (!is_last_msg(aux, list))
	{
		go_next_msg(list, &aux);
		print_message(aux->item, file);
	}
}

/* Procedure that free memory of a cell .
 * It not free the memory for the messages that the cell points */
void free_msg_cell(msg_list_pointer q)
{
   free(q);
}

/* Print an object */
void print_message(message_type message, FILE *fp_log)
{
	int *int_message=(int *)message;
	bin_number_type *bin_number_msg=(bin_number_type *)(&(int_message[1]));
	fprintf(fp_log, "Message type: %d Bin Number: %llu \n", int_message[0], bin_number_msg[0]);
}

/* Compare to messages */
int compare_messages(message_type msg1, message_type msg2)
{
	return (msg1==msg2);
}

/* Free the memory of the message structure */
void free_message(message_type message)
{
	free(message);
}

/* Allocates memory to the message structures */
int malloc_message(message_type *message, int size)
{
	*message=malloc(size*sizeof(char));
	return (*message!=NULL);
}
